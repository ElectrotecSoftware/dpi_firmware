#ifndef __ETHERNET_H
#define __ETHERNET_H
#include "webserver.h"
#include "nodes.h"


#define BUFSIZE_ETHERNET	45
//Errors aplicació
#define BUFFER_ETHERNET		10
#define MAX_DATA_ETH 		1280

extern volatile uint32_t msTicks_aplicacio[MAX_APLICACIONS];
extern volatile uint32_t msTicks_ACK;

extern volatile uint8_t Ethernet_Transmit_Buffer[MAX_APLICACIONS][BUFSIZE_ETHERNET];
extern volatile uint16_t taula_errors[MAX_APLICACIONS][MAX_NODES_CANAL];
extern volatile uint16_t taula_errors_2[MAX_APLICACIONS][MAX_NODES_CANAL];
extern volatile uint32_t counter_envios_fallidos;
extern volatile uint32_t counter_pollings_fallidos;
extern volatile uint32_t counter_error_CRC;
extern volatile uint32_t counter_watchdog_uart;
extern volatile uint32_t counter_error_3;
extern unsigned short RemoteMAC_xarxa[3];// MAC i IP per informació de la xarxa
extern unsigned short RemoteIP_xarxa[2];
extern unsigned short TCPLocalPort_xarxa;// TCP port per informació de la xarxa
extern uint16_t posicio_enviament_xarxa;//Serveix per indicar la posicio del enviament de la xarxa
//0b000xnnnn --> canal 0bx index taula configurats 0bnnnn (El canal va de 0-1 i el index de 0-249)
unsigned char*  FromBuffer(unsigned char * buffer);
void answer_frameDISPLAY_ACK(char *num_message,uint8_t tipus, char *node_char, uint8_t canal);
void answer_frameSTANDARD_ACK(char frameID, uint8_t tipus);
void Enviar_Ethernet (uint8_t *data, uint8_t data_length);
void Enviar_Ethernet_push (uint8_t *data, uint8_t data_length);
uint8_t process_frame(unsigned short NrOfDataBytes);
void Error_aplicacio(uint8_t tipus, uint8_t node, uint8_t canal);
void convert_BYTE_to_CHAR3(uint8_t dataBYTE, char *node_char);
void receive_frameDISPLAY(void);
void receive_frameDISPLAY_ACK(void);
void receive_frameGETMEM(void);
void receive_frameNETWORKDISTRIBUTION(void);
void receive_frameOPENSESSION(void);
void receive_frameSETMEM(void);
void receive_frameVERSION(void);
void receive_PROGnodeID(void);
void receive_PROGnodeINC(void);
void transmit_frameNETWORKDISTRIBUTION(void);
#endif /*ETHERNET.H*/

