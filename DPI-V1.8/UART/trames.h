/*****************************************************************************
 *   uart.h:  Header file for NXP LPC17xx Family Microprocessors
 *
 *   Copyright(C) 2009, NXP Semiconductor
 *   All rights reserved.
 *
 *   History
 *   2009.05.27  ver 1.00    Prelimnary version, first Release
 *
******************************************************************************/
#ifndef __TRAMES_H
#define __TRAMES_H

#include "uart.h"//per el MAX_NODES_CANAL

#define GUARDAR_MULTICAST_1			2
#define GUARDAR_MULTICAST_2			1
#define GUARDAR_NODE_ID				4
//display
#define ASCII_FORMAT				0
#define USER_DEFINED_FORMAT			1

#define AUTOSCROLL_ALWAYS			1
#define	AUTOSCROLL_NOT_ALWAYS		0//nomes quan hi ha un numero amb mes de 4 digits

#define AUTOSCROLL_VELOCITY_NORMAL	0
#define AUTOSCROLL_VELOCITY_QUICK	1

#define TEXT_SCROLL_ON				1
#define TEXT_SCROLL_OFF				0
#define TEXT_SCROLL_MANUAL			2

#define MAX_ERRORS 				 	0xFFFFFF
#define MAX_MALLOCS					90
#define TRIGGER_MALLOCS				50

#define BROADCAST_ADRESS			252

extern uint8_t getmem_adress;
extern uint16_t ultim_node_programat;
extern uint16_t primer_node_desprogramat;
extern uint16_t node_a_desprogramar;
extern uint16_t reprogramar;
extern uint8_t old_adress_programacio1_canal1;
extern uint8_t new_adress_programacio1_canal1;
extern uint8_t new_adress_programacio2_canal1;
extern uint8_t old_adress_programacio1_canal2;
extern uint8_t new_adress_programacio1_canal2;
extern uint8_t new_adress_programacio2_canal2;
extern uint8_t peticio_programacio;
extern uint8_t peticio_programacio_2;
extern uint8_t escrivint[MAX_NODES_CANAL];
extern uint8_t escrivint_2[MAX_NODES_CANAL];
extern uint8_t actualitza_nodes_RAM;

void setmem (uint8_t node,uint8_t memory_adress,uint8_t length,uint8_t *cadena,uint8_t refresh, uint8_t canal);
void setmem_display (uint8_t node,uint8_t display_config,uint8_t length,uint8_t *cadena,uint8_t refresh, uint8_t canal);
void getmem (uint8_t node, uint8_t memory_adress, uint8_t canal);
void programacio(uint8_t tipus, uint8_t old_adress, uint8_t new_adress, uint8_t guardar, uint8_t multicast_addr_1, uint8_t multicast_addr_2, uint8_t canal);
void heartbeat( uint8_t node , uint8_t canal);
void process_data( uint8_t *Buffer, uint32_t Length, uint8_t port );


#endif /* end __UART_H */
/*****************************************************************************
**                            End Of File
******************************************************************************/
