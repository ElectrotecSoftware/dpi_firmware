/*****************************************************************************
 *   uart.c:  UART API file for NXP LPC17xx Family Microprocessors
 *
 *   Copyright(C) 2009, NXP Semiconductor
 *   All rights reserved.
 *
 *   History
 *   2009.05.27  ver 1.00    Prelimnary version, first Release
 *
******************************************************************************/
#include "lpc17xx.h"
#include "type.h"
#include "all.h"
#include "uart.h"
#include "trames.h"
#include "nodes.h"
#include "timer.h"
#include "leds.h"
#include <stdio.h>
#include <stdlib.h>
#include "ethernet.h"
volatile uint32_t UART0Status, UART1Status, UART2Status, UART3Status;
volatile uint8_t UART0TxEmpty = 1, UART1TxEmpty = 1, UART2TxEmpty = 1, UART3TxEmpty = 1;
volatile uint8_t UART0Buffer[BUFSIZE], UART1Buffer[BUFSIZE], UART2Buffer[BUFSIZE], UART3Buffer[BUFSIZE];
volatile uint32_t UART0Count = 0, UART1Count = 0, UART2Count = 0, UART3Count = 0;
volatile uint8_t usedPORTCanal1=0, usedPORTCanal2=0;


/*****************************************************************************
** Function name:		UART0_IRQHandler
**
** Descriptions:		UART0 interrupt handler
**
** parameters:			CH1.A
** Returned value:		None
** 
*****************************************************************************/
void UART0_IRQHandler (void) //CH1.A
{
  uint8_t IIRValue, LSRValue;
  uint8_t Dummy = Dummy;
  uint8_t i,j,k;
  IIRValue = LPC_UART0->IIR;//Tipus d'interrupció
  IIRValue >>= 1;			/* skip pending bit in IIR */
  IIRValue &= 0x07;			/* check bit 1~3, interrupt identification */
 // if (usedPORTCanal1==PORT0)
 // {
  if ( IIRValue == IIR_RLS )		/* Receive Line Status */
  {
	LSRValue = LPC_UART0->LSR;
	/* Receive Line Status */
	if ( LSRValue & (LSR_OE|LSR_PE|LSR_FE|LSR_RXFE|LSR_BI) )
	{
		/*****************************************************************************
		** Paritat 1
		**
		*****************************************************************************/
		if(LSRValue & (LSR_PE|LSR_RXFE))
		{//Si la paritat no concorda --> primer o últim byte
			UART0Buffer[UART0Count] = LPC_UART0->RBR;
			if((UART0Buffer[UART0Count] == 0xFF)&&(usedPORTCanal1==PORT0))//Final de frame
			{
				reset_timer(1);
				disable_timer(1);
				if(escrivint[node_actual-1]|| UART0Count<=2)
				{//Fins que no s'ha escrit tot no continua
					StateUART_canal1 = PROXIM_NODE;
					UART0Count = 0;
					return;
				}
				if(UART0Buffer[UART0Count-1]==CRC((uint8_t *)UART0Buffer,0,UART0Count-2))//------------Versiov1.2
					process_data((uint8_t *)UART0Buffer,UART0Count,UART0);//Tractament de tipus de trama
				else
				{//ERROR de CRC
					if(malloc_buffer[node_actual-1])
					{
						malloc_buffer[node_actual-1]++;
						if(malloc_buffer[node_actual-1]>REENVIAMENTS)
						{
							//free(send_buffer[node_actual-1]);
							if(counter_envios_fallidos<MAX_ERRORS) counter_envios_fallidos++;
							else counter_envios_fallidos;
							Error_aplicacio(ENVIAR_TRAMA,node_actual,1);
							//send_buffer[node_actual-1]=NULL;
							uint8_t n; for(n=0;n<MAX_DADES_NODE;n++) send_buffer[node_actual-1][n]=0;
							malloc_buffer[node_actual-1]=0;
							//nombre_mallocs--;
						}
					}
				}

				for(i=0;i<=UART0Count;i++)UART0Buffer[i]=0;
				if(StateUART_canal1==ENVIANT_PROGRAMACIO)return;//Xk no es posi UART0Count a 0
																//ni es canvii StateUART_canal1
				UART0Count = 0;
				if(malloc_buffer[node_actual-1])
				{
					if(send_buffer[node_actual-1][0]!= 0 && send_buffer[node_actual-1][1]!= 0)
					{//Si s'ha d'enviar dades
						if(escrivint[node_actual-1])return;//Si s'està escrivint-->següent polling
						for(i=0;i<send_buffer[node_actual-1][0];i++)
						{//Agafes dades del buffer fins llargada
							UART0Transmit_Buffer[i]=send_buffer[node_actual-1][i+1];
							//send_buffer[node_actual-1][i+1]=0;
						}
						//send_buffer[node_actual-1][0]=0;//Borres inici
						if(i>=(MAX_DADES_NODE-1))
						{
							//free(send_buffer[node_actual-1]);
							if(counter_envios_fallidos<MAX_ERRORS) counter_envios_fallidos++;
							else counter_envios_fallidos;
							Error_aplicacio(ENVIAR_TRAMA,node_actual,1);
							//send_buffer[node_actual-1]=NULL;
							uint8_t n; for(n=0;n<MAX_DADES_NODE;n++) send_buffer[node_actual-1][n]=0;
							malloc_buffer[node_actual-1]=0;
							//nombre_mallocs--;
							StateUART_canal1 = PROXIM_NODE;
							UART0Count = 0;
							return;
						}
						/*send_buffer[node_actual-1][i]=0;
						for(j=0;j<=(MAX_DADES_NODE-1)-(i+1);j++)
						{//Shift left
							send_buffer[node_actual-1][j]=send_buffer[node_actual-1][i+1+j];
							send_buffer[node_actual-1][i+1+j] = 0;
						}*/
						UARTSend(0);
					}
					else//Si no s'ha d'enviar dades
					{
						//free(send_buffer[node_actual-1]);
						//send_buffer[node_actual-1]=NULL;
						//nombre_mallocs--;
						StateUART_canal1 = PROXIM_NODE;
						UART0Count = 0;
						uint8_t n; for(n=0;n<MAX_DADES_NODE;n++) send_buffer[node_actual-1][n]=0;
						malloc_buffer[node_actual-1]=0;
						enviament_broadcast = enviament_broadcast_1_anterior = reenviaments_broadcast_1 = 0;
					}
				}
				else if(Broadcast_polling&&enviament_broadcast!=10)
				{//Si es el polling del broadcast i no s'ha acabat els envios
					if(enviament_broadcast)
					{//Si no es el primer enviament es posiciona el punter "i" del buffer
						i=0;
						for(j=0;j<enviament_broadcast;j++)
						{
							//while(UARTTransmit_Broadcast[i]!=0xFF&&i<MAX_DADES_NODE)i++;
							k = i;
							while(i<UARTTransmit_Broadcast[k]+k)i++;
							i++;
						}
						i++;
						i++;
					}
					else
					{
						//i=1;
						i = 2;
					}
					j=1;
					k = i-2;
					UART0Transmit_Buffer[0]=node_actual;
					//while(UARTTransmit_Broadcast[i]!=0xFF&&i<MAX_DADES_NODE)
					while((i<=UARTTransmit_Broadcast[k]+k)&&i<MAX_DADES_NODE)
					{//Agafes dades del buffer fins 0xFF
						UART0Transmit_Buffer[j]=UARTTransmit_Broadcast[i];
						i++;
						j++;
					}
					if(i>=MAX_DADES_NODE)
					{
						StateUART_canal1 = PROXIM_NODE;
						UART0Count = 0;
						enviament_broadcast = enviament_broadcast_1_anterior = reenviaments_broadcast_1 = 0;
						return;
					}
					/*if(UARTTransmit_Broadcast[i+1]==0xFF)
					{//Per si CRC es 0xFF i no s'ha agafat final de caracter
						UART0Transmit_Buffer[j]=UARTTransmit_Broadcast[i];
						i++;
						j++;
					}*/
					UART0Transmit_Buffer[j-2]=CRC((uint8_t *)UART0Transmit_Buffer,0,j-3);
					//UART0Transmit_Buffer[j]=UARTTransmit_Broadcast[i-1];//ultim byte 0xFF
					enviament_broadcast_1_anterior = enviament_broadcast;
					enviament_broadcast++;
					if(UARTTransmit_Broadcast[i+1]==0)enviament_broadcast=10;//S'acaben els envios
					UARTSend(0);
				}
				else
				{
					StateUART_canal1 = PROXIM_NODE;
					UART0Count = 0;
					enviament_broadcast = enviament_broadcast_1_anterior = reenviaments_broadcast_1 = 0;
				}
			}
			else
			{
				UART0Count++;
			}
			if ( UART0Count == BUFSIZE )
			{
			  UART0Count = 0;		// buffer overflow
			}
			UART0Status = LSRValue;
			return;
		}
		else
		{
		  UART0Status = LSRValue;
		  Dummy = LPC_UART0->RBR;		/* Dummy read on RX to clear
									interrupt, then bail out */
		  return;
		}
	}
	/*****************************************************************************
	** Receive data available
	**
	*****************************************************************************/
	if ( (LSRValue & LSR_RDR)&&(usedPORTCanal1==PORT0) )	/* Receive Data Ready */
	{
	  /* If no error on RLS, normal ready, save into the data buffer. */
	  /* Note: read RBR will clear the interrupt */
	  UART0Buffer[UART0Count] = LPC_UART0->RBR;
	  UART0Count++;
	  if ( UART0Count == BUFSIZE )
	  {
		UART0Count = 0;		/* buffer overflow */
	  }	
	}
  }
  /*****************************************************************************
  ** Receive data available
  **
  *****************************************************************************/
  else if ( (IIRValue == IIR_RDA)&&(usedPORTCanal1==PORT0) )	/* Receive Data Available */
  {
	/* Receive Data Available */
	UART0Buffer[UART0Count] = LPC_UART0->RBR;
	UART0Count++;
	if ( UART1Count == BUFSIZE )
	{
	  UART0Count = 0;		/* buffer overflow */
	}
  }
  else if ( IIRValue == IIR_CTI )	/* Character timeout indicator */
  {
	/* Character Time-out indicator */
	UART0Status |= 0x100;		/* Bit 9 as the CTI error */
	Dummy = LPC_UART0->RBR;
  }
  else if ( IIRValue == IIR_THRE )	/* THRE, transmit holding register empty */
  {
	/* THRE interrupt */
	LSRValue = LPC_UART0->LSR;		/* Check status in the LSR to see if
									valid data in U0THR or not */
	/*****************************************************************************
	** Buffer lliure
	**
	*****************************************************************************/
	if ( LSRValue & LSR_THRE )//Buffer de transmissio lliure
	{
		UART0_THRE();
	}
	else
	{
	  UART0TxEmpty = 0;
	}
  }
  else
  {
	  Dummy = LPC_UART0->RBR;
  }
}

/*****************************************************************************
** Function name:		UART3_IRQHandler
**
** Descriptions:		UART3 interrupt handler
**
** parameters:			//CH1.B
** Returned value:		None
**
*****************************************************************************/
void UART3_IRQHandler (void) //CH1.B
{
  uint8_t IIRValue, LSRValue;
  uint8_t Dummy = Dummy;
  uint8_t i,j,k;

  IIRValue = LPC_UART3->IIR;//Tipus d'interrupció
  IIRValue >>= 1;			/* skip pending bit in IIR */
  IIRValue &= 0x07;			/* check bit 1~3, interrupt identification */

  if ( IIRValue == IIR_RLS )		/* Receive Line Status */
  {
	LSRValue = LPC_UART3->LSR;
	/* Receive Line Status */
	if ( LSRValue & (LSR_OE|LSR_PE|LSR_FE|LSR_RXFE|LSR_BI) )
	{
		/*****************************************************************************
		** Paritat 1
		**
		*****************************************************************************/
	  if(LSRValue & (LSR_PE|LSR_RXFE))//Si la paritat no concorda --> primer o últim byte
	  {//Si la paritat no concorda --> primer o últim byte
		  UART1Buffer[UART1Count] = LPC_UART3->RBR;
		/* Byte amb paritat 1 */
		if((UART1Buffer[UART1Count] == 0xFE)&&(usedPORTCanal1==PORT0))//Frame de comprovacio de cable
		{
			reset_timer(1);
			disable_timer(1);
			wire_correcte = TRUE;
			//leds_all_off();
			if((alarmes_activades[0]&(1<<(RUPTURA_ANELL-1)))!=0)
			{
				alarm('a',0,LPC_RTC->HOUR,LPC_RTC->MIN,LPC_RTC->SEC,1);
			}
			alarmes_activades[0] &= ~(1<<(RUPTURA_ANELL-1));
			uint8_t i;	for(i=0;i<MAX_APLICACIONS;i++) taula_errors[i][0]&=~(1<<RUPTURA_ANELL);	//Borrem el flag pq torni a mostrar el missatge d'error
			wire_count = 0;
			LPC_UART3->FCR = 0x07;//Borres TXFIFO
			//LPC_UART3->IER = 0;
			UART1Count = 0;
			StateUART_canal1 = PROXIM_NODE;
			return;
		}
		if((UART1Buffer[UART1Count] == 0xFF)&&(usedPORTCanal1==PORT1))//Final de frame
		{
			reset_timer(1);
			disable_timer(1);
			if(escrivint[node_actual-1] || UART1Count<=2)
			{//Si s'està escrivint-->següent polling
				StateUART_canal1 = PROXIM_NODE;
				UART1Count = 0;
				return;
			}
			if(UART1Buffer[UART1Count-1]==CRC((uint8_t *)UART1Buffer,0,UART1Count-2))//------------Versiov1.2
				process_data((uint8_t *)UART1Buffer,UART1Count,UART1);
			else
			{//ERROR de CRC
				if(malloc_buffer[node_actual-1])
				{
					malloc_buffer[node_actual-1]++;
					if(malloc_buffer[node_actual-1]>REENVIAMENTS)
					{
						//free(send_buffer[node_actual-1]);
						if(counter_envios_fallidos<MAX_ERRORS) counter_envios_fallidos++;
						else counter_envios_fallidos;
						Error_aplicacio(ENVIAR_TRAMA,node_actual,1);
						//send_buffer[node_actual-1]=NULL;
						uint8_t n; for(n=0;n<MAX_DADES_NODE;n++) send_buffer[node_actual-1][n]=0;
						malloc_buffer[node_actual-1]=0;
						//nombre_mallocs--;
					}
				}
			}
			for(i=0;i<UART1Count;i++)UART1Buffer[i]=0;
			if(malloc_buffer[node_actual-1])
			{
				if(send_buffer[node_actual-1][0]!= 0 && send_buffer[node_actual-1][1]!= 0)
				{//Si s'ha d'enviar dades
					if(escrivint[node_actual-1])return;//Si s'està escrivint-->següent polling
					for(i=0;i<send_buffer[node_actual-1][0];i++)
					{//Agafes dades del buffer fins llargada
						UART1Transmit_Buffer[i]=send_buffer[node_actual-1][i+1];
						//send_buffer[node_actual-1][i+1]=0;
					}
					//send_buffer[node_actual-1][0]=0;//Borres inici
					if(i>=(MAX_DADES_NODE-1))
					{
						//free(send_buffer[node_actual-1]);
						if(counter_envios_fallidos<MAX_ERRORS) counter_envios_fallidos++;
						else counter_envios_fallidos;
						Error_aplicacio(ENVIAR_TRAMA,node_actual,1);
						//send_buffer[node_actual-1]=NULL;
						uint8_t n; for(n=0;n<MAX_DADES_NODE;n++) send_buffer[node_actual-1][n]=0;
						malloc_buffer[node_actual-1]=0;
						//nombre_mallocs--;
						StateUART_canal1 = PROXIM_NODE;
						enviament_broadcast = enviament_broadcast_1_anterior = reenviaments_broadcast_1 = 0;
						UART1Count = 0;
						return;
					}
					/*send_buffer[node_actual-1][i]=0;
					for(j=0;j<=(MAX_DADES_NODE-1)-(i+1);j++)
					{//Shift left
						send_buffer[node_actual-1][j]=send_buffer[node_actual-1][i+1+j];
						send_buffer[node_actual-1][i+1+j] = 0;
					}*/
					UARTSend(1);
				}
				else//Si no s'ha d'enviar dades
				{
					//free(send_buffer[node_actual-1]);
					//send_buffer[node_actual-1]=NULL;
					//nombre_mallocs--;
					StateUART_canal1 = PROXIM_NODE;
					UART1Count = 0;
					uint8_t n; for(n=0;n<MAX_DADES_NODE;n++) send_buffer[node_actual-1][n]=0;
					malloc_buffer[node_actual-1]=0;
					enviament_broadcast = enviament_broadcast_1_anterior = reenviaments_broadcast_1 = 0;
				}
			}
			else if(Broadcast_polling&&enviament_broadcast!=10)
			{//Si es el polling del broadcast i no s'ha acabat els envios
				if(enviament_broadcast)
				{//Si no es el primer enviament es posiciona el punter "i" del buffer
					i=0;
					for(j=0;j<enviament_broadcast;j++)
					{
						//while(UARTTransmit_Broadcast[i]!=0xFF&&i<MAX_DADES_NODE)i++;
						k = i;
						while(i<UARTTransmit_Broadcast[k]+k)i++;
						i++;
					}
					i++;
					i++;
				}
				else
				{
					//i=1;
					i = 2;
				}
				j=1;
				k = i-2;
				UART1Transmit_Buffer[0]=node_actual;
				//while(UARTTransmit_Broadcast[i]!=0xFF&&i<MAX_DADES_NODE)
				while((i<=UARTTransmit_Broadcast[k]+k)&&i<MAX_DADES_NODE)
				{//Agafes dades del buffer fins 0xFF
					UART1Transmit_Buffer[j]=UARTTransmit_Broadcast[i];
					i++;
					j++;
				}
				if(i>=MAX_DADES_NODE)
				{
					StateUART_canal1 = PROXIM_NODE;
					UART0Count = 0;
					enviament_broadcast = enviament_broadcast_1_anterior = reenviaments_broadcast_1 = 0;
					return;
				}
				/*if(UARTTransmit_Broadcast[i+1]==0xFF)
				{//Per si CRC es 0xFF i no s'ha agafat final de caracter
					UART0Transmit_Buffer[j]=UARTTransmit_Broadcast[i];
					i++;
					j++;
				}*/
				UART1Transmit_Buffer[j-2]=CRC((uint8_t *)UART1Transmit_Buffer,0,j-3);
				//UART0Transmit_Buffer[j]=UARTTransmit_Broadcast[i-1];//ultim byte 0xFF
				enviament_broadcast_1_anterior = enviament_broadcast;
				enviament_broadcast++;
				if(UARTTransmit_Broadcast[i+1]==0)enviament_broadcast=10;//S'acaben els envios
				UARTSend(1);
			}
			else
			{
				StateUART_canal1 = PROXIM_NODE;
				UART1Count = 0;
				enviament_broadcast = enviament_broadcast_1_anterior = reenviaments_broadcast_1 = 0;
			}
		}
		else if (usedPORTCanal1==PORT1)
		{
			if(UART1Buffer[UART1Count] != 0)
			{
				UART1Count++;
			}
		}
		if ( UART1Count == BUFSIZE )
		{
		  UART1Count = 0;		/* buffer overflow */
		}
		UART1Status = LSRValue;
		return;
	  }
	  else
	  {
		  UART1Status = LSRValue;
		  Dummy = LPC_UART3->RBR;		/* Dummy read on RX to clear
									interrupt, then bail out */
		  return;
	  }
	}
	/*****************************************************************************
	** Receive data available
	**
	*****************************************************************************/
	if ( (LSRValue & LSR_RDR )&&(usedPORTCanal1==PORT1))	/* Receive Data Ready */
	{
	  /* If no error on RLS, normal ready, save into the data buffer. */
	  /* Note: read RBR will clear the interrupt */
	  UART1Buffer[UART1Count] = LPC_UART3->RBR;
	  UART1Count++;
	  if ( UART1Count == BUFSIZE )
	  {
		UART1Count = 0;		/* buffer overflow */
	  }
	}
  }
  /*****************************************************************************
  ** Receive data available
  **
  *****************************************************************************/
  else if (( IIRValue == IIR_RDA )&&(usedPORTCanal1==PORT1))	/* Receive Data Available */
  {
	/* Receive Data Available */
	UART1Buffer[UART1Count] = LPC_UART3->RBR;
	UART1Count++;
	if ( UART1Count == BUFSIZE )
	{
	  UART1Count = 0;		/* buffer overflow */
	}
  }
  else if ( IIRValue == IIR_CTI )	/* Character timeout indicator */
  {
	/* Character Time-out indicator */
	UART1Status |= 0x100;		/* Bit 9 as the CTI error */
	Dummy = LPC_UART3->RBR;
  }
  else if ( IIRValue == IIR_THRE )	/* THRE, transmit holding register empty */
  {
	/* THRE interrupt */
	LSRValue = LPC_UART3->LSR;		/* Check status in the LSR to see if
								valid data in U0THR or not */
	/*****************************************************************************
	** Buffer lliure
	**
	*****************************************************************************/
	if ( LSRValue & LSR_THRE )//Buffer de transmissió lliure
	{
		UART3_THRE();
	}
	else
	{
	  UART1TxEmpty = 0;
	}
  }
  else
  {
	  Dummy = LPC_UART3->RBR;
  }
}

/*****************************************************************************
** Function name:		UART1_IRQHandler
**
** Descriptions:		UART1 interrupt handler
**
** parameters:			//CH2.A
** Returned value:		None
** 
*****************************************************************************/
void UART1_IRQHandler (void) //CH2.A
{
  uint8_t IIRValue, LSRValue;
  uint8_t Dummy = Dummy;
  uint8_t i,j,k;

  IIRValue = LPC_UART1->IIR;//Tipus d'interrupció

  IIRValue >>= 1;			/* skip pending bit in IIR */
  IIRValue &= 0x07;			/* check bit 1~3, interrupt identification */
  if ( IIRValue == IIR_RLS )		/* Receive Line Status */
  {
	LSRValue = LPC_UART1->LSR;
	/* Receive Line Status */
	if ( LSRValue & (LSR_OE|LSR_PE|LSR_FE|LSR_RXFE|LSR_BI) )
	{
		/*****************************************************************************
		** Paritat 1
		**
		*****************************************************************************/
		if(LSRValue & (LSR_PE|LSR_RXFE))
		{//Si la paritat no concorda --> primer o últim byte
			UART2Buffer[UART2Count] = LPC_UART1->RBR;
			//if(UART2Buffer[UART2Count] == 0xFF|| UART2Count<=2)//Final de frame
			if((UART2Buffer[UART2Count] == 0xFF)&&(usedPORTCanal2==PORT2))//Final de frame
			{
				reset_timer(2);
				disable_timer(2);
				if(escrivint_2[node_actual_2-1]|| UART2Count<=2)
				{//Fins que no s'ha escrit tot no continua
					StateUART_canal2 = PROXIM_NODE;
					UART2Count = 0;
					return;
				}
				if(UART2Buffer[UART2Count-1]==CRC((uint8_t *)UART2Buffer,0,UART2Count-2))//------------Versiov1.2
					process_data((uint8_t *)UART2Buffer,UART2Count,UART2);
				else
				{//ERROR de CRC
					if(malloc_buffer_2[node_actual_2-1])
					{
						malloc_buffer_2[node_actual_2-1]++;
						if(malloc_buffer_2[node_actual_2-1]>REENVIAMENTS)
						{
							//free(send_buffer_2[node_actual_2-1]);
							if(counter_envios_fallidos<MAX_ERRORS) counter_envios_fallidos++;
							else counter_envios_fallidos;
							Error_aplicacio(ENVIAR_TRAMA,node_actual_2,2);
							uint8_t n; for(n=0;n<MAX_DADES_NODE;n++) send_buffer_2[node_actual_2-1][n]=0;
							//send_buffer_2[node_actual_2-1]=NULL;
							malloc_buffer_2[node_actual_2-1]=0;
							//nombre_mallocs_2--;
						}
					}
				}
				for(i=0;i<UART2Count;i++)UART2Buffer[i]=0;
				if(StateUART_canal2==ENVIANT_PROGRAMACIO)return;//Xk no es posi UART2Count a 0
																//ni es canvii StateUART_canal2
				UART2Count = 0;
				if(malloc_buffer_2[node_actual_2-1])
				{
					if(send_buffer_2[node_actual_2-1][0]!= 0 && send_buffer_2[node_actual_2-1][1]!= 0)
					{//Si s'ha d'enviar dades
						if(escrivint_2[node_actual_2-1])return;//Si s'està escrivint-->següent polling
						//for(i=0;send_buffer_2[node_actual_2-1][i]!=0xFF&&i<(MAX_DADES_NODE-1);i++)
						for(i=0;i<send_buffer_2[node_actual_2-1][0];i++)
						{//Agafes dades del buffer fins llargada
							UART2Transmit_Buffer[i]=send_buffer_2[node_actual_2-1][i+1];
							//send_buffer_2[node_actual_2-1][i+1]=0;
						}
						//send_buffer_2[node_actual_2-1][0]=0;//Borres inici
						if(i>=(MAX_DADES_NODE-1))
						{
							//free(send_buffer_2[node_actual_2-1]);
							if(counter_envios_fallidos<MAX_ERRORS) counter_envios_fallidos++;
							else counter_envios_fallidos;
							Error_aplicacio(ENVIAR_TRAMA,node_actual_2,2);
							//send_buffer_2[node_actual_2-1]=NULL;
							uint8_t n; for(n=0;n<MAX_DADES_NODE;n++) send_buffer_2[node_actual_2-1][n]=0;
							malloc_buffer_2[node_actual_2-1]=0;
							//nombre_mallocs_2--;
							StateUART_canal2 = PROXIM_NODE;
							UART2Count = 0;
							return;
						}
						/*send_buffer_2[node_actual_2-1][i]=0;
						for(j=0;j<=(MAX_DADES_NODE-1)-(i+1);j++)
						{//Shift left
							send_buffer_2[node_actual_2-1][j]=send_buffer_2[node_actual_2-1][i+1+j];
							send_buffer_2[node_actual_2-1][i+1+j] = 0;
						}*/
						UARTSend(2);
					}
					else//Si no s'ha d'enviar dades
					{
						//free(send_buffer_2[node_actual_2-1]);
						//send_buffer_2[node_actual_2-1]=NULL;
						//nombre_mallocs_2--;
						uint8_t n; for(n=0;n<MAX_DADES_NODE;n++) send_buffer_2[node_actual_2-1][n]=0;
						StateUART_canal2 = PROXIM_NODE;
						UART2Count = 0;
						malloc_buffer_2[node_actual_2-1]=0;
						enviament_broadcast_2 = enviament_broadcast_2_anterior = reenviaments_broadcast_2 = 0;
					}
				}
				else if(Broadcast_polling_2&&enviament_broadcast_2!=10)
				{//Si es el polling del broadcast i no s'ha acabat els envios
					if(enviament_broadcast_2)
					{//Si no es el primer enviament es posiciona l'índex "i" del buffer
						i=0;
						for(j=0;j<enviament_broadcast_2;j++)
						{
							//while(UARTTransmit_Broadcast_2[i]!=0xFF&&i<MAX_DADES_NODE&&UARTTransmit_Broadcast_2[i+1]!=BROADCAST_ADRESS)i++;
							k = i;
							while(i<UARTTransmit_Broadcast_2[k]+k)i++;
							i++;
						}
						i++;
						i++;
					}
					else
					{
						//i=1;
						i = 2;
					}
					j=1;
					k = i-2;
					UART2Transmit_Buffer[0]=node_actual_2;
					//while(UARTTransmit_Broadcast_2[i]!=0xFF&&i<MAX_DADES_NODE)
					while((i<=UARTTransmit_Broadcast_2[k]+k)&&i<MAX_DADES_NODE)
					{//Agafes dades del buffer fins 0xFF
						UART2Transmit_Buffer[j]=UARTTransmit_Broadcast_2[i];
						i++;
						j++;
					}
					if(i>=MAX_DADES_NODE)
					{//Per si es fa overflow
						StateUART_canal2 = PROXIM_NODE;
						UART2Count = 0;
						enviament_broadcast_2 = enviament_broadcast_2_anterior=reenviaments_broadcast_2= 0;
						return;
					}
					/*if(UARTTransmit_Broadcast_2[i+1]==0xFF)
					{//Per si CRC es 0xFF i no s'ha agafat final de caracter
						UART2Transmit_Buffer[j]=UARTTransmit_Broadcast_2[i];
						i++;
						j++;
					}*/
					UART2Transmit_Buffer[j-2]=CRC((uint8_t *)UART2Transmit_Buffer,0,j-3);
					//UART2Transmit_Buffer[j]=UARTTransmit_Broadcast_2[i];//ultim byte 0xFF
					enviament_broadcast_2_anterior = enviament_broadcast_2;
					enviament_broadcast_2++;
					if(UARTTransmit_Broadcast_2[i+1]==0)enviament_broadcast_2=10;//S'acaben els envios
					UARTSend(2);
				}
				else
				{
					StateUART_canal2 = PROXIM_NODE;
					UART2Count = 0;
					enviament_broadcast_2 = enviament_broadcast_2_anterior=reenviaments_broadcast_2= 0;
				}
			}
			else
			{
				UART2Count++;
			}
			if ( UART2Count == BUFSIZE )
			{
			  UART2Count = 0;		/* buffer overflow */
			}
			UART2Status = LSRValue;
			return;
		}
		else
		{
		  UART2Status = LSRValue;
		  Dummy = LPC_UART1->RBR;		/* Dummy read on RX to clear
									interrupt, then bail out */
		  return;
		}
	}
	/*****************************************************************************
	** Receive data available
	**
	*****************************************************************************/
	if ( (LSRValue & LSR_RDR) &&(usedPORTCanal2==PORT2))	/* Receive Data Ready */
	{
	  /* If no error on RLS, normal ready, save into the data buffer. */
	  /* Note: read RBR will clear the interrupt */
	  UART2Buffer[UART2Count] = LPC_UART1->RBR;
	  UART2Count++;
	  if ( UART2Count == BUFSIZE )
	  {
		UART2Count = 0;		/* buffer overflow */
	  }
	}
  }
  /*****************************************************************************
  ** Receive data available
  **
  *****************************************************************************/
  else if ( (IIRValue == IIR_RDA) &&(usedPORTCanal2==PORT2))	/* Receive Data Available */
  {
	/* Receive Data Available */
	UART2Buffer[UART2Count] = LPC_UART1->RBR;
	UART2Count++;
	if ( UART2Count == BUFSIZE )
	{
	  UART2Count = 0;		/* buffer overflow */
	}
  }
  else if ( IIRValue == IIR_CTI )	/* Character timeout indicator */
  {
	/* Character Time-out indicator */
	UART2Status |= 0x100;		/* Bit 9 as the CTI error */
	Dummy = LPC_UART1->RBR;
  }
  else if ( IIRValue == IIR_THRE )	/* THRE, transmit holding register empty */
  {
	/* THRE interrupt */

	LSRValue = LPC_UART1->LSR;		/* Check status in the LSR to see if
									valid data in U0THR or not */
	/*****************************************************************************
	** Buffer lliure
	**
	*****************************************************************************/
	if ( LSRValue & LSR_THRE )//Buffer de transmissio lliure
	{
		UART1_THRE();
	}
	else
	{
	  UART2TxEmpty = 0;
	}
  }
  else
  {
	  Dummy = LPC_UART1->RBR;
  }
}

/*****************************************************************************
** Function name:		UART2_IRQHandler
**
** Descriptions:		UART2 interrupt handler
**
** parameters:			//CH2.B
** Returned value:		None
**
*****************************************************************************/
void UART2_IRQHandler (void) //CH2.B
{
  uint8_t IIRValue, LSRValue;
  uint8_t Dummy = Dummy;
  uint8_t i,j,k;
	
  IIRValue = LPC_UART2->IIR;
    
  IIRValue >>= 1;			/* skip pending bit in IIR */
  IIRValue &= 0x07;			/* check bit 1~3, interrupt identification */
  if ( IIRValue == IIR_RLS )		/* Receive Line Status */
  {
	LSRValue = LPC_UART2->LSR;
	/* Receive Line Status */
	if ( LSRValue & (LSR_OE|LSR_PE|LSR_FE|LSR_RXFE|LSR_BI) )
	{
		/*****************************************************************************
		** Paritat 1
		**
		*****************************************************************************/
	  if(LSRValue & (LSR_PE|LSR_RXFE))//Si la paritat no concorda --> primer o últim byte
	  {//Si la paritat no concorda --> primer o últim byte
		  UART3Buffer[UART3Count] = LPC_UART2->RBR;
		/* Byte amb paritat 1 */
		if((UART3Buffer[UART3Count] == 0xFE)&&(usedPORTCanal2==PORT2))//Frame de comprovacio de cable
		{
			reset_timer(2);
			disable_timer(2);
			wire_correcte_2 = TRUE;
			//leds_all_off();
			if((alarmes_activades_2[0]&(1<<(RUPTURA_ANELL-1)))!=0)
			{
				alarm('a',0,LPC_RTC->HOUR,LPC_RTC->MIN,LPC_RTC->SEC,2);
			}
			alarmes_activades_2[0] &= ~(1<<(RUPTURA_ANELL-1));
			uint8_t i;	for(i=0;i<MAX_APLICACIONS;i++) taula_errors_2[i][0]&=~(1<<RUPTURA_ANELL);	//Borrem el flag pq torni a mostrar el missatge d'error
			wire_count_2 = 0;
			LPC_UART2->FCR = 0x07;//Borres TXFIFO
			UART3Count = 0;
			StateUART_canal2 = PROXIM_NODE;
			return;
		}
		if((UART3Buffer[UART3Count] == 0xFF)&&(usedPORTCanal2==PORT3))//Final de frame
		{
			reset_timer(2);
			disable_timer(2);
			if(escrivint_2[node_actual_2-1]|| UART3Count<=2)
			{//Si s'està escrivint-->següent polling
				StateUART_canal2 = PROXIM_NODE;
				UART3Count = 0;
				return;
			}
			if(UART3Buffer[UART3Count-1]==CRC((uint8_t *)UART3Buffer,0,UART3Count-2))//------------Versiov1.2
				process_data((uint8_t *)UART3Buffer,UART3Count,UART3);
			else
			{//ERROR de CRC
				if(malloc_buffer_2[node_actual_2-1])
				{
					malloc_buffer_2[node_actual_2-1]++;
					if(malloc_buffer_2[node_actual_2-1]>REENVIAMENTS)
					{
						//free(send_buffer_2[node_actual_2-1]);
						if(counter_envios_fallidos<MAX_ERRORS) counter_envios_fallidos++;
						else counter_envios_fallidos;
						Error_aplicacio(ENVIAR_TRAMA,node_actual_2,2);
						//send_buffer_2[node_actual_2-1]=NULL;
						uint8_t n; for(n=0;n<MAX_DADES_NODE;n++) send_buffer_2[node_actual_2-1][n]=0;
						malloc_buffer_2[node_actual_2-1]=0;
						//nombre_mallocs_2--;
					}
				}
			}
			for(i=0;i<UART3Count;i++)UART3Buffer[i]=0;
			if(malloc_buffer_2[node_actual_2-1])
			{
				if(send_buffer_2[node_actual_2-1][0]!= 0 && send_buffer_2[node_actual_2-1][1]!= 0)
				{//Si s'ha d'enviar dades
					if(escrivint_2[node_actual_2-1])return;//Si s'està escrivint-->següent polling
					//for(i=0;send_buffer_2[node_actual_2-1][i]!=0xFF&&i<(MAX_DADES_NODE-1);i++)
					for(i=0;i<send_buffer_2[node_actual_2-1][0];i++)
					{//Agafes dades del buffer fins llargada
						UART3Transmit_Buffer[i]=send_buffer_2[node_actual_2-1][i+1];
						//send_buffer_2[node_actual_2-1][i+1]=0;
					}
					//send_buffer_2[node_actual_2-1][0]=0;//Borres inici
					if(i>=(MAX_DADES_NODE-1))
					{
						//free(send_buffer_2[node_actual_2-1]);
						if(counter_envios_fallidos<MAX_ERRORS) counter_envios_fallidos++;
						else counter_envios_fallidos;
						Error_aplicacio(ENVIAR_TRAMA,node_actual_2,2);
						//send_buffer_2[node_actual_2-1]=NULL;
						uint8_t n; for(n=0;n<MAX_DADES_NODE;n++) send_buffer_2[node_actual_2-1][n]=0;
						malloc_buffer_2[node_actual_2-1]=0;
						//nombre_mallocs_2--;
						StateUART_canal2 = PROXIM_NODE;
						UART3Count = 0;
						return;
					}
					/*send_buffer_2[node_actual_2-1][i]=0;
					for(j=0;j<=(MAX_DADES_NODE-1)-(i+1);j++)
					{//Shift left
						send_buffer_2[node_actual_2-1][j]=send_buffer_2[node_actual_2-1][i+1+j];
						send_buffer_2[node_actual_2-1][i+1+j] = 0;
					}*/
					UARTSend(3);
				}
				else//Si no s'ha d'enviar dades
				{
					//free(send_buffer_2[node_actual_2-1]);
					//send_buffer_2[node_actual_2-1]=NULL;
					//nombre_mallocs_2--;
					uint8_t n; for(n=0;n<MAX_DADES_NODE;n++) send_buffer_2[node_actual_2-1][n]=0;
					StateUART_canal2 = PROXIM_NODE;
					UART3Count = 0;
					malloc_buffer_2[node_actual_2-1]=0;
				}
			}
			else if(Broadcast_polling_2&&enviament_broadcast_2!=10)
			{//Si es el polling del broadcast i no s'ha acabat els envios
				if(enviament_broadcast_2)
				{//Si no es el primer enviament es posiciona l'índex "i" del buffer
					i=0;
					for(j=0;j<enviament_broadcast_2;j++)
					{
						//while(UARTTransmit_Broadcast_2[i]!=0xFF&&i<MAX_DADES_NODE&&UARTTransmit_Broadcast_2[i+1]!=BROADCAST_ADRESS)i++;
						k = i;
						while(i<UARTTransmit_Broadcast_2[k]+k)i++;
						i++;
					}
					i++;
					i++;
				}
				else
				{
					//i=1;
					i = 2;
				}
				j=1;
				k = i-2;
				UART3Transmit_Buffer[0]=node_actual_2;
				//while(UARTTransmit_Broadcast_2[i]!=0xFF&&i<MAX_DADES_NODE)
				while((i<=UARTTransmit_Broadcast_2[k]+k)&&i<MAX_DADES_NODE)
				{//Agafes dades del buffer fins 0xFF
					UART3Transmit_Buffer[j]=UARTTransmit_Broadcast_2[i];
					i++;
					j++;
				}
				if(i>=MAX_DADES_NODE)
				{//Per si es fa overflow
					StateUART_canal2 = PROXIM_NODE;
					UART2Count = 0;
					enviament_broadcast_2 = enviament_broadcast_2_anterior=reenviaments_broadcast_2= 0;
					return;
				}
				/*if(UARTTransmit_Broadcast_2[i+1]==0xFF)
				{//Per si CRC es 0xFF i no s'ha agafat final de caracter
					UART2Transmit_Buffer[j]=UARTTransmit_Broadcast_2[i];
					i++;
					j++;
				}*/
				UART3Transmit_Buffer[j-2]=CRC((uint8_t *)UART3Transmit_Buffer,0,j-3);
				//UART2Transmit_Buffer[j]=UARTTransmit_Broadcast_2[i];//ultim byte 0xFF
				enviament_broadcast_2_anterior = enviament_broadcast_2;
				enviament_broadcast_2++;
				if(UARTTransmit_Broadcast_2[i+1]==0)enviament_broadcast_2=10;//S'acaben els envios
				UARTSend(3);
			}
			else
			{
				StateUART_canal2 = PROXIM_NODE;
				enviament_broadcast_2 = enviament_broadcast_2_anterior=reenviaments_broadcast_2= 0;
				UART3Count = 0;
			}
		}
		else
		{
			if(UART3Buffer[UART3Count] != 0)
			{
				UART3Count++;
			}
		}
		if ( UART3Count == BUFSIZE )
		{
		  UART3Count = 0;		/* buffer overflow */
		}
		UART3Status = LSRValue;
		return;
	  }
	  else
	  {
		  UART3Status = LSRValue;
		  Dummy = LPC_UART2->RBR;		/* Dummy read on RX to clear
									interrupt, then bail out */
		  return;
	  }

	}
	/*****************************************************************************
	** Receive data available
	**
	*****************************************************************************/
	if (( LSRValue & LSR_RDR )&&(usedPORTCanal2==PORT3))	/* Receive Data Ready */
	{
	  /* If no error on RLS, normal ready, save into the data buffer. */
	  /* Note: read RBR will clear the interrupt */
	  UART3Buffer[UART3Count] = LPC_UART2->RBR;
	  UART3Count++;
	  if ( UART3Count == BUFSIZE )
	  {
		UART3Count = 0;		/* buffer overflow */
	  }	
	}
  }
  /*****************************************************************************
  ** Receive data available
  **
  *****************************************************************************/
  else if (( IIRValue == IIR_RDA )&&(usedPORTCanal2==PORT3))	/* Receive Data Available */
  {
	/* Receive Data Available */
	UART3Buffer[UART3Count] = LPC_UART2->RBR;
	UART3Count++;
	if ( UART3Count == BUFSIZE )
	{
	  UART3Count = 0;		/* buffer overflow */
	}
  }
  else if ( IIRValue == IIR_CTI )	/* Character timeout indicator */
  {
	/* Character Time-out indicator */
	UART3Status |= 0x100;		/* Bit 9 as the CTI error */
	Dummy = LPC_UART2->RBR;
  }
  else if ( IIRValue == IIR_THRE )	/* THRE, transmit holding register empty */
  {
	/* THRE interrupt */
	LSRValue = LPC_UART2->LSR;		/* Check status in the LSR to see if
								valid data in U0THR or not */
	/*****************************************************************************
	** Buffer lliure
	**
	*****************************************************************************/
	if ( LSRValue & LSR_THRE )//Buffer de transmissió lliure
	{
		UART2_THRE();
	}
	else
	{
	  UART3TxEmpty = 0;
	}
  }
  else
  {
	  Dummy = LPC_UART2->RBR;
  }
}

/*****************************************************************************
** Function name:		UARTInit
**
** Descriptions:		Initialize UART port, setup pin select,
**						clock, parity, stop bits, FIFO, etc.
**
** parameters:			portNum(0 or 1) and UART baudrate
** Returned value:		true or false, return false only if the 
**						interrupt handler can't be installed to the 
**						VIC table
** 
*****************************************************************************/
uint32_t UARTInit( uint32_t PortNum, uint32_t baudrate )
{
  uint32_t Fdiv;
  uint32_t pclkdiv, pclk;
  uint8_t Dummy = Dummy;

  if ( PortNum == 0 )
  {
	LPC_PINCON->PINSEL0 &= ~0x000000F0;
	LPC_PINCON->PINSEL0 |= 0x00000050;  /* RxD0 is P0.3 and TxD0 is P0.2 */
	/* By default, the PCLKSELx value is zero, thus, the PCLK for
	all the peripherals is 1/4 of the SystemFrequency. */

	//inicialitzacio pin enable TX
	// Set P0_5 to 00 - GPIO
	LPC_PINCON->PINSEL0	&= (~0xC00);
	// Set GPIO - P0_5 - to be outputs
	LPC_GPIO0->FIODIR |= 1<<5;

	/* Bit 6~7 is for UART0 */
	pclkdiv = (LPC_SC->PCLKSEL0 >> 6) & 0x03;
	switch ( pclkdiv )
	{
	  case 0x00:
	  default:
		pclk = SystemCoreClock/4;
		break;
	  case 0x01:
		pclk = SystemCoreClock;
		break; 
	  case 0x02:
		pclk = SystemCoreClock/2;
		break; 
	  case 0x03:
		pclk = SystemCoreClock/8;
		break;
	}

    LPC_UART0->LCR = 0x83;		/* 8 bits, no Parity, 1 Stop bit */
	Fdiv = ( pclk / 16 ) / baudrate ;	/*baud rate */
    //LPC_UART0->DLM = Fdiv / 256;
    //LPC_UART0->DLL = Fdiv % 256;
	LPC_UART0->DLM = 0;
	LPC_UART0->DLL = 3;
	LPC_UART0->FDR = 0x10;
	LPC_UART0->LCR = 0x03;		/* DLAB = 0 --> enable interrupts*/
    LPC_UART0->FCR = 0x07;		/* Enable and reset TX and RX FIFO. */

    //-Afegit-------------------------------------------------------//

    //RS-485
    /* Read to clear the line status. */
    Dummy = LPC_UART0->LSR;
    /* Ensure a clean start, no data in either TX or RX FIFO. */
    while ( (LPC_UART0->LSR & (LSR_TEMT)) != (LSR_TEMT) );
    /*while ( LPC_UART0->LSR & LSR_RDR )
    {
  	Dummy = LPC_UART0->RBR;	// Dump data from RX FIFO
    }*/

    LPC_UART0->LCR |= ((0x3<<4)|(0x1<<3));	/* forced "0" sticky parity, parity enable */

    //--------------------------------------------------------------//

   	NVIC_EnableIRQ(UART0_IRQn);
   	NVIC_SetPriority(UART0_IRQn,0);
    LPC_UART0->IER = IER_RBR | IER_THRE | IER_RLS;	/* Enable UART0 interrupt */
    return (TRUE);
  }
  else if ( PortNum == 1 )
  {
	LPC_PINCON->PINSEL0 &= ~0x0000000F;
	LPC_PINCON->PINSEL0 |= 0x0000000A;	/* Enable RxD3 P0.1, TxD3 P0.0 */

	//inicialitzacio pin enable TX
	// Set P0_4 to 00 - GPIO
	LPC_PINCON->PINSEL0	&= (~0x300);
	// Set GPIO - P0_4 - to be outputs
	LPC_GPIO0->FIODIR |= 1<<4;
	//Power to UART3 no esta habilitat per defecte
	LPC_SC->PCONP |= (1 << 25);

	/* By default, the PCLKSELx value is zero, thus, the PCLK for
	all the peripherals is 1/4 of the SystemFrequency. */
	/* Bit 8,9 are for UART1 */
	pclkdiv = (LPC_SC->PCLKSEL1 >> 18) & 0x03;
	switch ( pclkdiv )
	{
	  case 0x00:
	  default:
		pclk = SystemCoreClock/4;
		break;
	  case 0x01:
		pclk = SystemCoreClock;
		break; 
	  case 0x02:
		pclk = SystemCoreClock/2;
		break; 
	  case 0x03:
		pclk = SystemCoreClock/8;
		break;
	}

    LPC_UART3->LCR = 0x83;		/* 8 bits, no Parity, 1 Stop bit */
	Fdiv = ( pclk / 16 ) / baudrate ;	/*baud rate */
    //LPC_UART3->DLM = Fdiv / 256;
    //LPC_UART3->DLL = Fdiv % 256;
	LPC_UART3->DLM = 0;
	LPC_UART3->DLL = 3;
	LPC_UART3->FDR = 0x10;
	LPC_UART3->LCR = 0x03;		/* DLAB = 0 --> enable interrupts*/
    LPC_UART3->FCR = 0x07;		/* Enable and reset TX and RX FIFO. */

    //-Afegit-------------------------------------------------------//

    //RS-485
    /* Read to clear the line status. */
    Dummy = LPC_UART3->LSR;
    /* Ensure a clean start, no data in either TX or RX FIFO. */
    while ( (LPC_UART3->LSR & (LSR_TEMT)) != (LSR_TEMT) );
    /*while ( LPC_UART3->LSR & LSR_RDR )
    {
  	Dummy = LPC_UART3->RBR;	// Dump data from RX FIFO
    }*/

    LPC_UART3->LCR |= ((0x3<<4)|(0x1<<3));	/* forced "0" sticky parity, parity enable */

    //--------------------------------------------------------------//

   	NVIC_EnableIRQ(UART3_IRQn);
   	NVIC_SetPriority(UART3_IRQn,3);
    LPC_UART3->IER = IER_RBR | IER_THRE | IER_RLS;	/* Disable UART1 interrupt */
    return (TRUE);
  }
  else if ( PortNum == 2 )
  {
	LPC_PINCON->PINSEL4 &= ~0x0000000F;
	LPC_PINCON->PINSEL4 |= 0x0000000A;	/* Enable RxD1 P2.1, TxD1 P2.0 */

	//inicialitzacio pin enable TX
	// Set P0_7 to 00 - GPIO
	LPC_PINCON->PINSEL0	&= (~0xC000);
	// Set GPIO - P0_7 - to be outputs
	LPC_GPIO0->FIODIR |= 1<<7;

	/* By default, the PCLKSELx value is zero, thus, the PCLK for
	all the peripherals is 1/4 of the SystemFrequency. */
	/* Bit 8,9 are for UART1 */
	pclkdiv = (LPC_SC->PCLKSEL0 >> 8) & 0x03;
	switch ( pclkdiv )
	{
	  case 0x00:
	  default:
		pclk = SystemCoreClock/4;
		break;
	  case 0x01:
		pclk = SystemCoreClock;
		break;
	  case 0x02:
		pclk = SystemCoreClock/2;
		break;
	  case 0x03:
		pclk = SystemCoreClock/8;
		break;
	}

    LPC_UART1->LCR = 0x83;		/* 8 bits, no Parity, 1 Stop bit */
	Fdiv = ( pclk / 16 ) / baudrate ;	/*baud rate */
    //LPC_UART1->DLM = Fdiv / 256;
    //LPC_UART1->DLL = Fdiv % 256;
	LPC_UART1->DLM = 0;
	LPC_UART1->DLL = 3;
	LPC_UART1->FDR = 0x10;
	LPC_UART1->LCR = 0x03;		/* DLAB = 0 --> enable interrupts*/
    LPC_UART1->FCR = 0x07;		/* Enable and reset TX and RX FIFO. */

    //-Afegit-------------------------------------------------------//

    //RS-485
    /* Read to clear the line status. */
    Dummy = LPC_UART1->LSR;
    /* Ensure a clean start, no data in either TX or RX FIFO. */
    while ( (LPC_UART1->LSR & (LSR_TEMT)) != (LSR_TEMT) );
    /*while ( LPC_UART1->LSR & LSR_RDR )
    {
  	Dummy = LPC_UART1->RBR;	// Dump data from RX FIFO
    }*/

    LPC_UART1->LCR |= ((0x3<<4)|(0x1<<3));	/* forced "0" sticky parity, parity enable */

    //--------------------------------------------------------------//

   	NVIC_EnableIRQ(UART1_IRQn);
   	NVIC_SetPriority(UART1_IRQn,1);
    LPC_UART1->IER = IER_RBR | IER_THRE | IER_RLS;	/* Enable UART1 interrupt */
    return (TRUE);
  }
  else if ( PortNum == 3 )
  {
	LPC_PINCON->PINSEL0 &= ~0x00F00000;
	LPC_PINCON->PINSEL0 |= 0x00500000;	/* Enable RxD2 P0.11, TxD2 P0.10 */

	//inicialitzacio pin enable TX
	// Set P0_6 to 00 - GPIO
	LPC_PINCON->PINSEL0	&= (~0x3000);
	// Set GPIO - P0_6 - to be outputs
	LPC_GPIO0->FIODIR |= 1<<6;
	//Power to UART3 no esta habilitat per defecte
	LPC_SC->PCONP |= (1 << 24);

	/* By default, the PCLKSELx value is zero, thus, the PCLK for
	all the peripherals is 1/4 of the SystemFrequency. */
	/* Bit 8,9 are for UART1 */
	pclkdiv = (LPC_SC->PCLKSEL1 >> 16) & 0x03;
	switch ( pclkdiv )
	{
	  case 0x00:
	  default:
		pclk = SystemCoreClock/4;
		break;
	  case 0x01:
		pclk = SystemCoreClock;
		break;
	  case 0x02:
		pclk = SystemCoreClock/2;
		break;
	  case 0x03:
		pclk = SystemCoreClock/8;
		break;
	}

    LPC_UART2->LCR = 0x83;		/* 8 bits, no Parity, 1 Stop bit */
	Fdiv = ( pclk / 16 ) / baudrate ;	/*baud rate */
    //LPC_UART2->DLM = Fdiv / 256;
    //LPC_UART2->DLL = Fdiv % 256;
	LPC_UART2->DLM = 0;
	LPC_UART2->DLL = 3;
	LPC_UART2->FDR = 0x10;
	LPC_UART2->LCR = 0x03;		/* DLAB = 0 --> enable interrupts*/
    LPC_UART2->FCR = 0x07;		/* Enable and reset TX and RX FIFO. */

    //-Afegit-------------------------------------------------------//

    //RS-485
    /* Read to clear the line status. */
    Dummy = LPC_UART2->LSR;
    /* Ensure a clean start, no data in either TX or RX FIFO. */
    while ( (LPC_UART2->LSR & (LSR_TEMT)) != (LSR_TEMT) );
    /*while ( LPC_UART2->LSR & LSR_RDR )
    {
  	Dummy = LPC_UART2->RBR;	// Dump data from RX FIFO
    }*/

    LPC_UART2->LCR |= ((0x3<<4)|(0x1<<3));	/* forced "0" sticky parity, parity enable */

    //--------------------------------------------------------------//

   	NVIC_EnableIRQ(UART2_IRQn);
   	NVIC_SetPriority(UART2_IRQn,2);
    LPC_UART2->IER = IER_RBR | IER_THRE | IER_RLS;	/* Disable UART2 interrupt */
    return (TRUE);
  }
  return( FALSE ); 
}

/*****************************************************************************
** Function name:		UARTSend
**
** Descriptions:		Send a block of data to the UART 0 port based
**						on the data length
**
** parameters:			portNum, buffer pointer, and data length
** Returned value:		None
** 
*****************************************************************************/
void UARTSend( uint32_t portNum )
{
	uint32_t Dummy=Dummy;
  if ( portNum == 0 )
  {
	watchdog_uart_1_enable = 1;
	watchdog_uart_1=0;
	comptador_leds_uart[0]++;
	if(comptador_leds_uart[0]>100)
	{
		comptador_leds_uart[0]=0;
		leds_invert(LED_1);
	}
	Dummy = LPC_UART0->LSR;
	while ( (LPC_UART0->LSR & (LSR_TEMT)) != (LSR_TEMT) );
	/*while ( LPC_UART0->LSR & LSR_RDR )
	{
		Dummy = LPC_UART0->RBR;
	}*/
	//LPC_UART0->IER = IER_THRE;			/* Disable RBR */ //deshabilites recepció

	usedPORTCanal1=PORT0;

	//LPC_UART3->IER = IER_THRE;			/* Disable RBR */ //deshabilites recepció
	#if INTERFACE_RS485 == 1
	LPC_GPIO0->FIOCLR = 1<<4;     //deshabilites enable TX port1
	LPC_GPIO0->FIOSET = 1<<5;     //habilites enable TX port0
	#endif
	UART0Count = 0;
    LPC_UART0->THR = UART0Transmit_Buffer[UART0Count];
    LPC_UART0->LCR &= ~(0x1<<4);		// Sticky parity to 1
    LPC_UART0->LCR |= (0x1<<3);
    enable_timer(1);
    if(UART0Transmit_Buffer[1]==1)length_UART0=8;
    else if(UART0Transmit_Buffer[1]==2)length_UART0=(UART0Transmit_Buffer[3]&0x7F)+6;
    else if(UART0Transmit_Buffer[1]==3)length_UART0=5;
    else if(UART0Transmit_Buffer[1]==4)length_UART0=4;
    if(StateUART_canal1 != PRIMER_FRAME)
	{
    	if(StateUART_canal1==PROGRAMACIO)StateUART_canal1=ENVIANT_PROGRAMACIO;
    	else StateUART_canal1=ENVIANT;
    	if(wire_correcte==FALSE&&wire_count==0)
    	{//Si hi ha problema al cable i es el polling de comprovacio guardar buffer port 1
    		UART1Transmit_Buffer[UART0Count]=UART0Transmit_Buffer[UART0Count];
    	}
    	//LPC_UART3->IER = 0;//Deshabilites recepcio port0 si dames frame
    	UART0Transmit_Buffer[UART0Count] = 0;//Buidar buffer
    	UART0Count++;
    	UART0TxEmpty = 0;
	}
    if(StateUART_canal1==PRIMER_FRAME)
    {
    	StateUART_canal1 = ESPERANT_RESPOSTA_PRIMER;
		UART0Count=0;
		Dummy = LPC_UART3->LSR;
		while ( (LPC_UART3->LSR & (LSR_TEMT)) != (LSR_TEMT) );
		/*while ( LPC_UART3->LSR & LSR_RDR )
		{
			Dummy = LPC_UART3->RBR;
		}*/
		enable_timer( 1 );
		LPC_UART3->LCR |= ((0x3<<4)|(0x1<<3));
		//LPC_GPIO0->FIOCLR = 1<<4;
		UART0TxEmpty = 0;
		UART0Transmit_Buffer[UART0Count] = 0;//Buidar buffer
    }

  }
  else if(portNum == 1)
  {
	watchdog_uart_2_enable = 1;
	watchdog_uart_2=0;
	comptador_leds_uart[1]++;
	if(comptador_leds_uart[1]>100)
	{
		comptador_leds_uart[1]=0;
		leds_invert(LED_2);
	}
	Dummy = LPC_UART3->LSR;
	while ( (LPC_UART3->LSR & (LSR_TEMT)) != (LSR_TEMT) );
	/*while ( LPC_UART3->LSR & LSR_RDR )
	{
		Dummy = LPC_UART3->RBR;
	}*/
	//LPC_UART0->IER = IER_THRE;		 //deshabilites recepció
	//LPC_UART3->IER = IER_THRE;		 //deshabilites recepció
	usedPORTCanal1=PORT1;
	//LPC_UART3->IER = IER_THRE;//Habilites recepcio port1 si primer frame
	//LPC_UART0->IER = IER_THRE;//Deshabilites recepcio port0 si dames frame
	#if INTERFACE_RS485 == 1
	LPC_GPIO0->FIOCLR = 1<<5;   //deshabilites enable TX port0
	LPC_GPIO0->FIOSET = 1<<4;   //habilites enable TX port1
	#endif
	UART1Count = 0;
	LPC_UART3->THR = UART1Transmit_Buffer[UART1Count];
	LPC_UART3->LCR &= ~(0x1<<4);
	LPC_UART3->LCR |= (0x1<<3);
	enable_timer(1);
    if(UART1Transmit_Buffer[1]==1)length_UART1=8;
    else if(UART1Transmit_Buffer[1]==2)length_UART1=(UART1Transmit_Buffer[3]&0x7F)+6;
    else if(UART1Transmit_Buffer[1]==3)length_UART1=5;
    else if(UART1Transmit_Buffer[1]==4)length_UART1=4;
	UART1TxEmpty = 0;
	UART1Transmit_Buffer[UART1Count] = 0;//Buidar buffer
	UART1Count++;
	StateUART_canal1=ENVIANT_2;
  }
  else if ( portNum == 2 )
  {
	watchdog_uart_3_enable = 1;
	watchdog_uart_3=0;
	comptador_leds_uart[2]++;
	if(comptador_leds_uart[2]>100)
	{
		comptador_leds_uart[2]=0;
		leds_invert(LED_3);
	}
	Dummy = LPC_UART1->LSR;
	while ( (LPC_UART1->LSR & (LSR_TEMT)) != (LSR_TEMT) );
	/*while ( LPC_UART1->LSR & LSR_RDR )
	{
		Dummy = LPC_UART1->RBR;
	}*/
	//LPC_UART1->IER = IER_THRE;			/* Disable RBR */ //deshabilites recepció
	//LPC_UART2->IER = IER_THRE;			/* Disable RBR */ //deshabilites recepció
	#if INTERFACE_RS485 == 1
	LPC_GPIO0->FIOCLR = 1<<6;     //deshabilites enable TX port1
	LPC_GPIO0->FIOSET = 1<<7;     //habilites enable TX port0
	#endif
	usedPORTCanal2=PORT2;
	UART2Count = 0;
    LPC_UART1->THR = UART2Transmit_Buffer[UART2Count];
    LPC_UART1->LCR &= ~(0x1<<4);		// Sticky parity to 1
    LPC_UART1->LCR |= (0x1<<3);
    enable_timer(2);
    if(UART2Transmit_Buffer[1]==1)length_UART2=8;
    else if(UART2Transmit_Buffer[1]==2)length_UART2=(UART2Transmit_Buffer[3]&0x7F)+6;
    else if(UART2Transmit_Buffer[1]==3)length_UART2=5;
    else if(UART2Transmit_Buffer[1]==4)length_UART2=4;
    if(StateUART_canal2 != PRIMER_FRAME)
	{
    	if(StateUART_canal2==PROGRAMACIO)StateUART_canal2=ENVIANT_PROGRAMACIO;
    	else StateUART_canal2=ENVIANT;
    	if(wire_correcte_2==FALSE&&wire_count_2==0)
    	{//Si hi ha problema al cable i es el polling de comprovacio guardar buffer port 3
    		UART3Transmit_Buffer[UART2Count]=UART2Transmit_Buffer[UART2Count];
    	}
    	//LPC_UART2->IER = 0;//Deshabilites recepcio port1 si primer frame
    	UART2Transmit_Buffer[UART2Count] = 0;//Buidar buffer
    	UART2Count++;
    	UART2TxEmpty = 0;
	}
    if(StateUART_canal2==PRIMER_FRAME)
    {
    	StateUART_canal2 = ESPERANT_RESPOSTA_PRIMER;
		UART2Count=0;
		Dummy = LPC_UART2->LSR;
		while ( (LPC_UART2->LSR & (LSR_TEMT)) != (LSR_TEMT) );
		/*while ( LPC_UART2->LSR & LSR_RDR )
		{
			Dummy = LPC_UART2->RBR;
		}*/
		enable_timer( 2 );
		LPC_UART2->LCR |= ((0x3<<4)|(0x1<<3));
		//LPC_GPIO0->FIOCLR = 1<<6;
		//LPC_UART2->IER = IER_THRE | IER_RLS | IER_RBR;//Habilites recepcio port1 si primer frame
		UART2TxEmpty = 0;
		UART2Transmit_Buffer[UART2Count] = 0;//Buidar buffer
    }
  }
  else if(portNum == 3)
  {
	watchdog_uart_4_enable = 1;
	watchdog_uart_4=0;
	comptador_leds_uart[3]++;
	if(comptador_leds_uart[3]>100)
	{
		comptador_leds_uart[3]=0;
		leds_invert(LED_4);
	}
	Dummy = LPC_UART2->LSR;
	while ( (LPC_UART2->LSR & (LSR_TEMT)) != (LSR_TEMT) );
	/*while ( LPC_UART2->LSR & LSR_RDR )
	{
		Dummy = LPC_UART2->RBR;
	}*/
	//LPC_UART1->IER = IER_THRE;		 //deshabilites recepció
	//LPC_UART2->IER = IER_THRE;		 //deshabilites recepció
	#if INTERFACE_RS485 == 1
	LPC_GPIO0->FIOCLR = 1<<7;   //deshabilites enable TX port0
	LPC_GPIO0->FIOSET = 1<<6;   //habilites enable TX port1
	#endif
	usedPORTCanal2=PORT3;
	UART3Count = 0;
	LPC_UART2->THR = UART3Transmit_Buffer[UART3Count];
	LPC_UART2->LCR &= ~(0x1<<4);
	LPC_UART2->LCR |= (0x1<<3);
	enable_timer(2);
    if(UART3Transmit_Buffer[1]==1)length_UART3=8;
    else if(UART3Transmit_Buffer[1]==2)length_UART3=(UART3Transmit_Buffer[3]&0x7F)+6;
    else if(UART3Transmit_Buffer[1]==3)length_UART3=5;
    else if(UART3Transmit_Buffer[1]==4)length_UART3=4;
	UART3TxEmpty = 0;
	UART3Transmit_Buffer[UART3Count] = 0;//Buidar buffer
	UART3Count++;
	StateUART_canal2=ENVIANT_2;
  }
  return;
}

/*****************************************************************************
** Function name:		CRC
**
** Descriptions:		genera el CRC del buffer
**
** parameters:			cadena pointer, inici i final(limitadors del CRC)
** Returned value:		CRC calculat
**
*****************************************************************************/
uint8_t CRC (uint8_t *cadena, uint8_t inici, uint8_t final)
{
	uint16_t i;
	uint8_t CRC=0;
	for(i=inici;i<=final;i++)
	{
		CRC=CRC^*(cadena+i);
	}
	return(CRC);
}

/*****************************************************************************
** Function name:		process_UART_canal1
**
** Descriptions:		procés principal que controla la màquina d'estats del canal 1
**
** parameters:			estat de la UART del canal1
** Returned value:		None
**
*****************************************************************************/
void process_UART_canal1 (void)
{
	//uint32_t Dummy=Dummy;
	uint8_t i;
	if(StateUART_canal1==FINAL && StateUART_canal2==FINAL)
	{
		ini_nodes();//Inicia taula de nodes
		//ini_taula_configurats();
		if(peticio_programacio  && StateUART_canal1!=PROGRAMACIO)//Si s'ha de començar una trama de programacio
		{
			StateUART_canal1=PRIMER_PROGRAMACIO;
			return;
		}
		if(Broadcast_polling)
		{//Si s'ha fet el polling de broadcast es baixa el flag i es buida buffer broadcast.
			Broadcast_polling=0;
			for(i=0;i<MAX_DADES_NODE;i++)
			{
				UARTTransmit_Broadcast[i]=0;
			}
		}
		if(UARTTransmit_Broadcast[0]!=0)
		{//Si hi ha dades al buffer de broadcast es fa el polling de broadcast
			Broadcast_polling = 1;
		}
		posicio_configurats = 0;
		UART0Transmit_Buffer[0]=0xFE;//trama comprovació cable
		StateUART_canal1 = PRIMER_FRAME;
		/*UART0Transmit_Buffer[0]=0;
		enable_timer( 1 );
    	StateUART_canal1 = ESPERANT_RESPOSTA_PRIMER;
		UART0Count=0;*/
		UARTSend(0);
	}
	else if(StateUART_canal1 == PROXIM_NODE && (StateUART_canal2 == PROXIM_NODE||StateUART_canal2 == FINAL||(despres_heartbeat&2) == 2))
	{
		if(posicio_configurats == 0)//Primer node a inspeccionar
		{
			if(wire_correcte == FALSE && wire_count > 0 && wire_count < 5)
			{
				wire_count++;
			}
			else if(wire_correcte == FALSE && wire_count >= 5)
			{
				wire_count = 0;
			}
			else if(wire_correcte == TRUE)
			{
				led_off(LED_2);
			}
		}
		if(taula_configurats[posicio_configurats]==0 || posicio_configurats==MAX_NODES_CANAL)
		{//Tots els nodes configurats inspeccionats
			canviar_taula_nodes_web();
			posicio_configurats=0;
			StateUART_canal1 = FINAL;
			if(wire_correcte==FALSE&&wire_count==0)wire_count++;
		}
		else
		{//Següent node a inspeccionar
			//guardar_RAM_broadcast(node_actual,1,UARTTransmit_Broadcast);
			node_actual=taula_configurats[posicio_configurats];
			posicio_configurats++;
			despres_heartbeat |= 1;
			heartbeat(node_actual,1);
		}
	}
	else if(StateUART_canal1 == ESPERANT_RESPOSTA && timer1_counter>TIMEOUT_NODES)
	{
		if (counter_pollings_fallidos<MAX_ERRORS) counter_pollings_fallidos++;
		else counter_pollings_fallidos=0;

		StateUART_canal1 = PROXIM_NODE;
		reset_timer(1);
		disable_timer(1);
		if((control_alarmes[node_actual-1])<10)
		{
			control_alarmes[node_actual-1]++;
			if(control_alarmes[node_actual-1]>=10)
			{
				if((alarmes_activades[node_actual-1]&(1<<(DESCONEXIO_NODE-1)))==0)
				{
					alarm(DESCONEXIO_NODE,node_actual,LPC_RTC->HOUR,LPC_RTC->MIN,LPC_RTC->SEC,1);
				}
				Error_aplicacio(DESCONEXIO_NODE,node_actual,1);
				alarmes_activades[node_actual-1] |= 1<<(DESCONEXIO_NODE-1);
				//control_alarmes[node_actual-1]&=~0x3F;
			}
		}
		/*else
		{
			control_alarmes[node_actual-1]=(control_alarmes[node_actual-1]&0xFFC0)|((control_alarmes[node_actual-1]&0x3F)+1);
		}*/
		if(malloc_buffer[node_actual-1])
		{
			malloc_buffer[node_actual-1]++;
			if(malloc_buffer[node_actual-1]>REENVIAMENTS)
			{
				//free(send_buffer[node_actual-1]);
				if(counter_envios_fallidos<MAX_ERRORS) counter_envios_fallidos++;
				else counter_envios_fallidos;
				Error_aplicacio(ENVIAR_TRAMA,node_actual,1);
				//send_buffer[node_actual-1]=NULL;
				uint8_t n; for(n=0;n<MAX_DADES_NODE;n++) send_buffer[node_actual-1][n]=0;
				malloc_buffer[node_actual-1]=0;
				//nombre_mallocs--;
			}
		}
		/*******************************
		 *
		 * POLLING BROADCAST FALLIDO
		 *
		 * *****************************/
		if(Broadcast_polling)
		{
			uint8_t j,k = 0;
			if(reenviaments_broadcast_1>REENVIAMENTS)
			{
				StateUART_canal1 = PROXIM_NODE;
				UART2Count = 0;
				enviament_broadcast = enviament_broadcast_1_anterior = reenviaments_broadcast_1 = 0;
				return;
			}
			if((taula_nodes[node_actual-1]&0b1)==0)
			{
				if(enviament_broadcast)
					enviament_broadcast=enviament_broadcast_1_anterior;//Si es diferent de zero es decrementa per reenviar
				if(enviament_broadcast)
				{//Si no es el primer enviament es posiciona el punter "i" del buffer
					i=0;
					for(j=0;j<enviament_broadcast;j++)
					{
						//while(UARTTransmit_Broadcast[i]!=0xFF&&i<MAX_DADES_NODE)i++;
						k = i;
						while(i<UARTTransmit_Broadcast[k]+k)i++;
						i++;
					}
					i++;
					i++;
				}
				else
				{
					//i=1;
					i = 2;
				}
				j=1;
				k = i-2;
				UART0Transmit_Buffer[0]=node_actual;
				//while(UARTTransmit_Broadcast[i]!=0xFF&&i<MAX_DADES_NODE)
				while((i<=UARTTransmit_Broadcast[k]+k)&&i<MAX_DADES_NODE)
				{//Agafes dades del buffer fins 0xFF
					UART0Transmit_Buffer[j]=UARTTransmit_Broadcast[i];
					i++;
					j++;
				}
				if(i>=MAX_DADES_NODE)
				{
					StateUART_canal1 = PROXIM_NODE;
					UART0Count = 0;
					enviament_broadcast = enviament_broadcast_1_anterior = reenviaments_broadcast_1 = 0;
					return;
				}
				/*if(UARTTransmit_Broadcast[i+1]==0xFF)
				{//Per si CRC es 0xFF i no s'ha agafat final de caracter
					UART0Transmit_Buffer[j]=UARTTransmit_Broadcast[i];
					i++;
					j++;
				}*/
				UART0Transmit_Buffer[j-2]=CRC((uint8_t *)UART0Transmit_Buffer,0,j-3);
				//UART0Transmit_Buffer[j]=UARTTransmit_Broadcast[i-1];//ultim byte 0xFF
				enviament_broadcast_1_anterior = enviament_broadcast;
				enviament_broadcast++;
				if(UARTTransmit_Broadcast[i+1]==0)enviament_broadcast=10;//S'acaben els envios
				UARTSend(0);
				reenviaments_broadcast_1++;
			}
			else if((taula_nodes[node_actual-1]&0b1)==1)
			{
				if(enviament_broadcast)
					enviament_broadcast=enviament_broadcast_1_anterior;//Si es diferent de zero es decrementa per reenviar
				if(enviament_broadcast)
				{//Si no es el primer enviament es posiciona el punter "i" del buffer
					i=0;
					for(j=0;j<enviament_broadcast;j++)
					{
						//while(UARTTransmit_Broadcast[i]!=0xFF&&i<MAX_DADES_NODE)i++;
						k = i;
						while(i<UARTTransmit_Broadcast[k]+k)i++;
						i++;
					}
					i++;
					i++;
				}
				else
				{
					//i=1;
					i = 2;
				}
				j=1;
				k = i-2;
				UART1Transmit_Buffer[0]=node_actual;
				//while(UARTTransmit_Broadcast[i]!=0xFF&&i<MAX_DADES_NODE)
				while((i<=UARTTransmit_Broadcast[k]+k)&&i<MAX_DADES_NODE)
				{//Agafes dades del buffer fins 0xFF
					UART1Transmit_Buffer[j]=UARTTransmit_Broadcast[i];
					i++;
					j++;
				}
				if(i>=MAX_DADES_NODE)
				{
					StateUART_canal1 = PROXIM_NODE;
					UART0Count = 0;
					enviament_broadcast = enviament_broadcast_1_anterior = reenviaments_broadcast_1 = 0;
					return;
				}
				/*if(UARTTransmit_Broadcast[i+1]==0xFF)
				{//Per si CRC es 0xFF i no s'ha agafat final de caracter
					UART0Transmit_Buffer[j]=UARTTransmit_Broadcast[i];
					i++;
					j++;
				}*/
				UART1Transmit_Buffer[j-2]=CRC((uint8_t *)UART1Transmit_Buffer,0,j-3);
				//UART0Transmit_Buffer[j]=UARTTransmit_Broadcast[i-1];//ultim byte 0xFF
				enviament_broadcast_1_anterior = enviament_broadcast;
				enviament_broadcast++;
				if(UARTTransmit_Broadcast[i+1]==0)enviament_broadcast=10;//S'acaben els envios
				UARTSend(1);
				reenviaments_broadcast_1++;
			}
		}
	}
	else if(StateUART_canal1 == ESPERANT_RESPOSTA_PRIMER && timer1_counter>TIMEOUT_NODES)
	{//problemes al cable
		wire_correcte =FALSE;
		if(taula_configurats[0]!=0) { //Només si hi ha nodes configurats, si hi ha zero no es considera
			if((alarmes_activades[0]&(1<<(RUPTURA_ANELL-1)))==0)
			{
				alarm(RUPTURA_ANELL,0,LPC_RTC->HOUR,LPC_RTC->MIN,LPC_RTC->SEC,1);
			}
			Error_aplicacio(RUPTURA_ANELL,1,1);
			alarmes_activades[0] |= 1<<(RUPTURA_ANELL-1);
		}
		StateUART_canal1 = PROXIM_NODE;
		reset_timer(1);
		disable_timer(1);
	}
	else if(StateUART_canal1 == ESPERANT_RESPOSTA_CABLE && timer1_counter>TIMEOUT_NODES)
	{//quan no reps resposta pel port0 i has de fer polling de comprovacio
		//counter_pollings_fallidos++;
		reset_timer(1);
		disable_timer(1);
		UARTSend(1);

	}
	else if(StateUART_canal1==PRIMER_PROGRAMACIO)
	{//esperes que s'hagi acabat el polling i comences a programar
		StateUART_canal1 = PROGRAMACIO;
		if(peticio_programacio == 1)
		{
			programacio(1,old_adress_programacio1_canal1,new_adress_programacio1_canal1,GUARDAR_NODE_ID,0,0,UART0);
		}
		else if(peticio_programacio == 2)
		{
			programacio(2,0,new_adress_programacio2_canal1,GUARDAR_NODE_ID,0,0,UART0);
		}
		else if(peticio_programacio == BROADCAST_ADRESS)
		{
			programacio(1,BROADCAST_ADRESS,0,GUARDAR_NODE_ID,0,0,UART0);
		}

	}
}

/*****************************************************************************
** Function name:		process_UART_canal2
**
** Descriptions:		procés principal que controla la màquina d'estats del canal 1
**
** parameters:			estat de la UART del canal1
** Returned value:		None
**
*****************************************************************************/
void process_UART_canal2 (void)
{
	//uint32_t Dummy=Dummy;
	uint8_t i;
	if(StateUART_canal2==FINAL && StateUART_canal1==ESPERANT_RESPOSTA_PRIMER)
	{
		ini_nodes_2();//Inicia taula de nodes
		if(peticio_programacio_2 && StateUART_canal2!=PROGRAMACIO)//Si s'ha de començar una trama de programacio
		{
			StateUART_canal2=PRIMER_PROGRAMACIO;
			return;
		}
		if(Broadcast_polling_2)
		{//Si s'ha fet el polling de broadcast es baixa el flag i es buida buffer broadcast.
			Broadcast_polling_2=0;
			for(i=0;i<MAX_DADES_NODE;i++)
			{
				UARTTransmit_Broadcast_2[i]=0;
			}
		}
		if(UARTTransmit_Broadcast_2[0]!=0)
		{//Si hi ha dades al buffer de broadcast es fa el polling de broadcast
			Broadcast_polling_2 = 1;
		}
		posicio_configurats_2 = 0;
		UART2Transmit_Buffer[0]=0xFE;//trama comprovació cable
		StateUART_canal2 = PRIMER_FRAME;
		UARTSend(2);
	}
	else if((StateUART_canal1 == PROXIM_NODE||(despres_heartbeat&1) == 1||StateUART_canal1 == FINAL) && StateUART_canal2 == PROXIM_NODE)
	{
		if(posicio_configurats_2 == 0)//Primer node a inspeccionar
		{
			if(wire_correcte_2 == FALSE && wire_count_2 > 0 && wire_count_2 < 5)
			{
				wire_count_2++;
			}
			else if(wire_correcte_2 == FALSE && wire_count_2 >= 5)
			{
				wire_count_2 = 0;
			}
			else if(wire_correcte_2 == TRUE)
			{
				led_off(LED_4);
			}
		}
		if(taula_configurats_2[posicio_configurats_2]==0 || posicio_configurats_2==MAX_NODES_CANAL)
		{//Tots els nodes configurats inspeccionats
			canviar_taula_nodes_web_2();
			posicio_configurats_2=0;
			StateUART_canal2 = FINAL;
			if(wire_correcte_2==FALSE&&wire_count_2==0)wire_count_2++;
		}
		else
		{//Següent node a inspeccionar
			//guardar_RAM_broadcast(node_actual_2,2,UARTTransmit_Broadcast_2);
			node_actual_2=taula_configurats_2[posicio_configurats_2];
			posicio_configurats_2++;
			despres_heartbeat |= 2;
			heartbeat(node_actual_2,2);//CANVIAR
		}
	}
	else if(StateUART_canal2 == ESPERANT_RESPOSTA && timer2_counter>TIMEOUT_NODES)
	{
		if (counter_pollings_fallidos<MAX_ERRORS) counter_pollings_fallidos++;
		else counter_pollings_fallidos=0;

		if(!Broadcast_polling_2)StateUART_canal2 = PROXIM_NODE;
		reset_timer(2);
		disable_timer(2);
		if(control_alarmes_2[node_actual_2-1]<10)
		{
			control_alarmes_2[node_actual_2-1]++;
			if(control_alarmes_2[node_actual_2-1]>=10)
			{
				if((alarmes_activades_2[node_actual_2-1]&(1<<(DESCONEXIO_NODE-1)))==0)
				{
					alarm(DESCONEXIO_NODE,node_actual_2,LPC_RTC->HOUR,LPC_RTC->MIN,LPC_RTC->SEC,2);
				}
				Error_aplicacio(DESCONEXIO_NODE,node_actual_2,2);
				alarmes_activades_2[node_actual_2-1] |= 1<<(DESCONEXIO_NODE-1);
				//control_alarmes_2[node_actual_2-1]&=~0x3F;
			}
		}
		if(malloc_buffer_2[node_actual_2-1])
		{
			malloc_buffer_2[node_actual_2-1]++;
			if(malloc_buffer_2[node_actual_2-1]>REENVIAMENTS)
			{
				//free(send_buffer_2[node_actual_2-1]);
				if(counter_envios_fallidos<MAX_ERRORS) counter_envios_fallidos++;
				else counter_envios_fallidos;
				Error_aplicacio(ENVIAR_TRAMA,node_actual_2,2);
				//send_buffer_2[node_actual_2-1]=NULL;
				uint8_t n; for(n=0;n<MAX_DADES_NODE;n++) send_buffer_2[node_actual_2-1][n]=0;
				malloc_buffer_2[node_actual_2-1]=0;
				//nombre_mallocs_2--;
			}
		}
		/*******************************
		 *
		 * POLLING BROADCAST FALLIDO
		 *
		 * *****************************/
		if(Broadcast_polling_2)
		{//Si es el polling de broadcast
			uint8_t j,k = 0;
			if(reenviaments_broadcast_2>REENVIAMENTS)
			{
				StateUART_canal2 = PROXIM_NODE;
				UART2Count = 0;
				enviament_broadcast_2 = enviament_broadcast_2_anterior = reenviaments_broadcast_2 = 0;
				return;
			}
			if((taula_nodes_2[node_actual_2-1]&0b1)==0)
			{//Si et contesta pel PORT A del canal 2
				if(enviament_broadcast_2)
					enviament_broadcast_2=enviament_broadcast_2_anterior;//Si es diferent de zero es decrementa per reenviar
				if(enviament_broadcast_2)
				{//Si no es el primer enviament es posiciona l'índex "i" del buffer
					i=0;
					for(j=0;j<enviament_broadcast_2;j++)
					{
						//while(UARTTransmit_Broadcast_2[i]!=0xFF&&i<MAX_DADES_NODE&&UARTTransmit_Broadcast_2[i+1]!=BROADCAST_ADRESS)i++;
						k = i;
						while(i<UARTTransmit_Broadcast_2[k]+k)i++;
						i++;
					}
					i++;
					i++;
				}
				else
				{
					//i=1;
					i = 2;
				}
				j=1;
				k = i-2;
				UART2Transmit_Buffer[0]=node_actual_2;
				//while(UARTTransmit_Broadcast_2[i]!=0xFF&&i<MAX_DADES_NODE)
				while((i<=UARTTransmit_Broadcast_2[k]+k)&&i<MAX_DADES_NODE)
				{//Agafes dades del buffer fins 0xFF
					UART2Transmit_Buffer[j]=UARTTransmit_Broadcast_2[i];
					i++;
					j++;
				}
				if(i>=MAX_DADES_NODE)
				{//Per si es fa overflow
					StateUART_canal2 = PROXIM_NODE;
					UART2Count = 0;
					enviament_broadcast_2 = enviament_broadcast_2_anterior=reenviaments_broadcast_2 = 0;
					return;
				}
				/*if(UARTTransmit_Broadcast_2[i+1]==0xFF)
				{//Per si CRC es 0xFF i no s'ha agafat final de caracter
					UART2Transmit_Buffer[j]=UARTTransmit_Broadcast_2[i];
					i++;
					j++;
				}*/
				UART2Transmit_Buffer[j-2]=CRC((uint8_t *)UART2Transmit_Buffer,0,j-3);
				//UART2Transmit_Buffer[j]=UARTTransmit_Broadcast_2[i];//ultim byte 0xFF
				enviament_broadcast_2_anterior = enviament_broadcast_2;
				enviament_broadcast_2++;
				if(UARTTransmit_Broadcast_2[i+1]==0)enviament_broadcast_2=10;//S'acaben els envios
				UARTSend(2);
				reenviaments_broadcast_2++;
			}
			else if((taula_nodes_2[node_actual_2-1]&0b1)==1)
			{
				if(enviament_broadcast_2)
					enviament_broadcast_2=enviament_broadcast_2_anterior;//Si es diferent de zero es decrementa per reenviar
				if(enviament_broadcast_2)
				{//Si no es el primer enviament es posiciona l'índex "i" del buffer
					i=0;
					for(j=0;j<enviament_broadcast_2;j++)
					{
						//while(UARTTransmit_Broadcast_2[i]!=0xFF&&i<MAX_DADES_NODE&&UARTTransmit_Broadcast_2[i+1]!=BROADCAST_ADRESS)i++;
						k = i;
						while(i<UARTTransmit_Broadcast_2[k]+k)i++;
						i++;
					}
					i++;
					i++;
				}
				else
				{
					//i=1;
					i = 2;
				}
				j=1;
				k = i-2;
				UART3Transmit_Buffer[0]=node_actual_2;
				//while(UARTTransmit_Broadcast_2[i]!=0xFF&&i<MAX_DADES_NODE)
				while((i<=UARTTransmit_Broadcast_2[k]+k)&&i<MAX_DADES_NODE)
				{//Agafes dades del buffer fins 0xFF
					UART3Transmit_Buffer[j]=UARTTransmit_Broadcast_2[i];
					i++;
					j++;
				}
				if(i>=MAX_DADES_NODE)
				{//Per si es fa overflow
					StateUART_canal2 = PROXIM_NODE;
					UART2Count = 0;
					enviament_broadcast_2 = enviament_broadcast_2_anterior=reenviaments_broadcast_2 = 0;
					return;
				}
				/*if(UARTTransmit_Broadcast_2[i+1]==0xFF)
				{//Per si CRC es 0xFF i no s'ha agafat final de caracter
					UART2Transmit_Buffer[j]=UARTTransmit_Broadcast_2[i];
					i++;
					j++;
				}*/
				UART3Transmit_Buffer[j-2]=CRC((uint8_t *)UART3Transmit_Buffer,0,j-3);
				//UART2Transmit_Buffer[j]=UARTTransmit_Broadcast_2[i];//ultim byte 0xFF
				enviament_broadcast_2_anterior = enviament_broadcast_2;
				enviament_broadcast_2++;
				if(UARTTransmit_Broadcast_2[i+1]==0)enviament_broadcast_2=10;//S'acaben els envios
				UARTSend(3);
				reenviaments_broadcast_2++;
			}
		}
	}
	else if(StateUART_canal2 == ESPERANT_RESPOSTA_PRIMER && timer2_counter>TIMEOUT_NODES)
	{//problemes al cable
		wire_correcte_2 =FALSE;
		if(taula_configurats_2[0]!=0) { //Només si hi ha nodes configurats, si hi ha zero no es considera
			if((alarmes_activades_2[0]&(1<<(RUPTURA_ANELL-1)))==0)
			{
				alarm(RUPTURA_ANELL,0,LPC_RTC->HOUR,LPC_RTC->MIN,LPC_RTC->SEC,2);
			}
			Error_aplicacio(RUPTURA_ANELL,1,2);
			alarmes_activades_2[0] |= 1<<(RUPTURA_ANELL-1);
		}
		StateUART_canal2 = PROXIM_NODE;
		reset_timer(2);
		disable_timer(2);
	}
	else if(StateUART_canal2 == ESPERANT_RESPOSTA_CABLE && timer2_counter>TIMEOUT_NODES)
	{//quan no reps resposta pel port0 i has de fer polling de comprovacio
		//counter_pollings_fallidos++;
		reset_timer(2);
		disable_timer(2);
		UARTSend(3);
		prova_2++;
		if(prova_2>100)
		{
			prova_2=0;
		}
	}
	else if(StateUART_canal2==PRIMER_PROGRAMACIO)
	{//esperes que s'hagi acabat el polling i comences a programar
		StateUART_canal2 = PROGRAMACIO;
		if(peticio_programacio_2 == 1)
		{
			programacio(1,old_adress_programacio1_canal2,new_adress_programacio1_canal2,GUARDAR_NODE_ID,0,0,UART2);
		}
		else if(peticio_programacio_2 == 2)
		{
			programacio(2,0,new_adress_programacio2_canal2,GUARDAR_NODE_ID,0,0,UART2);
		}
		else if(peticio_programacio_2 == BROADCAST_ADRESS)
		{
			programacio(1,BROADCAST_ADRESS,0,GUARDAR_NODE_ID,0,0,UART2);
		}
	}
}
/*****************************************************************************
** Function name:		UART0_THRE
**
** Descriptions:		procés d'escriptura d'un byte quan buffer lliure
**
** parameters:			None
** Returned value:		None
**
*****************************************************************************/
void UART0_THRE(void) //CH1.A
{
	uint8_t Dummy = Dummy;
	UART0TxEmpty = 1;

	if(StateUART_canal1 == ENVIANT || StateUART_canal1==ENVIANT_PROGRAMACIO)
	{
		watchdog_uart_1_enable = 1;
		watchdog_uart_1=0;
		reset_timer(1);
		disable_timer(1);
		//for(Dummy=0;Dummy<250;Dummy++);
		Dummy = 0;
		if(wire_correcte==FALSE&&wire_count==0)
		{//Si hi ha problema al cable i es el polling de comprovacio guardar buffer port 1
			UART1Transmit_Buffer[UART0Count]=UART0Transmit_Buffer[UART0Count];
		}
		UART0TxEmpty = 0;
		#if INTERFACE_RS485 == 1
		LPC_GPIO0->FIOSET = 1<<5;     //habilites enable TX port0
		#endif
		LPC_UART0->THR = UART0Transmit_Buffer[UART0Count];//Enviar següent byte
		//if(UART0Transmit_Buffer[UART0Count]==0xFF)//ultim byte
		if(UART0Count==length_UART0-1)
		{
			watchdog_uart_1_enable = 0;
			watchdog_uart_1=0;
			LPC_UART0->LCR &= ~(0x1<<4);//Paritat a 1
			LPC_UART0->LCR |= (0x1<<3);
			UART0Transmit_Buffer[UART0Count] = 0;//Buidar buffer
			UART0Count=0;
			if(StateUART_canal1!=ENVIANT_PROGRAMACIO)
			{
				despres_heartbeat &= ~1;
				if(wire_correcte==FALSE && wire_count==0)
				{//escoltes pel port0 si no reps resposta reenvies pel port1
					StateUART_canal1 = ESPERANT_RESPOSTA_CABLE;
				}
				else
				{//escoltes pel port0
					StateUART_canal1 = ESPERANT_RESPOSTA;
				}
				enable_timer( 1 );
			}
			else
			{
				StateUART_canal1=PROGRAMACIO;
			}
			Dummy = LPC_UART0->LSR;
			while ( (LPC_UART0->LSR & (LSR_TEMT)) != (LSR_TEMT) );
			/*while ( LPC_UART0->LSR & (LSR_RDR))
			{
			Dummy = LPC_UART0->RBR;
			}*/
			#if INTERFACE_RS485 == 1
			LPC_GPIO0->FIOCLR = 1<<5; //deshabilites enable TX port0
			LPC_GPIO0->FIOCLR = 1<<4; //deshabilites enable TX port1
			#endif
			LPC_UART0->LCR |= ((0x3<<4)|(0x1<<3));
			//LPC_UART0->IER = IER_RBR | IER_THRE | IER_RLS;	/* Enable UART0 interrupt */
		}
		else
		{
			LPC_UART0->LCR |= (0x1<<4);		    /* Sticky parity to 0 */
			LPC_UART0->LCR |= (0x1<<3);
			UART0Transmit_Buffer[UART0Count] = 0;//Buidar buffer
			UART0Count++;
			if ( UART0Count == BUFSIZE )
			{
			  UART0Count = 0;		/* buffer overflow */
			}
		}
	}
	else
	{
		watchdog_uart_1_enable = 0;
		watchdog_uart_1=0;
	}
}

/*****************************************************************************
** Function name:		UART1_THRE
**
** Descriptions:		procés d'escriptura d'un byte quan buffer lliure
**
** parameters:			None
** Returned value:		None
**
*****************************************************************************/
void UART1_THRE(void) //CH2.A
{
	uint8_t Dummy = Dummy;
	UART2TxEmpty = 1;
	if(StateUART_canal2 == ENVIANT || StateUART_canal2==ENVIANT_PROGRAMACIO)
	{
		watchdog_uart_3_enable = 1;
		watchdog_uart_3=0;
		reset_timer(2);
		disable_timer(2);
		Dummy = 0;
		if(wire_correcte_2==FALSE&&wire_count_2==0)
		{//Si hi ha problema al cable i es el polling de comprovacio guardar buffer port 1
			UART3Transmit_Buffer[UART2Count]=UART2Transmit_Buffer[UART2Count];
		}
		UART2TxEmpty = 0;
		#if INTERFACE_RS485 == 1
		LPC_GPIO0->FIOSET = 1<<7;     //habilites enable TX port0
		#endif
		LPC_UART1->THR = UART2Transmit_Buffer[UART2Count];//Enviar següent byte
		//if(UART2Transmit_Buffer[UART2Count]==0xFF)//ultim byte
		if(UART2Count==length_UART2-1)
		{
			watchdog_uart_3_enable = 0;
			watchdog_uart_3=0;
			LPC_UART1->LCR &= ~(0x1<<4);//Paritat a 1
			LPC_UART1->LCR |= (0x1<<3);
			UART2Transmit_Buffer[UART2Count] = 0;//Buidar buffer
			UART2Count=0;
			if(StateUART_canal2!=ENVIANT_PROGRAMACIO)
			{
				despres_heartbeat &= ~2;
				if(wire_correcte_2==FALSE && wire_count_2==0)
				{//escoltes pel port0 si no reps resposta reenvies pel port1
					StateUART_canal2 = ESPERANT_RESPOSTA_CABLE;
				}
				else
				{//escoltes pel port0
					StateUART_canal2 = ESPERANT_RESPOSTA;
				}
				enable_timer( 2 );
			}
			else
			{
				StateUART_canal2=PROGRAMACIO;
			}
			Dummy = LPC_UART1->LSR;
			while ( (LPC_UART1->LSR & (LSR_TEMT)) != (LSR_TEMT) );
			/*while ( LPC_UART1->LSR & LSR_RDR )
			{
				Dummy = LPC_UART1->RBR;
			}*/
			#if INTERFACE_RS485 == 1
			LPC_GPIO0->FIOCLR = 1<<7; //deshabilites enable TX port0
			LPC_GPIO0->FIOCLR = 1<<6; //deshabilites enable TX port1
			#endif
			LPC_UART1->LCR |= ((0x3<<4)|(0x1<<3));
			LPC_UART1->IER = IER_THRE | IER_RLS | IER_RBR;/*habilites recepció*/
		}
		else
		{
			LPC_UART1->LCR |= (0x1<<4);		    /* Sticky parity to 0 */
			LPC_UART1->LCR |= (0x1<<3);
			UART2Transmit_Buffer[UART2Count] = 0;//Buidar buffer
			UART2Count++;
			if ( UART2Count == BUFSIZE )
			{
			  UART2Count = 0;		/* buffer overflow */
			}
		}
	}
	else{
		watchdog_uart_3_enable = 0;
		watchdog_uart_3=0;
	}
}

/*****************************************************************************
** Function name:		UART2_THRE
**
** Descriptions:		procés d'escriptura d'un byte quan buffer lliure
**
** parameters:			None
** Returned value:		None
**
*****************************************************************************/
void UART2_THRE(void) //CH2.B
{
	uint8_t Dummy = Dummy;
	UART3TxEmpty = 1;
	if(StateUART_canal2 == ENVIANT_2)
	{
		watchdog_uart_4_enable = 1;
		watchdog_uart_4=0;
		reset_timer(2);
		disable_timer(2);
		LPC_UART2->THR = UART3Transmit_Buffer[UART3Count];//Enviar següent byte
		UART3TxEmpty = 0;
		//if(UART3Transmit_Buffer[UART3Count]==0xFF)//ultim byte
		if(UART3Count==length_UART3-1)
		{
			watchdog_uart_4_enable = 0;
			watchdog_uart_4=0;
			despres_heartbeat &= ~2;
			LPC_UART2->LCR &= ~(0x1<<4);		/* Sticky parity to 1 */
			LPC_UART2->LCR |= (0x1<<3);
			UART3Transmit_Buffer[UART3Count] = 0;//Buidar buffer
			UART3Count=0;
			StateUART_canal2 = ESPERANT_RESPOSTA;
			enable_timer( 2 );
			Dummy = LPC_UART2->LSR;
			while ( (LPC_UART2->LSR & (LSR_TEMT)) != (LSR_TEMT) );
			/*while ( LPC_UART2->LSR & LSR_RDR )
			{
				Dummy = LPC_UART2->RBR;
			}*/
			#if INTERFACE_RS485 == 1
			LPC_GPIO0->FIOCLR = 1<<6; //deshabilites enable TX port1
			LPC_GPIO0->FIOCLR = 1<<7; //deshabilites enable TX port0
			#endif
			LPC_UART2->LCR |= ((0x3<<4)|(0x1<<3));
			LPC_UART2->IER = IER_THRE | IER_RLS | IER_RBR;/*Re-enable RBR->habilites recepció*/
		}
		else
		{
			LPC_UART2->LCR |= (0x1<<4);		    /* Sticky parity to 0 */
			LPC_UART2->LCR |= (0x1<<3);
			UART3Transmit_Buffer[UART3Count] = 0;//Buidar buffer
			UART3Count++;
		}
	}
	else
	{
		watchdog_uart_4_enable = 1;
		watchdog_uart_4=0;
	}
}

/*****************************************************************************
** Function name:		UART3_THRE
**
** Descriptions:		procés d'escriptura d'un byte quan buffer lliure
**
** parameters:			None
** Returned value:		None
**
*****************************************************************************/
void UART3_THRE(void) //CH1.B
{
	uint8_t Dummy = Dummy;
	UART1TxEmpty = 1;
	if(StateUART_canal1 == ENVIANT_2)
	{
		watchdog_uart_2_enable = 1;
		watchdog_uart_2=0;
		reset_timer(1);
		disable_timer(1);
		LPC_UART3->THR = UART1Transmit_Buffer[UART1Count];//Enviar següent byte
		UART1TxEmpty = 0;
		//if(UART1Transmit_Buffer[UART1Count]==0xFF)//ultim byte
		if(UART1Count==length_UART1-1)
		{
			watchdog_uart_2_enable = 0;
			watchdog_uart_2=0;
			despres_heartbeat &= ~1;
			LPC_UART3->LCR &= ~(0x1<<4);		/* Sticky parity to 1 */
			LPC_UART3->LCR |= (0x1<<3);
			UART1Transmit_Buffer[UART1Count] = 0;//Buidar buffer
			UART1Count=0;
			StateUART_canal1 = ESPERANT_RESPOSTA;
			enable_timer( 1 );
			Dummy = LPC_UART3->LSR;
			while ( (LPC_UART3->LSR & (LSR_TEMT)) != (LSR_TEMT) );
			/*while ( LPC_UART3->LSR & LSR_RDR )
			{
				Dummy = LPC_UART3->RBR;
			}*/
			#if INTERFACE_RS485 == 1
			LPC_GPIO0->FIOCLR = 1<<4; //deshabilites enable TX port1
			LPC_GPIO0->FIOCLR = 1<<5; //deshabilites enable TX port0
			#endif
			LPC_UART3->LCR |= ((0x3<<4)|(0x1<<3));
			LPC_UART3->IER = IER_THRE | IER_RLS | IER_RBR;/*Re-enable RBR->habilites recepció*/
		}
		else
		{
			LPC_UART3->LCR |= (0x1<<4);		    /* Sticky parity to 0 */
			LPC_UART3->LCR |= (0x1<<3);
			UART1Transmit_Buffer[UART1Count] = 0;//Buidar buffer
			UART1Count++;
		}
	}
	else
	{
		watchdog_uart_2_enable = 1;
		watchdog_uart_2=0;
	}
}

/******************************************************************************
**                            End Of File
******************************************************************************/
