/*****************************************************************************
 *   uart.h:  Header file for NXP LPC17xx Family Microprocessors
 *
 *   Copyright(C) 2009, NXP Semiconductor
 *   All rights reserved.
 *
 *   History
 *   2009.05.27  ver 1.00    Prelimnary version, first Release
 *
******************************************************************************/
#ifndef __UART_H 
#define __UART_H

uint8_t prova;
uint8_t prova_2;
#include "nodes.h"
typedef enum {
  PRIMER_FRAME,
  ENVIANT,
  ESPERANT_RESPOSTA,
  ESPERANT_RESPOSTA_PRIMER,
  ESPERANT_RESPOSTA_CABLE,
  PROXIM_NODE,
  ENVIANT_2,
  FINAL,
  PRIMER_PROGRAMACIO,
  PROGRAMACIO,
  ENVIANT_PROGRAMACIO
} SStateUART;
extern SStateUART StateUART_canal1;
extern SStateUART StateUART_canal2;
//Matriu que compta les dades que s'han d'enviar de cada node
//Exemple: primer byte de dades del node 1 --> send_buffer[0][0]
#define MAX_DADES_NODE 	50  //Màxim de bytes que es pot transmetre per node a la seva seqüència.
//Seqüència: cada interval de temps que disposa el màster per enviar informació a un node.
//extern uint8_t **send_buffer;
/*typedef struct
{
	uint8_t DATA[MAX_DADES_NODE];
}SEND_BUFFER_TypeDef;*/
//extern SEND_BUFFER_TypeDef *buffer_prova;
#define REENVIAMENTS    5 //Reenviaments que es fan pel setmem i getmem
#define PORT0	0
#define PORT1	1
#define PORT2	2
#define PORT3	3
extern uint8_t send_buffer[250][MAX_DADES_NODE];//50 bytes per 250 displays (12500bytes)
extern uint8_t prova_send_buffer[50];
extern __attribute__ ((section(".bss.$RamAHB32*"))) uint8_t send_buffer_2[250][MAX_DADES_NODE]; //50 bytes per 250 displays (12500bytes)
extern uint8_t malloc_buffer[MAX_NODES_CANAL];//Serveix com a flag i comptador de reenviaments del canal 1
extern uint8_t malloc_buffer_2[MAX_NODES_CANAL];
extern volatile uint8_t nombre_mallocs;
extern volatile uint8_t nombre_mallocs_2;

extern volatile uint32_t watchdog_uart_1;//Port A canal 1
extern volatile uint32_t watchdog_uart_2;//Port B canal 1
extern volatile uint32_t watchdog_uart_3;//Port A canal 2
extern volatile uint32_t watchdog_uart_4;//Port B canal 2
extern volatile uint8_t watchdog_uart_1_enable;
extern volatile uint8_t watchdog_uart_2_enable;
extern volatile uint8_t watchdog_uart_3_enable;
extern volatile uint8_t watchdog_uart_4_enable;

extern volatile uint8_t length_UART0;
extern volatile uint8_t length_UART1;
extern volatile uint8_t length_UART2;
extern volatile uint8_t length_UART3;

extern volatile uint8_t posicio_configurats;
extern volatile uint8_t posicio_configurats_2;

extern volatile uint8_t wire_correcte,wire_count;
extern volatile uint8_t wire_correcte_2,wire_count_2;
extern volatile uint8_t comptador_leds_uart[4];
//wire_count-->	Es posa a 1 el primer cop que es rep resposta pel port1 al haver fet
//				el polling de comprovacio, a partir d'aquí es va augmentant a l'accio
//				process_UART_canal1() al estat PROXIM_NODE. Quan s'arriba a 5 es torna a
//				posar a 0 per tal de tornar a fer un polling de comprovació.

//continuar_enviant_port1-->	Veure trama heartbeat a trames.c. S'activa al rebre resposta
//								pel port 1.
#define BUFSIZE_TX	50    //Màxim de bytes que pot tenir un frame
extern volatile uint8_t UART0Transmit_Buffer[BUFSIZE_TX], UART1Transmit_Buffer[BUFSIZE_TX];
extern volatile uint8_t UART2Transmit_Buffer[BUFSIZE_TX], UART3Transmit_Buffer[BUFSIZE_TX];
extern volatile uint8_t UARTTransmit_Broadcast[MAX_DADES_NODE];
extern volatile uint8_t UARTTransmit_Broadcast_2[MAX_DADES_NODE];
extern volatile uint8_t Broadcast_polling;
extern volatile uint8_t Broadcast_polling_2;
extern volatile uint8_t enviament_broadcast;
extern volatile uint8_t enviament_broadcast_1_anterior;
extern volatile uint8_t reenviaments_broadcast_1;
extern volatile uint8_t enviament_broadcast_2;
extern volatile uint8_t enviament_broadcast_2_anterior;
extern volatile uint8_t reenviaments_broadcast_2;

extern volatile uint8_t node_actual;
extern volatile uint8_t node_actual_2;

extern volatile uint8_t despres_heartbeat;//Per sincronitzar els dos canals

#define IER_RBR		0x01
#define IER_THRE	0x02
#define IER_RLS		0x04

#define IIR_PEND	0x01
#define IIR_RLS		0x03
#define IIR_RDA		0x02
#define IIR_CTI		0x06
#define IIR_THRE	0x01

#define LSR_RDR		0x01
#define LSR_OE		0x02
#define LSR_PE		0x04
#define LSR_FE		0x08
#define LSR_BI		0x10
#define LSR_THRE	0x20
#define LSR_TEMT	0x40
#define LSR_RXFE	0x80

#define BUFSIZE		BUFSIZE_TX




uint32_t UARTInit( uint32_t portNum, uint32_t Baudrate );
/*void UART0_IRQHandler( void );
void UART3_IRQHandler( void );
void UART1_IRQHandler( void );
void UART2_IRQHandler( void );*/
void UARTSend( uint32_t portNum );
uint8_t CRC (uint8_t *cadena, uint8_t inici, uint8_t final);
void process_UART_canal1 (void);
void process_UART_canal2 (void);
void UART0_THRE(void);
void UART1_THRE(void);
void UART2_THRE(void);
void UART3_THRE(void);


#endif /* end __UART_H */
/*****************************************************************************
**                            End Of File
******************************************************************************/
