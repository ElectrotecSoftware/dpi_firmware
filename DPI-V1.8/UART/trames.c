/*****************************************************************************
 *   uart.c:  UART API file for NXP LPC17xx Family Microprocessors
 *
 *   Copyright(C) 2009, NXP Semiconductor
 *   All rights reserved.
 *
 *   History
 *   2009.05.27  ver 1.00    Prelimnary version, first Release
 *
******************************************************************************/
#include "lpc17xx.h"
#include "type.h"
#include "uart.h"
#include "trames.h"
#include "nodes.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "webserver.h"
#include "blockdev.h"
#include "ethernet.h"
#include "tcpip.h"
#include "leds.h"
#include "GlobalVars.h"

/*****************************************************************************
** Function name:		setmem
**
** Descriptions:		posa informació a una adreça de memòria de un node
**
** parameters:			node, adreça de memòria, llargada, dada a enviar(cadena) i refresh
** Returned value:		None
** //Refresh-->actualitza dada al node
*****************************************************************************/
extern volatile uint32_t msTicks_aplicacio[MAX_APLICACIONS];
extern volatile uint32_t msTicks_control_alarmes;
extern volatile uint32_t msTicks_programacio1;
extern volatile uint32_t msTicks_programacio2;
void ms_delay (int n) __attribute__((noinline));
uint8_t nodeGlogal;
void setmem (uint8_t node,uint8_t memory_adress,uint8_t length,uint8_t *cadena,uint8_t refresh, uint8_t canal)
{
	unsigned int i,j = 0;
	uint8_t posicio=0;
	uint8_t index_indicacio_llargada=0;
	if(canal == 1)
	{
		if(nombre_mallocs+nombre_mallocs_2>MAX_MALLOCS)return;
		if(node==BROADCAST_ADRESS)
		{
			if(UARTTransmit_Broadcast[0]!=0)
			{
				//for(posicio=0;UARTTransmit_Broadcast[posicio]!=0xFF||UARTTransmit_Broadcast[posicio+1]!=0;posicio++)
				for(posicio=0;UARTTransmit_Broadcast[posicio+1]!=0&&posicio<MAX_DADES_NODE;posicio++)
				{
					//if(posicio>=MAX_DADES_NODE-(3+1+length+2))return;
					posicio = posicio + UARTTransmit_Broadcast[posicio];
				}
				//posicio++;
				//return;
			}
			index_indicacio_llargada = posicio;
			posicio ++;
			if((3+1+length+1+1+index_indicacio_llargada)<MAX_DADES_NODE){
				UARTTransmit_Broadcast[posicio]=node;
				UARTTransmit_Broadcast[posicio+1]=0b010;//Tipus setmem
				UARTTransmit_Broadcast[posicio+2]=memory_adress;
				UARTTransmit_Broadcast[posicio+3]=(refresh&0b01)<<7|length;
				for(i=0;i<length;i++)
				{
					UARTTransmit_Broadcast[i+posicio+3+1]=*(cadena+i);
				}
				//UARTTransmit_Broadcast[posicio+3+1+length]=CRC((uint8_t *)UARTTransmit_Broadcast,posicio,posicio+3+length);
				UARTTransmit_Broadcast[posicio+3+1+length]=0;
				UARTTransmit_Broadcast[posicio+3+1+length+1]=0xFF;//final trama
				UARTTransmit_Broadcast[index_indicacio_llargada]=3+1+length+1+1;
			}else{
				//Comuniquem error d'enviament
				Error_aplicacio(WRONG_FORMAT,node,canal);
				UARTTransmit_Broadcast[index_indicacio_llargada]=0;
			}
			//UARTTransmit_Broadcast[posicio+3+1+length+2]=0;//Per comprovacio
			return;
		}
		//guardar_RAM(node,canal,cadena,length,memory_adress);
		if(!malloc_buffer[node-1])
		{//Si no s'ha fet un malloc s'ha de fer
			//send_buffer[node-1] = (uint8_t *)calloc(MAX_DADES_NODE+1,sizeof(uint8_t));
			//if(send_buffer[node-1]==NULL){malloc_buffer[node-1]=0;return;}
			malloc_buffer[node-1]=1;
			//nombre_mallocs++;
		}
		if(escrivint[node-1]==TRUE)
		{
			/*while (send_buffer [node-1][posicio]!=0xFF&&posicio<MAX_DADES_NODE)
			{
				if(send_buffer [node-1][posicio+1]==0xFF && send_buffer [node-1][posicio+2]!=0)
				{
					posicio++;
				}
				posicio++;
				if(posicio>=MAX_DADES_NODE-(3+1+length+2))return;
			}
			if(send_buffer [node-1][posicio]==0xFF)posicio++;*/
			nodeGlogal=node;
			posicio = 0;
			while(send_buffer[node-1][posicio+1]!=0&&posicio<MAX_DADES_NODE)
			{
				posicio = posicio + send_buffer[node-1][posicio];
				posicio++;
			}

		}
		escrivint[node-1]=TRUE;
		index_indicacio_llargada = posicio;
		posicio ++;
		send_buffer [node-1][posicio]=node;
		send_buffer [node-1][posicio+1]=0b010;//tipus setmem
		send_buffer [node-1][posicio+2]=memory_adress;
		send_buffer [node-1][posicio+3]=(refresh&0b01)<<7|length;
		for(i=0;i<length;i++)
		{
			send_buffer [node-1][i+posicio+3+1]=*(cadena+i);
		}
		send_buffer [node-1][posicio+3+1+length]=CRC((uint8_t *)send_buffer[node-1],posicio,posicio+3+length);
		send_buffer [node-1][posicio+3+1+length+1]=0xFF;//final trama
		send_buffer [node-1][index_indicacio_llargada]=3+1+length+1+1;
		//send_buffer [node-1][posicio+3+1+length+2]=0;//Per comprovacio
		while(j<posicio+3+1+length+2)
		{
			prova_send_buffer[j]=send_buffer [node-1][j];
			j++;
		}
		if(refresh)escrivint[node-1]=FALSE;
	}
	else if(canal == 2)
	{
		if(nombre_mallocs+nombre_mallocs_2>MAX_MALLOCS)return;
		if(node==BROADCAST_ADRESS)
		{
			if(UARTTransmit_Broadcast_2[0]!=0)
			{
				//for(posicio=0;UARTTransmit_Broadcast_2[posicio]!=0xFF||UARTTransmit_Broadcast_2[posicio+1]!=0;posicio++)
				for(posicio=0;UARTTransmit_Broadcast_2[posicio+1]!=0&&posicio<MAX_DADES_NODE;posicio++)
				{
					//if(posicio>=MAX_DADES_NODE-(3+1+length+2))return;
					posicio = posicio + UARTTransmit_Broadcast_2[posicio];
				}
				//posicio++;
				//return;
			}
			index_indicacio_llargada = posicio;
			posicio ++;
			if((3+1+length+1+1+index_indicacio_llargada)<MAX_DADES_NODE){
				UARTTransmit_Broadcast_2[posicio]=node;
				UARTTransmit_Broadcast_2[posicio+1]=0b010;//Tipus setmem
				UARTTransmit_Broadcast_2[posicio+2]=memory_adress;
				UARTTransmit_Broadcast_2[posicio+3]=(refresh&0b01)<<7|length;
				for(i=0;i<length;i++)
				{
					UARTTransmit_Broadcast_2[i+posicio+3+1]=*(cadena+i);
				}
				//UARTTransmit_Broadcast_2[posicio+3+1+length]=CRC((uint8_t *)UARTTransmit_Broadcast_2,posicio,posicio+3+length);
				UARTTransmit_Broadcast_2[posicio+3+1+length] = 0;
				UARTTransmit_Broadcast_2[posicio+3+1+length+1]=0xFF;//final trama
				UARTTransmit_Broadcast_2[index_indicacio_llargada]=3+1+length+1+1;
			}else{
				//Comuniquem error d'enviament
				Error_aplicacio(WRONG_FORMAT,node,canal);
				UARTTransmit_Broadcast_2[index_indicacio_llargada]=0;
			}
			//UARTTransmit_Broadcast[posicio+3+1+length+2]=0;//Per comprovacio
			return;
		}
		//guardar_RAM(node,canal,cadena,length,memory_adress);
		if(!malloc_buffer_2[node-1])
		{//Si no s'ha fet un malloc s'ha de fer
			//send_buffer_2[node-1] = (uint8_t *)calloc(MAX_DADES_NODE+1,sizeof(uint8_t));
			//if(send_buffer_2[node-1]==NULL){malloc_buffer_2[node-1]=0;return;}
			malloc_buffer_2[node-1]=1;
			//nombre_mallocs_2++;
		}
		if(escrivint_2[node-1]==TRUE)
		{
			/*while (send_buffer_2 [node-1][posicio]!=0xFF&&posicio<MAX_DADES_NODE)
			{
				if(send_buffer_2 [node-1][posicio+1]==0xFF && send_buffer_2 [node-1][posicio+2]!=0)
				{
					posicio++;
				}
				posicio++;
				if(posicio>=MAX_DADES_NODE-(3+1+length+2))return;
			}
			if(send_buffer_2 [node-1][posicio]==0xFF)posicio++;*/
			posicio = 0;
			while(send_buffer_2[node-1][posicio+1]!=0&&posicio<MAX_DADES_NODE)
			{
				posicio = posicio + send_buffer_2[node-1][posicio];
				posicio++;
			}
		}
		escrivint_2[node-1]=TRUE;
		index_indicacio_llargada = posicio;
		posicio ++;
		send_buffer_2 [node-1][posicio]=node;
		send_buffer_2 [node-1][posicio+1]=0b010;//tipus setmem
		send_buffer_2 [node-1][posicio+2]=memory_adress;
		send_buffer_2 [node-1][posicio+3]=(refresh&0b01)<<7|length;
		for(i=0;i<length;i++)
		{
			send_buffer_2 [node-1][i+posicio+3+1]=*(cadena+i);
		}
		send_buffer_2 [node-1][posicio+3+1+length]=CRC((uint8_t *)send_buffer_2[node-1],posicio,posicio+3+length);
		send_buffer_2 [node-1][posicio+3+1+length+1]=0xFF;//final trama
		send_buffer_2 [node-1][index_indicacio_llargada]=3+1+length+1+1;
		//send_buffer_2 [node-1][posicio+3+1+length+2]=0;//Per comprovacio
		if(refresh)escrivint_2[node-1]=FALSE;
	}

}

/*****************************************************************************
** Function name:		setmem_display
**
** Descriptions:		posa informació a una adreça de memòria de un node
**
** parameters:			node, adreça de memòria, llargada, dada a enviar(cadena) i refresh
** Returned value:		None
** Refresh-->actualitza dada al node
*****************************************************************************/
void setmem_display (uint8_t node,uint8_t display_config,uint8_t length,uint8_t *cadena,uint8_t refresh,
					uint8_t canal)
{
	unsigned int i,j=0;
	uint8_t posicio=0;
	uint8_t cadena_RAM[35];
	uint8_t index_indicacio_llargada=0;
	if(canal == 1)
	{
		if(nombre_mallocs+nombre_mallocs_2>MAX_MALLOCS)return;
		if(node==BROADCAST_ADRESS)
		{
			i=0;
			if(UARTTransmit_Broadcast[0]!=0)
			{
				//for(posicio=0;UARTTransmit_Broadcast[posicio]!=0xFF||UARTTransmit_Broadcast[posicio+1]!=0;posicio++)
				for(posicio=0;UARTTransmit_Broadcast[posicio+1]!=0&&posicio<MAX_DADES_NODE;posicio++)
				{
					//if(posicio>=(MAX_DADES_NODE-(5+1+length+2)))return;
					posicio = posicio + UARTTransmit_Broadcast[posicio];
				}
				//posicio++;
			}
			index_indicacio_llargada = posicio;
			posicio ++;
			if((6+length+1+1+index_indicacio_llargada)<MAX_DADES_NODE){
				UARTTransmit_Broadcast[posicio]=node;
				UARTTransmit_Broadcast[posicio+1]=0b010;//Tipus setmem
				UARTTransmit_Broadcast[posicio+2]=DISPLAY;
				UARTTransmit_Broadcast[posicio+3]=(refresh&0b01)<<7|(length+2);
				UARTTransmit_Broadcast[posicio+4]=display_config;
				UARTTransmit_Broadcast[posicio+5]=length;
				for(i=0;i<length;i++)
				{
					UARTTransmit_Broadcast[i+posicio+6]=*(cadena+i);
				}
				//UARTTransmit_Broadcast[posicio+5+1+length]=CRC((uint8_t *)UARTTransmit_Broadcast,posicio,posicio+5+length);
				UARTTransmit_Broadcast[posicio+6+length] = 0;
				UARTTransmit_Broadcast[posicio+6+length+1]=0xFF;//final trama
				UARTTransmit_Broadcast[index_indicacio_llargada] = 6+length+1+1;
			}else{
				//Comuniquem error d'enviament
				Error_aplicacio(WRONG_FORMAT,node,canal);
				UARTTransmit_Broadcast[index_indicacio_llargada]=0;
			}
			//UARTTransmit_Broadcast[posicio+5+1+length+2]=0;//Per comprovacio
			return;
		}
		//cadena_RAM = display_config + cadena;
		memcpy(&cadena_RAM,&display_config,1);
		for(i=0;i<35;i++)
		{
			cadena_RAM[i+1]=*(cadena+i);
		}
		//strcat((char *)cadena_RAM,(char *)cadena);
		//guardar_RAM(node,canal,cadena_RAM,length,DISPLAY);
		//SD_state = llegir_RAM(5);
		//SD_state = llegir_RAM(6);
		//SD_state = 0;
		if(!malloc_buffer[node-1])
		{//Si no s'ha fet un malloc s'ha de fer
			//send_buffer[node-1] = (uint8_t *)calloc(MAX_DADES_NODE+1,sizeof(uint8_t));
			//if(send_buffer[node-1]==NULL){malloc_buffer[node-1]=0;return;}
			//led_off(LED_ERROR);
			malloc_buffer[node-1]=1;
			//nombre_mallocs++;
		}
		if(escrivint[node-1]==TRUE)
		{
			/*while (send_buffer [node-1][posicio]!=0xFF)
			{
				if(send_buffer [node-1][posicio+1]==0xFF && send_buffer [node-1][posicio+2]!=0)
				{
					posicio++;
				}
				posicio++;
				if(posicio>=(MAX_DADES_NODE-(5+1+length+2)))return;
			}
			if(send_buffer [node-1][posicio]==0xFF)posicio++;*/
			posicio = 0;
			while(send_buffer[node-1][posicio+1]!=0&&posicio<MAX_DADES_NODE)
			{
				posicio = posicio + send_buffer[node-1][posicio];
				posicio++;
			}

		}
		escrivint[node-1]=TRUE;
		index_indicacio_llargada = posicio;
		posicio++;
		send_buffer [node-1][posicio]=node;
		send_buffer [node-1][posicio+1]=0b010;//tipus setmem
		send_buffer [node-1][posicio+2]=DISPLAY;
		send_buffer [node-1][posicio+3]=(refresh&0b01)<<7|(length+2);
		send_buffer [node-1][posicio+4]=display_config;
		send_buffer [node-1][posicio+5]=length;
		for(i=0;i<length;i++)
		{
			send_buffer [node-1][i+posicio+6]=*(cadena+i);
		}
		send_buffer [node-1][6+posicio+length]=CRC((uint8_t *)send_buffer[node-1],posicio,5+posicio+length);
		send_buffer [node-1][6+posicio+length+1]=0xFF;//final trama
		send_buffer [node-1][index_indicacio_llargada]=6+length+1+1;
		//send_buffer [node-1][5+posicio+1+length+2]=0;//Per comprovacio
		while(j<5+posicio+1+length+2)
		{
			prova_send_buffer[j]=send_buffer [node-1][j];
			j++;
		}
		if(refresh)escrivint[node-1]=FALSE;
	}
	else if (canal == 2)
	{
		if(nombre_mallocs+nombre_mallocs_2>MAX_MALLOCS)return;
		if(node==BROADCAST_ADRESS)
		{
			i=0;
			if(UARTTransmit_Broadcast_2[0]!=0)
			{
				//for(posicio=0;UARTTransmit_Broadcast_2[posicio]!=0xFF||UARTTransmit_Broadcast_2[posicio+1]!=0;posicio++)
				for(posicio=0;UARTTransmit_Broadcast_2[posicio+1]!=0&&posicio<MAX_DADES_NODE;posicio++)
				{
					//if(posicio>=(MAX_DADES_NODE-(5+1+length+2)))return;
					posicio = posicio + UARTTransmit_Broadcast_2[posicio];
				}
				//posicio++;
			}
			index_indicacio_llargada = posicio;
			posicio ++;
			if((6+length+1+1+index_indicacio_llargada)<MAX_DADES_NODE){
				UARTTransmit_Broadcast_2[posicio]=node;
				UARTTransmit_Broadcast_2[posicio+1]=0b010;//Tipus setmem
				UARTTransmit_Broadcast_2[posicio+2]=DISPLAY;
				UARTTransmit_Broadcast_2[posicio+3]=(refresh&0b01)<<7|(length+2);
				UARTTransmit_Broadcast_2[posicio+4]=display_config;
				UARTTransmit_Broadcast_2[posicio+5]=length;
				for(i=0;i<length;i++)
				{
					UARTTransmit_Broadcast_2[i+posicio+5+1]=*(cadena+i);
				}
				//UARTTransmit_Broadcast_2[posicio+5+1+length]=CRC((uint8_t *)UARTTransmit_Broadcast_2,posicio,posicio+5+length);
				UARTTransmit_Broadcast_2[posicio+6+length] = 0;
				UARTTransmit_Broadcast_2[posicio+6+length+1]=0xFF;//final trama
				UARTTransmit_Broadcast_2[index_indicacio_llargada] = 6+length+1+1;
			}else{
				//Comuniquem error d'enviament
				Error_aplicacio(WRONG_FORMAT,node,canal);
				UARTTransmit_Broadcast_2[index_indicacio_llargada]=0;
			}
			//UARTTransmit_Broadcast_2[posicio+5+1+length]=CRC((uint8_t *)UARTTransmit_Broadcast_2,posicio,posicio+5+length);
			return;
		}
		//cadena_RAM = display_config + cadena;
		memcpy(&cadena_RAM,&display_config,1);
		for(i=0;i<35;i++)
		{
			cadena_RAM[i+1]=*(cadena+i);
		}
		//strcat((char *)cadena_RAM,(char *)cadena);
		//guardar_RAM(node,canal,cadena_RAM,length,DISPLAY);
		//SD_state = llegir_RAM(5);
		//SD_state = llegir_RAM(6);
		//SD_state = 0;
		if(!malloc_buffer_2[node-1])
		{//Si no s'ha fet un malloc s'ha de fer
			//send_buffer_2[node-1] = (uint8_t *)calloc(MAX_DADES_NODE+1,sizeof(uint8_t));
			//if(send_buffer_2[node-1]==NULL){malloc_buffer_2[node-1]=0;return;}
			malloc_buffer_2[node-1]=1;
			//nombre_mallocs_2++;
		}
		if(escrivint_2[node-1]==TRUE)
		{
			/*while (send_buffer_2 [node-1][posicio]!=0xFF)
			{
				if(send_buffer_2 [node-1][posicio+1]==0xFF && send_buffer_2 [node-1][posicio+2]!=0)
				{
					posicio++;
				}
				posicio++;
				if(posicio>=(MAX_DADES_NODE-(5+1+length+2)))return;
			}
			if(send_buffer_2 [node-1][posicio]==0xFF)posicio++;*/
			posicio = 0;
			while(send_buffer_2[node-1][posicio+1]!=0&&posicio<MAX_DADES_NODE)
			{
				posicio = posicio + send_buffer_2[node-1][posicio];
				posicio++;
			}
		}
		escrivint_2[node-1]=TRUE;
		index_indicacio_llargada = posicio;
		posicio++;
		send_buffer_2 [node-1][posicio]=node;
		send_buffer_2 [node-1][posicio+1]=0b010;//tipus setmem
		send_buffer_2 [node-1][posicio+2]=DISPLAY;
		send_buffer_2 [node-1][posicio+3]=(refresh&0b01)<<7|(length+2);
		send_buffer_2 [node-1][posicio+4]=display_config;
		send_buffer_2 [node-1][posicio+5]=length;
		for(i=0;i<length;i++)
		{
			send_buffer_2 [node-1][i+posicio+6]=*(cadena+i);
		}
		send_buffer_2[node-1][6+posicio+length]=CRC((uint8_t *)send_buffer_2[node-1],posicio,5+posicio+length);
		send_buffer_2 [node-1][6+posicio+length+1]=0xFF;//final trama
		send_buffer_2 [node-1][index_indicacio_llargada]=6+length+1+1;
		//send_buffer_2 [node-1][5+posicio+1+length+2]=0;//Per comprovacio

		if(refresh)escrivint_2[node-1]=FALSE;
	}

}

/*****************************************************************************
** Function name:		getmem
**
** Descriptions:		obté informació de una adreça de memòria de un node
**
** parameters:			node i adreça de memòria
** Returned value:		None
** 
*****************************************************************************/
void getmem (uint8_t node, uint8_t memory_adress, uint8_t canal)
{
	if(canal == 1)
	{
		if(!malloc_buffer[node-1])
		{//Si no s'ha fet un malloc s'ha de fer
			//send_buffer[node-1] = (uint8_t *)calloc(MAX_DADES_NODE+1,sizeof(uint8_t));
			//if(send_buffer[node-1]==NULL){malloc_buffer[node-1]=0;return;}
			malloc_buffer[node-1]=1;
			//nombre_mallocs++;
		}
		getmem_adress = memory_adress;
		send_buffer [node-1][1]=node;
		send_buffer [node-1][2]=0b011;
		send_buffer [node-1][3]=memory_adress;
		send_buffer [node-1][4]=node^0b011^memory_adress;
		send_buffer [node-1][5]=0xFF;
		send_buffer [node-1][0]=5;
	}
	else if (canal == 2)
	{
		if(!malloc_buffer_2[node-1])
		{//Si no s'ha fet un malloc s'ha de fer
			//send_buffer_2[node-1] = (uint8_t *)calloc(MAX_DADES_NODE+1,sizeof(uint8_t));
			//if(send_buffer_2[node-1]==NULL){malloc_buffer_2[node-1]=0;return;}
			malloc_buffer_2[node-1]=1;
			//nombre_mallocs_2++;
		}
		getmem_adress = memory_adress;
		send_buffer_2 [node-1][1]=node;
		send_buffer_2 [node-1][2]=0b011;
		send_buffer_2 [node-1][3]=memory_adress;
		send_buffer_2 [node-1][4]=node^0b011^memory_adress;
		send_buffer_2 [node-1][5]=0xFF;
		send_buffer_2 [node-1][0]=5;
	}
}

/*****************************************************************************
** Function name:		programacio
**
** Descriptions:		programa un node
**
** parameters:			tipus de programacio, adreces antiga i nova, guardar i adreces multicasts
** Returned value:		None
** 
*****************************************************************************/
void programacio(uint8_t tipus, uint8_t old_adress, uint8_t new_adress, uint8_t guardar, uint8_t multicast_addr_1, uint8_t multicast_addr_2, uint8_t canal)
{
	if(canal<2)//CANAL 1
	{
		if(tipus == 1 && old_adress!=BROADCAST_ADRESS)//Revisar
		{
			peticio_programacio = tipus;
			if(StateUART_canal1==PROGRAMACIO)
			{
				UART0Transmit_Buffer[0]=old_adress;
				UART0Transmit_Buffer[1]=0b001;//tipus programacio
				UART0Transmit_Buffer[2]=guardar<<4|tipus;
				UART0Transmit_Buffer[3]=new_adress;
				UART0Transmit_Buffer[4]= multicast_addr_1;
				UART0Transmit_Buffer[5]= multicast_addr_2;
				UART0Transmit_Buffer[6]=CRC((uint8_t *)UART0Transmit_Buffer,0,5);
				UART0Transmit_Buffer[7]=0xFF;//final trama
				UARTSend(0);
				msTicks_programacio1=0;//Per si no reps resposta del node es posa al estat FINAL
			}
			else
			{
				old_adress_programacio1_canal1 = old_adress;
				new_adress_programacio1_canal1 = new_adress;
			}

		}
		else if (tipus == 2 || tipus == 4 || old_adress==BROADCAST_ADRESS)
		{
		//Primer poses peticio_programacio a TRUE per tal de començar a programar despres
		//d'haver fet un polling, garantint així abscència d'interrupcions que podrien malmetre
		//el correcte funcionament de la programació tipus 2.

			if(old_adress==BROADCAST_ADRESS)peticio_programacio=old_adress;
			else peticio_programacio = tipus,new_adress_programacio2_canal1=new_adress;
			if(StateUART_canal1==PROGRAMACIO)
			{
				//Començar trama de programacio
				UART0Transmit_Buffer [0]=old_adress;
				UART0Transmit_Buffer [1]=0b001;//tipus programacio
				UART0Transmit_Buffer [2]=guardar<<4|tipus;
				UART0Transmit_Buffer [3]=new_adress;
				UART0Transmit_Buffer [4]= multicast_addr_1;
				UART0Transmit_Buffer [5]= multicast_addr_2;
				UART0Transmit_Buffer [6]=CRC((uint8_t *)UART0Transmit_Buffer,0,5);
				UART0Transmit_Buffer [7]=0xFF;//final trama
				UARTSend(0);
				if(tipus == 2)
				{
					msTicks_programacio2=0;//Timeout per tota la programació 2
				}
				if(tipus == 4 || old_adress==BROADCAST_ADRESS)
				{
					msTicks_programacio2 = (1000*60*16);
					peticio_programacio = 0;
					new_adress_programacio2_canal1 = 0;
					ms_delay(10);
					StateUART_canal1=FINAL;//potser es necessita retard
				}
			}
		}
	}
	else//CANAL 2
	{
		if(tipus == 1 && old_adress!=BROADCAST_ADRESS)//Revisar
				{
					peticio_programacio_2 = tipus;
					if(StateUART_canal2==PROGRAMACIO)
					{
						UART2Transmit_Buffer[0]=old_adress;
						UART2Transmit_Buffer[1]=0b001;//tipus programacio
						UART2Transmit_Buffer[2]=guardar<<4|tipus;
						UART2Transmit_Buffer[3]=new_adress;
						UART2Transmit_Buffer[4]= multicast_addr_1;
						UART2Transmit_Buffer[5]= multicast_addr_2;
						UART2Transmit_Buffer[6]=CRC((uint8_t *)UART2Transmit_Buffer,0,5);
						UART2Transmit_Buffer[7]=0xFF;//final trama
						UARTSend(2);
						msTicks_programacio1=0;//Per si no reps resposta del node es posa al estat FINAL
					}
					else
					{
						old_adress_programacio1_canal2 = old_adress;
						new_adress_programacio1_canal2 = new_adress;
					}

				}
				else if (tipus == 2 || tipus == 4 || old_adress==BROADCAST_ADRESS)
				{
				//Primer poses peticio_programacio a TRUE per tal de començar a programar despres
				//d'haver fet un polling garantint aixi abscencia d'interrupcions que podrien malmetre
				//el correcte funcionament de la programacio tipus 2.

					if(old_adress==BROADCAST_ADRESS)peticio_programacio_2=old_adress;
					else peticio_programacio_2 = tipus,new_adress_programacio2_canal2=new_adress;
					if(StateUART_canal2==PROGRAMACIO)
					{
						//Començar trama de programacio
						UART2Transmit_Buffer [0]=old_adress;
						UART2Transmit_Buffer [1]=0b001;//tipus programacio
						UART2Transmit_Buffer [2]=guardar<<4|tipus;
						UART2Transmit_Buffer [3]=new_adress;
						UART2Transmit_Buffer [4]= multicast_addr_1;
						UART2Transmit_Buffer [5]= multicast_addr_2;
						UART2Transmit_Buffer [6]=CRC((uint8_t *)UART2Transmit_Buffer,0,5);
						UART2Transmit_Buffer [7]=0xFF;//final trama
						UARTSend(2);
						if(tipus == 2)
						{
							msTicks_programacio2=0;
						}
						if(tipus == 4 || old_adress==BROADCAST_ADRESS)
						{
							msTicks_programacio2 = (1000*60*16);
							peticio_programacio_2 = 0;
							new_adress_programacio2_canal2 = 0;
							ms_delay(10);
							StateUART_canal2=FINAL;//potser es necessita retard
						}
					}
				}
			}

}

/*****************************************************************************
** Function name:		heartbeat
**
** Descriptions:		genera la trama heartbeat a un node
**
** parameters:			node al qual faràs heartbeat
** Returned value:		none
** 
*****************************************************************************/
void heartbeat( uint8_t node , uint8_t canal)
{
	/*if(continuar_enviant_port1==TRUE)
	{//Si ja s'ha enviat pel port 1 i s'ha rebut resposta --> Continuar.
		UART1Transmit_Buffer[0]=node;
		UART1Transmit_Buffer[1]=0b100;//tipus heartbeat
		UART1Transmit_Buffer[2]=CRC((uint8_t *)UART1Transmit_Buffer,0,1);
		UART1Transmit_Buffer[3]=0xFF;//final trama
		//UARTSend( 1, (uint8_t *)UART1Transmit_Buffer, 5);
		UARTSend(1);
	}*/
	if(canal == 1)
	{//Si no s'ha enviat pel port 1
		if(((taula_nodes_web[node-1]&0x1)==1)&&(!wire_correcte && wire_count!=0))
		{//Mirar si el node_actual ha respost pel port 1 i no es el polling de comprovacio
			UART1Transmit_Buffer[0]=node;
			UART1Transmit_Buffer[1]=0b100;//tipus heartbeat
			UART1Transmit_Buffer[2]=CRC((uint8_t *)UART1Transmit_Buffer,0,1);
			UART1Transmit_Buffer[3]=0xFF;//final trama
			//UARTSend( 1, (uint8_t *)UART1Transmit_Buffer, 5);
			UARTSend(1);
		}
		else
		{//Si el node_actual ha respost pel port 0 o es el polling de comprovacio
			UART0Transmit_Buffer[0]=node;
			UART0Transmit_Buffer[1]=0b100;//tipus heartbeat
			UART0Transmit_Buffer[2]=CRC((uint8_t *)UART0Transmit_Buffer,0,1);
			UART0Transmit_Buffer[3]=0xFF;//final trama
			//UARTSend( 0, (uint8_t *)UART0Transmit_Buffer, 5);
			UARTSend(0);
		}
	}
	else if(canal == 2)
	{
		if(((taula_nodes_web_2[node-1]&0x1)==1)&&(!wire_correcte_2 && wire_count_2!=0))
		{//Mirar si el node_actual ha respost pel port 1 i no es el polling de comprovacio
			UART3Transmit_Buffer[0]=node;
			UART3Transmit_Buffer[1]=0b100;//tipus heartbeat
			UART3Transmit_Buffer[2]=CRC((uint8_t *)UART3Transmit_Buffer,0,1);
			UART3Transmit_Buffer[3]=0xFF;//final trama
			//UARTSend( 1, (uint8_t *)UART1Transmit_Buffer, 5);
			UARTSend(3);
		}
		else
		{//Si el node_actual ha respost pel port 0 o es el polling de comprovacio
			UART2Transmit_Buffer[0]=node;
			UART2Transmit_Buffer[1]=0b100;//tipus heartbeat
			UART2Transmit_Buffer[2]=CRC((uint8_t *)UART2Transmit_Buffer,0,1);
			UART2Transmit_Buffer[3]=0xFF;//final trama
			//UARTSend( 0, (uint8_t *)UART0Transmit_Buffer, 5);
			UARTSend(2);
		}
	}
}

/*****************************************************************************
** Function name:		process_data
**
** Descriptions:		processa la dada rebuda pels nodes
**
** parameters:			Buffer de dades i llargada
** Returned value:		none
**
*****************************************************************************/
uint8_t test = 0;

/*void control_alarmes(uint8_t *Buffer, uint32_t Length, uint8_t port){

}*/
void process_data( uint8_t *Buffer, uint32_t Length, uint8_t port )
{
	uint8_t nodeID;

	uint8_t status;
	uint8_t frameID;
	uint8_t tipus=0,pulsadors=0,byte1=0,byte2=0;
	uint8_t aplicacio;
	uint8_t i=0,j=0;
	uint8_t Ethernet_buffer_local[MAX_APLICACIONS][BUFSIZE_ETHERNET];
	char byte[3]={0,0,0};
	char node_char[3]={0,0,0};
	nodeID = *Buffer;
	frameID = *(Buffer + 1);
	//------------------ CONTROL ALARMA DESCONNEXIÓ ------------------------------
	//Si el node respon ja no està desconnectat
	if(port<2)
	{//CANAL 1
		if((alarmes_activades[nodeID-1]&(1<<(DESCONEXIO_NODE-1)))!=0)
		{
			alarm('b',nodeID,LPC_RTC->HOUR,LPC_RTC->MIN,LPC_RTC->SEC,1);
		}
		alarmes_activades[nodeID-1] &= ~(1<<(DESCONEXIO_NODE-1));
		for(i=0;i<MAX_APLICACIONS;i++) taula_errors[i][nodeID-1]&= ~(1<<(DESCONEXIO_NODE));//Borrem la variable pq es torni a notificar a la aplicació
		//control_alarmes[node_actual-1]&=~0x3F;//Poses a zero comptador de desconnexio
		control_alarmes[node_actual-1]=0;//Poses a zero comptador de desconnexio
		//Poses a zero el comptador de desconnexio de node i incrementes el comptador de varis nodes.
		//El comptador de varis nodes activarà la alarma de varis nodes amb la mateixa ID si es detecta
		//varies vegades commutació de un node entre desconnectat i connectat posan-hi un timeout.
		nodes_correctes++;
	}
	else
	{//CANAL 2
		if((alarmes_activades_2[nodeID-1]&(1<<(DESCONEXIO_NODE-1)))!=0)
		{
			alarm('b',nodeID,LPC_RTC->HOUR,LPC_RTC->MIN,LPC_RTC->SEC,2);
		}
		alarmes_activades_2[nodeID-1] &= ~(1<<(DESCONEXIO_NODE-1));
		for(i=0;i<MAX_APLICACIONS;i++) taula_errors_2[i][nodeID-1]&= ~(1<<(DESCONEXIO_NODE));//Borrem la variable pq es torni a notificar a la aplicació
		//control_alarmes_2[node_actual_2-1]&=~0x3F;//Poses a zero comptador de desconnexio
		control_alarmes_2[node_actual_2-1]=0;//Poses a zero comptador de desconnexio
		//Poses a zero el comptador de desconnexio de node i incrementes el comptador de varis nodes.
		//El comptador de varis nodes activarà la alarma de varis nodes amb la mateixa ID si es detecta
		//varies vegades commutació de un node entre desconnectat i connectat posan-hi un timeout.
		nodes_correctes_2++;
	}

	//------------------ TIPUS DE RESPOSTA ------------------------------
	if(((frameID & 0xF0)>>4)==0xF)
	{
		return;//Error de CRC
	}
	else if (((frameID & 0xF0)>>4)==0x1);//ACK1
	else;//No hi ha dades --> o getmem o heartbeat

	//------------------ HEARTBEAT ------------------------------
	if((frameID&0xF)==0b100)//Heartbeat
	{
		tipus = (*(Buffer+2)&0xF0)>>4;
		status = *(Buffer+2)&0x0F;
		#if INTERFACE_RS485==1
		status = NORMAL;
		#endif
		if(status == NORMAL || status == TEST)
		{
			//desactivar alarmes si activades;
			if(port<2)
			{
				control_alarmes_HW[nodeID-1]=0;
				if((alarmes_activades[nodeID-1]&(1<<(ERROR_HW-1)))!=0)
				{
					alarm('d',nodeID,LPC_RTC->HOUR,LPC_RTC->MIN,LPC_RTC->SEC,1);
					alarmes_activades[nodeID-1] &= ~(1<<(ERROR_HW-1));
				}
			}
			else
			{
				control_alarmes_HW_2[nodeID-1]=0;
				if((alarmes_activades_2[nodeID-1]&(1<<(ERROR_HW-1)))!=0)
				{
					alarm('d',nodeID,LPC_RTC->HOUR,LPC_RTC->MIN,LPC_RTC->SEC,2);
					alarmes_activades_2[nodeID-1] &= ~(1<<(ERROR_HW-1));
				}
			}
		}
		else if (status == INIT)//Definir estat inicial
		{
			//if(port<2)carrega_RAM(nodeID,1,tipus);
			//else carrega_RAM(nodeID,2,tipus);
		}
		else//Ni estat inicial ni normal ni test
		{
			//activar alarmes
			status = ERROR_NODE;
			if(port<2)
			{
				if (control_alarmes_HW[nodeID-1]<10)
				{
					control_alarmes_HW[nodeID-1]++;
					if (control_alarmes_HW[nodeID-1]>=10)
					{
						if((alarmes_activades[nodeID-1]&(1<<(ERROR_HW-1)))==0)
						{
							alarm(ERROR_HW,nodeID,LPC_RTC->HOUR,LPC_RTC->MIN,LPC_RTC->SEC,1);
						}
						Error_aplicacio(ERROR_HW,nodeID,1);
						alarmes_activades[nodeID-1] |= 1<<(ERROR_HW-1);
					}
				}
			}
			else
			{
				if (control_alarmes_HW_2[nodeID-1]<10)
				{
					control_alarmes_HW_2[nodeID-1]++;
					if (control_alarmes_HW_2[nodeID-1]>=10)
					{
						if((alarmes_activades_2[nodeID-1]&(1<<(ERROR_HW-1)))==0)
						{
							alarm(ERROR_HW,nodeID,LPC_RTC->HOUR,LPC_RTC->MIN,LPC_RTC->SEC,2);
						}
						Error_aplicacio(ERROR_HW,nodeID,2);
						alarmes_activades_2[nodeID-1] |= 1<<(ERROR_HW-1);
					}
				}
			}
		}
		if((tipus == 7)||(tipus == 10))//RS-232
		{//-------------------------------------------------------------------------------Actualitzat ver 1.2
			pulsadors=0;
			//status=RS_232;
			if(tipus!= taula_SD[nodeID-1])
			{//activar alarmes
				status=ERROR_NODE;
			}
			if(port<2)taula_SD[nodeID-1]=tipus;//Canal 1
			else taula_SD_2[nodeID-1]=tipus;//Canal 2

			actualitza_node(port,status,tipus,nodeID-1,pulsadors);

			if(*(Buffer+4)&0x80)//Si bit activat --> llegir bytes
			{
				pulsadors = *(Buffer+3);
				//S'envia la trama a totes les aplicacions
				for(aplicacio=0;aplicacio<MAX_APLICACIONS;aplicacio++)
				{
					if(((*(Buffer+4))&0x7F)>DADES_RS232)break;
					for(i=0;i<((*(Buffer+4))&0x7F);i++)
					{
						bytes_RS232[i+1]=*(Buffer+5+i);
					}
					bytes_RS232[0]=i;
					if (PICK_SIMULATOR == 1) checkTestMode(port,nodeID,pulsadors,bytes_RS232[5]);
					if(StatePagina==ESPERA_RS232||StatePagina==ACTUALITZA_RS232)actualitza_display = TRUE;
					if((aplicacio_MAC[aplicacio][0]!=0||aplicacio_MAC[aplicacio][1]!=0||aplicacio_MAC[aplicacio][2]!=0)&&
					   (aplicacio_IP [aplicacio][0]!=0||aplicacio_IP [aplicacio][1]!=0))
					{
						itoa(nodeID,node_char,10); //itoa converteix a caràcters. El fi de conversió està en detectar un [0] al caràcter
						if(node_char[0]==0) node_char[2]=node_char[1]=node_char[0]=0x30;
						else if(node_char[1]==0){ node_char[2]=node_char[0];node_char[1]=node_char[0]=0x30;}
						else if(node_char[2]==0){ node_char[2]=node_char[1];node_char[1]=node_char[0];node_char[0]=0x30;}
						TCPLocalPort=PORT_COMUNICACIO;
						Ethernet_buffer_local[aplicacio][0]=2;
						Ethernet_buffer_local[aplicacio][1]='@'; //ID trama
						Ethernet_buffer_local[aplicacio][2]=5;
						Ethernet_buffer_local[aplicacio][3]=node_char[0];
						Ethernet_buffer_local[aplicacio][4]=node_char[1];
						Ethernet_buffer_local[aplicacio][5]=node_char[2];
						Ethernet_buffer_local[aplicacio][6]=5;
						if(port<2)Ethernet_buffer_local[aplicacio][7]='1';//Canal 1
						else Ethernet_buffer_local[aplicacio][7]='2';//Canal 2
						Ethernet_buffer_local[aplicacio][8]=5;
						for(i=0;i<((*(Buffer+4))&0x7F);i++)
						{
							//if(*(Buffer+5+i)==',') Ethernet_buffer_local[aplicacio][9+i] = '.';
							//else Ethernet_buffer_local[aplicacio][9+i]=*(Buffer+5+i);
							if(*(Buffer+5+i)==2) Ethernet_buffer_local[aplicacio][9+i]=17; //Convertim caràcter STX en DC1
							else if(*(Buffer+5+i)==3) Ethernet_buffer_local[aplicacio][9+i]=18; //Convertim caràcter ETX en DC2
							else if(*(Buffer+5+i)==5) Ethernet_buffer_local[aplicacio][9+i]=19; //Convertim caràcter ENQ en DC3
							else Ethernet_buffer_local[aplicacio][9+i]=*(Buffer+5+i);
						}
						Ethernet_buffer_local[aplicacio][9+i]=3;
						if(*(Buffer+5+i)==CRC(Buffer,0,5+i-1))//Si el CRC es correcte
						{
							aplicacio_actual = aplicacio;
							memcpy(&RemoteMAC, &aplicacio_MAC[aplicacio], 6);
							memcpy(&RemoteIP, &aplicacio_IP[aplicacio], 4);
							if(!esperant_ACK[aplicacio_actual])
							{
								Enviar_Ethernet((uint8_t *)Ethernet_buffer_local[aplicacio],9+i+1);
								esperant_ACK[aplicacio_actual]=0;
							}
							Enviar_Ethernet((uint8_t *)Ethernet_buffer_local[aplicacio],9+i+1);
						}
						else//CRC incorrecte
						{
							//Notificar error
							counter_error_CRC++;
						}
					}
				}
				TCPLocalPort=TCP_PORT_HTTP;
				memcpy(&RemoteMAC, &web_MAC, 6);              // save opponents MAC and IP
				memcpy(&RemoteIP, &web_IP, 4);                // for later use
			}
		}//
		else//Displays normals
		{
			i=2;
			//pulsadors = (*(Buffer+3))&0xF;
			pulsadors = *(Buffer+3);
			byte1 = *(Buffer+4);
			byte2 = *(Buffer+5);
		}
		if(*(Buffer+4+i)==CRC(Buffer,0,4+i-1))//Si el CRC es correcte
		{

			if(port<2)
			{
				if(tipus!= taula_SD[nodeID-1])
				{//activar alarmes
					status=ERROR_NODE;
				}
			}
			else
			{
				if(tipus!= taula_SD_2[nodeID-1])
				{//activar alarmes
					status=ERROR_NODE;
				}
			}
			actualitza_node(port,status,tipus,nodeID-1,pulsadors);
			if(port<2)
			{
				if(nodeID==node_display)
				{
					if(byte2_web!=byte2||byte1_web!=byte1)//Si els bytes no coincideixen actualitza
					{
						byte1_web=byte1;
						byte2_web=byte2;
						if((StatePagina==ESPERA_DISPLAY || StatePagina==ACTUALITZA_DISPLAY))actualitza_display = TRUE;
						else if((StatePagina==ESPERA_DISPLAY_2 || StatePagina==ACTUALITZA_DISPLAY_2))actualitza_display_2 = TRUE;
					}
				}
				if(1)//(status == NORMAL)
				{//Si status normal
					if((taula_nodes_web[nodeID-1]&0xFF00)<(taula_nodes[nodeID-1]&0xFF00))
					{//Si pulsadors diferent que anteriorment (només al pulsar)
						if (PICK_SIMULATOR == 1) checkTestMode(port,nodeID,pulsadors,0);
						pulsadors=pulsadors&(~(taula_nodes_web[nodeID-1]&0xFF00)>>8); //Només considerem el pulsador que ha canviat
						for(aplicacio=0;aplicacio<MAX_APLICACIONS;aplicacio++)
						{
							if((aplicacio_MAC[aplicacio][0]!=0||aplicacio_MAC[aplicacio][1]!=0||aplicacio_MAC[aplicacio][2]!=0)&&
							   (aplicacio_IP [aplicacio][0]!=0||aplicacio_IP [aplicacio][1]!=0))
							{

								itoa(nodeID,node_char,10); //itoa converteix a caràcters. El fi de conversió està en detectar un [0] al caràcter
								if(node_char[0]==0) node_char[2]=node_char[1]=node_char[0]=0x30;
								else if(node_char[1]==0){ node_char[2]=node_char[0];node_char[1]=node_char[0]=0x30;}
								else if(node_char[2]==0){ node_char[2]=node_char[1];node_char[1]=node_char[0];node_char[0]=0x30;}


								TCPLocalPort=PORT_COMUNICACIO;
								Ethernet_buffer_local[aplicacio][0]=2;
								Ethernet_buffer_local[aplicacio][1]='6';
								Ethernet_buffer_local[aplicacio][2]=5;
								//Ethernet_buffer_local[aplicacio][3]=nodeID+0x30;
								Ethernet_buffer_local[aplicacio][3]=node_char[0];
								Ethernet_buffer_local[aplicacio][4]=node_char[1];
								Ethernet_buffer_local[aplicacio][5]=node_char[2];
								Ethernet_buffer_local[aplicacio][6]=5;
								if(port<2)Ethernet_buffer_local[aplicacio][7]='1';//Canal 1
								else Ethernet_buffer_local[aplicacio][7]='2';//Canal 2
								Ethernet_buffer_local[aplicacio][8]=5;
								Ethernet_buffer_local[aplicacio][9]=tipus+0x30;
								Ethernet_buffer_local[aplicacio][10]=',';
								Ethernet_buffer_local[aplicacio][11]=pulsadors+0x30;
								Ethernet_buffer_local[aplicacio][12]=',';
								if(pulsadors == 2)Ethernet_buffer_local[aplicacio][13]='V';//Simbolo
								else if (pulsadors == 1)Ethernet_buffer_local[aplicacio][13]='+';//Simbolo
								else if (pulsadors == 8)Ethernet_buffer_local[aplicacio][13]='-';//Simbolo
								else if (pulsadors == 4)Ethernet_buffer_local[aplicacio][13]='F';//Simbolo
								else if (pulsadors == 16){Ethernet_buffer_local[aplicacio][11]='5';Ethernet_buffer_local[aplicacio][13]='X';}//Simbolo
								else Ethernet_buffer_local[aplicacio][13]='?';//Simbolo
								Ethernet_buffer_local[aplicacio][14]=3;
								aplicacio_actual = aplicacio;
								memcpy(&RemoteMAC, &aplicacio_MAC[aplicacio], 6);
								memcpy(&RemoteIP, &aplicacio_IP[aplicacio], 4);
								if(!esperant_ACK[aplicacio_actual])
								{
									Enviar_Ethernet((uint8_t *)Ethernet_buffer_local[aplicacio],15);
									esperant_ACK[aplicacio_actual]=0;
								}
								Enviar_Ethernet((uint8_t *)Ethernet_buffer_local[aplicacio],15);
							}
						}
						TCPLocalPort=TCP_PORT_HTTP;
						memcpy(&RemoteMAC, &web_MAC, 6);              // save opponents MAC and IP
						memcpy(&RemoteIP, &web_IP, 4);                // for later use
					}
				}
			}
			else
			{
				if(nodeID==node_display)
				{
					if(byte2_web!=byte2||byte1_web!=byte1)//Si els bytes no coincideixen actualitza
					{
						byte1_web=byte1;
						byte2_web=byte2;
						if((StatePagina==ESPERA_DISPLAY_2 || StatePagina==ACTUALITZA_DISPLAY_2))actualitza_display_2 = TRUE;
					}
				}
				if(1)//(status == NORMAL)
				{//Si status normal
					if((taula_nodes_web_2[nodeID-1]&0xFF00)<(taula_nodes_2[nodeID-1]&0xFF00))
					{//Si pulsadors diferent que anteriorment (només al pulsar)
						pulsadors=pulsadors&(~(taula_nodes_web[nodeID-1]&0xFF00)>>8); //Només considerem el pulsador que ha canviat
						for(aplicacio=0;aplicacio<MAX_APLICACIONS;aplicacio++)
						{
							if((aplicacio_MAC[aplicacio][0]!=0||aplicacio_MAC[aplicacio][1]!=0||aplicacio_MAC[aplicacio][2]!=0)&&
							   (aplicacio_IP [aplicacio][0]!=0||aplicacio_IP [aplicacio][1]!=0))
							{
								itoa(nodeID,node_char,10);
								//if(node_char[2]==0&&node_char[1]!=0&&node_char[0]!=0){node_char[2]=node_char[1];node_char[1]=node_char[0];node_char[0]=0x30;}
								//else if(node_char[2]==0&&node_char[1]==0&&node_char[0]!=0){node_char[2]=node_char[0];node_char[1]=node_char[0]=0x30;}
								if(node_char[0]==0) node_char[2]=node_char[1]=node_char[0]=0x30;
								else if(node_char[1]==0){ node_char[2]=node_char[0];node_char[1]=node_char[0]=0x30;}
								else if(node_char[2]==0){ node_char[2]=node_char[1];node_char[1]=node_char[0];node_char[0]=0x30;}
								TCPLocalPort=PORT_COMUNICACIO;
								Ethernet_buffer_local[aplicacio][0]=2;
								Ethernet_buffer_local[aplicacio][1]='6';
								Ethernet_buffer_local[aplicacio][2]=5;
								//Ethernet_buffer_local[aplicacio][3]=nodeID+0x30;
								Ethernet_buffer_local[aplicacio][3]=node_char[0];
								Ethernet_buffer_local[aplicacio][4]=node_char[1];
								Ethernet_buffer_local[aplicacio][5]=node_char[2];
								Ethernet_buffer_local[aplicacio][6]=5;
								if(port<2)Ethernet_buffer_local[aplicacio][7]='1';//Canal 1
								else Ethernet_buffer_local[aplicacio][7]='2';//Canal 2
								Ethernet_buffer_local[aplicacio][8]=5;
								Ethernet_buffer_local[aplicacio][9]=tipus+0x30;
								Ethernet_buffer_local[aplicacio][10]=',';
								Ethernet_buffer_local[aplicacio][11]=pulsadors+0x30;
								Ethernet_buffer_local[aplicacio][12]=',';
								if(pulsadors == 2)Ethernet_buffer_local[aplicacio][13]='V';//Simbolo
								else if (pulsadors == 1)Ethernet_buffer_local[aplicacio][13]='+';//Simbolo
								else if (pulsadors == 8)Ethernet_buffer_local[aplicacio][13]='-';//Simbolo
								else if (pulsadors == 4)Ethernet_buffer_local[aplicacio][13]='F';//Simbolo
								else if (pulsadors == 16){Ethernet_buffer_local[aplicacio][11]='5';Ethernet_buffer_local[aplicacio][13]='X';}//Simbolo
								else Ethernet_buffer_local[aplicacio][13]='?';//Simbolo
								Ethernet_buffer_local[aplicacio][14]=3;
								aplicacio_actual = aplicacio;
								memcpy(&RemoteMAC, &aplicacio_MAC[aplicacio], 6);
								memcpy(&RemoteIP, &aplicacio_IP[aplicacio], 4);
								if(!esperant_ACK[aplicacio_actual])//Per fer l'enviament més ràpid
								{
									Enviar_Ethernet((uint8_t *)Ethernet_buffer_local[aplicacio],15);
									esperant_ACK[aplicacio_actual]=0;
								}
								Enviar_Ethernet((uint8_t *)Ethernet_buffer_local[aplicacio],15);
							}
						}
						TCPLocalPort=TCP_PORT_HTTP;
						memcpy(&RemoteMAC, &web_MAC, 6);              // save opponents MAC and IP
						memcpy(&RemoteIP, &web_IP, 4);                // for later use
					}
				}
			}
		}
		else
		{
			counter_error_CRC++;
			return;//Error de CRC
		}
	}
	//------------------------------GETMEM-----------------------------
	else if((frameID&0xF)==0b011)//Getmem
	{
		uint8_t posicio_final = 0;
		if(port<2)
		{
			if(malloc_buffer[node_actual -1])
			{//Si es el setmem de broadcast no entres
				posicio_final = send_buffer[node_actual-1][0];
				for(j=0;j<=(MAX_DADES_NODE-1)-(posicio_final+1);j++)
				{//Shift left
					send_buffer[node_actual-1][j]=send_buffer[node_actual-1][posicio_final+1+j];
					send_buffer[node_actual-1][posicio_final+1+j] = 0;
				}
				malloc_buffer[node_actual-1]=1;
			}
		}
		else
		{
			if(malloc_buffer_2[node_actual_2 -1])
			{//Si es el setmem de broadcast no entres
				posicio_final = send_buffer_2[node_actual_2-1][0];
				for(j=0;j<=(MAX_DADES_NODE-1)-(posicio_final+1);j++)
				{//Shift left
					send_buffer_2[node_actual_2-1][j]=send_buffer_2[node_actual_2-1][posicio_final+1+j];
					send_buffer_2[node_actual_2-1][posicio_final+1+j] = 0;
				}
				malloc_buffer_2[node_actual_2-1]=1;
			}
		}
		if(getmem_aplicacio)
		{
			TCPLocalPort=PORT_COMUNICACIO;
			aplicacio_actual = getmem_aplicacio-1;
			memcpy(&RemoteMAC, &aplicacio_MAC[getmem_aplicacio-1], 6);
			memcpy(&RemoteIP, &aplicacio_IP[getmem_aplicacio-1], 4);
			Ethernet_buffer_local[getmem_aplicacio-1][0]=2;
			Ethernet_buffer_local[getmem_aplicacio-1][1]='8';
			Ethernet_buffer_local[getmem_aplicacio-1][2]=5;
			itoa(nodeID,byte,10);
			j=3;
			if(byte[2]==0&&byte[1]!=0&&byte[0]!=0)
			{
				Ethernet_buffer_local[getmem_aplicacio-1][j]=byte[0];
				Ethernet_buffer_local[getmem_aplicacio-1][j+1]=byte[1];
				j+=2;
			}
			else if(byte[2]==0&&byte[1]==0&&byte[0]!=0)
			{
				Ethernet_buffer_local[getmem_aplicacio-1][j]=byte[0];
				j+=1;
			}
			else if(byte[2]!=0&&byte[1]!=0&&byte[0]!=0)
			{
				Ethernet_buffer_local[getmem_aplicacio-1][j]=byte[0];
				Ethernet_buffer_local[getmem_aplicacio-1][j+1]=byte[1];
				Ethernet_buffer_local[getmem_aplicacio-1][j+2]=byte[2];
				j+=3;
			}
			Ethernet_buffer_local[getmem_aplicacio-1][j]=5;
			j++;
			byte[2]=byte[1]=byte[0]=0;
			for(i=0;i<6;i++)
			{
				itoa(*(Buffer+2+i),byte,10);
				if(byte[2]==0&&byte[1]!=0&&byte[0]!=0)
				{
					Ethernet_buffer_local[getmem_aplicacio-1][j]=byte[0];
					Ethernet_buffer_local[getmem_aplicacio-1][j+1]=byte[1];
					j+=2;
				}
				else if(byte[2]==0&&byte[1]==0&&byte[0]!=0)
				{
					Ethernet_buffer_local[getmem_aplicacio-1][j]=byte[0];
					j+=1;
				}
				else if(byte[2]!=0&&byte[1]!=0&&byte[0]!=0)
				{
					Ethernet_buffer_local[getmem_aplicacio-1][j]=byte[0];
					Ethernet_buffer_local[getmem_aplicacio-1][j+1]=byte[1];
					Ethernet_buffer_local[getmem_aplicacio-1][j+2]=byte[2];
					j+=3;
				}
				byte[2]=byte[1]=byte[0]=0;
				if(i!=5)Ethernet_buffer_local[getmem_aplicacio-1][j]=',';
				else Ethernet_buffer_local[getmem_aplicacio-1][j]=3;
				j++;
			}
			if(!esperant_ACK[aplicacio_actual])
			{
				Enviar_Ethernet((uint8_t *)Ethernet_buffer_local[getmem_aplicacio-1],j);
				esperant_ACK[aplicacio_actual]=0;
			}
			Enviar_Ethernet((uint8_t *)Ethernet_buffer_local[getmem_aplicacio-1],j);
			getmem_aplicacio = 0;
			TCPLocalPort=TCP_PORT_HTTP;
		    memcpy(&RemoteMAC, &web_MAC, 6);              // save opponents MAC and IP
		    memcpy(&RemoteIP, &web_IP, 4);                // for later use
		}
		//Trama per la pagina web
		//Al obrir la pàgina del display es fa getmem de l'adreça FIRM_VERSION
		//quan es rep ACK de l'adreça FIRM_VERSION es fa getmem de l'adreça NUM_SERIE_4
		//quan es rep ACK de l'adreça NUM_SERIE_4 s'actualitza la pàgina del display.
		else if (getmem_adress == FIRM_VERSION)
		{
			sprintf(firm_ware,"%02x",*(Buffer+2));
			sprintf(data_fabricacio,"%04x",*(Buffer+3) | *(Buffer+4));
			if(StatePagina==ESPERA_DISPLAY || StatePagina == ESPERA_LC)getmem(nodeID,NUM_SERIE_4,1);
			else if(StatePagina==ESPERA_DISPLAY_2 || StatePagina == ESPERA_LC_2)getmem(nodeID,NUM_SERIE_4,2);
		}
		else if (getmem_adress == NUM_SERIE_4)
		{
			//numero_serie_buffer = *Buffer | *(Buffer+1) | *(Buffer+2) | *(Buffer+3);
			//getmem(nodeID,TEC);
			sprintf(numero_serie,"%08x",*(Buffer+2) | *(Buffer+3) | *(Buffer+4) | *(Buffer+5));
			if(StatePagina==ESPERA_DISPLAY)actualitza_display = TRUE;//Actualitzes pàgina display
			else if(StatePagina==ESPERA_DISPLAY_2)actualitza_display_2 = TRUE;//Actualitzes pàgina display
			else if(StatePagina==ESPERA_LC)getmem(nodeID,DISPLAY,1);
			else if(StatePagina==ESPERA_LC_2)getmem(nodeID,DISPLAY,2);
			//actualitza_display = TRUE;
		}
		else if (getmem_adress == DISPLAY)
		{
			sprintf(configurats_LC,"%03d",*(Buffer+2));
			sprintf(detectats_LC,"%03d",*(Buffer+3));
			sprintf(error_LC,"%01d",*(Buffer+4));
			sprintf(retard_LC,"%03d",*(Buffer+6));
			if(StatePagina == ESPERA_LC)actualitza_display = TRUE;
			else if(StatePagina == ESPERA_LC_2)actualitza_display_2 = TRUE;
		}
	}
	//------------------------------SETMEM-----------------------------
	else if((frameID&0xF)==0b010)//Setmem
	{
		uint8_t posicio_final = 0;
		if(port<2)
		{
			if(malloc_buffer[node_actual -1])
			{//Si es el setmem de broadcast no entres
				posicio_final = send_buffer[node_actual-1][0];
				for(j=0;j<=(MAX_DADES_NODE-1)-(posicio_final+1);j++)
				{//Shift left
					send_buffer[node_actual-1][j]=send_buffer[node_actual-1][posicio_final+1+j];
					send_buffer[node_actual-1][posicio_final+1+j] = 0;
				}
				malloc_buffer[node_actual-1]=1;
			}
		}
		else
		{
			if(malloc_buffer_2[node_actual_2 -1])
			{//Si es el setmem de broadcast no entres
				posicio_final = send_buffer_2[node_actual_2-1][0];
				for(j=0;j<=(MAX_DADES_NODE-1)-(posicio_final+1);j++)
				{//Shift left
					send_buffer_2[node_actual_2-1][j]=send_buffer_2[node_actual_2-1][posicio_final+1+j];
					send_buffer_2[node_actual_2-1][posicio_final+1+j] = 0;
				}
				malloc_buffer_2[node_actual_2-1]=1;
			}
		}
	}
	else if((frameID&0xF)==0b001)//Programacio
	{
		if(port<2)//CANAL 1
		{
			if(StateUART_canal1!=PROGRAMACIO)return;
			if((frameID&0xF0)>>4==1)//ACK1
			{
				if(peticio_programacio==2)//Programacio tipus 2
				{
					ultim_node_programat = nodeID;
					new_adress_programacio2_canal1 = nodeID;
					taula_SD[nodeID-1]=*(Buffer + 2);//Guardar tipus de node a adreça nodeID.
					if (nodeID>0) programacio(2,0,nodeID+1,GUARDAR_NODE_ID,0,0,port);
				}
				else if(peticio_programacio==1)//Programacio tipus 1
				{
					msTicks_programacio1 = 1000;
					if (reprogramar==FALSE)
					{
						taula_SD[old_adress_programacio1_canal1-1]=0;
						peticio_programacio = 0;
						old_adress_programacio1_canal1 = 0;
						new_adress_programacio1_canal1 = 0;
						taula_SD[nodeID-1]=*(Buffer+2);
						BlockDevWrite(ADRESS_TAULA_SD,taula_SD,250);
						for(i=0;i<NUM_NODES;i++)taula_configurats[i]=0;
						for(i=0;i<NUM_NODES_2;i++)taula_configurats_2[i]=0;
						NUM_NODES=NUM_NODES_2=0;
						ini_taula_configurats();
						StateUART_canal1=FINAL;//potser es necessita retard
					}
					else if (reprogramar)
					{
						if(node_a_desprogramar > ultim_node_programat)
						{
							reprogramar = FALSE;
							old_adress_programacio1_canal1 = 0;
							new_adress_programacio1_canal1 = 0;
							programacio(2,0,primer_node_desprogramat,GUARDAR_NODE_ID,0,0,port);
						}
						if(node_a_desprogramar<=ultim_node_programat)
						{
							programacio(1,node_a_desprogramar,0,GUARDAR_NODE_ID,0,0,port);
							node_a_desprogramar++;
						}
					}
				}
			}
			else if((frameID&0xF0)>>4==2)//ACK2
			{
				reprogramar = TRUE;
				primer_node_desprogramat = nodeID;
				programacio(1,nodeID,0,GUARDAR_NODE_ID,0,0,port); //Posem a adreça zero al node que ha pulsat
				node_a_desprogramar = nodeID + 1;
			}
		}
		else//CANAL 2
		{
			if(StateUART_canal2!=PROGRAMACIO)return;
			if((frameID&0xF0)>>4==1)//ACK1
			{
				if(peticio_programacio_2==2)//Programacio tipus 2
				{
					ultim_node_programat = nodeID;
					new_adress_programacio2_canal2 = nodeID;
					taula_SD_2[nodeID-1]=*(Buffer + 2);//Guardar tipus de node a adreça nodeID.
					if (nodeID>0) programacio(2,0,nodeID+1,GUARDAR_NODE_ID,0,0,port);
				}
				else if(peticio_programacio_2==1)//Programacio tipus 1
				{
					msTicks_programacio1 = 1000;
					if (reprogramar==FALSE)
					{
						taula_SD_2[old_adress_programacio1_canal2-1]=0;
						peticio_programacio_2 = 0;
						old_adress_programacio1_canal2 = 0;
						new_adress_programacio1_canal2 = 0;
						taula_SD_2[nodeID-1]=*(Buffer+2);
						BlockDevWrite(ADRESS_TAULA_SD_2,taula_SD_2,250);
						for(i=0;i<NUM_NODES;i++)taula_configurats[i]=0;
						for(i=0;i<NUM_NODES_2;i++)taula_configurats_2[i]=0;
						NUM_NODES=NUM_NODES_2=0;
						ini_taula_configurats();
						StateUART_canal2=FINAL;//potser es necessita retard
					}
					else if (reprogramar)
					{
						if(node_a_desprogramar > ultim_node_programat)
						{
							reprogramar = FALSE;
							old_adress_programacio1_canal2 = 0;
							new_adress_programacio1_canal2 = 0;
							programacio(2,0,primer_node_desprogramat,GUARDAR_NODE_ID,0,0,port);
						}
						if(node_a_desprogramar<=ultim_node_programat)
						{
							programacio(1,node_a_desprogramar,0,GUARDAR_NODE_ID,0,0,port);
							node_a_desprogramar++;
						}
					}
				}
			}
			else if((frameID&0xF0)>>4==2)//ACK2
			{
				reprogramar = TRUE;
				primer_node_desprogramat = nodeID;
				programacio(1,nodeID,0,GUARDAR_NODE_ID,0,0,port); //Posem a zero el node clicat
				node_a_desprogramar = nodeID + 1;
			}
		}

	}
}

//Mode test que es pot activar si la constant  PICK_SIMULATOR és 1
void checkTestMode(uint8_t port, uint8_t node, char tecla, char DPW_char){
	uint8_t cadena_display[12];
	//DPW_Char conté una lletra identificativa.
	//Només mirem el contingut de una lletra i amb això diferenciem els codis llegits
	//Tecla V - Borra els missatges
	if(tecla==2) { //Tecla V
		//Setmem de TEXT
		setmem_display(node,0,1,cadena_display,0,1); //Enviem text per borrar pantalla
		//R,G,B,Intermitència
		//Intermitència--> 0=sense, 1=0`25s, 2=0`5s, 4=1s.
		cadena_display[1]=0;
		//EEPROM,Key beep on,make beep
		cadena_display[0]=2;
		//Setmem de BUZZER+LEDS
		setmem(node,BUZZER,2,cadena_display,1,1);
	}else if(DPW_char=='1'){
		cadena_display[0]='1';//Setmem de TEXT
		setmem_display(1,0,1,cadena_display,0,1); //Enviem text al node 1 per mostrar pantalla
		cadena_display[0]='3';//Setmem de TEXT
		setmem_display(3,0,1,cadena_display,0,1); //Enviem text al node 3 per mostrar pantalla
		//cadena_display[1]=0b00010010; //R,G,B,Intermitència //Intermitència--> 0=sense, 1=0`25s, 2=0`5s, 4=1s.
		cadena_display[1]=0b00000010; //R,G,B,Intermitència //Intermitència--> 0=sense, 1=0`25s, 2=0`5s, 4=1s.
		cadena_display[0]=1; //EEPROM,Key beep on,make beep
		setmem(1,BUZZER,2,cadena_display,1,1); //Setmem de BUZZER+LEDS
		setmem(3,BUZZER,2,cadena_display,1,1); //Setmem de BUZZER+LEDS
	}else if(DPW_char=='2'){
		cadena_display[0]='F';//Setmem de TEXT
		cadena_display[1]=' ';//Setmem de TEXT
		cadena_display[3]='2';//Setmem de TEXT
		setmem_display(2,0,4,cadena_display,0,1); //Enviem text al node 1 per mostrar pantalla
		cadena_display[0]='1';//Setmem de TEXT
		cadena_display[0]='2';//Setmem de TEXT
		setmem_display(3,0,1,cadena_display,0,1); //Enviem text al node 3 per mostrar pantalla
		//cadena_display[1]=0b00010010; //R,G,B,Intermitència //Intermitència--> 0=sense, 1=0`25s, 2=0`5s, 4=1s.
		cadena_display[1]=0b00000100; //R,G,B,Intermitència //Intermitència--> 0=sense, 1=0`25s, 2=0`5s, 4=1s.
		cadena_display[0]=1; //EEPROM,Key beep on,make beep
		setmem(2,BUZZER,2,cadena_display,1,1); //Setmem de BUZZER+LEDS
		setmem(3,BUZZER,2,cadena_display,1,1); //Setmem de BUZZER+LEDS
	}else if(DPW_char=='3'){
		cadena_display[0]='u';//Setmem de TEXT
		cadena_display[1]='3';//Setmem de TEXT
		setmem_display(1,0,2,cadena_display,0,1); //Enviem text al node 1 per mostrar pantalla
		cadena_display[0]='2';//Setmem de TEXT
		setmem_display(2,0,2,cadena_display,0,1); //Enviem text al node 3 per mostrar pantalla
		setmem_display(3,0,2,cadena_display,0,1); //Enviem text al node 3 per mostrar pantalla
		//cadena_display[1]=0b00010010; //R,G,B,Intermitència //Intermitència--> 0=sense, 1=0`25s, 2=0`5s, 4=1s.
		cadena_display[1]=0b00000101; //R,G,B,Intermitència //Intermitència--> 0=sense, 1=0`25s, 2=0`5s, 4=1s.
		cadena_display[0]=1; //EEPROM,Key beep on,make beep
		setmem(1,BUZZER,2,cadena_display,1,1); //Setmem de BUZZER+LEDS
		setmem(2,BUZZER,2,cadena_display,1,1); //Setmem de BUZZER+LEDS
		cadena_display[1]=0b00010010; //R,G,B,Intermitència //Intermitència--> 0=sense, 1=0`25s, 2=0`5s, 4=1s.
		setmem(3,BUZZER,2,cadena_display,1,1); //Setmem de BUZZER+LEDS
	}else if(DPW_char=='o'){
		cadena_display[0]='T';cadena_display[1]='E';cadena_display[2]='C';cadena_display[3]='N';
		cadena_display[4]='Y';cadena_display[5]='S';cadena_display[6]='T';cadena_display[7]='A';
		cadena_display[8]='N';cadena_display[9]='D';
		setmem_display(BROADCAST_ADRESS,0,10,cadena_display,1,1); //Enviem text al node 1 per mostrar pantalla
		//setmem_display(2,0,10,cadena_display,0,1); //Enviem text al node 2 per mostrar pantalla
		//setmem_display(3,0,10,cadena_display,0,1); //Enviem text al node 3 per mostrar pantalla
		cadena_display[1]=0b00010010; //R,G,B,Intermitència //Intermitència--> 0=sense, 1=0`25s, 2=0`5s, 4=1s.
		cadena_display[0]=1; //EEPROM,Key beep on,make beep
		setmem(1,BUZZER,2,cadena_display,1,1); //Setmem de BUZZER+LEDS
		cadena_display[1]=0b00010001; //R,G,B,Intermitència //Intermitència--> 0=sense, 1=0`25s, 2=0`5s, 4=1s.
		setmem(2,BUZZER,2,cadena_display,1,1); //Setmem de BUZZER+LEDS
		cadena_display[1]=0b00010011; //R,G,B,Intermitència //Intermitència--> 0=sense, 1=0`25s, 2=0`5s, 4=1s.
		setmem(3,BUZZER,2,cadena_display,1,1); //Setmem de BUZZER+LEDS
	}
}

/******************************************************************************
**                            End Of File
******************************************************************************/
