/*****************************************************************************
 *   rtc.h: RTC H file for NXP LPC17xx Family Microprocessors
 *
 *   Copyright(C) 2008, NXP Semiconductor
 *   All rights reserved.
 *
 *   History
 *   2009.05.28  ver 1.00    Prelimnary version, first Release
 *
******************************************************************************/

#ifndef __RTC_H
#define __RTC_H


/* PUBLIC DEFINITIONS */
/* Definitions needed to calculate the calibration value and direction */
#define IDEAL_RTC_FREQ		32768000	//in mHz (milli Hz)
	//#define ACTUAL_RTC_FREQ		32768000	//Ideal freq test
	/* For the RTC demo we choose to make an adjustment every 3 seconds */
	#define ACTUAL_RTC_FREQ 32768000 * 1.3	//in mHz Backwards every 3 seconds
	//#define ACTUAL_RTC_FREQ 32768000 * 0.7 //in mHz Forwards every 3 seconds
	//#define ACTUAL_RTC_FREQ 32768000 - 251 //in mHz closest allowable forward correction
	//#define ACTUAL_RTC_FREQ 32768000 + 251 //in mHz closest allowable backward correction



/* PRIVATE DEFINITIONS */
/* # of bits used in the Calibration value */
#define CALIBRATION_VALUE_SIZE	17
/* If the actual frequency is within +/- of this value,
	calibration should be disabled because of overflow */			
#define MAX_DELTA_FREQUENCY_VALUE	(IDEAL_RTC_FREQ / ((1 << CALIBRATION_VALUE_SIZE) - 1))

/* To be used in the Calibration Register (CALIBRATION) */
#define CALDIR(n)	(n << 17)	//1 = Backward, 0 = Forward

/* To be used with the Clock Control Register (CCR) */
#define CLKEN	(1<<0)
#define CTCRST	(1<<1)
#define CCALEN	(1<<4)

void InitRTC(void);
void StartRTC(void);

#endif	/* __RTC_H */
