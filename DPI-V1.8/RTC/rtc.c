/*****************************************************************************
 *   rtc.c: RTC C file for NXP LPC17xx Family Microprocessors
 *
 *   Copyright(C) 2008, NXP Semiconductor
 *   All rights reserved.
 *
 *   History
 *   2009.05.28  ver 1.00    Prelimnary version, first Release
 *
******************************************************************************/

#include "LPC17xx.h"
#include "type.h"
#include "rtc.h"
#include <stdio.h>
/*****************************************************************************
** Function name:		init_calibration_val_dir
**
** Descriptions:		Function calculates the required calibration value and
**						direction values for the calibration register.
**						If the actual_rtc_frequency is within 
**						+/- (32.768kHz / (2^17 - 1)) == +/- 0.250 Hz, then disable
**						the auto calibration due to RTCAL overflow.
**						Else, save the proper RTC calibration value and direction.
**
** parameters:			The actual RTC oscilating frequency should be passed
**						in mHz (milli Hz).			 
** 						
** Returned value:		None
** 
*****************************************************************************/
void init_calibration_val_dir(uint32_t actual_rtc_frequency) {
	uint32_t	calibration_register = 0x00000000;
	int32_t		rtc_frequency_delta = (IDEAL_RTC_FREQ - actual_rtc_frequency);

	/* Check for valid RTC frequency */
	/* Disable calibration if the correction value is +/- .250Hz offset
		It would otherwise cause an overflow in RTCCAL */
	if((rtc_frequency_delta <= MAX_DELTA_FREQUENCY_VALUE) && 
		(rtc_frequency_delta >= -MAX_DELTA_FREQUENCY_VALUE)){
		 LPC_RTC->CCR |= CCALEN;

	/* Determine the calibration direction CALDIR and CALVAL*/
	}else if(			rtc_frequency_delta > MAX_DELTA_FREQUENCY_VALUE){
		/* Forward direction */
		/* CALVAL */
		calibration_register = IDEAL_RTC_FREQ / 
			(IDEAL_RTC_FREQ - actual_rtc_frequency);	

		/* CALDIR */
		calibration_register &= ~CALDIR(0);	
		
		/* Enable calibration */
		LPC_RTC->CCR &= ~CCALEN;				
	
	}else{		   									 					
		/* Backward direction */
		/* CALVAL */
		calibration_register = IDEAL_RTC_FREQ / 
			(actual_rtc_frequency - IDEAL_RTC_FREQ);

		/* CALDIR */
		calibration_register |= CALDIR(1);	
		
		/* Enable calibration */
		LPC_RTC->CCR &= ~CCALEN;
	
	}

	/* Save new calibration register value */
	LPC_RTC->CALIBRATION = calibration_register;	

	return;
}

/*****************************************************************************
** Function name:		InitRTC
**
** Descriptions:		Disables time counters in the Clock Control Register
**						Resets the time counters to 0
**						Enables the calibration counter
**
** parameters:			None			 
** 						
** Returned value:		None
** 
*****************************************************************************/
void InitRTC(void) {
	
	/* Disable RTC Clock, Reset Clock, Disable Calibration */
	LPC_RTC->CCR  =	( (!CLKEN) |  CTCRST );

	/* Set Calibration value and direction */
	init_calibration_val_dir(ACTUAL_RTC_FREQ);

	/* Disable Clock Reset */	
	LPC_RTC->CCR &= ~CTCRST;

	/* Clear RTC DOY, HOUR, MIN and SEC registers */
	LPC_RTC->DOY  = 0;
	LPC_RTC->HOUR = 0;
	LPC_RTC->MIN  = 0;
	LPC_RTC->SEC  = 0;

	return;
}

/*****************************************************************************
** Function name:		StartRTC
**
** Descriptions:		Enables the counter to increment every second
**
** parameters:			None			 
** 						
** Returned value:		None
** 
*****************************************************************************/
void StartRTC(void) {

	/* Assuming the RTC has already been initialized */
	LPC_RTC->CCR |=	( CLKEN );	

	return;
}

/*****************************************************************************
** Function name:		StopRTC
**
** Descriptions:		Stops the RTC from incrementing
**
** parameters:			None			 
** 						
** Returned value:		None
** 
*****************************************************************************/
void StopRTC(void) {

	/* Stopping RTC */
	LPC_RTC->CCR &=	( !CLKEN );	

	return;
}
