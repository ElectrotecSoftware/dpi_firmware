/******************************************************************
 *****                                                        *****
 *****  Name: easyweb.c                                       *****
 *****  Ver.: 1.0                                             *****
 *****  Date: 07/05/2001                                      *****
 *****  Auth: Andreas Dannenberg                              *****
 *****        HTWK Leipzig                                    *****
 *****        university of applied sciences                  *****
 *****        Germany                                         *****
 *****        adannenb@et.htwk-leipzig.de                     *****
 *****  Func: implements a dynamic HTTP-server by using       *****
 *****        the easyWEB-API                                 *****
 *****  Rem.: In IAR-C, use  linker option                    *****
 *****        "-e_medium_write=_formatted_write"              *****
 *****                                                        *****
 ******************************************************************/

// Modifications by Code Red Technologies for NXP LPC1768

// CodeRed - removed header for MSP430 microcontroller
//#include "msp430x14x.h"

#include "LPC17xx.h"
#include "type.h"
#include "webserver.h"
#include "lcd.h"
#include "md5.h"
#include "rtc.h"
#include "trames.h"
#include "uart.h"
#include "all.h"
// CodeRed - removed header for original ethernet controller
//#include "cs8900.c"                              // ethernet packet driver

//CodeRed - added for LPC ethernet controller
//#include "ethmac.h"

// CodeRed - include .h rather than .c file
// #include "tcpip.c"                               // easyWEB TCP/IP stack
#include "tcpip.h"                               // easyWEB TCP/IP stack

// CodeRed - added NXP LPC register definitions header

#include "nodes.h"

#include "blockdev.h"
#include "spi.h"


// CodeRed - include renamed .h rather than .c file
// #include "webside.c"                             // webside for our HTTP server (HTML)
#include "webside.h"                             // webside for our HTTP server (HTML)
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//extern volatile uint32_t msTicks;


const unsigned char GetResponse_favicon[] =              // 1st thing our server sends to a client
{
  "HTTP/1.1 200 OK\r\n"                          // protocol ver 1.0, code 200, reason OK
  "Content-Type: image/x-icon\r\n"                  // type of data we want to send
  "Cache-Control: max-age=3600\r\n"
  "Connection: close\r\n"
  "Content-length: 318\r\n"
  "\r\n"                                         // indicate end of HTTP-header
};
const unsigned char GetResponse_image[] =              // 1st thing our server sends to a client
{
  "HTTP/1.1 200 OK\r\n"                          // protocol ver 1.0, code 200, reason OK
  "Content-Type: image/jpeg\r\n"                  // type of data we want to send
  "Connection: close\r\n"
  "\r\n"                                         // indicate end of HTTP-header
};
const unsigned char GetResponse[] =              // 1st thing our server sends to a client
{
  "HTTP/1.1 200 OK\r\n"                          // protocol ver 1.0, code 200, reason OK
  "Content-Type: text/html\r\n"                  // type of data we want to send
  "Connection: close\r\n"
  "\r\n"                                         // indicate end of HTTP-header
};
const unsigned char GetResponse_petit[] =              // 1st thing our server sends to a client
{
  "HTTP/1.1 200 OK\r\n"                          // protocol ver 1.0, code 200, reason OK
  "Content-Type: text/html\r\n"                  // type of data we want to send
  "Connection: close\r\n"
                                          // indicate end of HTTP-header
};



unsigned char valor_actualitzat[1];
volatile unsigned int aaScrollbar = 400;
char explore_node[3] = "001";
// This function implements a very simple dynamic HTTP-server.
// It waits until connected, then sends a HTTP-header and the
// HTML-code stored in memory. Before sending, it replaces
// some special strings with dynamic values.
// NOTE: For strings crossing page boundaries, replacing will
// not work. In this case, simply add some extra lines
// (e.g. CR and LFs) to the HTML-code.
void HTTPServer(SStatePagina Pagina)
{
  if (SocketStatus & SOCK_CONNECTED)             // check if somebody has connected to our TCP
  {
    if (SocketStatus & SOCK_DATA_AVAILABLE)      // check if remote TCP sent data
      TCPReleaseRxBuffer();                      // and throw it away

    if (SocketStatus & SOCK_TX_BUF_RELEASED)     // check if buffer is free for TX
    {
      if (!(HTTPStatus & HTTP_SEND_PAGE))        // init byte-counter and pointer to webside
      {                                          // if called the 1st time
          if (sesio_iniciada)
          {
        	if (Pagina == MENU)PProgram = (unsigned char *)Menu, HTTPBytesToSend = sizeof(Menu) - 1;
          	else if (Pagina == OPERATION) PProgram = (unsigned char *)Operation, HTTPBytesToSend = sizeof(Operation) - 1;
          	else if (Pagina == INICI_WEB) PProgram = (unsigned char *)Inici, HTTPBytesToSend = sizeof(Inici) - 1 +sizeof(GetResponse_petit) - 1;
          	else if (Pagina == IMATGE) PProgram = (unsigned char *)Imatge, HTTPBytesToSend = sizeof(Imatge);
          	else if (Pagina == CAN_1) PProgram = (unsigned char *)Can1, HTTPBytesToSend = sizeof(Can1) - 1;
          	else if (Pagina == CAN_2) PProgram = (unsigned char *)Can1, HTTPBytesToSend = sizeof(Can1) - 1;
          	else if (Pagina == DISPLAY_) PProgram = (unsigned char *)Display, HTTPBytesToSend = sizeof(Display) - 1;
          	else if (Pagina == DISPLAY_2) PProgram = (unsigned char *)Display, HTTPBytesToSend = sizeof(Display) - 1;
          	else if (Pagina == ALARMS) PProgram = (unsigned char *)Alarms, HTTPBytesToSend = sizeof(Alarms) - 1;
          	else if (Pagina == CONFIGURACION_ETHERNET) PProgram = (unsigned char *)Configuracion_ethernet, HTTPBytesToSend = sizeof(Configuracion_ethernet)-1;
          	else if (Pagina == DIRECCION_VARIOS_NODOS) PProgram = (unsigned char *)Direccion_varios_nodos, HTTPBytesToSend = sizeof(Direccion_varios_nodos)-1;
          	else if (Pagina == DIRECCION_UN_NODO) PProgram = (unsigned char *)Direccion_un_nodo, HTTPBytesToSend = sizeof(Direccion_un_nodo)-1;
          	else if (Pagina == RESUMEN_OPERATION) PProgram = (unsigned char *)Resumen_operation, HTTPBytesToSend = sizeof(Resumen_operation)-1;
          	else if (Pagina == CANVI_USUARI) PProgram = (unsigned char *)Canvi_usuari, HTTPBytesToSend = sizeof(Canvi_usuari)-1;
          	else if (Pagina == NODOS_ARRANQUE) PProgram = (unsigned char *)Nodos_arranque, HTTPBytesToSend = sizeof(Nodos_arranque)-1;
          	else if (Pagina == RS232) PProgram = (unsigned char *)RS232_pagina, HTTPBytesToSend = sizeof(RS232_pagina)-1;
          	else if (Pagina == LC) PProgram = (unsigned char *)LC_pagina, HTTPBytesToSend = sizeof(LC_pagina)-1;
          	else if (Pagina == LC_2) PProgram = (unsigned char *)LC_pagina, HTTPBytesToSend = sizeof(LC_pagina)-1;
          	else if (Pagina == CONFIGURACIO_GENERAL){ PProgram = (unsigned char *)Configuracion_general; HTTPBytesToSend = sizeof(Configuracion_general)-1;}
          	else if (Pagina == ERROR)PProgram = (unsigned char *)Error, HTTPBytesToSend = sizeof(Error)-1;
          }
          else
		  {
        	if (Pagina == IMATGE) PProgram = (unsigned char *)Imatge, HTTPBytesToSend = sizeof(Imatge);
        	else if (Pagina == MENU) PProgram = (unsigned char *)Password_inicial, HTTPBytesToSend = sizeof(Password_inicial) - 1;
        	else if (Pagina == ERROR)PProgram = (unsigned char *)Error, HTTPBytesToSend = sizeof(Error)-1;
		  }
      }

      if (HTTPBytesToSend > MAX_TCP_TX_DATA_SIZE)     // transmit a segment of MAX_SIZE
      {
        if (!(HTTPStatus & HTTP_SEND_PAGE))           // 1st time, include HTTP-header
        {
        	if (Pagina != IMATGE)
        	{
        		memcpy(TCP_TX_BUF, GetResponse, sizeof(GetResponse) - 1);
				memcpy(TCP_TX_BUF + sizeof(GetResponse) - 1, PProgram, MAX_TCP_TX_DATA_SIZE - sizeof(GetResponse) + 1);
				HTTPBytesToSend -= MAX_TCP_TX_DATA_SIZE - sizeof(GetResponse) + 1;
				PProgram += MAX_TCP_TX_DATA_SIZE - sizeof(GetResponse) + 1;
        	}
        	else
        	{
        		memcpy(TCP_TX_BUF, GetResponse_image, sizeof(GetResponse_image) - 1);
				memcpy(TCP_TX_BUF + sizeof(GetResponse_image) - 1, PProgram, MAX_TCP_TX_DATA_SIZE - sizeof(GetResponse_image) + 1);
				HTTPBytesToSend -= MAX_TCP_TX_DATA_SIZE - sizeof(GetResponse_image) + 1;
				PProgram += MAX_TCP_TX_DATA_SIZE - sizeof(GetResponse_image) + 1;
        	}
        }
        else
        {
			memcpy(TCP_TX_BUF, PProgram, MAX_TCP_TX_DATA_SIZE);
			HTTPBytesToSend -= MAX_TCP_TX_DATA_SIZE;
			PProgram += MAX_TCP_TX_DATA_SIZE;
        }
        TCPTxDataCount = MAX_TCP_TX_DATA_SIZE;   // bytes to xfer
        InsertDynamicNodes();					 // exchange some strings...
        TCPTransmitTxBuffer();                   // xfer buffer
        if(HTTPBytesToSend>=MAX_TCP_TX_DATA_SIZE)
        {
        	  two_sending_TCP = TRUE;
			  memcpy(TCP_TX_BUF_3, PProgram, MAX_TCP_TX_DATA_SIZE);
			  HTTPBytesToSend -= MAX_TCP_TX_DATA_SIZE;
			  PProgram += MAX_TCP_TX_DATA_SIZE;
			  SocketStatus |= SOCK_TX_BUF_RELEASED;
			  TCPTxDataCount = MAX_TCP_TX_DATA_SIZE;   // bytes to xfer
			  InsertDynamicNodes();					 // exchange some strings...
			  comptador_TCP++;
			  TCPTransmitTxBuffer();                   // xfer buffer
			  comptador_TCP=0;
  	    }
      }
      else if (HTTPBytesToSend)                  // transmit leftover bytes
      {
    	two_sending_TCP = FALSE;
        memcpy(TCP_TX_BUF, PProgram, HTTPBytesToSend);
        TCPTxDataCount = HTTPBytesToSend;        // bytes to xfer
        InsertDynamicNodes();					 // exchange some strings...
        TCPTransmitTxBuffer();                   // send last segment
        //TCPClose();                              // and close connection
        HTTPBytesToSend = 0;                     // all data sent
        conexio_oberta = FALSE;
      }
      HTTPStatus |= HTTP_SEND_PAGE;              // ok, 1st loop executed
    }
  }
  else
    HTTPStatus &= ~HTTP_SEND_PAGE;               // reset help-flag if not connected
}
void HTTPServer_petit(SStatePagina Pagina)
{

  if (SocketStatus & SOCK_CONNECTED)             // check if somebody has connected to our TCP
  {
    if (SocketStatus & SOCK_DATA_AVAILABLE)      // check if remote TCP sent data
      TCPReleaseRxBuffer();                      // and throw it away

    if (SocketStatus & SOCK_TX_BUF_RELEASED)     // check if buffer is free for TX
    {
      if (!(HTTPStatus & HTTP_SEND_PAGE))        // init byte-counter and pointer to webside
      {                                          // if called the 1st time
			if (Pagina == ACTUALITZA)PProgram = (unsigned char *)Actualitza, HTTPBytesToSend = sizeof(Actualitza)-1+sizeof(GetResponse_petit) - 1;
			else if (Pagina == ACTUALITZA_DISPLAY) PProgram = (unsigned char *)Actualitza_display, HTTPBytesToSend = sizeof(Actualitza_display)-1+sizeof(GetResponse_petit) - 1;
			else if (Pagina == ACTUALITZA_2)PProgram = (unsigned char *)Actualitza, HTTPBytesToSend = sizeof(Actualitza)-1+sizeof(GetResponse_petit) - 1;
			else if (Pagina == ACTUALITZA_DISPLAY_2) PProgram = (unsigned char *)Actualitza_display, HTTPBytesToSend = sizeof(Actualitza_display)-1+sizeof(GetResponse_petit) - 1;
			//else if (Pagina == CAMBIAR_LENGUAJE) PProgram = (unsigned char *)Cambiar_lenguaje, HTTPBytesToSend = sizeof(Cambiar_lenguaje)-1+sizeof(GetResponse_petit) - 1;
			else if (Pagina == BLANC) PProgram = (unsigned char *)Blanc, HTTPBytesToSend = sizeof(Blanc)-1+sizeof(GetResponse_petit) - 1;
			else if (Pagina == BLANC_2) PProgram = (unsigned char *)Blanc, HTTPBytesToSend = sizeof(Blanc)-1+sizeof(GetResponse_petit) - 1;
			else if (Pagina == BLANC_3) PProgram = (unsigned char *)Blanc, HTTPBytesToSend = sizeof(Blanc)-1+sizeof(GetResponse_petit) - 1;
			else if (Pagina == BLANC_4) PProgram = (unsigned char *)Blanc, HTTPBytesToSend = sizeof(Blanc)-1+sizeof(GetResponse_petit) - 1;
			else if (Pagina == FAVICON) PProgram = (unsigned char *)Favicon, HTTPBytesToSend = sizeof(Favicon) +sizeof(GetResponse_favicon) - 1;
			//else if (Pagina == INICI_WEB) PProgram = (unsigned char *)Inici, HTTPBytesToSend = sizeof(Inici) - 1 +sizeof(GetResponse_petit) - 1;
			else if (Pagina == ACTUALITZA_RS232) PProgram = (unsigned char *)Actualitza_node, HTTPBytesToSend = sizeof(Actualitza_node) - 1+sizeof(GetResponse_petit) - 1;
			else if (Pagina == ACTUALITZA_LC) PProgram = (unsigned char *)Actualitza_LC, HTTPBytesToSend = sizeof(Actualitza_LC) - 1+sizeof(GetResponse_petit) - 1;
			else if (Pagina == ACTUALITZA_LC_2) PProgram = (unsigned char *)Actualitza_LC, HTTPBytesToSend = sizeof(Actualitza_LC) - 1+sizeof(GetResponse_petit) - 1;
			if(Pagina!=FAVICON)
			{
				memcpy(TCP_TX_BUF, GetResponse_petit, sizeof(GetResponse_petit) - 1);
				memcpy(TCP_TX_BUF + sizeof(GetResponse_petit) - 1, PProgram, (HTTPBytesToSend+2-sizeof(GetResponse_petit)) - 1);
			}
			else
			{
				memcpy(TCP_TX_BUF, GetResponse_favicon, sizeof(GetResponse_favicon) - 1);
				memcpy(TCP_TX_BUF + sizeof(GetResponse_favicon) - 1, PProgram, (HTTPBytesToSend+2-sizeof(GetResponse_favicon)) - 1);
			}
      }
	  TCPTxDataCount = HTTPBytesToSend;        // bytes to xfer
	  InsertDynamicValues();                   // exchange some strings...
	  InsertDynamicNodes();                   // exchange some strings...
	  TCPTransmitTxBuffer();                   // send last segment
	  //TCPClose();                              // and close connection
	  HTTPBytesToSend = 0;                     // all data sent
	  conexio_oberta = FALSE;
      HTTPStatus |= HTTP_SEND_PAGE;              // ok, 1st loop executed
      if(CanviarPagina!=NO_CANVIAR_PAGINA)
      {
    	  StatePagina=CanviarPagina;
    	  CanviarPagina=NO_CANVIAR_PAGINA;
      }
    }
  }
  else
    HTTPStatus &= ~HTTP_SEND_PAGE;               // reset help-flag if not connected
}

void InsertDynamicValues(void)
{
  unsigned char *Key;
           char NewKey[50];
  unsigned int i,c;
  uint16_t node;
  if (TCPTxDataCount < 4) return;                     // there can't be any special string
  
  Key = TCP_TX_BUF;
  i = 0;
  c = 0;
  for (i = 0; i < (TCPTxDataCount - 3); i++)
  {
	  if (comprovar("CONT",Key,sizeof("CONT")))
	  {
		  if(StatePagina==ACTUALITZA)
		  {
			  for(node=0;node<MAX_NODES_CANAL;node++)
			  {
				  sprintf(NewKey, "%1c", (taula_nodes_web[node]));
				  memcpy(Key, NewKey, 1);
				  Key++;
			  }
		  }
		  else if (StatePagina==ACTUALITZA_2)
		  {
			  for(node=0;node<MAX_NODES_CANAL;node++)
			  {
				  sprintf(NewKey, "%1c", (taula_nodes_web_2[node]));
				  memcpy(Key, NewKey, 1);
				  Key++;
			  }
		  }

	  }
	  if (comprovar("DISPLAY",Key,sizeof("DISPLAY")))
	  {
		  //pulsadors
		  sprintf(NewKey, "%1c", pulsadors_display );
		  memcpy(Key, NewKey, 1);
		  Key++;
		  //byte 1
		  sprintf(NewKey, "%02x", byte1_web );
		  memcpy(Key, NewKey, 2);
		  Key+=2;
		  //byte 2
		  sprintf(NewKey, "%02x", byte2_web );
		  memcpy(Key, NewKey, 2);
		  Key+=2;
		  //lec
		  sprintf(NewKey, "%2s", "00" );
		  memcpy(Key, NewKey, 2);
		  Key+=2;
		  //error flags
		  sprintf(NewKey, "%1c", error_flags );
		  memcpy(Key, NewKey, 1);
		  Key++;
		  //remoto/local
		  sprintf(NewKey, "%1c", remoto_local );
		  memcpy(Key, NewKey, 1);
		  Key++;
		  //firmware
		  sprintf(NewKey, "%2s", firm_ware );
		  memcpy(Key, NewKey, 2);
		  Key+=2;
		  //data_fabricacio
		  sprintf(NewKey, "%4s", data_fabricacio );
		  memcpy(Key, NewKey, 4);
		  Key+=4;
		  //numero de serie
		  sprintf(NewKey, "%8s", numero_serie );
		  memcpy(Key, NewKey, 8);
		  Key+=8;
		  return;
	  }
	  if (comprovar("NODE_RS232",Key,sizeof("NODE_RS232")))
	  {
		  //firmware
		  sprintf(NewKey, "%2s", firm_ware );
		  memcpy(Key, NewKey, 2);
		  Key+=2;
		  //data_fabricacio
		  sprintf(NewKey, "%4s", data_fabricacio );
		  memcpy(Key, NewKey, 4);
		  Key+=4;
		  //numero de serie
		  sprintf(NewKey, "%8s", numero_serie );
		  memcpy(Key, NewKey, 8);
		  Key+=8;
		  for(node=0;node<bytes_RS232[0];node++)
		  {
			  sprintf(NewKey, "%1c", (bytes_RS232[node+1]));
			  memcpy(Key, NewKey, 1);
			  Key++;
		  }
		  return;
	  }
	  if (comprovar("NODE_LC",Key,sizeof("NODE_LC")))
	  {
		  //firmware
		  sprintf(NewKey, "%2s", firm_ware );
		  memcpy(Key, NewKey, 2);
		  Key+=2;
		  //data_fabricacio
		  sprintf(NewKey, "%4s", data_fabricacio );
		  memcpy(Key, NewKey, 4);
		  Key+=4;
		  //numero de serie
		  sprintf(NewKey, "%8s", numero_serie );
		  memcpy(Key, NewKey, 8);
		  Key+=8;
		  //configurados
		  sprintf(NewKey, "%3s", configurats_LC );
		  memcpy(Key, NewKey, 3);
		  Key+=3;
		  //detectados
		  sprintf(NewKey, "%3s", detectats_LC );
		  memcpy(Key, NewKey, 3);
		  Key+=3;
		  //error
		  sprintf(NewKey, "%1s", error_LC );
		  memcpy(Key, NewKey, 1);
		  Key+=1;
		  //parametros (retardo)
		  sprintf(NewKey, "%3s", retard_LC );
		  memcpy(Key, NewKey, 3);
		  Key+=3;
		  return;
	  }
    Key++;
  }
}

void InsertDynamicNodes(void)
{
  unsigned char *Key;
           char NewKey[5];
  unsigned int i;
  uint16_t posicio=0;
  uint8_t numero=0;

  if (TCPTxDataCount < 4) return;                     // there can't be any special string

  Key = TCP_TX_BUF;
  if(two_sending_TCP)Key = TCP_TX_BUF_3;//Si es el segon envio de TCP

  for (i = 0; i < (TCPTxDataCount); i++,Key++)
  {
	    if (*Key == 'A')
	         if (*(Key + 1) == 'D')
	           if (*(Key + 2) == '1')
	           {
	        	   if(StatePagina==CAN_2)
	        	   {
		        		if (NUM_NODES_2<=9)sprintf(NewKey, "%1u", NUM_NODES_2),memcpy(Key, NewKey, 1),Key++,sprintf(NewKey, "%2s", "  "),memcpy(Key, NewKey, 2);
						if (9<NUM_NODES_2&&NUM_NODES_2<=99) sprintf(NewKey, "%2u", NUM_NODES_2), memcpy(Key, NewKey, 2),Key+=2,sprintf(NewKey, "%1s", " "), memcpy(Key, NewKey, 1);
						if (99<NUM_NODES_2&&NUM_NODES_2<=999) sprintf(NewKey, "%3u", NUM_NODES_2), memcpy(Key, NewKey, 3);
	        	   }
	        	   else
	        	   {
	        		if (NUM_NODES<=9)sprintf(NewKey, "%1u", NUM_NODES),memcpy(Key, NewKey, 1),Key++,sprintf(NewKey, "%2s", "  "),memcpy(Key, NewKey, 2);
					if (9<NUM_NODES&&NUM_NODES<=99) sprintf(NewKey, "%2u", NUM_NODES), memcpy(Key, NewKey, 2),Key+=2,sprintf(NewKey, "%1s", " "), memcpy(Key, NewKey, 1);
					if (99<NUM_NODES&&NUM_NODES<=999) sprintf(NewKey, "%3u", NUM_NODES), memcpy(Key, NewKey, 3);
	        	   }
	           }
	    if (*Key == 'A')
	         if (*(Key + 1) == 'D')
	           if (*(Key + 2) == '4')
	           {
	        	   if(StatePagina==CAN_2)
	        	   {
	       	        if (nodes_correctes_web_2<=9) sprintf(NewKey, "%2s", "00"), memcpy(Key, NewKey, 2),Key+=2,sprintf(NewKey, "%1u", nodes_correctes_web_2), memcpy(Key, NewKey, 1);
	       	        if (9<nodes_correctes_web_2&&nodes_correctes_web_2<=99) sprintf(NewKey, "%1s", "0"), memcpy(Key, NewKey, 1),Key+=1,sprintf(NewKey, "%2u", nodes_correctes_web_2), memcpy(Key, NewKey, 2);
	       	        if (99<nodes_correctes_web_2&&nodes_correctes_web_2<=999) sprintf(NewKey, "%3u", nodes_correctes_web_2), memcpy(Key, NewKey, 3);
	        	   }
	        	   else
	        	   {
	        		if (nodes_correctes_web<=9) sprintf(NewKey, "%2s", "00"), memcpy(Key, NewKey, 2),Key+=2,sprintf(NewKey, "%1u", nodes_correctes_web), memcpy(Key, NewKey, 1);
					if (9<nodes_correctes_web&&nodes_correctes_web<=99) sprintf(NewKey, "%1s", "0"), memcpy(Key, NewKey, 1),Key+=1,sprintf(NewKey, "%2u", nodes_correctes_web), memcpy(Key, NewKey, 2);
					if (99<nodes_correctes_web&&nodes_correctes_web<=999) sprintf(NewKey, "%3u", nodes_correctes_web), memcpy(Key, NewKey, 3);
	        	   }
	           }
	    if (*Key == 'A')
	         if (*(Key + 1) == 'D')
	           if (*(Key + 2) == '3')
	    	{
	        		if (NUM_NODES_2<=9)sprintf(NewKey, "%1u", NUM_NODES_2),memcpy(Key, NewKey, 1),Key++,sprintf(NewKey, "%2s", "  "),memcpy(Key, NewKey, 2);
					if (9<NUM_NODES_2&&NUM_NODES_2<=99) sprintf(NewKey, "%2u", NUM_NODES_2), memcpy(Key, NewKey, 2),Key+=2,sprintf(NewKey, "%1s", " "), memcpy(Key, NewKey, 1);
					if (99<NUM_NODES_2&&NUM_NODES_2<=999) sprintf(NewKey, "%3u", NUM_NODES_2), memcpy(Key, NewKey, 3);
	    }
	    if (*Key == 'A')
	         if (*(Key + 1) == 'D')
	           if (*(Key + 2) == '2')
	    	{
	        	   if(StatePagina==CAN_1)
	        	   {
		        	   //Adreça maxima plena
		        	   for (i=0;i<MAX_NODES_CANAL;i++)
		        	   {
		        		   if(taula_nodes_web[i]!=0)maxim_full_adress=i+1;
		        	   }
		   	        if (maxim_full_adress<=9) sprintf(NewKey, "%2s", "00"), memcpy(Key, NewKey, 2),Key+=2,sprintf(NewKey, "%1u", maxim_full_adress), memcpy(Key, NewKey, 1);
		   	        if (9<maxim_full_adress&&maxim_full_adress<=99) sprintf(NewKey, "%1s", "0"), memcpy(Key, NewKey, 1),Key+=1,sprintf(NewKey, "%2u", maxim_full_adress), memcpy(Key, NewKey, 2);
		   	        if (99<maxim_full_adress&&maxim_full_adress<=999) sprintf(NewKey, "%3u", maxim_full_adress), memcpy(Key, NewKey, 3);
	        	   }
	        	   else if (StatePagina==CAN_2)
	        	   {
		        	   //Adreça maxima plena
		        	   for (i=0;i<MAX_NODES_CANAL;i++)
		        	   {
		        		   if(taula_nodes_web_2[i]!=0)maxim_full_adress=i+1;
		        	   }
		   	        if (maxim_full_adress<=9) sprintf(NewKey, "%2s", "00"), memcpy(Key, NewKey, 2),Key+=2,sprintf(NewKey, "%1u", maxim_full_adress), memcpy(Key, NewKey, 1);
		   	        if (9<maxim_full_adress&&maxim_full_adress<=99) sprintf(NewKey, "%1s", "0"), memcpy(Key, NewKey, 1),Key+=1,sprintf(NewKey, "%2u", maxim_full_adress), memcpy(Key, NewKey, 2);
		   	        if (99<maxim_full_adress&&maxim_full_adress<=999) sprintf(NewKey, "%3u", maxim_full_adress), memcpy(Key, NewKey, 3);
	        	   }

	    	}
	    if (*Key == 'N')
	         if (*(Key + 1) == 'O')
	           if (*(Key + 2) == 'D')
	    	{
	        		if (node_display<=9)sprintf(NewKey, "%1u", node_display),memcpy(Key, NewKey, 1),Key++,sprintf(NewKey, "%2s", "  "),memcpy(Key, NewKey, 2);
					if (9<node_display&&node_display<=99) sprintf(NewKey, "%2u", node_display), memcpy(Key, NewKey, 2),Key+=2,sprintf(NewKey, "%1s", " "), memcpy(Key, NewKey, 1);
					if (99<node_display&&node_display<=999) sprintf(NewKey, "%3u", node_display), memcpy(Key, NewKey, 3);
	    	}
	    if (*Key == '%')
	    if (*(Key+1) == 'C')
	    {
     	   if(StatePagina==CAN_1)
     	   {
     		  sprintf(NewKey, "%2s", " 1");
     		  memcpy(Key, NewKey, 2);
     	   }
     	   else if (StatePagina==CAN_2)
     	   {
      		  sprintf(NewKey, "%2s", " 2");
      		  memcpy(Key, NewKey, 2);
     	   }
	    }
	    if (*Key == '%')
	    if (*(Key+1) == 'Z')
	    {
	    	if(tipus_display == 5)tipus_display = 3;
	    	sprintf(NewKey, "%2u", tipus_display);
	    	//sprintf(NewKey, "%2u", 3);
	    	memcpy(Key, NewKey, 2);
	    }
	    if (comprovar("ARRAY_ALARM",Key,sizeof("ARRAY_ALARM")))
	    {
 		   for(posicio=0;posicio<LONGITUD_ALARMS;posicio++)
 		   {
			   sprintf(NewKey, "%1c", (taula_alarms[posicio]));
			   memcpy(Key, NewKey, 1);
			   Key++;
			   i++;
 		   }
	    }
	    if (*Key == 'I')
	         if (*(Key + 1) == 'P')
	        	 if(*(Key + 2)<0x35&&*(Key + 2)>0x30)
	    	{
	        if(*(Key + 2) == '1')numero=MYIP_1;
	        else if(*(Key + 2) == '2')numero=MYIP_2;
	        else if(*(Key + 2) == '3')numero=MYIP_3;
	        else if(*(Key + 2) == '4')numero=MYIP_4;
    		if (numero<=9)sprintf(NewKey, "%1u", numero),memcpy(Key, NewKey, 1),Key++,sprintf(NewKey, "%2s", "  "),memcpy(Key, NewKey, 2);
			if (9<numero&&numero<=99) sprintf(NewKey, "%2u", numero), memcpy(Key, NewKey, 2),Key+=2,sprintf(NewKey, "%1s", " "), memcpy(Key, NewKey, 1);
			if (99<numero&&numero<=999) sprintf(NewKey, "%3u", numero), memcpy(Key, NewKey, 3);
	    }
	    if (*Key == 'P')
	         if (*(Key + 1) == 'O')
	        	 if (*(Key + 2) == 'R')
	        		 if (*(Key + 3) == 'T')
	    	{
    		if (PORT_COMUNICACIO<=9)sprintf(NewKey, "%1u", PORT_COMUNICACIO),memcpy(Key, NewKey, 1),Key++,sprintf(NewKey, "%3s", "   "),memcpy(Key, NewKey, 3);
			if (9<PORT_COMUNICACIO&&PORT_COMUNICACIO<=99) sprintf(NewKey, "%2u", PORT_COMUNICACIO), memcpy(Key, NewKey, 2),Key+=2,sprintf(NewKey, "%2s", "  "), memcpy(Key, NewKey, 2);
			if (99<PORT_COMUNICACIO&&PORT_COMUNICACIO<=999) sprintf(NewKey, "%3u", PORT_COMUNICACIO), memcpy(Key, NewKey, 3),Key+=3,sprintf(NewKey, "%1s", " "), memcpy(Key, NewKey, 1);
			if (999<PORT_COMUNICACIO&&PORT_COMUNICACIO<=9999) sprintf(NewKey, "%4u", PORT_COMUNICACIO), memcpy(Key, NewKey, 4);
	    	}
	    if (*Key == 'G')
	         if (*(Key + 1) == 'W')
	        	 if(*(Key + 2)<0x35&&*(Key + 2)>0x30)
	    	{
	        if(*(Key + 2) == '1')numero=GWIP_1;
	        else if(*(Key + 2) == '2')numero=GWIP_2;
	        else if(*(Key + 2) == '3')numero=GWIP_3;
	        else if(*(Key + 2) == '4')numero=GWIP_4;
    		if (numero<=9)sprintf(NewKey, "%1u", numero),memcpy(Key, NewKey, 1),Key++,sprintf(NewKey, "%2s", "  "),memcpy(Key, NewKey, 2);
			if (9<numero&&numero<=99) sprintf(NewKey, "%2u", numero), memcpy(Key, NewKey, 2),Key+=2,sprintf(NewKey, "%1s", " "), memcpy(Key, NewKey, 1);
			if (99<numero&&numero<=999) sprintf(NewKey, "%3u", numero), memcpy(Key, NewKey, 3);
	    }
	    if (*Key == 'N')
	         if (*(Key + 1) == 'M')
	        	 if(*(Key + 2)<0x35&&*(Key + 2)>0x30)
	    	{
	        if(*(Key + 2) == '1')numero=SUBMASK_1;
	        else if(*(Key + 2) == '2')numero=SUBMASK_2;
	        else if(*(Key + 2) == '3')numero=SUBMASK_3;
	        else if(*(Key + 2) == '4')numero=SUBMASK_4;
    		if (numero<=9)sprintf(NewKey, "%1u", numero),memcpy(Key, NewKey, 1),Key++,sprintf(NewKey, "%2s", "  "),memcpy(Key, NewKey, 2);
			if (9<numero&&numero<=99) sprintf(NewKey, "%2u", numero), memcpy(Key, NewKey, 2),Key+=2,sprintf(NewKey, "%1s", " "), memcpy(Key, NewKey, 1);
			if (99<numero&&numero<=999) sprintf(NewKey, "%3u", numero), memcpy(Key, NewKey, 3);
	    }
	    if (comprovar("ARRAY_NODES",Key,sizeof("ARRAY_NODES")))
	    {
	    	if(StatePagina==CAN_1)
	    	{
	  		   for(posicio=0;posicio<MAX_NODES_CANAL;posicio++)
	  		   {
	 			   sprintf(NewKey, "%1d", (taula_SD[posicio]));
	 			   memcpy(Key, NewKey, 1);
	 			   Key++;
	 			   i++;
	  		   }
	    	}
	    	else if(StatePagina==CAN_2)
	    	{
	  		   for(posicio=0;posicio<MAX_NODES_CANAL;posicio++)
	  		   {
	 			   sprintf(NewKey, "%1d", (taula_SD_2[posicio]));
	 			   memcpy(Key, NewKey, 1);
	 			   Key++;
	 			   i++;
	  		   }
	    	}
	    }
	    if (comprovar("LLARG",Key,sizeof("LLARG")))
	    {
	    	sprintf(NewKey, "%5d", HTTPBytesToSend);
	    	memcpy(Key, NewKey, 5);
	    	Key+=5;
	    }
	    if (comprovar("id=\"actualizacion\" checked",Key,sizeof("id=\"actualizacion\" checked")))
	    {
	    	Key+=19;
	    	if(!actualitza_nodes_RAM)
	    		memcpy(Key, " ", 7);
	    }
  }
}
void pasar_caracter_numero_3(unsigned char *buf, uint32_t *numero, uint8_t *comptador)
{
	char num_char[3]={0,0,0};
	uint8_t i=0;
	  while ((*(buf)!='&'))
	  {
		  if(*(buf)=='%'&&*(buf+1)=='2'&&*(buf+2)=='0'){buf+=3;i+=3;}
		  else
		  {
			  num_char[2] = *(buf);
			  if((*(buf+1)!='&')&&(*(buf+1)!='%'))
			  {
				  num_char[0] = num_char[1];
				  num_char[1] = num_char[2]-0x30;
			  }
			  buf++;
			  i++;
		  }
	  }
	  *comptador = i;
	  *numero = (num_char[0])*100+(num_char[1])*10+(num_char[2]-0x30)*1;
}
uint32_t comprovar(char cadena[], unsigned char *buf, uint8_t size_of_cadena)
{
	unsigned i=0;
	while(i < (size_of_cadena - 1))
	{
		if(cadena[i]==*(buf+i))
		{
			i++;
		}else
		{
			return (FALSE);
		}
	}
	return (TRUE);

}
uint32_t comprovar_password(unsigned char *cadena, unsigned char *buf, uint8_t size_of_cadena)
{
	unsigned i=0;
	uint8_t buf_i=0;
	while(i < size_of_cadena)
	{
		if (((*(buf+i))<0x3A)&&((*(buf+i))>0x2F)) buf_i = *(buf+i)-0x30;
		if (((*(buf+i))==0x61)) buf_i = 0xA;
		if (((*(buf+i))==0x62)) buf_i = 0xB;
		if (((*(buf+i))==0x63)) buf_i = 0xC;
		if (((*(buf+i))==0x64)) buf_i = 0xD;
		if (((*(buf+i))==0x65)) buf_i = 0xE;
		if (((*(buf+i))==0x66)) buf_i = 0xF;
		if(*(cadena+i)==buf_i)
		{
			i++;
		}else
		{
			return (FALSE);
		}
	}
	return (TRUE);

}
void tractament_pagina (void)
{
	  unsigned char *buf;
	  unsigned int i=0,j=0;
	  unsigned char encoded[32];
	  uint16_t old=0,new=0;
	  uint32_t numero=0;
	  uint8_t comptador=0;
	  uint16_t byte_5_4=0;
	  uint32_t byte_3_0=0;
	  uint8_t adress_addr=0;
	  uint8_t scroll_1=0,scroll_2=0;//es creen les dues variables per enviar en ultim lloc el setmem dels displays
	  char old_c[3]={0,0,0};
	  char new_c[3]={0,0,0};
	  char node_dis[3]={0,0,0};
	  char cadena_display[34]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	  uint8_t transmit_buffer_local[6]={0,0,0,0,0,0};
	  uint8_t longitud_cadena_display=0;
	  buf = RxTCPBuffer;
	  uint8_t user_comprovacio = 0;
  for (i = 0; i < (NrOfDataBytes); i++)
  {
		if ((sesio_MAC[0] != rebut_MAC[0])
				|| (sesio_MAC[1] != rebut_MAC[1]) || (sesio_MAC[2]
				!= rebut_MAC[2]))//si la MAC no concorda
			if(sesio_iniciada)return;//Serveix per no posar cap pàgina quan es fa petició des d'un
									 //altre PC.
	//PAGINA PASSWORD
	if (comprovar("GET /?user=",buf,sizeof("GET /?user=")))
	{
	  buf=buf+11;
	  md5(usuario,encoded);
	  if (comprovar_password(encoded,buf,32))
	  {
		  buf=buf+32;
		  user_comprovacio = 2;
	  }else
	  {
		  md5(usuario_admin,encoded);
		  if (comprovar_password(encoded,buf,32))
		  {
			  buf=buf+32;
			  user_comprovacio = 1;
		  }
		  else
		  {
			  password_incorrecte=TRUE;
			  return;
		  }
	  }
	  if (comprovar("&password=", buf,sizeof("&password=")))
		{
		  buf=buf+10;
		  md5(contrasenya_admin,encoded);
		  if (comprovar_password(encoded,buf,32)&&user_comprovacio==1)
		  {
			  if(sesio_iniciada)return;
			  sesio_iniciada = TRUE;
			  /* Initialize RTC with RTCCAL and RTCDIR values */
			  InitRTC();
			  /* start RTC */
			  StartRTC();
			  sesio_MAC[0]=rebut_MAC[0];
			  sesio_MAC[1]=rebut_MAC[1];
			  sesio_MAC[2]=rebut_MAC[2];
			  StatePagina = BLANC;
			  tipus_usuari_web = 1;
			  return;
		  }
		  md5(contrasenya,encoded);
		  if (comprovar_password(encoded,buf,32)&&user_comprovacio==2)
		  {
			  if(sesio_iniciada)return;
			  sesio_iniciada = TRUE;
			  /* Initialize RTC with RTCCAL and RTCDIR values */
			  InitRTC();
			  /* start RTC */
			  StartRTC();
			  sesio_MAC[0]=rebut_MAC[0];
			  sesio_MAC[1]=rebut_MAC[1];
			  sesio_MAC[2]=rebut_MAC[2];
			  StatePagina = BLANC;
			  tipus_usuari_web = 0;
			  return;
		  }
		  else
		  {
			  password_incorrecte=TRUE;
			  return;
		  }
		}
	}
	else if (comprovar("GET / HTTP/1.1",buf,sizeof("GET / HTTP/1.1")))//al iniciar la conexio
	{
	  StatePagina = MENU;
	  return;
	}
	else if (comprovar("GET /?menu",buf,sizeof("GET /?menu")))
	{
		if(password_incorrecte==TRUE){StatePagina = ERROR; password_incorrecte=FALSE;}
		else StatePagina = MENU;
		return;
	}
	//PAGINA INICI
    else if (comprovar("GET /?inicio",buf,sizeof("GET /?inicio")))
	{
		StatePagina = INICI_WEB;
	}
	//PAGINA OPERACIO
	else if (comprovar("GET /?operation",buf,sizeof("GET /?operation")))
	{
	  if(tipus_usuari_web==0){StatePagina = ERROR;return;}
	  StatePagina = OPERATION;
	  return;
	}
	else if (comprovar("cas_",buf,sizeof("cas_")))//a la pagina operation
	{
	  if(tipus_usuari_web==0){StatePagina = ERROR;return;}
	  buf = buf+4;
		 switch (*buf)
		 {
  	 	   case '0' ://direccion de un nodo
		   {
			   StatePagina = RESUMEN_OPERATION;
			   return;
			   break;
		   }
		   case '1' ://direccion de un nodo
		   {
			   StatePagina = DIRECCION_UN_NODO;
			   return;
			   break;
		   }
		   case '2' ://direccion de varios nodos
		   {
			   StatePagina = DIRECCION_VARIOS_NODOS;
			   return;
			   break;
		   }
		   case '3' ://resetear nodos
		   {
			   StatePagina = RESUMEN_OPERATION;
			   buf+=2;
			   if(*buf=='1')
			   {
				   programacio(1,BROADCAST_ADRESS,0,GUARDAR_NODE_ID,0,0,UART0);//posar tots els nodes a zero
				   ini_SD();
				   BlockDevWrite(ADRESS_TAULA_SD,taula_SD,250);
				   ini_taula_web();
			   }
			   else if(*buf=='2')
			   {
				   programacio(1,BROADCAST_ADRESS,0,GUARDAR_NODE_ID,0,0,UART2);//posar tots els nodes a zero
				   ini_SD_2();
				   BlockDevWrite(ADRESS_TAULA_SD_2,taula_SD_2,250);
				   ini_taula_web();
			   }

			   return;
			   break;
		   }
		   case '4' ://configuracion nodos en el arranque
		   {
			   StatePagina = NODOS_ARRANQUE;
			   //programar(2,0,1,GUARDAR_NODE_ID,0,0);//començar trama de programacio 2
			   return;
			   break;
		   }
		   case '5' ://cambiar lenguaje
		   {
			   StatePagina = CAMBIAR_LENGUAJE;
			   return;
			   break;
		   }
		   case '6' ://configuracion ethernet
		   {
			   StatePagina = CONFIGURACION_ETHERNET;
			   return;
			   break;
		   }
		   case '7' ://cambiar usuarios
		   {
			   StatePagina = CANVI_USUARI;
			   return;
			   break;
		   }
		   case '8' :
		   {
			   StatePagina = CONFIGURACIO_GENERAL;
			   return;
			   break;
		   }
		 }
	}
	else if (comprovar("GET /cas_1?old=",buf,sizeof("GET /cas_1?old=")))
	{
	  if(tipus_usuari_web==0){StatePagina = ERROR;return;}
	  buf = buf + 15;
	  if(*(buf)=='&')return;//Si esta buit return
	  while (*(buf)!='&')
	  {
		  old_c[2] = *(buf);
		  if(*(buf+1) != '&')
		  {
			  old_c[0] = old_c[1];
			  old_c[1]=old_c[2]-0x30;
		  }
		  buf++;
	  }
	  old = (old_c[0])*100+(old_c[1])*10+(old_c[2]-0x30)*1;
	  if (comprovar("&new=",buf,sizeof("&new=")))
	  {
		  buf = buf + 5;
		  while (*(buf)!='&')
		  {
			  new_c[2] = *(buf);
			  if(*(buf+1) != '&')
			  {
				  new_c[0] = new_c[1];
				  new_c[1]=new_c[2]-0x30;
			  }
			  buf++;
		  }
		  new = (new_c[0])*100+(new_c[1])*10+(new_c[2]-0x30)*1;
		  if (comprovar("&canal=",buf,sizeof("&canal=")))
		  {
			  buf += 7;
			  if(*buf==0x31)programacio(1,old,new,GUARDAR_NODE_ID,0,0,UART0);
			  else programacio(1,old,new,GUARDAR_NODE_ID,0,0,UART2);
		  }
		  //StateCAN = NORMAL_CAN;
		  return;
	  }
	}
	else if (comprovar("GET /cas_4?inici_programacio=",buf,sizeof("GET /cas_4?inici_programacio=")))
	{
		if(tipus_usuari_web==0){StatePagina = ERROR;return;}
		buf = buf + 29;
		while (*(buf)!='&')
		{
			new_c[2] = *(buf);
			if(*(buf+1) != '&')
			{
				new_c[0] = new_c[1];
				new_c[1]= new_c[2]-0x30;
			}
			buf++;
		}
		new = (new_c[0])*100+(new_c[1])*10+(new_c[2]-0x30)*1;
		buf++;
		if(!comprovar("canal=",buf,sizeof("canal=")))return;
		buf+=6;
		//inici programació
		if(*buf=='1')programacio(2,0,new,GUARDAR_NODE_ID,0,0,UART0);
		else if(*buf=='2')programacio(2,0,new,GUARDAR_NODE_ID,0,0,UART2);
		return;
	}
	else if (comprovar("GET /cas_4?acaba_programacio=",buf,sizeof("GET /cas_4?acaba_programacio=")))
	{
		//fi programació
		if(tipus_usuari_web==0){StatePagina = ERROR;return;}
		if(StateUART_canal1!=PROGRAMACIO&&StateUART_canal2!=PROGRAMACIO)return;
		if(StateUART_canal1==PROGRAMACIO)
		{
			programacio(4,BROADCAST_ADRESS,0,0,0,0,UART0);
			BlockDevWrite(ADRESS_TAULA_SD,taula_SD,250); //gravem dades a SD
			for(j=0;j<NUM_NODES;j++)taula_configurats[j]=0;
			for(j=0;j<NUM_NODES_2;j++)taula_configurats_2[j]=0;
			NUM_NODES=NUM_NODES_2=0;
			ini_taula_configurats();
			return;
		}
		else if(StateUART_canal2==PROGRAMACIO)
		{
			programacio(4,BROADCAST_ADRESS,0,0,0,0,UART2);
			BlockDevWrite(ADRESS_TAULA_SD_2,taula_SD_2,250); //gravem dades a SD
			for(j=0;j<NUM_NODES;j++)taula_configurats[j]=0;
			for(j=0;j<NUM_NODES_2;j++)taula_configurats_2[j]=0;
			NUM_NODES=NUM_NODES_2=0;
			ini_taula_configurats();
			return;
		}
	}
	else if (comprovar("GET /cas_6?IP_1=",buf,sizeof("GET /cas_6?IP_1=")))
	{
	  if(tipus_usuari_web==0){StatePagina = ERROR;return;}
	  buf = buf + 16;
	  new_c[2]=new_c[1]=new_c[0]=0;
	  pasar_caracter_numero_3(buf,&numero,&comptador);
	  MYIP_1=numero;
	  buf += comptador;
	  if (comprovar("&IP_2=",buf,sizeof("&IP_2=")))
	  {
		  buf += 6;
		  pasar_caracter_numero_3(buf,&numero,&comptador);
		  MYIP_2=numero;
		  buf += comptador;
		  if (comprovar("&IP_3=",buf,sizeof("&IP_3=")))
		  {
			  buf += 6;
			  pasar_caracter_numero_3(buf,&numero,&comptador);
			  MYIP_3=numero;
			  buf += comptador;
			  if (comprovar("&IP_4=",buf,sizeof("&IP_4=")))
			  {
				  buf += 6;
				  pasar_caracter_numero_3(buf,&numero,&comptador);
				  MYIP_4=numero;
				  buf += comptador;
				  if (comprovar("&NM_1=",buf,sizeof("&NM_1=")))
				  {
					  buf += 6;
					  pasar_caracter_numero_3(buf,&numero,&comptador);
					  SUBMASK_1=numero;
					  buf += comptador;
					  if (comprovar("&NM_2=",buf,sizeof("&NM_2=")))
					  {
						  buf += 6;
						  pasar_caracter_numero_3(buf,&numero,&comptador);
						  SUBMASK_2=numero;
						  buf += comptador;
						  if (comprovar("&NM_3=",buf,sizeof("&NM_3=")))
						  {
							  buf += 6;
							  pasar_caracter_numero_3(buf,&numero,&comptador);
							  SUBMASK_3=numero;
							  buf += comptador;
							  if (comprovar("&NM_4=",buf,sizeof("&NM_4=")))
							  {
								  buf += 6;
								  pasar_caracter_numero_3(buf,&numero,&comptador);
								  SUBMASK_4=numero;
								  buf += comptador;
								  if (comprovar("&GW_1=",buf,sizeof("&GW_1=")))
								  {
									  buf += 6;
									  pasar_caracter_numero_3(buf,&numero,&comptador);
									  GWIP_1=numero;
									  buf += comptador;
									  if (comprovar("&GW_2=",buf,sizeof("&GW_2=")))
									  {
										  buf += 6;
										  pasar_caracter_numero_3(buf,&numero,&comptador);
										  GWIP_2=numero;
										  buf += comptador;
										  if (comprovar("&GW_3=",buf,sizeof("&GW_3=")))
										  {
											  buf += 6;
											  pasar_caracter_numero_3(buf,&numero,&comptador);
											  GWIP_3=numero;
											  buf += comptador;
											  if (comprovar("&GW_4=",buf,sizeof("&GW_4=")))
											  {
												  buf += 6;
												  pasar_caracter_numero_3(buf,&numero,&comptador);
												  GWIP_4=numero;
												  buf += comptador;
												  if (comprovar("&port=",buf,sizeof("&port=")))
												  {
													  buf += 6;
													  //cadena_display[0]=cadena_display[1]=cadena_display[2]=cadena_display[3]=0;
													  while ((*(buf)!='&'))
													  {
														  if(*(buf)=='%'&&*(buf+1)=='2'&&*(buf+2)=='0'){buf+=3;}
														  else
														  {
															  cadena_display[3] = *(buf);
															  if((*(buf+1)!='&')&&(*(buf+1)!='%'))
															  {
																  cadena_display[0] = cadena_display[1];
																  cadena_display[1] = cadena_display[2];
																  cadena_display[2] = cadena_display[3]-0x30;
															  }
															  buf++;
														  }
													  }
													  //pasar_caracter_numero_3(buf,&numero,&comptador);
													  numero = (cadena_display[0])*1000+(cadena_display[1])*100+(cadena_display[2])*10+(cadena_display[3]-0x30)*1;
													  PORT_COMUNICACIO=numero;
													  port_comunicacio_RAM[0]=(PORT_COMUNICACIO&0xFF00)>>8;
													  port_comunicacio_RAM[1]=PORT_COMUNICACIO&0xFF;
													  BlockDevWrite(ADRESS_PORT,port_comunicacio_RAM,2);
													  cambiar_ethernet=3;//cambiar ethernet i LCD
													  StatePagina=BLANC;
													  sesio_iniciada = FALSE;
													  return;
												  }
											  }
										  }
									  }
								  }
							  }
						  }
					  }
				  }
			  }
		  }
	  }
	}
	else if (comprovar("GET /cas_7?usuario_old=",buf,sizeof("GET /cas_7?usuario_old=")))
	{
		if(tipus_usuari_web==0){StatePagina = ERROR;return;}
		buf = buf+23;
		if (comprovar(usuario,buf,punter_usuari))
		{
			buf = buf + punter_usuari;
			if(comprovar("&password_old=",buf,sizeof("&password_old=")))
			{
				buf = buf+14;
				if (comprovar(contrasenya,buf,punter_password))
				{
					buf = buf + punter_password;
					if (comprovar("&usuario_new=",buf,sizeof("&usuario_new=")))
					{
						buf = buf+13;
						punter_usuari = 0;
						//new_usuario = '00000000';
						while (*(buf+punter_usuari) != '&')
						{
							new_usuario[punter_usuari] = *(buf+punter_usuari);
							punter_usuari++;
						}
						usuario = new_usuario;
						//BlockDevWrite(ADRESS_USUARI,usuario,8);
						buf = buf + punter_usuari;
						if (comprovar("&password_new=",buf,sizeof("&password_new=")))
						{
							buf = buf+14;
							punter_password = 0;
							//new_password = "00000000";
							while (*(buf+punter_password) != '&')
							{
								new_password[punter_password] = *(buf+punter_password);
								punter_password++;
							}
							contrasenya = new_password;
							//BlockDevWrite(ADRESS_PASSWORD,contrasenya,8);
							buf = buf + punter_password;
							strcpy(new_password,NULL);
							strcpy(new_usuario,NULL);
							return;
						}
					}
				}
			}
		}
	}
	else if (comprovar("GET /cas_8?actualizacion=on",buf,sizeof("GET /cas_8?actualizacion=on")))
	{
		actualitza_nodes_RAM=1;
		BlockDevWrite(ADRESS_RAM,(uint8_t *)&actualitza_nodes_RAM,1);
		return;
	}
	else if(comprovar("GET /cas_8?final=",buf,sizeof("GET /cas_8?final=")))
	{
		actualitza_nodes_RAM=0;
		BlockDevWrite(ADRESS_RAM,(uint8_t *)&actualitza_nodes_RAM,1);
		return;
	}
	//PAGINA CANAL 1
	else if (comprovar("GET /?actualitza",buf,sizeof("GET /?actualitza")))//actualitza
	{
	  buf=buf+16;
	  if(*buf!='_')
	  {
		  if(StatePagina==CAN_1)StatePagina = ACTUALITZA;//La primera vegada
		  else if (StatePagina==CAN_2)StatePagina = ACTUALITZA_2;
		  else if(StatePagina==ACTUALITZA)StatePagina = ESPERA_NODES;
		  else if(StatePagina==ACTUALITZA_2)StatePagina = ESPERA_NODES_2;
	  }
	  if(comprovar("_display_",buf,sizeof("_display_")))
	  {
		  buf = buf+9;
		  pasar_caracter_numero_3(buf,&node_display,&comptador);
		  buf+=comptador;
		  //pulsadors_display = ((taula_nodes_web[node_display-1]&0x0F00)>>8);
		  if(StatePagina==DISPLAY_||StatePagina==ACTUALITZA||StatePagina==CAN_1||StatePagina==ESPERA_NODES)
		  {
			  getmem(node_display,FIRM_VERSION,1);//fins que no es faci tot el getmem no
			  	  	  	  	  	  	  	  	  	  //s'actualitza la pagina
			  //actualitza_display = TRUE;
			  StatePagina = ESPERA_DISPLAY;
			  //getmem(node_display,TEC);
		  }
		  else if((StatePagina==DISPLAY_2||StatePagina==ACTUALITZA_2||StatePagina==CAN_2||StatePagina==ESPERA_NODES_2))
		  {
			  getmem(node_display,FIRM_VERSION,2);//fins que no es faci tot el getmem no
			  	  	  	  	  	  	  	  	  	  //s'actualitza la pagina
			  //actualitza_display = TRUE;
			  StatePagina = ESPERA_DISPLAY_2;
			  //getmem(node_display,TEC);
		  }
		  else if(StatePagina==ACTUALITZA_DISPLAY||StatePagina==BLANC)
		  {
			  StatePagina = ESPERA_DISPLAY;
		  }
		  else if(StatePagina==ACTUALITZA_DISPLAY_2||StatePagina==BLANC_2)
		  {
			  StatePagina = ESPERA_DISPLAY_2;
		  }

	  }
	  else if(comprovar("_RS_232_",buf,sizeof("_RS_232_")))
	  {
		  buf = buf+8;
		  pasar_caracter_numero_3(buf,&node_display,&comptador);
		  buf+=comptador;
		  if(StatePagina==RS232||StatePagina==ACTUALITZA||StatePagina==CAN_1||StatePagina==ESPERA_NODES)
		  {
			  getmem(node_display,FIRM_VERSION,1);//fins que no es faci tot el getmem no
			  	  	  	  	  	  	  	  	  	  //s'actualitza la pagina
			  //actualitza_display = TRUE;
		  }
		  StatePagina = ESPERA_RS232;
	  }
	  else if(comprovar("_LC_",buf,sizeof("_LC_")))
	  {
		  buf = buf+4;
		  pasar_caracter_numero_3(buf,&node_display,&comptador);
		  buf+=comptador;
		  if(StatePagina==LC||StatePagina==ACTUALITZA||StatePagina==CAN_1||StatePagina==ESPERA_NODES)
		  {
			  getmem(node_display,FIRM_VERSION,1);//fins que no es faci tot el getmem no
			  	  	  	  	  	  	  	  	  	  //s'actualitza la pagina
			  StatePagina = ESPERA_LC;
		  }
		  else if((StatePagina==LC_2||StatePagina==ACTUALITZA_2||StatePagina==CAN_2||StatePagina==ESPERA_NODES_2))
		  {
			  getmem(node_display,FIRM_VERSION,2);//fins que no es faci tot el getmem no
			  	  	  	  	  	  	  	  	  	  //s'actualitza la pagina
			  StatePagina = ESPERA_LC_2;
		  }
		  else if(StatePagina==ACTUALITZA_LC||StatePagina==BLANC)
		  {
			  StatePagina = ESPERA_LC;
		  }
		  else if(StatePagina==ACTUALITZA_LC_2||StatePagina==BLANC_2)
		  {
			  StatePagina = ESPERA_LC_2;
		  }

	  }
		return;
	}
	else if (comprovar("GET /?RS485_1=",buf,sizeof("GET /?RS485_1")))
	{
	  StatePagina = CAN_1;
	  return;
	}
	else if (comprovar("GET /?RS485_2=",buf,sizeof("GET /?RS485_2")))
	{
	  StatePagina = CAN_2;
	  return;
	}
	else if (comprovar("display=",buf,sizeof("display=")))
	{
	  if(StatePagina==CAN_1||StatePagina==ACTUALITZA||StatePagina==ESPERA_NODES)StatePagina = DISPLAY_;
	  else if(StatePagina==CAN_2||StatePagina==ACTUALITZA_2||StatePagina==ESPERA_NODES_2)StatePagina = DISPLAY_2;
	  buf = buf + 8;
	  if(*(buf) == ' ')
	  {
		  return;
	  }
	  while (*(buf)!='&')
	  {
		  node_dis[2] = *(buf);
		  if(*(buf+1) != '&')
		  {
			  node_dis[0] = node_dis[1];
			  node_dis[1]=node_dis[2]-0x30;
		  }
		  buf++;
	  }
	  node_display = (node_dis[0])*100+(node_dis[1])*10+(node_dis[2]-0x30)*1;
	  if (comprovar("&tipus=",buf,sizeof("&tipus=")))
	  {
		  buf = buf + 7;
		  tipus_display = *(buf) - 0x30;

		  if(tipus_display == 6)
		  {
			  if(StatePagina==DISPLAY_)StatePagina=LC;
			  else if(StatePagina==DISPLAY_2)StatePagina = LC_2;
		  }
		  else if(tipus_display == 7)StatePagina = RS232;
	  }
	  return;
	}
	else if (comprovar("GET /escriure_display=",buf,sizeof("GET /escriure_display=")))
	{
	  buf = buf+22;
	  pasar_caracter_numero_3(buf,&node_display,&comptador);
	  buf+=comptador;
	  /*while (*(buf)!='&')
	  {
		  node_dis[2] = *(buf);
		  if(*(buf+1) != '&')
		  {
			  node_dis[0] = node_dis[1];
			  node_dis[1]=node_dis[2]-0x30;
		  }
		  buf++;
	  }
	  node_display = (node_dis[0])*100+(node_dis[1])*10+(node_dis[2]-0x30)*1;*/
	  if(node_display>250&&node_display!=999)return;
	  if(*(buf+1)==0x31)
	  {
		  if(!taula_SD[node_display-1]&&node_display!=999)return;
	  }
	  else if(*(buf+1)==0x32)
	  {
		  if(!taula_SD_2[node_display-1]&&node_display!=999)return;
	  }
	  /*else
	  {
		  return;
	  }*/
	  if(node_display==999)node_display=BROADCAST_ADRESS;//broadcast a test global web
	  //escriure_dada(node_display,ACCIONS_COMUNES,0,(((*(buf+1)-0x30)<<5)|((*(buf+2)-0x30)<<2)|((*(buf+3)-0x30)))|((*(buf+10)-0x30)<<8),(((*(buf+4)-0x30)<<5)|((*(buf+5)-0x30)<<4)|((*(buf+6)-0x30)<<3)|((*(buf+7)-0x30))|((*(buf+8)-0x30)<<2)|((*(buf+9)-0x30)<<1))<<24,4);
	  if(StatePagina==DIRECCION_VARIOS_NODOS||StatePagina==BLANC_3||StatePagina==BLANC_4)
	  {
		  if(*(buf+1)==0x31)
		  {
			  StatePagina = BLANC_3;
		  }
		  else if (*(buf+1)==0x32)
		  {
			  StatePagina = BLANC_4;
		  }
		  /*else
		  {
			  return;
		  }*/
		  buf++;
	  }
	  //Código función
	  transmit_buffer_local[0]=(*(buf+10)-0x30);
	  //Buzzer
	  transmit_buffer_local[1]=(((*(buf+1)-0x30)<<5)|((*(buf+2)-0x30)<<2)|((*(buf+3)-0x30)));
	  //Leds
	  transmit_buffer_local[2]=(((*(buf+4)-0x30)<<5)|((*(buf+5)-0x30)<<4)|((*(buf+6)-0x30)<<3)|((*(buf+7)-0x30))|((*(buf+8)-0x30)<<2)|((*(buf+9)-0x30)<<1));
	  if(StatePagina==ESPERA_DISPLAY||StatePagina==ACTUALITZA_DISPLAY||StatePagina==DISPLAY_||StatePagina==BLANC_3)setmem(node_display,ACCIONS_COMUNES,3,(uint8_t *)transmit_buffer_local,0,1);
	  else if(StatePagina==ESPERA_DISPLAY_2||StatePagina==ACTUALITZA_DISPLAY_2||StatePagina==DISPLAY_2||StatePagina==BLANC_4)setmem(node_display,ACCIONS_COMUNES,3,(uint8_t *)transmit_buffer_local,0,2);
	  buf = buf+11;
	  longitud_cadena_display=0;
	  if(*(buf)!='&')
	  {
		  while (*(buf)!='&')
		  {
			  if (comprovar("%20",buf,sizeof("%20")))
			  {
				 cadena_display[longitud_cadena_display]=' ';
				 longitud_cadena_display++;
				 buf=buf+3;
			  }
			  else if(comprovar("%127",buf,sizeof("%127")))
			  {
				  cadena_display[longitud_cadena_display]=127;
				  longitud_cadena_display++;
				  buf=buf+4;
			  }
			  else
			  {
				cadena_display[longitud_cadena_display]=*(buf);
				longitud_cadena_display++;
				buf++;
			  }
		  }
	  }
	  else
	  {
		  cadena_display[0]=' ';
		  longitud_cadena_display = 1;
	  }
	  //es creen les dues variables per enviar en ultim lloc el setmem dels displays
	  scroll_1=*(buf+1);
	  scroll_2=*(buf+2);
	  buf+=3;
	  if(*buf!= ' ')//Si es el test global
	  {
		  buf++;
		  //agafes addr
		  pasar_caracter_numero_3(buf,&numero,&comptador);
		  adress_addr = numero;
		  buf+=comptador+1;
		  //agafes byte 0
		  pasar_caracter_numero_3(buf,&numero,&comptador);
		  byte_5_4|=(numero<<8);
		  buf+=comptador+1;
		  //agafes byte 1
		  pasar_caracter_numero_3(buf,&numero,&comptador);
		  byte_5_4|=(numero);
		  buf+=comptador+1;
		  //agafes byte 2
		  pasar_caracter_numero_3(buf,&numero,&comptador);
		  byte_3_0|=(numero<<24);
		  buf+=comptador+1;
		  //agafes byte 3
		  pasar_caracter_numero_3(buf,&numero,&comptador);
		  byte_3_0|=(numero<<16);
		  buf+=comptador+1;
		  //agafes byte 4
		  pasar_caracter_numero_3(buf,&numero,&comptador);
		  byte_3_0|=(numero<<8);
		  buf+=comptador+1;
		  //agafes byte 5
		  pasar_caracter_numero_3(buf,&numero,&comptador);
		  byte_3_0|=(numero);
		  buf+=comptador+1;
		  //agafes DLC
		  pasar_caracter_numero_3(buf,&numero,&comptador);
		  transmit_buffer_local[0]=(byte_5_4&0xFF00)>>8;
		  transmit_buffer_local[1]=byte_5_4&0xFF;
		  transmit_buffer_local[2]=(byte_3_0&0xFF000000)>>24;
		  transmit_buffer_local[3]=(byte_3_0&0xFF0000)>>16;
		  transmit_buffer_local[4]=(byte_3_0&0xFF00)>>8;
		  transmit_buffer_local[5]=byte_3_0&0xFF;
		  //escriure_dada(node_display,adress_addr,0,byte_5_4,byte_3_0,numero);
		  if(StatePagina==ESPERA_DISPLAY||StatePagina==ACTUALITZA_DISPLAY||StatePagina==DISPLAY_||StatePagina==BLANC_3)setmem(node_display,adress_addr,numero,(uint8_t *)transmit_buffer_local,0,1);
		  else if(StatePagina==ESPERA_DISPLAY_2||StatePagina==ACTUALITZA_DISPLAY_2||StatePagina==DISPLAY_2||StatePagina==BLANC_4)setmem(node_display,adress_addr,numero,(uint8_t *)transmit_buffer_local,0,2);

	  }
	  //escriure_display(node_display,(ASCII_FORMAT<<4)|(scroll_1-0x30)<<3|(scroll_2-0x30)<<2|0,cadena_display,longitud_cadena_display+1);
	  if(StatePagina==ESPERA_DISPLAY||StatePagina==ACTUALITZA_DISPLAY||StatePagina==DISPLAY_||StatePagina==BLANC_3)
	  {
		  setmem_display(node_display,(ASCII_FORMAT<<4)|(scroll_1-0x30)<<3|(scroll_2-0x30)<<2|0,longitud_cadena_display,(uint8_t *)cadena_display,1,1);
		  if(StatePagina!=BLANC_3)StatePagina = BLANC;
	  }
	  else if(StatePagina==ESPERA_DISPLAY_2||StatePagina==ACTUALITZA_DISPLAY_2||StatePagina==DISPLAY_2||StatePagina==BLANC_4)
	  {
		  setmem_display(node_display,(ASCII_FORMAT<<4)|(scroll_1-0x30)<<3|(scroll_2-0x30)<<2|0,longitud_cadena_display,(uint8_t *)cadena_display,1,2);
		  if(StatePagina!=BLANC_4)StatePagina = BLANC_2;
	  }
	  return;
	}
	else if (comprovar("GET /escriure_LC=",buf,sizeof("GET /escriure_LC=")))
	{
		  buf = buf+17;
		  pasar_caracter_numero_3(buf,&node_display,&comptador);
		  buf+=comptador;
		  buf++;//El caracter '&'
		  pasar_caracter_numero_3(buf,&j,&comptador);
		  if(StatePagina==ESPERA_LC||StatePagina==ACTUALITZA_LC||StatePagina==LC||StatePagina==BLANC)
		  {
			  setmem(node_display,DISPLAY+4,1,(uint8_t *)j,1,1);
			  StatePagina = BLANC;
		  }
		  else if(StatePagina==ESPERA_LC_2||StatePagina==ACTUALITZA_LC_2||StatePagina==LC_2||StatePagina==BLANC_2)
		  {
			  setmem(node_display,DISPLAY+4,1,(uint8_t *)j,1,2);
			  StatePagina = BLANC_2;
		  }
		  //getmem(node_display,FIRM_VERSION,1);

	}
	//PAGINA ALARMES
	else if (comprovar("GET /?alarms",buf,sizeof("GET /?alarms")))
	{
		StatePagina = ALARMS;
		return;
	}
	else if (comprovar("GET /?borrar_alarmas",buf,sizeof("GET /?borrar_alarmas")))
	{
		StatePagina = ALARMS;
		ini_taula_alarms();
		alarmes_activades_canal1=alarmes_activades_canal2=0;
		return;
	}
	//ALTRES
	else if (comprovar("GET /?Log_Out",buf,sizeof("GET /?Log_Out")))
	{
	  if((sesio_MAC[0]==rebut_MAC[0])||(sesio_MAC[1]==rebut_MAC[1])||(sesio_MAC[2]==rebut_MAC[2]))
	  {//pq no tanquin la sesio desde un altre ordinador
		sesio_iniciada = FALSE;
		StatePagina = MENU;
		tipus_usuari_web=0;
	  }
	  return;
	}
	else if (comprovar("GET /logo.jpg",buf,sizeof("GET /logo.jpg")))
	{
		StatePagina = IMATGE;
		return;
	}
	else if (comprovar("GET /favicon.ico",buf,sizeof("GET /favicon.ico")))
	{
		CanviarPagina = StatePagina;
		StatePagina = FAVICON;
		return;
	}
	buf++;
  }
}
void ini_aplicacio(void)
{
	uint8_t i=0;
	uint8_t j=0;
	while(i<MAX_APLICACIONS)
	{
		while(j<3)
		{
			aplicacio_MAC[i][j]=0;
			j++;
		}
		i++;
	}
	i=j=0;
	while(i<MAX_APLICACIONS)
	{
		while(j<2)
		{
			aplicacio_IP[i][j]=0;
			j++;
		}
		i++;
	}

}


