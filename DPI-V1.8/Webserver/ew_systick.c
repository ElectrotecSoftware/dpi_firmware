#include "webserver.h"
#include "type.h"
#include "LPC17xx.h"
#include <stdio.h>
#include "lcd.h"
#include "ethernet.h"
#include "tcpip.h"
#include "string.h"
#include "leds.h"
#include "trames.h"
#include "blockdev.h"

extern void TCPClockHandler(void);

volatile uint32_t TimeTick = 0;
volatile uint32_t TimeTick2 = 0;
extern volatile uint32_t msTicks_timeout_sesio;
extern volatile uint32_t msTicks_LCD;
extern volatile uint32_t msTicks_aplicacio[MAX_APLICACIONS];
extern volatile uint32_t msTicks_led;
extern volatile uint32_t msTicks_control_alarmes;
extern volatile uint32_t msTicks_programacio1;
extern volatile uint32_t msTicks_programacio2;
extern volatile uint32_t msTicks_ACK;
extern volatile uint32_t msTicks_led_error;
// ****************
//  SysTick_Handler
void SysTick_Handler(void)
{
	uint8_t i = 0;
	//Webserver
	TimeTick++;		// Increment first SysTick counter
	TimeTick2++;	// Increment second SysTick counter
	msTicks_led_error++;
	for(i=0;i<MAX_APLICACIONS;i++)
	{
		if(msTicks_aplicacio[i]<2500)
		{
			msTicks_aplicacio[i]++;
			if(msTicks_aplicacio[i]>2000)
			{
				if(esperant_ACK[i]==2)
				{//Si s'està esperant ACK de una solicitud d'informació de la xarxa
					msTicks_aplicacio[i]=3000;
					esperant_ACK[i]=0;
					return;
				}
				if(RetryCounter_aplicacio[i])
				{
					TCPLocalPort=PORT_COMUNICACIO;
					aplicacio_actual = i;
					memcpy(&RemoteMAC, &aplicacio_MAC[i], 6);
					memcpy(&RemoteIP, &aplicacio_IP[i], 4);
					Enviar_Ethernet((uint8_t *)Ethernet_Transmit_Buffer[i],llargada_trama[i]);
					RetryCounter_aplicacio[i]--;
					msTicks_aplicacio[i]=0;
					//Enviar_Ethernet_push((uint8_t *)Ethernet_Transmit_Buffer[i],llargada_trama[i]);
					TCPLocalPort=TCP_PORT_HTTP;
				    memcpy(&RemoteMAC, &web_MAC, 6);              // save opponents MAC and IP
				    memcpy(&RemoteIP, &web_IP, 4);                // for later use
				}
				else
				{
					esperant_ACK[aplicacio_actual]=0;
					msTicks_aplicacio[i]=3000;
					RetryCounter_aplicacio[i]=MAX_RETRYS;
				}
			}
		}
	}
	// After 100 ticks (1000 x 1ms = 1sec)
	if (TimeTick >= 1000) {
	  TimeTick = 0;	// Reset counter
	}
	// After 20 ticks (200 x 1ms = 1/5sec)
	if (TimeTick2 >= 200) {
	  TimeTick2 = 0; // Reset counter
	  TCPClockHandler();  // Call TCP handler
	}  
	if(sesio_iniciada)
	{
		msTicks_timeout_sesio++;
		if(msTicks_timeout_sesio>(1000*60*5))sesio_iniciada=FALSE;//5 min d'inactivitat tanca sesio
	}else if (!sesio_iniciada)
	{
		msTicks_timeout_sesio = 0;
	}
	if(msTicks_ACK>1)
	{
		msTicks_ACK--;
	}
	if(msTicks_led<1000)
	{
		msTicks_led++;
		if(msTicks_led>10)
		{
			led_off(LED_ACTIVITY);
			msTicks_led=1000;
		}
	}
	if(msTicks_control_alarmes<1000)
	{
		msTicks_control_alarmes++;
		if(msTicks_control_alarmes==10)
		{

		}
	}
	//LCD
	if(change_state==3)
	{
		msTicks_LCD++;
	}
	else if(apretada==UP_DOWN_LCD && msTicks_LCD<50000)
	{
		msTicks_LCD++;
		if(msTicks_LCD == 2000)
		{
			led_on(LED_ACTIVITY);
			led_on(LED_ERROR);
			MyIP[0] = 192 | 168 << 8;
			MyIP[1] = 0 | 200 << 8;
			SubnetMask[0] = 255 | 255 << 8;
			SubnetMask[1] = 255 | 0 << 8;
			GatewayIP[0] = 192 | 168 << 8;
			GatewayIP[1] = 0 | 1 << 8;
			usuario = "user";
			contrasenya = "user";
			BlockDevWrite(ADRESS_IP, (uint8_t *) MyIP, 4);
			BlockDevWrite(ADRESS_NM, (uint8_t *) SubnetMask, 4);
			BlockDevWrite(ADRESS_GW, (uint8_t *) GatewayIP, 4);
			BlockDevWrite(ADRESS_PORT,3030,2);
			//BlockDevWrite(ADRESS_USUARI,(uint8_t *)usuario,8);
			//BlockDevWrite(ADRESS_PASSWORD,(uint8_t *)contrasenya,8);
			while(1);//Bucle infinit perque salti watchdog
		}
	}
	else
	{
		msTicks_LCD = 0;
	}
	if(msTicks_programacio1<1000)
	{
		msTicks_programacio1++;
		if(msTicks_programacio1==10)
		{
			msTicks_programacio1=1000;
			if(StateUART_canal1==PROGRAMACIO)
			{
				peticio_programacio = 0;
				old_adress_programacio1_canal1 = 0;
				new_adress_programacio1_canal1 = 0;
				StateUART_canal1=FINAL;
			}
			else if(StateUART_canal2==PROGRAMACIO)
			{
				peticio_programacio_2 = 0;
				old_adress_programacio1_canal2 = 0;
				new_adress_programacio1_canal2 = 0;
				StateUART_canal2=FINAL;
			}
		}
	}
	if(msTicks_programacio2<(1000*60*16))
	{
		msTicks_programacio2++;
		//Cada 5segons repetim trama programació
		if(msTicks_programacio2 % 5000==0){
			if(StateUART_canal1==PROGRAMACIO) programacio(2,0,new_adress_programacio2_canal1,GUARDAR_NODE_ID,0,0,UART0);
			else if(StateUART_canal2==PROGRAMACIO) programacio(2,0,new_adress_programacio2_canal2,GUARDAR_NODE_ID,0,0,UART2);
		}
		//Passat un minut sense resposta cancelem programació
		if(msTicks_programacio2==1000*60*1)
		{
			msTicks_programacio2 = (1000*60*16);
			if(StateUART_canal1==PROGRAMACIO)
			{
				programacio(4,BROADCAST_ADRESS,0,0,0,0,UART0);
				BlockDevWrite(ADRESS_TAULA_SD,taula_SD,250);
				for(i=0;i<NUM_NODES;i++)taula_configurats[i]=0;
				for(i=0;i<NUM_NODES_2;i++)taula_configurats_2[i]=0;
				NUM_NODES=NUM_NODES_2=0;
				ini_taula_configurats();
			}
			else if(StateUART_canal2==PROGRAMACIO)
			{
				programacio(4,BROADCAST_ADRESS,0,0,0,0,UART2);
				BlockDevWrite(ADRESS_TAULA_SD_2,taula_SD_2,250);
				for(i=0;i<NUM_NODES;i++)taula_configurats[i]=0;
				for(i=0;i<NUM_NODES_2;i++)taula_configurats_2[i]=0;
				NUM_NODES=NUM_NODES_2=0;
				ini_taula_configurats();
			}
		}
	}
	if(counter_error_3 < 2000)
	{//Comptador per control dels comptadors d'error del led d'error.
		counter_error_3++;
	}
}

// ****************
// Setup SysTick Timer to interrupt at 1 msec intervals
void Start_SysTick1ms(void)
{
	if (SysTick_Config(SystemCoreClock / 1000)) {
		while (1);  // Capture error
	}
}
