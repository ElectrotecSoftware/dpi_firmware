/******************************************************************
 *****                                                        *****
 *****  Name: easyweb.h                                       *****
 *****  Ver.: 1.0                                             *****
 *****  Date: 07/05/2001                                      *****
 *****  Auth: Andreas Dannenberg                              *****
 *****        HTWK Leipzig                                    *****
 *****        university of applied sciences                  *****
 *****        Germany                                         *****
 *****  Func: header-file for easyweb.c                       *****
 *****                                                        *****
 ******************************************************************/

#ifndef __WEBSERVER_H
#define __WEBSERVER_H

#include <stdint.h>


typedef enum {
  MENU,
  OPERATION,
  ERROR,
  CAN_1,
  CAN_2,
  ACTUALITZA,//Nodes canal 1
  ACTUALITZA_2,//Nodes canal 2
  ACTUALITZA_DISPLAY,
  ACTUALITZA_DISPLAY_2,
  DISPLAY_,
  DISPLAY_2,
  IMATGE,
  FAVICON,
  DIRECCION_UN_NODO,
  DIRECCION_VARIOS_NODOS,
  NODOS_ARRANQUE,
  CAMBIAR_LENGUAJE,
  CONFIGURACION_ETHERNET,
  CANVI_USUARI,
  CONFIGURACIO_GENERAL,
  BLANC,
  BLANC_2,
  BLANC_3,
  BLANC_4,
  ALARMS,
  ESPERA_DISPLAY, //per a poder fer un server push display
  ESPERA_DISPLAY_2,
  ESPERA_NODES,//push nodes canal 1
  ESPERA_NODES_2,//push nodes canal 2
  ESPERA_RS232, //push RS232
  ESPERA_RS232_2,
  ESPERA_LC, //push LC
  ESPERA_LC_2,
  INICI_WEB,
  ACTUALITZA_RS232,
  RS232,
  ACTUALITZA_LC,
  ACTUALITZA_LC_2,
  LC,
  LC_2,
  NO_CANVIAR_PAGINA,
  RESUMEN_OPERATION
} SStatePagina;

#define DADES_RS232		60

extern uint8_t numero_aplicacions;
extern uint8_t bytes_RS232[DADES_RS232+1];//Se li suma 1 perque hi ha primer byte que indica llargada
extern SStatePagina StatePagina;
extern SStatePagina CanviarPagina;//Serveix per guardar pàgina quan es fa peticio des de un altre PC
extern char  *usuario;
extern uint8_t *send_buffer_prova;
extern char *contrasenya;
extern char  *usuario_admin;
extern char *contrasenya_admin;
extern uint16_t maxim_full_adress;
extern char new_usuario[8];
extern char new_password[8];
extern uint8_t punter_usuari;
extern uint8_t punter_password;
extern uint32_t node_display;//per saber quin node serà testejat
extern uint8_t tipus_display;//per saber quin tipus de node serà testejat
extern char firm_ware[3];
extern char data_fabricacio[5];
extern char numero_serie[9];
extern char configurats_LC [4];
extern char detectats_LC [4];
extern char error_LC [2];
extern char retard_LC [2];
extern uint8_t error_flags;
extern char remoto_local;
extern uint8_t pulsadors_display;
extern uint8_t byte2_web;
extern uint8_t byte1_web;
unsigned char *PProgram;                         // pointer to webside
extern unsigned int HTTPBytesToSend;                    // bytes left to send
extern unsigned char HTTPStatus;                        // status byte
extern uint8_t actualitza_display;
extern uint8_t actualitza_display_2;
extern uint8_t actualitza_nodes;
extern uint8_t actualitza_nodes_2;
extern uint8_t password_incorrecte;
extern uint8_t comptador_TCP;//Per enviar dos trames de transmissió de http
extern uint8_t two_sending_TCP;
extern uint8_t sesio_iniciada;
extern uint8_t tipus_usuari_web;
extern unsigned short sesio_MAC[3];            // 48 bit MAC
extern unsigned short rebut_MAC[3];            // 48 bit MAC

//Aplicacio
#define MAX_APLICACIONS 4
#define MAX_DADES_INTERMIG	50
extern unsigned short aplicacio_MAC[MAX_APLICACIONS][3];
extern unsigned short aplicacio_IP[MAX_APLICACIONS][2];
extern uint8_t aplicacio_actual;
extern uint8_t aplicacio_sesioActiva;
extern uint8_t esperant_ACK[MAX_APLICACIONS];
extern uint8_t iniciar_sesio_aplicacio;
extern uint8_t llargada_trama[MAX_APLICACIONS];
extern uint8_t RetryCounter_aplicacio[MAX_APLICACIONS];               // nr. of retransmissions
extern uint8_t getmem_aplicacio;
extern uint8_t buffer_intermig_ethernet[MAX_APLICACIONS][MAX_DADES_INTERMIG];
extern uint16_t posicio_buffer_intermig[MAX_APLICACIONS];


void HTTPServer(SStatePagina Pagina);
void HTTPServer_petit(SStatePagina Pagina);
void InsertDynamicValues(void);
void InsertDynamicNodes(void);
uint32_t comprovar(char cadena[], unsigned char *buf,uint8_t size_of_cadena);
uint32_t comprovar_password(unsigned char *cadena, unsigned char *buf, uint8_t size_of_cadena);
void tractament_pagina (void);
void pasar_caracter_numero_3(unsigned char *buf, uint32_t *numero, uint8_t *comptador);
void ini_aplicacio(void);


#define HTTP_SEND_PAGE               0x01        // help flag

#endif
