################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../LCD/lcd_bitmap.c \
../LCD/lcd_driver.c \
../LCD/lcd_shapes.c \
../LCD/lcd_terminal.c \
../LCD/lcd_text.c \
../LCD/system_fixed_be_8_15.c 

OBJS += \
./LCD/lcd_bitmap.o \
./LCD/lcd_driver.o \
./LCD/lcd_shapes.o \
./LCD/lcd_terminal.o \
./LCD/lcd_text.o \
./LCD/system_fixed_be_8_15.o 

C_DEPS += \
./LCD/lcd_bitmap.d \
./LCD/lcd_driver.d \
./LCD/lcd_shapes.d \
./LCD/lcd_terminal.d \
./LCD/lcd_text.d \
./LCD/system_fixed_be_8_15.d 


# Each subdirectory must supply rules for building sources it contributes
LCD/%.o: ../LCD/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DNDEBUG -D__CODE_RED -D__USE_CMSIS=CMSISv2p00_LPC17xx -I"C:\Users\Alba\Google Drive\ELE - Displays_develop\Devices\DPI\FIRMWARE\DPI-V1.8\Altres" -I"C:\Users\Alba\Google Drive\ELE - Displays_develop\Devices\DPI\FIRMWARE\DPI-V1.8\Bitmaps" -I"C:\Users\Alba\Google Drive\ELE - Displays_develop\Devices\DPI\FIRMWARE\DPI-V1.8\Ethernet" -I"C:\Users\Alba\Google Drive\ELE - Displays_develop\Devices\DPI\FIRMWARE\DPI-V1.8\LCD" -I"C:\Users\Alba\Google Drive\ELE - Displays_develop\Devices\DPI\FIRMWARE\DPI-V1.8\RTC" -I"C:\Users\Alba\Google Drive\ELE - Displays_develop\Devices\DPI\FIRMWARE\DPI-V1.8\UART" -I"C:\Users\Alba\Google Drive\ELE - Displays_develop\Devices\DPI\FIRMWARE\DPI-V1.8\Webserver" -I"C:\Users\Alba\Google Drive\ELE - Displays_develop\Devices\DPI\FIRMWARE\CMSISv2p00_LPC17xx\inc" -I"C:\Users\Alba\Google Drive\ELE - Displays_develop\Devices\DPI\FIRMWARE\DPI-V1.8\SD-EEPROM" -Os -Os -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


