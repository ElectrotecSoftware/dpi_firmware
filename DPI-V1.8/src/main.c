//last version
#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <NXP/crp.h>

// Variable to store CRP value in. Will be placed automatically
// by the linker when "Enable Code Read Protect" selected.
// See crp.h header for more information
__CRP const unsigned int CRP_WORD = CRP_NO_CRP;

// TODO: insert other include files here

#include "core_cm3.h"
//#include "type.h"

#include <stdio.h>

#include "all.h"
#include "casa.h"
#include "networking.h"
#include "settings.h"
#include "logo.h"
#include "watchdog.h"

/*****************************************************************************
 **
 **
 ** Descriptions:		Variables globals
 **
 **
 *****************************************************************************/
//char canal1_displayData1[150][44]; //44 bytes per 150 displays (6600bytes)
//char canal1_displayData2[100][44] __attribute__ ((section("AHBSRAM1"))); //44 bytes per 100 displays (4400bytes)
//__attribute__ ((section(".bss.$RamAHB32*"))) char canal2_displayData[250][44]; //44 bytes per 250 displays (11000bytes)
/*****************************************************************************
 **
 ** Descriptions:		Variables nodes.h
 **
 *****************************************************************************/
volatile uint8_t NUM_NODES;//nodes_configurats
volatile uint8_t NUM_NODES_2;//nodes_configurats
volatile uint8_t nodes_correctes;
volatile uint8_t nodes_correctes_2;
volatile uint8_t nodes_correctes_web;
volatile uint8_t nodes_correctes_web_2;
volatile uint8_t alarmes_activades[MAX_NODES_CANAL];
volatile uint8_t alarmes_activades_2[MAX_NODES_CANAL];
volatile uint8_t control_alarmes[MAX_NODES_CANAL];
volatile uint8_t control_alarmes_2[MAX_NODES_CANAL];
volatile uint8_t control_alarmes_HW[MAX_NODES_CANAL];
volatile uint8_t control_alarmes_HW_2[MAX_NODES_CANAL];
volatile uint8_t gestio_led_error;
volatile uint8_t taula_alarms[LONGITUD_ALARMS];
volatile uint16_t posicio_taula_alarms;
volatile uint16_t alarmes_activades_canal1;
volatile uint16_t alarmes_activades_canal2;
volatile uint16_t taula_nodes[MAX_NODES_CANAL];
volatile uint16_t taula_nodes_2[MAX_NODES_CANAL];
 uint16_t taula_nodes_web[MAX_NODES_CANAL];
 uint16_t taula_nodes_web_2[MAX_NODES_CANAL];
volatile uint8_t taula_SD[MAX_NODES_CANAL];
volatile uint8_t taula_SD_2[MAX_NODES_CANAL];
volatile uint8_t taula_configurats[MAX_NODES_CANAL];
volatile uint8_t taula_configurats_2[MAX_NODES_CANAL];
/*****************************************************************************
 **
 ** Descriptions:		Variables timer.h
 **
 *****************************************************************************/
uint32_t timer0_counter;
uint32_t timer1_counter;
uint32_t timer2_counter;
/*****************************************************************************
 **
 ** Descriptions:		Variables blockdev.h
 **
 *****************************************************************************/
volatile uint8_t SD_state;
/*****************************************************************************
 **
 ** Descriptions:		Variables ethernet.h
 **
 *****************************************************************************/
volatile uint8_t Ethernet_Transmit_Buffer[MAX_APLICACIONS][BUFSIZE_ETHERNET];
volatile uint16_t taula_errors[MAX_APLICACIONS][MAX_NODES_CANAL];
volatile uint16_t taula_errors_2[MAX_APLICACIONS][MAX_NODES_CANAL];
volatile uint32_t counter_envios_fallidos;
volatile uint32_t counter_pollings_fallidos;
volatile uint32_t counter_error_CRC;
volatile uint32_t counter_watchdog_uart;
volatile uint32_t counter_error_3;//Serveix per saber quan reinicialitzar les variables
						 //counter_envios_fallidos i counter_pollings_fallidos
unsigned short RemoteMAC_xarxa[3];// MAC i IP per informació de la xarxa
unsigned short RemoteIP_xarxa[2];
unsigned short TCPLocalPort_xarxa;// TCP port per informació de la xarxa
uint16_t posicio_enviament_xarxa;//Serveix per indicar la posicio del enviament de la xarxa
/*****************************************************************************
 **
 ** Descriptions:		Variables timetick
 **
 *****************************************************************************/
volatile uint32_t msTicks_timeout_sesio;
volatile uint32_t msTicks_LCD;
volatile uint32_t msTicks_aplicacio[MAX_APLICACIONS];
volatile uint32_t msTicks_led;
volatile uint32_t msTicks_control_alarmes;
volatile uint32_t msTicks_programacio1;
volatile uint32_t msTicks_programacio2;
volatile uint32_t msTicks_ACK;
volatile uint32_t msTicks_led_error;
/*****************************************************************************
 **
 ** Descriptions:		Variables tcpip.h
 **
 *****************************************************************************/
uint8_t conexio_oberta;
uint8_t MYIP_1; // our internet protocol (IP) address
uint8_t MYIP_2;
uint8_t MYIP_3;
uint8_t MYIP_4;
unsigned short MyIP[2];
unsigned short SubnetMask[2];
uint8_t SUBMASK_1; // subnet mask
uint8_t SUBMASK_2;
uint8_t SUBMASK_3;
uint8_t SUBMASK_4;
unsigned short GatewayIP[2];
uint8_t GWIP_1; // standard gateway (used if remote
uint8_t GWIP_2; // IP is no part of our subnet)
uint8_t GWIP_3;
uint8_t GWIP_4;
unsigned char MyMAC[6];
uint8_t cambiar_ethernet;
TTCPStateMachine TCPStateMachine; // perhaps the most important var at all ;-)
TLastFrameSent LastFrameSent; // retransmission type
unsigned short ISNGenHigh; // upper word of our Initial Sequence Number
unsigned long TCPSeqNr; // next sequence number to send
unsigned long TCPSeqNr_aplicacio[MAX_APLICACIONS]; // next sequence number to send
unsigned long TCPUNASeqNr; // last unaknowledged sequence number
// incremented AFTER sending data
unsigned long TCPAckNr; // next seq to receive and ack to send
// incremented AFTER receiving data
unsigned long TCPAckNr_aplicacio[MAX_APLICACIONS];
unsigned char TCPTimer; // inc'd each 262ms
unsigned char RetryCounter; // nr. of retransmissions
unsigned short RecdFrameLength; // EMAC reported frame length
unsigned short RecdFrameMAC[3]; // 48 bit MAC
unsigned short RecdFrameIP[2]; // 32 bit IP
unsigned short RecdIPFrameLength; // 16 bit IP packet length
unsigned short _TxFrame1[(ETH_HEADER_SIZE + IP_HEADER_SIZE + TCP_HEADER_SIZE
		+ MAX_TCP_TX_DATA_SIZE) / 2];
unsigned short _TxFrame3[(ETH_HEADER_SIZE + IP_HEADER_SIZE + TCP_HEADER_SIZE
		+ MAX_TCP_TX_DATA_SIZE) / 2];
unsigned short _TxFrame4[(ETH_HEADER_SIZE + IP_HEADER_SIZE + TCP_HEADER_SIZE
		+ MAX_TCP_TX_DATA_SIZE) / 2];
unsigned short _TxFrame2[(ETH_HEADER_SIZE + MAX_ETH_TX_DATA_SIZE) / 2];
unsigned short _RxTCPBuffer[MAX_TCP_RX_DATA_SIZE / 2];
uint16_t PORT_COMUNICACIO;
uint8_t port_comunicacio_RAM[2];
uint8_t frame_display;
unsigned short TxFrame1Size; // bytes to send in TxFrame1
unsigned char TxFrame2Size; // bytes to send in TxFrame2
unsigned short TxFrame3Size; // bytes to send in TxFrame3
unsigned short TxFrame4Size; // bytes to send in TxFrame4
unsigned char TransmitControl;
unsigned char TCPFlags;
unsigned short NrOfDataBytes;
unsigned short TCPRxDataCount; // nr. of bytes rec'd
unsigned short TCPTxDataCount; // nr. of bytes to send
unsigned short TCPTxDataCount_ethernet; // nr. of bytes to send
unsigned short TCPLocalPort; // set port we want to listen to
unsigned short TCPRemotePort;
unsigned short TCPRemotePort_aplicacio[MAX_APLICACIONS];
unsigned short TCPRemotePort_RST;
unsigned short RemoteMAC[3]; // MAC and IP of current TCP-session
unsigned short RemoteIP[2];
unsigned short web_MAC[3];
unsigned short web_IP[2];
unsigned char SocketStatus;
/*****************************************************************************
 **
 ** Descriptions:		Variables webserver.h
 **
 *****************************************************************************/
uint8_t numero_aplicacions;
uint8_t bytes_RS232[DADES_RS232 + 1];
SStatePagina StatePagina;
SStatePagina CanviarPagina;
char *usuario;
uint8_t *send_buffer_prova;
char *contrasenya;
char *usuario_admin;
char *contrasenya_admin;
uint16_t maxim_full_adress;
char new_usuario[8];
char new_password[8];
uint8_t punter_usuari;
uint8_t punter_password;
uint32_t node_display;//per saber quin node serà testejat
uint8_t tipus_display;//per saber quin tipus de node serà testejat
char firm_ware[3];
char data_fabricacio[5];
char numero_serie[9];
char configurats_LC [4];
char detectats_LC [4];
char error_LC [2];
char retard_LC [2];
uint8_t error_flags;
char remoto_local;
uint8_t pulsadors_display;
uint8_t byte2_web;
uint8_t byte1_web;
unsigned int HTTPBytesToSend; // bytes left to send
unsigned char HTTPStatus; // clear HTTP-server's flag register
uint8_t actualitza_display; //per fer push a la web display
uint8_t actualitza_display_2; //per fer push a la web display
uint8_t actualitza_nodes; //per fer push a la web nodes
uint8_t actualitza_nodes_2; //per fer push a la web nodes
uint8_t password_incorrecte;
uint8_t comptador_TCP;//Per enviar dos trames de transmissió de http
uint8_t two_sending_TCP;
uint8_t sesio_iniciada;
uint8_t tipus_usuari_web;
unsigned short sesio_MAC[3]; // 48 bit MAC
unsigned short rebut_MAC[3]; // 48 bit MAC
uint8_t aplicacio_actual;
uint8_t aplicacio_sesioActiva;
uint8_t llargada_trama[MAX_APLICACIONS];
uint8_t RetryCounter_aplicacio[MAX_APLICACIONS];
uint8_t getmem_aplicacio;
uint8_t buffer_intermig_ethernet[MAX_APLICACIONS][MAX_DADES_INTERMIG];
uint16_t posicio_buffer_intermig[MAX_APLICACIONS];
uint8_t esperant_ACK[MAX_APLICACIONS];
uint8_t iniciar_sesio_aplicacio;
unsigned short aplicacio_MAC[MAX_APLICACIONS][3];
unsigned short aplicacio_IP[MAX_APLICACIONS][2];
/*****************************************************************************
 **
 ** Descriptions:		Variables trames.h
 **
 *****************************************************************************/
uint8_t getmem_adress;
uint16_t ultim_node_programat;
uint16_t primer_node_desprogramat;
uint16_t node_a_desprogramar;
uint16_t reprogramar;
uint8_t old_adress_programacio1_canal1;
uint8_t new_adress_programacio1_canal1;
uint8_t new_adress_programacio2_canal1;
uint8_t old_adress_programacio1_canal2;
uint8_t new_adress_programacio1_canal2;
uint8_t new_adress_programacio2_canal2;
uint8_t peticio_programacio;
uint8_t peticio_programacio_2;
uint8_t escrivint[MAX_NODES_CANAL];
uint8_t escrivint_2[MAX_NODES_CANAL];
uint8_t actualitza_nodes_RAM;
/*****************************************************************************
 **
 ** Descriptions:		Variables uart.h
 **
 *****************************************************************************/
SStateUART StateUART_canal1;
SStateUART StateUART_canal2;
//SEND_BUFFER_TypeDef *buffer_prova=(SEND_BUFFER_TypeDef *)(0x2007C000);
//__attribute__ ((section(".bss.$RamAHB32*"))) char canal2_displayData[250][44]; //44 bytes per 250 displays (11000bytes)
uint8_t send_buffer[250][MAX_DADES_NODE];//50 bytes per 250 displays (12500bytes)
uint8_t prova_send_buffer[50];
__attribute__ ((section(".bss.$RamAHB32*"))) uint8_t send_buffer_2[250][MAX_DADES_NODE]; //50 bytes per 250 displays (12500bytes)
uint8_t malloc_buffer[MAX_NODES_CANAL];
uint8_t malloc_buffer_2[MAX_NODES_CANAL];
volatile uint8_t nombre_mallocs;
volatile uint8_t nombre_mallocs_2;
volatile uint32_t watchdog_uart_1;
volatile uint32_t watchdog_uart_2;
volatile uint32_t watchdog_uart_3;
volatile uint32_t watchdog_uart_4;
volatile uint8_t watchdog_uart_1_enable;
volatile uint8_t watchdog_uart_2_enable;
volatile uint8_t watchdog_uart_3_enable;
volatile uint8_t watchdog_uart_4_enable;
volatile uint8_t length_UART0;
volatile uint8_t length_UART1;
volatile uint8_t length_UART2;
volatile uint8_t length_UART3;
volatile uint8_t posicio_configurats;
volatile uint8_t posicio_configurats_2;
volatile uint8_t wire_correcte;
volatile uint8_t wire_correcte_2;
volatile uint8_t wire_count;
volatile uint8_t wire_count_2;
volatile uint8_t comptador_leds_uart[4];
volatile uint8_t UART0Transmit_Buffer[BUFSIZE_TX], UART1Transmit_Buffer[BUFSIZE_TX];
volatile uint8_t UART2Transmit_Buffer[BUFSIZE_TX], UART3Transmit_Buffer[BUFSIZE_TX];
volatile uint8_t UARTTransmit_Broadcast[MAX_DADES_NODE];
volatile uint8_t UARTTransmit_Broadcast_2[MAX_DADES_NODE];
volatile uint8_t Broadcast_polling;
volatile uint8_t Broadcast_polling_2;
volatile uint8_t enviament_broadcast; //Per si es fa mes d'un enviament en un sol polling
volatile uint8_t enviament_broadcast_1_anterior;
volatile uint8_t reenviaments_broadcast_1;
volatile uint8_t enviament_broadcast_2;
volatile uint8_t enviament_broadcast_2_anterior;
volatile uint8_t reenviaments_broadcast_2;
volatile uint8_t node_actual;
volatile uint8_t node_actual_2;
volatile uint8_t despres_heartbeat;
/*****************************************************************************
 **
 ** Descriptions:		Variables lcd_driver.h
 **
 *****************************************************************************/
SStateLCD StateLCD;
uint8_t change_state;
uint8_t brillo;
uint8_t PWM_high;
uint8_t PWM_cont;
uint8_t x_config;
uint8_t y_config;
uint8_t fila;
uint8_t columna;
char numero_LCD[2];
char text[16];
uint8_t IP_LCD[15];//={1,9,2,0,1,6,8,0,0,0,1,0,2,0,0};
uint8_t NM_LCD[15];//={2,5,5,0,2,5,5,0,2,5,5,0,0,0,0};
uint8_t GW_LCD[15];//={1,9,2,0,1,6,8,0,0,0,2,0,0,5,4};
uint8_t cont_LCD;
/*****************************************************************************
 ** Function name:		main
 **
 ** Descriptions:		main routine for UART and webserver
 **
 ** parameters:			None
 ** Returned value:		int
 **
 *****************************************************************************/
//uint8_t comptador = 0;
//uint8_t x;
void Inicialitzacio(void) {
	uint16_t i, j = 0;
	/*****************************************************************************
	 **
	 **
	 ** Descriptions:		Variables globals
	 **
	 **
	 *****************************************************************************/
	/*****************************************************************************
	 **
	 ** Descriptions:		Variables nodes.h
	 **
	 *****************************************************************************/
	NUM_NODES = 0;
	NUM_NODES_2 = 0;
	nodes_correctes = 0;
	nodes_correctes_2 = 0;
	nodes_correctes_web = 0;
	nodes_correctes_web_2 = 0;
	for (i = 0; i < MAX_NODES_CANAL; i++) {
		alarmes_activades[i] = 0;
		alarmes_activades_2[i] = 0;
		control_alarmes[i] = 0;
		control_alarmes_2[i] = 0;
		control_alarmes_HW[i] = 0;
		control_alarmes_HW_2[i] = 0;
		taula_nodes[i] = 0;
		taula_nodes_2[i] = 0;
		taula_nodes_web[i] = 0;
		taula_nodes_web_2[i] = 0;
		taula_SD[i] = 0;
		taula_SD_2[i] = 0;
		taula_configurats[i] = 0;
		taula_configurats_2[i] = 0;
	}

	gestio_led_error = 0;
	for (i = 0; i < LONGITUD_ALARMS; i++) {
		taula_alarms[i] = 0;
	}
	posicio_taula_alarms = 0;
	alarmes_activades_canal1 = 0;
	alarmes_activades_canal2 = 0;

	/*****************************************************************************
	 **
	 ** Descriptions:		Variables timer.h
	 **
	 *****************************************************************************/
	timer0_counter = 0;
	timer1_counter = 0;
	timer2_counter = 0;
	/*****************************************************************************
	 **
	 ** Descriptions:		Variables blockdev.h
	 **
	 *****************************************************************************/
	SD_state = 0;
	/*****************************************************************************
	 **
	 ** Descriptions:		Variables ethernet.h
	 **
	 *****************************************************************************/
	for (i = 0; i < MAX_APLICACIONS; i++) {
		for (j = 0; j < BUFSIZE_ETHERNET; j++) {
			Ethernet_Transmit_Buffer[i][j] = 0;
		}
	}
	for (i = 0; i < MAX_APLICACIONS; i++) {
		for (j = 0; j < MAX_NODES_CANAL; j++) {
			taula_errors[i][j] = 0;
		}
	}
	for (i = 0; i < MAX_APLICACIONS; i++) {
		for (j = 0; j < MAX_NODES_CANAL; j++) {
			taula_errors_2[i][j] = 0;
		}
	}
	counter_envios_fallidos = 0;
	counter_pollings_fallidos = 0;
	counter_error_CRC = 0;
	counter_watchdog_uart = 0;
	counter_error_3 = 2000;
	for(i=0;i<3;i++)
	{
		RemoteMAC_xarxa[i]=0;
	}
	for(i=0;i<2;i++)
	{
		RemoteIP_xarxa[i]=0;
	}
	TCPLocalPort_xarxa = 0;
	posicio_enviament_xarxa = 0;
	/*****************************************************************************
	 **
	 ** Descriptions:		Variables timetick
	 **
	 *****************************************************************************/
	msTicks_timeout_sesio = 0;
	msTicks_LCD = 0;
	for (i = 0; i < MAX_APLICACIONS; i++) {
		msTicks_aplicacio[i] = 3000;
	}
	msTicks_led = 1000;
	msTicks_control_alarmes = 1000;
	msTicks_programacio1 = 1000;
	msTicks_programacio2 = (1000 * 60 * 16);
	msTicks_led_error = 0;
	msTicks_ACK = 0;
	/*****************************************************************************
	 **
	 ** Descriptions:		Variables tcpip.h
	 **
	 *****************************************************************************/
	conexio_oberta = 0;
	MYIP_1 = 192; // our internet protocol (IP) address
	MYIP_2 = 168;
	MYIP_3 = 0;
	MYIP_4 = 200;
	for (i = 0; i < 2; i++) {
		MyIP[i] = 0;
	}
	for (i = 0; i < 2; i++) {
		SubnetMask[i] = 0;
	}
	SUBMASK_1 = 255; // subnet mask
	SUBMASK_2 = 255;
	SUBMASK_3 = 255;
	SUBMASK_4 = 0;
	for (i = 0; i < 2; i++) {
		GatewayIP[i] = 0;
	}
	GWIP_1 = 192; // standard gateway (used if remote
	GWIP_2 = 168; // IP is no part of our subnet)
	GWIP_3 = 0;
	GWIP_4 = 1;
	cambiar_ethernet = 0;
	TCPStateMachine = CLOSED; // perhaps the most important var at all ;-)
	LastFrameSent = 0; // retransmission type
	ISNGenHigh = 0; // upper word of our Initial Sequence Number
	TCPSeqNr = 0; // next sequence number to send
	for (i = 0; i < MAX_APLICACIONS; i++) {
		TCPSeqNr_aplicacio[i] = 0; // next sequence number to send
	}
	TCPUNASeqNr = 0; // last unaknowledged sequence number
	// incremented AFTER sending data
	TCPAckNr = 0; // next seq to receive and ack to send
	// incremented AFTER receiving data
	for (i = 0; i < MAX_APLICACIONS; i++) {
		TCPAckNr_aplicacio[i] = 0;
	}
	TCPTimer = 0; // inc'd each 262ms
	RetryCounter = 0; // nr. of retransmissions
	RecdFrameLength = 0; // EMAC reported frame length
	for (i = 0; i < 3; i++) {
		RecdFrameMAC[i] = 0; // 48 bit MAC
	}
	for (i = 0; i < 2; i++) {
		RecdFrameIP[i] = 0; // 32 bit IP
	}
	RecdIPFrameLength = 0; // 16 bit IP packet length
	for (i = 0; i < (ETH_HEADER_SIZE + IP_HEADER_SIZE + TCP_HEADER_SIZE
			+ MAX_TCP_TX_DATA_SIZE) / 2; i++) {
		_TxFrame1[i] = 0;
		_TxFrame2[i] = 0;
		_TxFrame3[i] = 0;
		_TxFrame4[i] = 0;
	}
	for (i = 0; i < MAX_TCP_RX_DATA_SIZE / 2; i++) {
		_RxTCPBuffer[i] = 0;
	}
	PORT_COMUNICACIO = 3030;
	for(i=0;i<2;i++)
	{
		port_comunicacio_RAM[i]=0;
	}
	frame_display = 0;
	for (i = 0; i < 3; i++) {
		RecdFrameMAC[i] = 0;
	}
	for (i = 0; i < 2; i++) {
		RecdFrameIP[i] = 0;
	}
	TxFrame1Size = 0; // bytes to send in TxFrame1
	TxFrame2Size = 0; // bytes to send in TxFrame2
	TxFrame3Size = 0; // bytes to send in TxFrame3
	TxFrame4Size = 0; // bytes to send in TxFrame4
	TransmitControl = 0;
	TCPFlags = 0;
	NrOfDataBytes = 0;
	TCPRxDataCount = 0; // nr. of bytes rec'd
	TCPTxDataCount = 0; // nr. of bytes to send
	TCPTxDataCount_ethernet = 0; // nr. of bytes to send
	TCPLocalPort = TCP_PORT_HTTP; // set port we want to listen to
	TCPRemotePort = 0;
	for (i = 0; i < MAX_APLICACIONS; i++) {
		TCPRemotePort_aplicacio[i] = 0;
	}
	TCPRemotePort_RST = 0;
	for (i = 0; i < 3; i++) {
		RemoteMAC[i] = 0; // MAC and IP of current TCP-session
	}
	for (i = 0; i < 2; i++) {
		RemoteIP[i] = 0; // MAC and IP of current TCP-session
	}
	for (i = 0; i < 3; i++) {
		web_MAC[i] = 0;
	}
	for (i = 0; i < 2; i++) {
		web_IP[i] = 0;
	}
	SocketStatus = 0;
	/*****************************************************************************
	 **
	 ** Descriptions:		Variables webserver.h
	 **
	 *****************************************************************************/
	numero_aplicacions = 0;
	for (i = 0; i < DADES_RS232 + 1; i++) {
		bytes_RS232[i] = 0;
	}
	StatePagina = MENU;
	CanviarPagina = NO_CANVIAR_PAGINA;
	usuario = "user";
	send_buffer_prova = 0;
	contrasenya = "user";
	usuario_admin = "admin";
	contrasenya_admin = "admin";
	maxim_full_adress = 0;
	for (i = 0; i < 8; i++) {
		new_usuario[i] = 0;
	}
	for (i = 0; i < 8; i++) {
		new_password[i] = 0;
	}
	punter_usuari = 4;
	punter_password = 4;
	node_display = 0;//per saber quin node serà testejat
	tipus_display = 0;//per saber quin tipus de node serà testejat
	for (i = 0; i < 3; i++) {
		firm_ware[i] = 0;
	}
	for (i = 0; i < 5; i++) {
		data_fabricacio[i] = 0;
	}
	for (i = 0; i < 9; i++) {
		numero_serie[i] = 0;
	}
	for (i = 0; i < 4; i++) {
		configurats_LC[i] = 0;
	}
	for (i = 0; i < 4; i++) {
		detectats_LC[i] = 0;
	}
	for (i = 0; i < 2; i++) {
		error_LC[i] = 0;
	}
	for (i = 0; i < 2; i++) {
		retard_LC[i] = 0;
	}
	error_flags = 0;
	remoto_local = '0';
	pulsadors_display = 0;
	byte2_web = 0;
	byte1_web = 0;
	HTTPBytesToSend = 0; // bytes left to send
	HTTPStatus = 0; // clear HTTP-server's flag register
	actualitza_display = 0; //per fer push a la web display
	actualitza_display_2 = 0; //per fer push a la web display
	actualitza_nodes = 0; //per fer push a la web nodes
	actualitza_nodes_2 = 0; //per fer push a la web nodes
	password_incorrecte = 0;
	comptador_TCP = 0;//Per enviar dos trames de transmissió de http
	two_sending_TCP = 0;
	sesio_iniciada = 0;
	tipus_usuari_web = 0;
	for (i = 0; i < 3; i++) {
		sesio_MAC[i] = 0; // 48 bit MAC
	}
	for (i = 0; i < 3; i++) {
		rebut_MAC[i] = 0; // 48 bit MAC
	}
	aplicacio_actual = 0;
	for (i = 0; i < MAX_APLICACIONS; i++) {
		llargada_trama[i] = 0;
	}
	for (i = 0; i < MAX_APLICACIONS; i++) {
		RetryCounter_aplicacio[i] = MAX_RETRYS;
	}
	getmem_aplicacio = 0;

	for (i = 0; i < MAX_APLICACIONS; i++) {
		for (j = 0; j < MAX_DADES_INTERMIG; j++) {
			buffer_intermig_ethernet[i][j] = 0;
		}
	}

	for (i = 0; i < MAX_APLICACIONS; i++) {
		posicio_buffer_intermig[i] = 0;
	}
	for (i = 0; i < MAX_APLICACIONS; i++) {
		esperant_ACK[i] = 0;
	}
	iniciar_sesio_aplicacio = 0;
	for (i = 0; i < MAX_APLICACIONS; i++) {
		for (j = 0; j < 3; j++) {
			aplicacio_MAC[i][j] = 0;
		}
	}
	for (i = 0; i < MAX_APLICACIONS; i++) {
		for (j = 0; j < 2; j++) {
			aplicacio_IP[i][j] = 0;
		}
	}
	/*****************************************************************************
	 **
	 ** Descriptions:		Variables trames.h
	 **
	 *****************************************************************************/
	getmem_adress = 0;
	ultim_node_programat = 0;
	primer_node_desprogramat = 0;
	node_a_desprogramar = 0;
	reprogramar = 0;
	old_adress_programacio1_canal1 = 0;
	new_adress_programacio1_canal1 = 0;
	new_adress_programacio2_canal1 = 0;
	old_adress_programacio1_canal2 = 0;
	new_adress_programacio1_canal2 = 0;
	new_adress_programacio2_canal2 = 0;
	peticio_programacio = 0;
	peticio_programacio_2 = 0;
	for (i = 0; i < MAX_NODES_CANAL; i++) {
		escrivint[i] = 0;
	}
	for (i = 0; i < MAX_NODES_CANAL; i++) {
		escrivint_2[i] = 0;
	}
	actualitza_nodes_RAM = 1;
	/*****************************************************************************
	 **
	 ** Descriptions:		Variables uart.h
	 **
	 *****************************************************************************/
	StateUART_canal1 = FINAL;
	StateUART_canal2 = FINAL;
	//MAX_DADES_NODE
	//MAX_NODES_CANAL
	//send_buffer = (uint8_t **) calloc(MAX_NODES_CANAL + 1, sizeof(uint8_t *));
	//send_buffer_2 = (uint8_t **) calloc(MAX_NODES_CANAL + 1, sizeof(uint8_t *));
	for (i = 0; i < MAX_NODES_CANAL; i++) {
		malloc_buffer[i] = 0;
	}
	for (i = 0; i < MAX_NODES_CANAL; i++) {
		malloc_buffer_2[i] = 0;
	}
	nombre_mallocs = 0;
	nombre_mallocs_2 = 0;
	watchdog_uart_1 = 0;
	watchdog_uart_2 = 0;
	watchdog_uart_3 = 0;
	watchdog_uart_4 = 0;
	watchdog_uart_1_enable = 0;
	watchdog_uart_2_enable = 0;
	watchdog_uart_3_enable = 0;
	watchdog_uart_4_enable = 0;
	length_UART0 = 0;
	length_UART1 = 0;
	length_UART2 = 0;
	length_UART3 = 0;
	posicio_configurats = 0;
	posicio_configurats_2 = 0;
	wire_correcte = TRUE;
	wire_correcte_2 = TRUE;
	wire_count = 0;
	wire_count_2 = 0;
	for (i = 0; i < 4; i++) {
		comptador_leds_uart[i] = 0;
	}
	for (i = 0; i < BUFSIZE_TX; i++) {
		UART0Transmit_Buffer[i] = 0;
		UART1Transmit_Buffer[i] = 0;
		UART2Transmit_Buffer[i] = 0;
		UART3Transmit_Buffer[i] = 0;
	}
	for (i = 0; i < MAX_DADES_NODE; i++) {
		UARTTransmit_Broadcast[i] = 0;
	}
	for (i = 0; i < MAX_DADES_NODE; i++) {
		UARTTransmit_Broadcast_2[i] = 0;
	}
	Broadcast_polling = 0;
	Broadcast_polling_2 = 0;
	enviament_broadcast = 0;
	enviament_broadcast_1_anterior = 0;
	reenviaments_broadcast_1 = 0;
	enviament_broadcast_2 = 0;
	enviament_broadcast_2_anterior = 0;
	reenviaments_broadcast_2 = 0;
	node_actual = 0;
	node_actual_2 = 0;
	despres_heartbeat = 0;
	/*****************************************************************************
	 **
	 ** Descriptions:		Variables lcd_driver.h
	 **
	 *****************************************************************************/
	StateLCD = INICI;
	change_state = 0;
	brillo = 100;
	PWM_high = 4;
	PWM_cont = 0;
	x_config = 4;
	y_config = 37;
	fila = 1;
	columna = 1;
	for (i = 0; i < 2; i++) {
		numero_LCD[i] = '0';
	}
	for (i = 0; i < 16; i++) {
		text[i] = '0';
	}
	for (i = 0; i < 15; i++) {
		IP_LCD[i] = 0;
		NM_LCD[i] = 0;
		GW_LCD[i] = 0;
	}
	cont_LCD = 0;
	//for(i=0;i<250;i++)for(j=0;j<44;j++) canal2_displayData[i][j]=1;
	cont_LCD = 0;
}
/*****************************************************************************
** Function name:		ReinvokeISP
**
** Descriptions:		Funció per entrar en ISP el mòdul
**
** parameters:			None
** Returned value:		None
**
*****************************************************************************/
//IAP
#define IAP_ADDRESS 0x1FFF1FF1
unsigned param_table[5]; unsigned result_table[5];

void iap_entry(unsigned param_tab[],unsigned result_tab[])
{
 void (*iap)(unsigned [],unsigned []);
 iap = (void (*)(unsigned [],unsigned []))IAP_ADDRESS;
 iap(param_tab,result_tab);
}
void DisconnectPLL0(){
    // good practice to disable before feeding
    __disable_irq();
    // disconnect
    LPC_SC->PLL0CON = 0x1;
    LPC_SC->PLL0FEED = 0xAA;
    LPC_SC->PLL0FEED = 0x55;
    while (LPC_SC->PLL0STAT&(1<<25));
    // power down
    LPC_SC->PLL0CON = 0x0;
    LPC_SC->PLL0FEED = 0xAA;
    LPC_SC->PLL0FEED = 0x55;
    while (LPC_SC->PLL0STAT&(1<<24));
    // This is the default flash read/write setting for IRC
    LPC_SC->FLASHCFG &= 0x0fff;
    LPC_SC->FLASHCFG |= 0x5000;
    LPC_SC->CCLKCFG = 0x0;
    //  Select the IRC as clk
    LPC_SC->CLKSRCSEL = 0x00;
    // not using XTAL anymore
    LPC_SC->SCS = 0x00;
}


 int main(void) {
	Inicialitzacio();
	SystemCoreClockUpdate();
	//SystemInit();
	leds_joystick_init();
	LCDdriver_initialisation();
	if(!(LPC_GPIO1->FIOPIN & (OK_LCD))) //Per posar en mode ISP al premer OK al inici
	{
		//ReinvokeISP();
		NVIC->ICER[0] = 0xFFFFFFFF;    //disable all interrupts
		DisconnectPLL0();
		NVIC->ICER[0] = 0xFFFFFFFF;    //disable all interrupts
		//! stop all ISRs
		   __disable_irq();
		param_table[0] = 57;            //IAP command
		led_on(LED_ERROR); //Encenem led error per indicar que entrem en mode programació
		iap_entry(param_table,result_table);
	}

	if(LPC_GPIO3->FIOPIN & (1<<26))
	{//Si jumper no posat
		LPC_GPIO1->FIOCLR = (1 << 29); //rele
	}
	else
	{//Si jumper posat
		LPC_GPIO1->FIOSET = (1 << 29); //rele
	}
	#if INTERFACE_RS485 == 1
	LPC_GPIO1->FIOCLR = (1 << 29); //rele
	#endif
	//PWM
	LPC_PINCON->PINSEL4 &= (~0x000C0000);
	LPC_GPIO2->FIODIR |= (1 << 9);
	LPC_GPIO2->FIOSET = (1 << 9);

	LCD_ClearScreen(COLOR_WHITE);
	LCD_PlotBitmap24(3, 44, logo_pixel_data, logo_width, logo_height);
	//Start_SysTick1ms();
	ini_nodes();
	ini_nodes_2();
	ini_taula_alarms();
	ini_aplicacio();
	ini_SD();	//Inicialització Taula de nodes SD canal 1
	ini_SD_2(); //Inicialització Taula de nodes SD canal 2
	if (tarjetaSDinit()!=1)
	{
		gestio_led_error |= (1<<3);
	}
	llegirMAC_EEPROM();

	led_on(LED_ACTIVITY); //Encenem led blau per veure si tenim HWfault aqui
	TCPLowLevelInit();
	led_off(LED_ACTIVITY); //Apaguem led blau que indica HWfault

	//Inicialitza la taula amb 1 display DPAZ1 i 4 displays DPA1
	//taula_SD[0]=2;
	//taula_SD[1]=1;taula_SD[2]=1;taula_SD[3]=1;taula_SD[4]=1;
	//Inicialitza la taula amb 250 display DPA1 i 250 displays LC
	//char i;
	//for(i=0; i<250;i++) taula_SD[i]=1;
	//for(i=0; i<5;i++) taula_SD_2[i]=1;
	//taula_SD_2[0]=1; taula_SD_2[1]=1;
	//taula_SD_2[5]=10;
	//IP inicial
	IP_LCD[0] = MYIP_1 / 100;
	IP_LCD[1] = (MYIP_1 - ((IP_LCD[0]) * 100)) / 10;
	IP_LCD[2] = MYIP_1 - (((IP_LCD[0]) * 100) + ((IP_LCD[1]) * 10));
	IP_LCD[4] = MYIP_2 / 100;
	IP_LCD[5] = (MYIP_2 - ((IP_LCD[4]) * 100)) / 10;
	IP_LCD[6] = MYIP_2 - (((IP_LCD[4]) * 100) + ((IP_LCD[5]) * 10));
	IP_LCD[8] = MYIP_3 / 100;
	IP_LCD[9] = (MYIP_3 - ((IP_LCD[8]) * 100)) / 10;
	IP_LCD[10] = MYIP_3 - (((IP_LCD[8]) * 100) + ((IP_LCD[9]) * 10));
	IP_LCD[12] = MYIP_4 / 100;
	IP_LCD[13] = (MYIP_4 - ((IP_LCD[12]) * 100)) / 10;
	IP_LCD[14] = MYIP_4 - (((IP_LCD[12]) * 100) + ((IP_LCD[13]) * 10));
	//NM inicial
	NM_LCD[0] = SUBMASK_1 / 100;
	NM_LCD[1] = (SUBMASK_1 - ((NM_LCD[0]) * 100)) / 10;
	NM_LCD[2] = SUBMASK_1 - (((NM_LCD[0]) * 100) + ((NM_LCD[1]) * 10));
	NM_LCD[4] = SUBMASK_2 / 100;
	NM_LCD[5] = (SUBMASK_2 - ((NM_LCD[4]) * 100)) / 10;
	NM_LCD[6] = SUBMASK_2 - (((NM_LCD[4]) * 100) + ((NM_LCD[5]) * 10));
	NM_LCD[8] = SUBMASK_3 / 100;
	NM_LCD[9] = (SUBMASK_3 - ((NM_LCD[8]) * 100)) / 10;
	NM_LCD[10] = SUBMASK_3 - (((NM_LCD[8]) * 100) + ((NM_LCD[9]) * 10));
	NM_LCD[12] = SUBMASK_4 / 100;
	NM_LCD[13] = (SUBMASK_4 - ((NM_LCD[12]) * 100)) / 10;
	NM_LCD[14] = SUBMASK_4 - (((NM_LCD[12]) * 100) + ((NM_LCD[13]) * 10));
	//GW inicial
	GW_LCD[0] = GWIP_1 / 100;
	GW_LCD[1] = (GWIP_1 - ((GW_LCD[0]) * 100)) / 10;
	GW_LCD[2] = GWIP_1 - (((GW_LCD[0]) * 100) + ((GW_LCD[1]) * 10));
	GW_LCD[4] = GWIP_2 / 100;
	GW_LCD[5] = (GWIP_2 - ((GW_LCD[4]) * 100)) / 10;
	GW_LCD[6] = GWIP_2 - (((GW_LCD[4]) * 100) + ((GW_LCD[5]) * 10));
	GW_LCD[8] = GWIP_3 / 100;
	GW_LCD[9] = (GWIP_3 - ((GW_LCD[8]) * 100)) / 10;
	GW_LCD[10] = GWIP_3 - (((GW_LCD[8]) * 100) + ((GW_LCD[9]) * 10));
	GW_LCD[12] = GWIP_4 / 100;
	GW_LCD[13] = (GWIP_4 - ((GW_LCD[12]) * 100)) / 10;
	GW_LCD[14] = GWIP_4 - (((GW_LCD[12]) * 100) + ((GW_LCD[13]) * 10));
	PORT_COMUNICACIO =port_comunicacio_RAM[0]<<8|port_comunicacio_RAM[1];
	/*for (cont_LCD = 0; cont_LCD < 10; cont_LCD++) {
		taula_SD[cont_LCD] = 1;
	}
	for (cont_LCD = 0; cont_LCD < 10; cont_LCD++) {
		taula_SD_2[cont_LCD] = 1;
	}
	cont_LCD = 0;*/
	ini_taula_configurats();
	ini_taula_web();

	/* Initialize RTC with RTCCAL and RTCDIR values */
	InitRTC();
	/* start RTC */
	StartRTC();

	UARTInit(0, 460800); /* Port A Canal 1 */
	UARTInit(1, 460800); /* Port B Canal 1 */
	UARTInit(2, 460800); /* Port A Canal 2 */
	UARTInit(3, 460800); /* Port B Canal 2 */

	// Initialise all timers
	//init_timer(0,SystemCoreClock/100000 - 1);//cada 10us
	init_timer(1, SystemCoreClock / 100000 - 1);//cada 10us
	init_timer(2, SystemCoreClock / 100000 - 1);//cada 10us

	LCD_PlotBitmap24(10, 5, casa_pixel_data, casa_width, casa_height);
	LCD_PlotBitmap24(31, 5, networking_pixel_data, networking_width,
			networking_height);
	LCD_PlotBitmap24(52, 5, settings_pixel_data, settings_width,
			settings_height);
	LCD_Rect(8, 27, 2, 22, COLOR_BLAU_CEL);
	LCD_FilledCircle(88, 12, 8, COLOR_GREEN);
	LCD_PrintString(88 - 4, 12 - 6, "1", COLOR_BLACK, COLOR_GREEN);
	LCD_FilledCircle(109, 12, 8, COLOR_GREEN);
	LCD_PrintString(109 - 3, 12 - 6, "2", COLOR_BLACK, COLOR_GREEN);
	LCD_Line(0, 128, 25, 25, COLOR_BLUE);
	LCD_Line(0, 128, 24, 24, COLOR_BLUE);
	LCD_FilledRect(0, 127, 26, 127, COLOR_BLAU_CEL);
	//if(SD_state)LCD_PrintString(109-3,12-6,"4", COLOR_BLACK, COLOR_RED);
	#if IS_DEBUG==0
		wd_init(2);//Watchdog reseteja als 2s d'inactivitat
	#endif
	while (1) {
		#if IS_DEBUG==0
			wd_reset();//Watchdog reset
		#endif
		//WEBSERVER
		if (!(SocketStatus & SOCK_ACTIVE))
			TCPPassiveOpen();
		DoNetworkStuff();
		//for (i=0;i<30000;i++) for (j=0;j<30000;j++) for (k=0;k<30000;k++);
		if (conexio_oberta && StatePagina != ESPERA_DISPLAY && StatePagina
				!= ESPERA_NODES && StatePagina != ESPERA_DISPLAY_2
				&& StatePagina != ESPERA_NODES_2 && StatePagina != ESPERA_RS232
				&& StatePagina != ESPERA_LC && StatePagina != ESPERA_LC_2) {
			if (!sesio_iniciada && StatePagina != MENU && StatePagina != IMATGE
					&& StatePagina != FAVICON)
			{
				StatePagina = ERROR;
			}
			if ((sesio_MAC[0] != rebut_MAC[0])
					|| (sesio_MAC[1] != rebut_MAC[1]) || (sesio_MAC[2]
					!= rebut_MAC[2]) || StatePagina == FAVICON)//si la MAC no concorda
			{
				if (StatePagina != FAVICON && sesio_iniciada == TRUE)
				{
					CanviarPagina = StatePagina;
					StatePagina = ERROR;
				}
			}
			if ((StatePagina != ACTUALITZA && StatePagina != ACTUALITZA_DISPLAY
					&& StatePagina != ACTUALITZA_2 && StatePagina
					!= ACTUALITZA_DISPLAY_2 && StatePagina != ACTUALITZA_RS232
					&& StatePagina != CAMBIAR_LENGUAJE && StatePagina != BLANC
					&& StatePagina != BLANC_2 && StatePagina != BLANC_3
					&& StatePagina != BLANC_4 && StatePagina != FAVICON
					//&& StatePagina != INICI_WEB //&& StatePagina != ERROR
					&& StatePagina != ACTUALITZA_LC && StatePagina != ACTUALITZA_LC_2)) {
				HTTPServer(StatePagina);
			} else {
				HTTPServer_petit(StatePagina);
			}
		}

		//UART
		process_UART_canal1();//Procés principal de tractament de la UART (canal1)

		process_UART_canal2();//Procés principal de tractament de la UART (canal2)

		//AJAX WEBSERVER
		if (StatePagina == ESPERA_DISPLAY || StatePagina == ESPERA_RS232 ||
				StatePagina == ESPERA_LC )//Push server display
		{
			if (actualitza_display == TRUE) {
				//TCPAckNr=TCPAck_push;
				HTTPStatus &= ~HTTP_SEND_PAGE;
				if (StatePagina == ESPERA_DISPLAY)
					StatePagina = ACTUALITZA_DISPLAY;
				else if (StatePagina == ESPERA_RS232)
					StatePagina = ACTUALITZA_RS232;
				else if (StatePagina == ESPERA_LC)
					StatePagina = ACTUALITZA_LC;
				actualitza_display = FALSE;
				SocketStatus |= SOCK_DATA_AVAILABLE; // indicate the new data to user
				TCPTimer = 0;
				//memcpy(&RemoteMAC, &web_MAC, 6);              // save opponents MAC and IP
				//memcpy(&RemoteIP, &web_IP, 4);                // for later use
			}
		} else if (StatePagina == ESPERA_DISPLAY_2 || StatePagina == ESPERA_LC_2)//||StatePagina == ESPERA_3)//Push server display
		{
			if (actualitza_display_2 == TRUE) {
				//TCPAckNr=TCPAck_push;
				HTTPStatus &= ~HTTP_SEND_PAGE;
				if (StatePagina == ESPERA_DISPLAY_2)
					StatePagina = ACTUALITZA_DISPLAY_2;
				else if(StatePagina == ESPERA_LC_2)
					StatePagina = ACTUALITZA_LC_2;
				//else if(StatePagina==ESPERA_3)StatePagina = ACTUALITZA_RS232;
				actualitza_display_2 = FALSE;
				SocketStatus |= SOCK_DATA_AVAILABLE; // indicate the new data to user
				TCPTimer = 0;
				//memcpy(&RemoteMAC, &web_MAC, 6);              // save opponents MAC and IP
				//memcpy(&RemoteIP, &web_IP, 4);                // for later use
			}
		}
		if (StatePagina == ESPERA_NODES)//Push server nodes
		{
			if (actualitza_nodes == TRUE) {
				//TCPAckNr=TCPAck_push;
				HTTPStatus &= ~HTTP_SEND_PAGE;
				StatePagina = ACTUALITZA;
				SocketStatus |= SOCK_DATA_AVAILABLE; // indicate the new data to user
				TCPTimer = 0;
				actualitza_nodes = FALSE;
				//memcpy(&RemoteMAC, &web_MAC, 6);              // save opponents MAC and IP
				//memcpy(&RemoteIP, &web_IP, 4);                // for later use
			}
		} else if (StatePagina == ESPERA_NODES_2) {
			if (actualitza_nodes_2 == TRUE) {
				//TCPAckNr=TCPAck_push;
				HTTPStatus &= ~HTTP_SEND_PAGE;
				StatePagina = ACTUALITZA_2;
				SocketStatus |= SOCK_DATA_AVAILABLE; // indicate the new data to user
				TCPTimer = 0;
				actualitza_nodes_2 = FALSE;
				//memcpy(&RemoteMAC, &web_MAC, 6);              // save opponents MAC and IP
				//memcpy(&RemoteIP, &web_IP, 4);                // for later use
			}
		}

		//LCD
		process_menu_LCD();
		//process_PWM_LCD();

		//WATCHDOG UART's
		if(StateUART_canal1!=PROGRAMACIO&&StateUART_canal2!=PROGRAMACIO)
		{
			if(watchdog_uart_1>100000||watchdog_uart_2>100000||watchdog_uart_3>100000||watchdog_uart_4>100000)
			{
				StateUART_canal1 = FINAL;
				StateUART_canal2 = FINAL;
				posicio_configurats=0;
				if((wire_correcte==FALSE)&&(wire_count==0))wire_count++;
				posicio_configurats_2=0;
				if((wire_correcte_2==FALSE)&&(wire_count_2==0))wire_count_2++;
				watchdog_uart_1=watchdog_uart_2=watchdog_uart_3=watchdog_uart_4=0;
				counter_watchdog_uart++;
				watchdog_uart_1_enable=watchdog_uart_2_enable=watchdog_uart_3_enable=watchdog_uart_4_enable=0;
			}
			else
			{
				if(watchdog_uart_1_enable==1 && StateUART_canal1==ENVIANT) watchdog_uart_1++;
				if(watchdog_uart_2_enable==1 && StateUART_canal1==ENVIANT_2) watchdog_uart_2++;
				if(watchdog_uart_3_enable==1 && StateUART_canal2==ENVIANT) watchdog_uart_3++;
				if(watchdog_uart_4_enable==1 && StateUART_canal2==ENVIANT_2) watchdog_uart_4++;
			}
		}

		led_error();
	}
	return 0;
}

/******************************************************************************
 **                            End Of File
 ******************************************************************************/
