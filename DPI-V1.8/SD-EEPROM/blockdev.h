/*
	LPCUSB, an USB device driver for LPC microcontrollers	
	Copyright (C) 2006 Bertrik Sikken (bertrik@sikken.nl)

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright
	   notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright
	   notice, this list of conditions and the following disclaimer in the
	   documentation and/or other materials provided with the distribution.
	3. The name of the author may not be used to endorse or promote products
	   derived from this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
	IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
	OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, 
	INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
	NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
	THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
	THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#include <stdint.h>
#include <stdio.h>

// CodeRed - added DEBUG_MESSAGES, so still get output for Release builds
//#define DEBUG_MESSAGES 1
//#ifdef DEBUG
#ifdef DEBUG_MESSAGES
#define DBG	printf
#define ASSERT(x)	if(!(x)){DBG("\nAssertion '%s' failed in %s:%s#%d!\n",#x,__FILE__,__FUNCTION__,__LINE__);while(1);}
#else
#define DBG(x ...)
#define ASSERT(x)
#endif

#define ADRESS_TAULA_SD 	230
#define ADRESS_IP 			0
#define ADRESS_NM 			250
#define ADRESS_GW			260
#define ADRESS_TAULA_SD_2	270
#define ADRESS_USUARI		280
#define ADRESS_PASSWORD		290
#define	ADRESS_PORT			300
#define ADRESS_RAM			310 //Flag per activació/desactivació de la RAM
#define ADRESS_STATICCRC	320 //Adreça per guardar les dades estàtiques que sempre guardarem
extern volatile uint8_t SD_state;

int BlockDevInit(void);

int BlockDevWrite(uint32_t dwAddress, uint8_t * pbBuf, uint8_t length_dades);
int BlockDevRead(uint32_t dwAddress, uint8_t* pbBuf, uint8_t length_dades_valides);

int BlockDevGetSize(uint32_t *pdwDriveSize);
int BlockDevGetStatus(void);
uint8_t tarjetaSDinit();
