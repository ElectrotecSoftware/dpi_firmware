#include <stdint.h>
#include <stdio.h>
#include "uart.h"

#define BLOC_NODE_RAM	50 //Nombre de bytes que es guarden per node a la memòria RAM
/********************************
 * 1er byte 	= tipus de node
 * 2on byte 	= byte de LEDS
 * 3er byte 	= byte de sortides digitals
 * 4art byte 	= 1er byte de sortides analògiques
 * 5è byte 		= 2on byte de sortides analògiques
 * 6è byte		= display config
 * 7...41è byte	= bytes de display
 *
 * *******************************/

void escriure_EEPROM (uint16_t address, uint8_t byte);
uint8_t llegirMAC_EEPROM (void);
void inicialitza_EEPROM (void);
void guardar_EEPROM (uint8_t node, uint8_t canal, uint8_t cadena [], uint8_t length, uint8_t memory_address);
void guardar_EEPROM_broadcast (uint8_t node, uint8_t canal, uint8_t cadena [MAX_DADES_NODE]);
void carrega_EEPROM (uint8_t node, uint8_t canal, uint8_t tipus);
