#include "blockdev.h"
#include "ethmac.h"
#include "spi.h"
#include <stdio.h>
#include "LPC17xx.h"
#include "nodes.h"
#include "EEPROM.h"
#include "uart.h"
#include "trames.h"

extern unsigned char MyMAC[6];

//#define SELECT_CARD()   LPC_GPIO0->FIOCLR = (1 << SPI_SSEL_PIN)
//#define UNSELECT_CARD() LPC_GPIO0->FIOSET = (1 << SPI_SSEL_PIN)
#define SPI_EEPROM_CS_PIN	  22		/* EEPROM-Select	P0.22 GPIO out	 */
#define SELECT_SPI_EEPROM()   LPC_GPIO0->FIOCLR = (1 << SPI_EEPROM_CS_PIN)
#define UNSELECT_SPI_EEPROM() LPC_GPIO0->FIOSET = (1 << SPI_EEPROM_CS_PIN)

uint8_t llegir_EEPROM(uint8_t address){
	uint8_t i = 0;
	uint8_t byte=0;

	LPC_SPI->SPDR = 0b00000011;//READ sequence
	while (!(LPC_SPI->SPSR & (1 << 7)));
	byte= LPC_SPI->SPDR;
	LPC_SPI->SPDR = address;
	while (!(LPC_SPI->SPSR & (1 << 7)));
	LPC_SPI->SPDR = 0xFF;
	while (!(LPC_SPI->SPSR & (1 << 7)));
	byte= LPC_SPI->SPDR;
	byte= LPC_SPI->SPDR;
	byte= LPC_SPI->SPDR;
	byte= LPC_SPI->SPDR;
	return byte;
}

uint8_t llegirMAC_EEPROM (void)
{
	uint8_t i;
	uint8_t MAC_EE[6]; // our MAC address
	uint8_t zeros=0;
	uint8_t FFs=0;

	LPC_GPIO0->FIOSET = (1 << 16); //Deshabilitem SD
	for(i=0;i<6;i++)
	{
		SELECT_SPI_EEPROM(); //Habilitem EEPROM
		MAC_EE[i]=llegir_EEPROM(0xFF-i);
		UNSELECT_SPI_EEPROM(); //Deshabilitem EEPROM

		//Comrpovem que la MAC no son tot 0 o tot 0xFF
		if (MAC_EE[i]==0) zeros++;
		else if(MAC_EE[i]==0xFF) FFs++;
	}
	if((FFs==6)||(zeros==6)){
		MAC_EE[0]= MYMAC_1; MAC_EE[1]= MYMAC_2; MAC_EE[2]= MYMAC_3;
		MAC_EE[3]= MYMAC_4; MAC_EE[4]= MYMAC_5; MAC_EE[5]= MYMAC_6;
	}
	MAC_EE[0]=0x2C;
	for(i=0;i<6;i++) MyMAC[i]=MAC_EE[i]; //Copiem el valor final de la MAC


}
void inicialitza_EEPROM (void)
{

}
void guardar_EEPROM (uint8_t node, uint8_t canal, uint8_t cadena [], uint8_t length, uint8_t memory_address)
{

}
