/*****************************************************************************\
*              efs - General purpose Embedded Filesystem library              *
*          --------------------- -----------------------------------          *
*                                                                             *
* Filename : sd.c                                                             *
* Revision : Initial developement                                             *
* Description : This file contains the functions needed to use efs for        *
*               accessing files on an SD-card.                                *
*                                                                             *
* This library is free software; you can redistribute it and/or               *
* modify it under the terms of the GNU Lesser General Public                  *
* License as published by the Free Software Foundation; either                *
* version 2.1 of the License, or (at your option) any later version.          *
*                                                                             *
* This library is distributed in the hope that it will be useful,             *
* but WITHOUT ANY WARRANTY; without even the implied warranty of              *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU           *
* Lesser General Public License for more details.                             *
*                                                                             *
*                                                    (c)2005 Michael De Nil   *
*                                                    (c)2005 Lennart Yseboodt *
\*****************************************************************************/

/*
	2006, Bertrik Sikken, modified for LPCUSB
*/

// CodeRed - update to point to LPCUSB type.h


#include "blockdev.h"
#include "LPC17xx.h"
#include "tcpip.h"
#include "spi.h"
#include "nodes.h"
#include "string.h"
#include "trames.h"
#include <stdio.h>


#define CMD_GOIDLESTATE		0
#define CMD_SENDOPCOND		1
#define CMD_55				55
#define ACMD_41				41
#define	CMD_READCSD       	9
#define CMD_READCID			10
#define CMD_SENDSTATUS		13
#define	CMD_READSINGLEBLOCK	17
#define	CMD_WRITE			24
#define CMD_WRITE_MULTIPLE	25

#define SPI_SSEL_PIN		16		/* Card-Select	P0.16 GPIO out	(PINSEL1) */
#define SELECT_CARD()   LPC_GPIO0->FIOCLR = (1 << SPI_SSEL_PIN)
#define UNSELECT_CARD() LPC_GPIO0->FIOSET = (1 << SPI_SSEL_PIN)

static void Command(uint8_t cmd, uint32_t param)
{
	uint8_t	abCmd[8];

	// create buffer
	abCmd[0] = 0xff;
	abCmd[1] = 0x40 | cmd;
	abCmd[2] = (uint8_t)(param >> 24);
	abCmd[3] = (uint8_t)(param >> 16);
	abCmd[4] = (uint8_t)(param >> 8);
	abCmd[5] = (uint8_t)(param);
	abCmd[6] = 0x95;			/* Checksum (should be only valid for first command (0) */
	abCmd[7] = 0xff;			/* eat empty command - response */

	SPISendN(abCmd, 8, 8);
}


/*****************************************************************************/

static uint8_t Resp8b(void)
{
	uint8_t i;
	uint8_t resp;

	/* Respone will come after 1 - 8 pings */
	for (i = 0; i < 8; i++) {
		resp = SPISend(0xff);
		if (resp != 0xff) {
			return resp;
		}
	}

	return resp;
}

/*****************************************************************************/

static void Resp8bError(uint8_t value)
{
	switch (value) {
	case 0x40:	DBG("Argument out of bounds.\n"); 				break;
	case 0x20:	DBG("Address out of bounds.\n");				break;
	case 0x10:	DBG("Error during erase sequence.\n"); 			break;
	case 0x08:	DBG("CRC failed.\n"); 							break;
	case 0x04:	DBG("Illegal command.\n"); 						break;
	case 0x02:	DBG("Erase reset (see SanDisk docs p5-13).\n");	break;
	case 0x01:	DBG("Card is initialising.\n"); 				break;
	default: 	
		DBG("Unknown error 0x%x (see SanDisk docs p5-13).\n", value); 
		break;
	}
}


/* ****************************************************************************
 calculates size of card from CSD 
 (extension by Martin Thomas, inspired by code from Holger Klabunde)
 */
int BlockDevGetSize(uint32_t *pdwDriveSize)
{
	uint8_t cardresp, i, by;
	uint8_t iob[16];
	uint16_t c_size, c_size_mult, read_bl_len;

	Command(CMD_READCSD, 0);
	do {
		cardresp = Resp8b();
	} while (cardresp != 0xFE);

	DBG("CSD:");
	for (i = 0; i < 16; i++) {
		iob[i] = SPISend(0xFF);
		DBG(" %02x", iob[i]);
	}
	DBG("\n");

	SPISend(0xff);
	SPISend(0xff);

	c_size = iob[6] & 0x03;		// bits 1..0
	c_size <<= 10;
	c_size += (uint16_t) iob[7] << 2;
	c_size += iob[8] >> 6;

	by = iob[5] & 0x0F;
	read_bl_len = 1 << by;

	by = iob[9] & 0x03;
	by <<= 1;
	by += iob[10] >> 7;

	c_size_mult = 1 << (2 + by);

	*pdwDriveSize = (uint32_t) (c_size + 1) * (uint32_t) c_size_mult *(uint32_t) read_bl_len;

	return 0;
}


/*****************************************************************************/

static uint16_t Resp16b(void)
{
	uint16_t resp;

	resp = (Resp8b() << 8) & 0xff00;
	resp |= SPISend(0xff);

	return resp;
}


/*****************************************************************************/

static int State(void)
{
	uint16_t value;

	Command(CMD_SENDSTATUS, 0);
	value = Resp16b();

	switch (value) {
	case 0x0000: return 1;
	case 0x0001: DBG("Card is Locked.\n"); 													break;
	case 0x0002: DBG("WP Erase Skip, Lock/Unlock Cmd Failed.\n"); 							break;
	case 0x0004: DBG("General / Unknown error -- card broken?.\n"); 						break;
	case 0x0008: DBG("Internal card controller error.\n"); 									break;
	case 0x0010: DBG("Card internal ECC was applied, but failed to correct the data.\n");	break;
	case 0x0020: DBG("Write protect violation.\n"); 										break;
	case 0x0040: DBG("An invalid selection, sectors for erase.\n"); 						break;
	case 0x0080: DBG("Out of Range, CSD_Overwrite.\n"); 									break;
	default:
		if (value > 0x00FF) {
			Resp8bError((uint8_t) (value >> 8));
		}
		else {
			DBG("Unknown error: 0x%x (see SanDisk docs p5-14).\n", value);
		}
		break;
	}
	return -1;
}

/*****************************************************************************/




int BlockDevInit(void)
{
	int i;
	uint8_t resp;

	SPIInit();				/* init at low speed */

	SELECT_CARD();
	/* Try to send reset command up to 100 times */
	i = 100;
	do {
		Command(CMD_GOIDLESTATE, 0);
		resp = Resp8b();
	} while (resp != 1 && i--);

	if (resp != 1) {
		if (resp == 0xff) {
			DBG("resp=0xff\n");
			return -1;
		}
		else {
			Resp8bError(resp);
			DBG("resp!=0xff\n");
			return -2;
		}
	}

	/* Wait till card is ready initialising (returns 0 on CMD_1) */
	/* Try up to 32000 times. */
	i = 5000;
	do {
		Command(CMD_SENDOPCOND, 0);

		resp = Resp8b();
		if (resp != 0) {
			Resp8bError(resp);
		}
	} while (resp == 1 && i--);

	if (resp != 0) {
		Resp8bError(resp);
		return -3;
	}

	/* increase speed after init */
	//SPISetSpeed(SPI_PRESCALE_MIN);
	SPISetSpeed(254);
	//SPISetSpeed(30);

	//Delay per donar temps a la SD a carregar
	for(i=0;i<250;i++);												//ActualizacionSDv1.3

	i = 100;														//ActualizacionSDv1.3
	do {															//ActualizacionSDv1.3
		resp=State();												//ActualizacionSDv1.3
	} while (resp != 1 && i--);										//ActualizacionSDv1.3

	if (resp != 1) {												//ActualizacionSDv1.3
	//if (State() != 1) {
		DBG("Card didn't return the ready state, breaking up...\n");
		return -2;
	}

	DBG("Init done...\n");

	return 0;
}

/***********************************************************************
 *  powerOffSD: - Treiem alimentacio a la SD
 *  powerOnSD: 	- Donem  alimentacio a la SD
 ****************************************************************************/
void powerOffSD(){
	LPC_GPIO1->FIOSET=(1<<21);
}
void powerOnSD(){
	LPC_GPIO1->FIOCLR=(1<<21);
}
/***********************************************************************
 *  resetSD: - Hard reset a la SD i posada en marxa desde la condició d'inici
 ****************************************************************************/
uint8_t resetSD(){
	char i=0;
	uint16_t x;

	// Inicialitzem port SPI i la tarja SD
	powerOffSD();
	for(x=0;x<10000;x++);		//Delay
	powerOnSD();
	for(x=0;x<10000;x++);		//Delay
	while ((BlockDevInit() != 0)&&(i<6)){
		i++;
		if(i==3) {
			powerOffSD();
			for(x=0;x<10000;x++);		//Delay
			powerOnSD();
			for(x=0;x<10000;x++);		//Delay
		}
	}
	if (i>5){//Si SD_state==1 --> SD correcte
		SD_state=0;
		return 0; //Error de opertura SD
	}else {
		SD_state = 1;
	}
	return 1;
}
/***********************************************************************
 *  SDinit: - Seqüència d'inicialització del port SPI
 *  		- Seqüència d'inicialització de la SD
 *  		- Lectura de totes les dades guardades a la SD
 ****************************************************************************/
uint8_t tarjetaSDinit(){
	static char const cadenaElectro[7]={'E','L','E','C','T','R','O'};
	char data[7];
	char i=0;
	uint16_t x=0,error_init=0;
	// Inicialitzem port SPI i la tarja SD
	if (resetSD()!=1)error_init=1; //Error d'opertura
	//Lectura de totes les dades d'arrencada
	if(!error_init)
	{
		for(i=0;i<4;i++){																	//ActualizacionSDv1.3
			BlockDevRead(ADRESS_TAULA_SD, taula_SD, 250);
			BlockDevRead(ADRESS_IP, (uint8_t *) MyIP, 4);
			BlockDevRead(ADRESS_NM, (uint8_t *) SubnetMask, 4);
			BlockDevRead(ADRESS_GW, (uint8_t *) GatewayIP, 4);
			BlockDevRead(ADRESS_TAULA_SD_2, taula_SD_2, 250);
			//BlockDevRead(ADRESS_USUARI, (uint8_t *)usuario, 8);
			//BlockDevRead(ADRESS_PASSWORD, (uint8_t *)contrasenya, 8);
			BlockDevRead(ADRESS_PORT, port_comunicacio_RAM, 2);
			BlockDevRead(ADRESS_RAM,(uint8_t *)&actualitza_nodes_RAM,1);
			BlockDevRead(ADRESS_STATICCRC,(uint8_t *)data,7);
			if(strncmp(data,cadenaElectro,7)==0) break;										//ActualizacionSDv1.3
		}
	}
	if(MyIP[0]==0&&MyIP[1]==0)
	{
		MyIP[0] = 192 | 168 << 8;
		MyIP[1] = 0 | 200 << 8;
		BlockDevWrite(ADRESS_IP, (uint8_t *) MyIP, 4);
	}
	if(SubnetMask[0]==0&&SubnetMask[1]==0)
	{
		SubnetMask[0] = 255 | 255 << 8;
		SubnetMask[1] = 255 | 0 << 8;
		BlockDevWrite(ADRESS_NM, (uint8_t *) SubnetMask, 4);
	}
	if(GatewayIP[0]==0&&GatewayIP[1]==0)
	{
		GatewayIP[0] = 192 | 168 << 8;
		GatewayIP[1] = 0 | 1 << 8;
		BlockDevWrite(ADRESS_GW, (uint8_t *) GatewayIP, 4);
	}
	if(usuario[0]==0&&usuario[0]==0&&usuario[0]==0&&usuario[0]==0
			&&usuario[0]==0&&usuario[0]==0&&usuario[0]==0&&usuario[0]==0)
	{
		usuario = "user";
		BlockDevWrite(ADRESS_USUARI,(uint8_t *)usuario,8);
	}
	if(contrasenya[0]==0&&contrasenya[0]==0&&contrasenya[0]==0&&contrasenya[0]==0
			&&contrasenya[0]==0&&contrasenya[0]==0&&contrasenya[0]==0&&contrasenya[0]==0)
	{
		contrasenya = "user";
		BlockDevWrite(ADRESS_PASSWORD,(uint8_t *)contrasenya,8);
	}
	if(port_comunicacio_RAM[0]==0&&port_comunicacio_RAM[1]==0)
	{
		port_comunicacio_RAM[0] = (3030&0xFF00)>>8;
		port_comunicacio_RAM[1] = 3030&0xFF;
		PORT_COMUNICACIO = 3030;
		BlockDevWrite(ADRESS_PORT,port_comunicacio_RAM,2);

	}
	if(strncmp(data,cadenaElectro,7)!=0)
	{//Escrius ELECTRO si la targeta no està grabada
		BlockDevWrite(ADRESS_STATICCRC,(uint8_t *) cadenaElectro,7);					//ActualizacionSDv1.3
	}
	MYIP_1 = MyIP[0] & 0xFF;
	MYIP_2 = (MyIP[0] & 0xFF00) >> 8;
	MYIP_3 = MyIP[1] & 0xFF;
	MYIP_4 = (MyIP[1] & 0xFF00) >> 8;
	SUBMASK_1 = SubnetMask[0] & 0xFF;
	SUBMASK_2 = (SubnetMask[0] & 0xFF00) >> 8;
	SUBMASK_3 = SubnetMask[1] & 0xFF;
	SUBMASK_4 = (SubnetMask[1] & 0xFF00) >> 8;
	GWIP_1 = GatewayIP[0] & 0xFF;
	GWIP_2 = (GatewayIP[0] & 0xFF00) >> 8;
	GWIP_3 = GatewayIP[1] & 0xFF;
	GWIP_4 = (GatewayIP[1] & 0xFF00) >> 8;
	return 1;
}
/*****************************************************************************/



/*****************************************************************************/
/*
* WAIT ?? -- FIXME
 * CMD_WRITE
 * WAIT
 * CARD RESP
 * WAIT
 * DATA BLOCK OUT
 *      START BLOCK
 *      DATA
 *      CHKS (2B)
 * BUSY...
 */

int BlockSDWrite(uint32_t dwAddress, uint8_t * pbBuf, uint8_t length_dades)
{
	uint32_t place;
	uint16_t t = 0;
	if(SD_state==1)
	{
		//place = 512 * dwAddress;
		place = dwAddress<<9;
		Command(CMD_WRITE, place);

		Resp8b();				/* Card response */

		SPISend(0xfe);			/* Start block */
		SPISendN(pbBuf, 512,length_dades);
		SPISend(0xff);			/* Checksum part 1 */
		SPISend(0xff);			/* Checksum part 2 */

		SPISend(0xff);

		while (SPISend(0xff) != 0xff) {
			t++;
			if(t>200)return 1;
		}
	}
	return 0;
}

/*****************************************************************************/

/* ****************************************************************************
 * WAIT ?? -- FIXME
 * CMD_WRITE
 * WAIT
 * CARD RESP
 * WAIT
 * DATA BLOCK OUT
 *      START BLOCK
 *      DATA
 *      CHKS (2B)
 * BUSY...
 */

int BlockDevWrite(uint32_t dwAddress, uint8_t * pbBuf, uint8_t length_dades)
{
	uint8_t dades[length_dades];
	char i=0;
	uint32_t x=0;
	char errorEscritura=0;

	//Provem d'escriure les dades i després llegir-les
	do{
		BlockSDWrite(dwAddress, pbBuf, length_dades);
		for(x=0;x<30000;x++);
		resetSD();
		for(x=0;x<30000;x++);
		/*for(x=0;x<30000;x++);
		resetSD();
		for(x=0;x<30000;x++);*/
		BlockDevRead(dwAddress,(uint8_t *) dades, length_dades);
		errorEscritura=0;
		for(x=0;x<length_dades;x++){
			if(*(pbBuf+x)!=*(dades+x)){ //Si les dades grabades no són les mateixes que les llegides
				errorEscritura=1;
			}
		}
		i++;
		if((errorEscritura)&&(i==3)) if (resetSD()!=1) {
			errorEscritura=1;
			break;
		}

	}while((errorEscritura==1)&&(i<6));
	//En acabar comprovem si hi ha hagut error en l'escriptura i ho notifiquem
	if(errorEscritura==1) {gestio_led_error |= (1<<3);return 0;} //No s'ha pogut grabar
	return 1;
}

/*****************************************************************************/

/* ****************************************************************************
 * WAIT ?? -- FIXME
 * CMD_CMD_
 * WAIT
 * CARD RESP
 * WAIT
 * DATA BLOCK IN
 * 		START BLOCK
 * 		DATA
 * 		CHKS (2B)
 */

int BlockDevRead(uint32_t dwAddress, uint8_t * pbBuf, uint8_t length_dades)
{
	uint8_t cardresp;
	uint8_t firstblock;
	uint16_t fb_timeout = 0xfff;
	uint32_t place;
	if (SD_state==1)
	{
		//place = 512 * dwAddress;
		place = dwAddress<<9;
		Command(CMD_READSINGLEBLOCK, place);

		cardresp = Resp8b();		/* Card response */

		/* Wait for startblock */
		do {
			firstblock = Resp8b();
		} while (firstblock == 0xff && fb_timeout--);

		if (cardresp != 0x00 || firstblock != 0xfe) {
			Resp8bError(firstblock);
			return -1;
		}

		SPIRecvN(pbBuf, 512, length_dades);

		/* Checksum (2 byte) - ignore for now */
		SPISend(0xff);
		SPISend(0xff);
	}
	return 0;
}

/*****************************************************************************/

