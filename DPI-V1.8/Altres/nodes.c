#include "lpc17xx.h"
#include "leds.h"
#include "type.h"
#include "nodes.h"
#include <stdio.h>
#include <stdlib.h>
#include "webserver.h"
#include "uart.h"
#include "all.h"
void ini_nodes(void)
{
	uint8_t i=0;
	while(i<MAX_NODES_CANAL)
	{
		taula_nodes[i] = 0;
		i++;
	}
}
void ini_nodes_2(void)
{
	uint8_t i=0;
	while(i<MAX_NODES_CANAL)
	{
		taula_nodes_2[i] = 0;
		i++;
	}
}
void ini_SD(void)
{
	int i=0;
	while(i<MAX_NODES_CANAL)
	{
		taula_SD[i] = 0;
		taula_configurats[i]=0;//Només per si es resetejen els nodes
		i++;
	}
	NUM_NODES=0;//No hi ha nodes configurats
}
void ini_SD_2(void)
{
	int i=0;
	while(i<MAX_NODES_CANAL)
	{
		taula_SD_2[i] = 0;
		taula_configurats_2[i]=0;//Només per si es resetejen els nodes
		i++;
	}
	NUM_NODES_2=0;//No hi ha nodes configurats
}
void ini_taula_alarms(void)
{
	int i=0;
	while(i<LONGITUD_ALARMS)
	{
		taula_alarms[i] = '0';
		if(i<MAX_NODES_CANAL)
		{
			alarmes_activades[i] = 0;
			alarmes_activades_2[i] = 0;
		}
		i++;
	}
}
void ini_taula_web(void)
{
	int i=0;
	while(i<MAX_NODES_CANAL)
	{
		taula_nodes_web[i]=(taula_SD[i]<<4);
		taula_nodes_web_2[i]=(taula_SD_2[i]<<4);
		i++;
	}
}
void ini_taula_configurats(void)
{
	uint8_t i=0;
	uint8_t j=0;
	uint8_t k=0;
	while(i<MAX_NODES_CANAL)
	{
		if(taula_SD[i]!=0)//Si el node esta a la taula SD
		{
			taula_configurats[j]=i+1;
			j++;
			NUM_NODES++;
		}
		if(taula_SD_2[i]!=0)//Si el node esta a la taula SD
		{
			taula_configurats_2[k]=i+1;
			k++;
			NUM_NODES_2++;
		}
		i++;
	}
}
uint8_t getlastpulsadors_node (uint8_t port, uint8_t node){
	if(port<2)
	{
		return  (taula_nodes[node]>>8);
	}else{
		return  (taula_nodes_2[node]>>8);
	}
}
void actualitza_node (uint8_t port, uint8_t status, uint8_t tipus, uint16_t node, uint8_t pulsadors)
{
	if(port<2)
	{//Canal 1

		if(port==UART0)taula_nodes[node] = (0) | (status<<1) | (tipus<<4) | (pulsadors<<8);
		if(port==UART1)taula_nodes[node] = (1) | (status<<1) | (tipus<<4) | (pulsadors<<8);
	}
	else
	{//Canal 2

		if(port==UART2)taula_nodes_2[node] = (0) | (status<<1) | (tipus<<4) | (pulsadors<<8);
		if(port==UART3)taula_nodes_2[node] = (1) | (status<<1) | (tipus<<4) | (pulsadors<<8);
	}

}
void canviar_taula_nodes_web(void)
{
	uint16_t i;
	uint8_t nodes_port_A=0;
	uint8_t actualitzar = 0;//Serveix per indicar si s'ha d'actualitzar la taula o no

	if(nodes_correctes!=nodes_correctes_web)nodes_correctes_web=nodes_correctes;
	nodes_correctes=0;
	for (i=0;i<MAX_NODES_CANAL;i++)
	{
		//Fem una actualització per què el tipus s'envi correctament ( la web no té els mateixos tipus que els stándards)
		taula_nodes_web[i] &= ~(0xF0);
		if (taula_SD[i]==TIPUS_LCIN) taula_nodes_web[i] |= (6<<4); // el 6 és el tipus a la web
		else if (taula_SD[i]==TIPUS_RS232) taula_nodes_web[i] |= (7<<4);	// el 7 és el tipus a la web
		else taula_nodes_web[i] |= (taula_SD[i]<<4);
		if(!(taula_nodes[i]&0x1)&&(taula_nodes[i]&0xE))nodes_port_A++;
		if((taula_nodes_web[i]&0xF)!=(taula_nodes[i]&0xF) || ((taula_nodes[i]&0xF0)>>4)!=taula_SD[i] || (taula_nodes_web[i]&0xFF00)!=(taula_nodes[i]&0xFF00))
		{//Estat diferent, configuracio diferent o pulsadors diferents

			//En el cas de desconnexió només actualitzem si s'ha assolit el contador de desconnexió
			actualitzar = 0;
			if(((taula_nodes[i]&0xE)>>1)==0)
			{//Si s'ha detectat que el node no respon
				if((alarmes_activades[i]&(1<<(DESCONEXIO_NODE-1))))
				{
					actualitzar = 1;
				}
			}
			else
			{//Si no està desconnectat actualitzes
				actualitzar = 1;
			}
			if(actualitzar)
			{
				if (StatePagina==ESPERA_NODES || StatePagina ==ACTUALITZA)//Nodes pagina web
				{
					//if((taula_nodes[i]&0xF!=0)||((taula_nodes[i]&0xF==0)&&(alarmes_activades[i]&(1<<(DESCONEXIO_NODE-1))))){
					if((taula_nodes_web[i]&0xE)!=(taula_nodes[i]&0xE)){
						//if((((taula_nodes[i]&0xF)>>1)!=0)||((((taula_nodes[i]&0xF)>>1)==0)&&(alarmes_activades[i]&(1<<(DESCONEXIO_NODE-1))))){
							actualitza_nodes = TRUE;
						//}
					}

					//}
				}

				if((StatePagina==ESPERA_DISPLAY || StatePagina==ACTUALITZA_DISPLAY)&&(taula_nodes_web[node_display-1]&0xFF00)!=(taula_nodes[node_display-1]&0xFF00))
				{//Pulsadors pagina web
					pulsadors_display = ((taula_nodes[node_display-1]&0x0F00)>>8);
					actualitza_display = TRUE;
					//getmem(node_display,TEC);
				}

																	//Estat		+ PORT			 TIPUS				PULSADORS
					if (taula_SD[i]==TIPUS_LCIN) taula_nodes_web[i]=(taula_nodes[i] & 0x0F) | 6<<4 | (taula_nodes[i] & 0xFF00);  //---ver1.2
					else if (taula_SD[i]==TIPUS_RS232) taula_nodes_web[i]=(taula_nodes[i] & 0x0F) | 7<<4 | (taula_nodes[i] & 0xFF00);  //---ver1.3
					else taula_nodes_web[i]=(taula_nodes[i] & 0x0F) | taula_SD[i]<<4 | (taula_nodes[i] & 0xFF00);

			}
		}
	}
	if(nodes_port_A==NUM_NODES)led_off(LED_2);
}
void canviar_taula_nodes_web_2(void)
{
	uint16_t i;
	uint8_t nodes_port_A=0;
	uint8_t actualitzar = 0;//Serveix per indicar si s'ha d'actualitzar la taula o no

	if(nodes_correctes_2!=nodes_correctes_web_2)nodes_correctes_web_2=nodes_correctes_2;
	nodes_correctes_2=0;
	for (i=0;i<MAX_NODES_CANAL;i++)
	{
		taula_nodes_web_2[i] &= ~(0xF0);
		if (taula_SD_2[i]==TIPUS_LCIN) taula_nodes_web_2[i] |= (6<<4);
		else if (taula_SD_2[i]==TIPUS_RS232) taula_nodes_web_2[i] |= (7<<4);	// el 7 és el tipus a la web
		else taula_nodes_web_2[i] |= (taula_SD_2[i]<<4);
		if(!(taula_nodes_2[i]&0x1)&&(taula_nodes_2[i]&0xE))nodes_port_A++;
		if((taula_nodes_web_2[i]&0xF)!=(taula_nodes_2[i]&0xF) || ((taula_nodes_2[i]&0xF0)>>4)!=taula_SD_2[i] || (taula_nodes_web_2[i]&0xFF00)!=(taula_nodes_2[i]&0xFF00))
		{//Estat diferent, configuracio diferent o pulsadors diferents
			//En el cas de desconnexió només actualitzem si s'ha assolit el contador de desconnexió
			actualitzar = 0;
			if(((taula_nodes_2[i]&0xE)>>1)==0)
			{//Si s'ha detectat que el node no respon
				if((alarmes_activades_2[i]&(1<<(DESCONEXIO_NODE-1))))
				{
					actualitzar = 1;
				}
			}
			else
			{//Si no està desconnectat actualitzes
				actualitzar = 1;
			}
			if(actualitzar)
			{
				if (StatePagina==ESPERA_NODES_2 || StatePagina ==ACTUALITZA_2)//Nodes pagina web
				{
					if((taula_nodes_web_2[i]&0xE)!=(taula_nodes_2[i]&0xE))actualitza_nodes_2 = TRUE;
				}
				if((StatePagina==ESPERA_DISPLAY_2 || StatePagina==ACTUALITZA_DISPLAY_2)&&(taula_nodes_web_2[node_display-1]&0xFF00)!=(taula_nodes_2[node_display-1]&0xFF00))
				{//Pulsadors pagina web
					pulsadors_display = ((taula_nodes_2[node_display-1]&0x0F00)>>8);
					actualitza_display_2 = TRUE;
					//getmem(node_display,TEC);
				}
										//Estat		+ PORT			 TIPUS				PULSADORS
				if (taula_SD_2[i]==TIPUS_LCIN)  taula_nodes_web_2[i]=(taula_nodes_2[i] & 0x0F) | 6<<4 | (taula_nodes_2[i] & 0xFF00); //----Ver1.2
				if (taula_SD_2[i]==TIPUS_RS232)  taula_nodes_web_2[i]=(taula_nodes_2[i] & 0x0F) | 7<<4 | (taula_nodes_2[i] & 0xFF00); //----Ver1.3
				else taula_nodes_web_2[i]=(taula_nodes_2[i] & 0x0F) | taula_SD_2[i]<<4 | (taula_nodes_2[i] & 0xFF00);
				/*if(taula_SD[i]!=0 && taula_nodes[i]==0)
				{//desconexio de node-->alarma
					if((alarmes_activades[i]&(1<<(DESCONEXIO_NODE-1)))==0)alarm(DESCONEXIO_NODE,i+1,LPC_RTC->HOUR,LPC_RTC->MIN,LPC_RTC->SEC,1);
					alarmes_activades[i] |= 1<<(DESCONEXIO_NODE-1);
				}*/
			}
		}
	}
	if(nodes_port_A==NUM_NODES)led_off(LED_4);
}
void alarm (char tipus, uint16_t node, uint8_t hora, uint8_t minut, uint8_t segon, uint8_t canal)
{
	uint16_t i;
	if(tipus<= 9)tipus +=0x30;
	char node_char [3]={0,0,0};
	char hora_char [2]={0,0};
	char minut_char [2]={0,0};
	char segon_char [2]={0,0};
	itoa(node,node_char,10);
	//if(node_char[2]==0&&node_char[1]!=0&&node_char[0]!=0){node_char[2]=node_char[1];node_char[1]=node_char[0];node_char[0]=0x30;}
	//else if(node_char[2]==0&&node_char[1]==0&&node_char[0]!=0){node_char[2]=node_char[0];node_char[1]=node_char[0]=0x30;}
	if(node_char[0]==0) node_char[2]=node_char[1]=node_char[0]=0x30;
	else if(node_char[1]==0){ node_char[2]=node_char[0];node_char[1]=node_char[0]=0x30;}
	else if(node_char[2]==0){ node_char[2]=node_char[1];node_char[1]=node_char[0];node_char[0]=0x30;}
	itoa(hora,hora_char,10);
	if(hora_char[1]==0){hora_char[1]=hora_char[0];hora_char[0]=0x30;}
	itoa(minut,minut_char,10);
	if(minut_char[1]==0){minut_char[1]=minut_char[0];minut_char[0]=0x30;}
	itoa(segon,segon_char,10);
	if(segon_char[1]==0){segon_char[1]=segon_char[0];segon_char[0]=0x30;}
	if(!sesio_iniciada)hora_char[0]=hora_char[1]=minut_char[0]=minut_char[1]=segon_char[0]=segon_char[1]=0x30;
	if(1)//abans es feia servir comprovar_alarm
	{
		if(posicio_taula_alarms==LONGITUD_ALARMS)
		{
			for(i=0;i<LONGITUD_ALARMS-11;i++)
			{
				taula_alarms[i]=taula_alarms[i+11];
			}
			for(i=LONGITUD_ALARMS-11;i<LONGITUD_ALARMS;i++)
			{
				taula_alarms[i]=0x30;
			}
			posicio_taula_alarms = LONGITUD_ALARMS-11;
		}
		taula_alarms[posicio_taula_alarms]=canal+0x30;
		taula_alarms[posicio_taula_alarms+1]=hora_char[0];
		taula_alarms[posicio_taula_alarms+2]=hora_char[1];
		taula_alarms[posicio_taula_alarms+3]=minut_char[0];
		taula_alarms[posicio_taula_alarms+4]=minut_char[1];
		taula_alarms[posicio_taula_alarms+5]=segon_char[0];
		taula_alarms[posicio_taula_alarms+6]=segon_char[1];
		taula_alarms[posicio_taula_alarms+7]=tipus;
		taula_alarms[posicio_taula_alarms+8]=node_char[0];
		taula_alarms[posicio_taula_alarms+9]=node_char[1];
		taula_alarms[posicio_taula_alarms+10]=node_char[2];
		posicio_taula_alarms +=11;
		if(tipus>0x30&&tipus<0x39)
		{
			if(canal==1)alarmes_activades_canal1++;
			else alarmes_activades_canal2++;
		}
		else
		{
			if(canal==1)alarmes_activades_canal1--;
			else alarmes_activades_canal2--;
		}
	}
}
/*char comprovar_alarm (char tipus, char node[])//TRUE si s'ha de fer l'alarma
{
	uint16_t i=0;
	while(i<LONGITUD_ALARMS)
	{
		if(taula_alarms[i]-0x30==tipus)
		{
			if(node[0]==taula_alarms[i+1])
				if(node[1]==taula_alarms[i+2])
					if(node[2]==taula_alarms[i+3])
					{
						return(FALSE);
					}
		}
		i++;
	}
	return(TRUE);
}*/

