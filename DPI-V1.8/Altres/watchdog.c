#include "watchdog.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "lpc17xx.h"

void wd_init (uint8_t time)
{
	LPC_WDT->WDCLKSEL = 0x1;                // Set CLK src to PCLK
	uint32_t clk = SystemCoreClock / 16;    // WD has a fixed /4 prescaler, PCLK default is /4
	LPC_WDT->WDTC = time * clk;
	LPC_WDT->WDMOD = 0x3;                   // Enabled and Reset
	wd_reset();
}

void wd_reset (void)
{
    LPC_WDT->WDFEED = 0xAA;
    LPC_WDT->WDFEED = 0x55;
}



