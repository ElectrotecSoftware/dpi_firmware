#ifndef __ALL_H
#define __ALL_H


#include "GlobalVars.h"
#include "leds.h"
#include "md5.h"
#include "nodes.h"
#include "timer.h"
#include "type.h"
#include "blockdev.h"
#include "spi.h"
#include "ethernet.h"
#include "allwebserver.h"
#include "rtc.h"
#include "trames.h"
#include "uart.h"
#include "lcd.h"
#include "EEPROM.h"


#endif

