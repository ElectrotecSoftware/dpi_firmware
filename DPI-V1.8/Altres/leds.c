//*****************************************************************************
//   +--+       
//   | ++----+   
//   +-++    |  
//     |     |  
//   +-+--+  |   
//   | +--+--+  
//   +----+    Copyright (c) 2009 Code Red Technologies Ltd. 
//
// leds.c provided functions to access the  "user LEDs" on the
// RDB1768 development board (LEDs 2-5).
//
//
// Software License Agreement
// 
// The software is owned by Code Red Technologies and/or its suppliers, and is 
// protected under applicable copyright laws.  All rights are reserved.  Any 
// use in violation of the foregoing restrictions may subject the user to criminal 
// sanctions under applicable laws, as well as to civil liability for the breach 
// of the terms and conditions of this license.
// 
// THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
// OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
// USE OF THIS SOFTWARE FOR COMMERCIAL DEVELOPMENT AND/OR EDUCATION IS SUBJECT
// TO A CURRENT END USER LICENSE AGREEMENT (COMMERCIAL OR EDUCATIONAL) WITH
// CODE RED TECHNOLOGIES LTD. 

#include "LPC17xx.h"
#include "leds.h"
#include <stdio.h>
#include "nodes.h"
#include "ethernet.h"

// Function to initialise GPIO to access the user LEDs
extern volatile uint32_t msTicks_led;
extern volatile uint32_t msTicks_led_error;

void leds_joystick_init (void)
{
	// Set P1_22, P1_23 to 00 - GPIO (led 2-5)
	LPC_PINCON->PINSEL3	&= (~0x0000F000);
	LPC_PINCON->PINSEL3	&= (~0x0F0FFFC0);
	LPC_PINCON->PINSEL0	&= (~0x000F0000);
	LPC_PINCON->PINSEL9 &= (~0x0F000000);
	//Jumper
	LPC_PINCON->PINSEL7 &= (~0x000C0000);
	LPC_GPIO3->FIODIR &= ~((1<<26));

	//LPC_PINCON->PINSEL1 &=~(3<<0);//chip select SD
	LPC_PINCON->PINSEL1 &=~(3<<12);//chip select RAM
	// Leds, transistors i rele outputs
	LPC_GPIO4->FIODIR |=(1<<29)|(1<<28);
	LPC_GPIO1->FIODIR |=(1<<19)|(1<<20)|(1<<21)|(1<<29)|(1<<22)|(1<<23);
	LPC_GPIO0->FIODIR |= ((1<<8)|(1<<9));
	//LPC_GPIO0->FIODIR |= (1<<16);//chip select SD
	LPC_GPIO0->FIODIR |= (1<<22);//chip select RAM
	//Polsadors inputs
	LPC_GPIO1->FIODIR &= ~((1<<24)|(1<<25)|(1<<28));
}


// Function to turn all leds on
void leds_all_on (void)
{
	//LPC_GPIO1->FIOSET = LED_ALL;

}

// Function to turn all leds off
void leds_all_off (void)
{
	//LPC_GPIO1->FIOCLR = LED_ALL;

}

// Function to invert current state of leds
void leds_invert (unsigned int led)
{
	int ledstate;

	// Read current state of GPIO P1_16..31, which includes LEDs
	//ledstate = LPC_GPIO1->FIOPIN;
	// Turn off LEDs that are on 
	// (ANDing to ensure we only affect the LED outputs)
	//LPC_GPIO1->FIOCLR = ledstate & LED_ALL;
	// Turn on LEDs that are off 
	// (ANDing to ensure we only affect the LED outputs)	
	//LPC_GPIO1->FIOSET = ((~ledstate) & LED_ALL);
	switch (led)
	{
		case 1:
			ledstate = LPC_GPIO0->FIOPIN;
			LPC_GPIO0->FIOSET = ((~ledstate) & (1<<9));
			LPC_GPIO0->FIOCLR = ledstate & (1<<9);
			break;
		case 2:
			ledstate = LPC_GPIO0->FIOPIN;
			LPC_GPIO0->FIOSET = ((~ledstate) & (1<<8));
			LPC_GPIO0->FIOCLR = ledstate & (1<<8);
			break;
		case 3:
			ledstate = LPC_GPIO4->FIOPIN;
			LPC_GPIO4->FIOSET = ((~ledstate) & (1<<29));
			LPC_GPIO4->FIOCLR = ledstate & (1<<29);
			break;
		case 4:
			ledstate = LPC_GPIO4->FIOPIN;
			LPC_GPIO4->FIOSET = ((~ledstate) & (1<<28));
			LPC_GPIO4->FIOCLR = ledstate & (1<<28);
			break;
		case 5:
			ledstate = LPC_GPIO1->FIOPIN;
			LPC_GPIO1->FIOSET = ((~ledstate) & (1<<23));
			LPC_GPIO1->FIOCLR = ledstate & (1<<23);
			break;
		case 6:
			ledstate = LPC_GPIO1->FIOPIN;
			LPC_GPIO1->FIOSET = ((~ledstate) & (1<<23));
			LPC_GPIO1->FIOCLR = ledstate & (1<<23);
			break;
	}
}

// Function to turn on chosen led(s)
void led_on (unsigned int ledstate)
{
	// Turn on requested LEDs
	// (ANDing to ensure we only affect the LED outputs)
	switch (ledstate)
	{
		case 1:
			LPC_GPIO0->FIOSET = (1<<9);
			break;
		case 2:
			LPC_GPIO0->FIOSET = (1<<8);
			break;
		case 3:
			LPC_GPIO4->FIOSET = (1<<29);
			break;
		case 4:
			LPC_GPIO4->FIOSET = (1<<28);
			break;
		case 5:
			LPC_GPIO1->FIOSET = (1<<23);
			msTicks_led=0;
			break;
		case 6:
			LPC_GPIO1->FIOSET = (1<<22);
			break;
	}

}

// Function to turn off chosen led(s)
void led_off (unsigned int ledstate)
{
	// Turn off requested LEDs
	// (ANDing to ensure we only affect the LED outputs)
	//LPC_GPIO1->FIOCLR = ledstate & LED_ALL;
	switch (ledstate)
	{
		case 1:
			LPC_GPIO0->FIOCLR = (1<<9);
			break;
		case 2:
			LPC_GPIO0->FIOCLR = (1<<8);
			break;
		case 3:
			LPC_GPIO4->FIOCLR = (1<<29);
			break;
		case 4:
			LPC_GPIO4->FIOCLR = (1<<28);
			break;
		case 5:
			LPC_GPIO1->FIOCLR = (1<<23);
			break;
		case 6:
			LPC_GPIO1->FIOCLR = (1<<22);
			break;
	}
}

void led_error(void)
{
	char i;
	//if(counter_envios_fallidos+counter_pollings_fallidos>100000)
	if(0)//En aquesta versió no es té en compte aquest error
	{
		if(counter_error_3 == 2000)
		{
			//Notificacio del error per ser la primera vegada
			gestio_led_error|=1<<1;
		}
		else
		{//No és la primera vegada
			counter_error_3 = 0;
		}
	}
	//Controlem nodes desconectats segons alarmes activades
	gestio_led_error&=~(1); //Resetejar variable error nodes desconectats canal 1
	for (i=0;i<MAX_NODES_CANAL;i++)
		if((alarmes_activades[i]&(1<<(DESCONEXIO_NODE-1)))) gestio_led_error|=1;
	gestio_led_error&=~(1<<2); //Resetejar variable error nodes desconectats canal 2
	for (i=0;i<MAX_NODES_CANAL;i++)
		if((alarmes_activades[i]&(1<<(DESCONEXIO_NODE-1)))) gestio_led_error|=1<<2;

	if((gestio_led_error&0b1)||(gestio_led_error&0b100))
	{//Node/s desconnectat del canal 1 o 2
		if(msTicks_led_error>100&&msTicks_led_error<200)led_on(LED_ERROR);
		if(msTicks_led_error>200&&msTicks_led_error<300)led_off(LED_ERROR);
		if(msTicks_led_error>300&&msTicks_led_error<400)led_on(LED_ERROR);
		if(msTicks_led_error>400)led_off(LED_ERROR);
	}
	else if(gestio_led_error&0b10)
	{//Sobrepassat counter d'enviament o polling
		if(msTicks_led_error>100&&msTicks_led_error<200)led_on(LED_ERROR);
		if(msTicks_led_error>200&&msTicks_led_error<300)led_off(LED_ERROR);
		if(msTicks_led_error>300&&msTicks_led_error<400)led_on(LED_ERROR);
		if(msTicks_led_error>400&&msTicks_led_error<500)led_off(LED_ERROR);
		if(msTicks_led_error>500&&msTicks_led_error<600)led_on(LED_ERROR);
		if(msTicks_led_error>600)led_off(LED_ERROR);
	}
	if(gestio_led_error&0b1000)
	{
		led_on(LED_ERROR);
	}
	if((gestio_led_error&0b1111)==0) led_off(LED_ERROR);
	if(msTicks_led_error>1000)
	{
		msTicks_led_error=0;
		return;//En aquesta versió no es té en compte aquest error
		/*if(counter_envios_fallidos>=MAX_ERRORS||counter_pollings_fallidos>=MAX_ERRORS)
		{
			if(counter_envios_fallidos==MAX_ERRORS&&counter_pollings_fallidos==MAX_ERRORS)
			{//Si els comptadors no han canviat durant l'últim segon
				counter_envios_fallidos=counter_pollings_fallidos=0;
				gestio_led_error&=~(1<<2);
				if((alarmes_activades[0]&(1<<(LIMITE_SUPERADO-1)))!=0)
				{
					alarm('c',0,LPC_RTC->HOUR,LPC_RTC->MIN,LPC_RTC->SEC,1);
				}
				alarmes_activades[0] &= ~(1<<(LIMITE_SUPERADO-1));
			}
			else
			{//Si els comptadors han canviat durant l'últim segon
				counter_envios_fallidos=counter_pollings_fallidos=MAX_ERRORS-100;
				gestio_led_error|=1<<1;
				if((alarmes_activades[0]&(1<<(LIMITE_SUPERADO-1)))==0)
				{
					alarm(LIMITE_SUPERADO,0,LPC_RTC->HOUR,LPC_RTC->MIN,LPC_RTC->SEC,1);
					Error_aplicacio(LIMITE_SUPERADO,1,1);
				}
				alarmes_activades[0] |= 1<<(LIMITE_SUPERADO-1);
			}
		}*/
	}
}
