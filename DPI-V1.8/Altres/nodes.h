#ifndef NODES_H_
#define NODES_H_

#include <stdint.h>
#include "GlobalVars.h"

//Port de sortida
#define UART0				0
#define UART1				1
#define UART2				2
#define UART3				3



//Status dels nodes
#define UNKNOWN				0
#define INIT				1
#define TEST				2//Remoto/local
#define NORMAL				3
#define ERROR_NODE			4
#define PROGRAMACIO_STATUS	5
#define TIPUS_RS232			10//Interface RS232
#define TIPUS_LCIN			11//Interface LC

//Tipus de node
#define UNKNOWN_TYPE		0
#define DISPLAY_4			1 //Display 4 digits 3 botons, BUZZER i led RGB (DPD43BCAN)
#define DISPLAY_12			2 //Display 12 digits 4 botons, BUZZER i led RGB (DPD124BCAN)
//----------------------------------------------------//
//Adreces

	//Configuracio del node

//#define NODE_ID_2			0
//#define NODE_ID_1			1
#define MULTICAST_ID_1		0
#define MULTICAST_ID_2		1
#define NODE_TYPE			2
#define FIRM_VERSION		3
#define NUM_SERIE_4			6
#define NUM_SERIE_3			7
#define NUM_SERIE_2			8
#define NUM_SERIE_1			9
#define DATA_FABRICACIO_2	4
#define DATA_FABRICACIO_1	5
#define STATUS				12

	//Errors
#define	TEC					13//Transmit error counter
#define REC					14//Receive error counter
#define LEC					15//Last error counter
	//Accions comunes
#define ACCIONS_COMUNES		23

	//Sortides digitals

#define BUZZER				24
#define LEDS				25

//#define DO_1				26
//#define DO_2				27
#define DO_3				26
#define DO_4				27
#define DO_5				28
#define DO_6				29

	//Entrades digitals

#define POLSADORS			30

//#define DI_1				36
#define DI_2				31
#define DI_3				32
#define DI_4				33
#define DI_5				34
#define DI_6				35

	//Sortides analogiques

#define AO_1				36
#define AO_2				37
#define AO_3				38
#define AO_4				39
#define AO_5				40
#define AO_6				41

	//Entrades analogiques

#define AI_1				42
#define AI_2				43
#define AI_3				44
#define AI_4				45
#define AI_5				46
#define AI_6				47

	//Adreces especifiques
#define DISPLAY				48

//----------------------------------------------------//
//----------------------------------------------------//
//Alarmes
#define RUPTURA_ANELL			1
#define DESCONEXIO_NODE			2
#define ERROR_HW				3 //abans 4
#define WRONG_FORMAT			4 //Abans 5
#define NODE_NOCONFIGURAT		5
#define ENVIAR_TRAMA			6
#define LIMITE_SUPERADO			8 //abans 3
#define MESSAGE_OK				9
//char RUPTURA_ANELL_OK 	=	'a';
//char DESCONEXIO_NODE_OK   =	'b';
//char LIMITE_SUPERADO_OK	=	'c';
//char ERROR_HW_OK 			=	'd';
#define LONGITUD_ALARMS  		418
#define MAX_NODES_CANAL			250

extern volatile uint8_t NUM_NODES;
extern volatile uint8_t NUM_NODES_2;
extern volatile uint8_t nodes_correctes;
extern volatile uint8_t nodes_correctes_2;
extern volatile uint8_t nodes_correctes_web;
extern volatile uint8_t nodes_correctes_web_2;
extern volatile uint8_t alarmes_activades[MAX_NODES_CANAL];
extern volatile uint8_t alarmes_activades_2[MAX_NODES_CANAL];
extern volatile uint8_t control_alarmes[MAX_NODES_CANAL];	//Comptador per desconnexio de node i varis nodes
extern volatile uint8_t control_alarmes_2[MAX_NODES_CANAL];	//0xFF00-->Timeout 0x00C0-->Comptador varis nodes
															//0x003F-->Comptador desconnexió
extern volatile uint8_t control_alarmes_HW[MAX_NODES_CANAL];
extern volatile uint8_t control_alarmes_HW_2[MAX_NODES_CANAL];

extern volatile uint8_t gestio_led_error;

/*Grups de onze*/
/*1-->tipus d'alarma 2,3,4--> node*/
extern volatile uint8_t taula_alarms [LONGITUD_ALARMS];
extern volatile uint16_t posicio_taula_alarms;
extern volatile uint16_t alarmes_activades_canal1;//nombre d'alarmes activades del canal 1
extern volatile uint16_t alarmes_activades_canal2;//nombre d'alarmes activades del canal 2
//----------------------------------------------------//
//Taula de nodes
/*0-->Port de sortida (CAN) 3,2,1-->Status node 7,6,5,4-->Tipus node*/
extern volatile uint16_t taula_nodes [MAX_NODES_CANAL]; //S'actualitza a cada pooling i es posa a zero en acabar
extern volatile uint16_t taula_nodes_2 [MAX_NODES_CANAL]; //S'actualitza a cada pooling i es posa a zero en acabar
extern uint16_t taula_nodes_web [MAX_NODES_CANAL]; //És estàtica, no reflexa els canvis
extern uint16_t taula_nodes_web_2 [MAX_NODES_CANAL]; //És estàtica, no reflexa els canvis
extern volatile uint8_t taula_SD [MAX_NODES_CANAL]; //Indica els nodes configurats a la SD
extern volatile uint8_t taula_SD_2 [MAX_NODES_CANAL]; //Indica els nodes configurats a la SD
extern volatile uint8_t taula_configurats [MAX_NODES_CANAL]; //Indica els nodes configurats
extern volatile uint8_t taula_configurats_2 [MAX_NODES_CANAL];//Indica els nodes configurats


void ini_nodes(void);
void ini_nodes_2(void);
void ini_SD(void);
void ini_SD_2(void);
void ini_taula_alarms(void);
void ini_taula_web(void);
void ini_taula_configurats(void);
void actualitza_node (uint8_t port, uint8_t status, uint8_t tipus, uint16_t node, uint8_t pulsadors);
void canviar_taula_nodes_web(void);
void canviar_taula_nodes_web_2(void);
void alarm (char tipus, uint16_t node, uint8_t hora, uint8_t minut, uint8_t segon, uint8_t canal);

#endif /*NODES_H_*/
