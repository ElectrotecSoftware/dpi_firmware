//*****************************************************************************
//   +--+       
//   | ++----+   
//   +-++    |  
//     |     |  
//   +-+--+  |   
//   | +--+--+  
//   +----+    Copyright (c) 2009 Code Red Technologies Ltd. 
//
// leds.h header declares functions and constansts for accessing the "user LEDs"
// on the RDB1768 development board (LEDs 2-5).
//
//
// Software License Agreement
// 
// The software is owned by Code Red Technologies and/or its suppliers, and is 
// protected under applicable copyright laws.  All rights are reserved.  Any 
// use in violation of the foregoing restrictions may subject the user to criminal 
// sanctions under applicable laws, as well as to civil liability for the breach 
// of the terms and conditions of this license.
// 
// THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
// OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
// USE OF THIS SOFTWARE FOR COMMERCIAL DEVELOPMENT AND/OR EDUCATION IS SUBJECT
// TO A CURRENT END USER LICENSE AGREEMENT (COMMERCIAL OR EDUCATIONAL) WITH
// CODE RED TECHNOLOGIES LTD. 

#ifndef LEDS_H_
#define LEDS_H_

// Set of defines for RDB1768 board user leds
#define LED_1 			1
#define LED_2 			2
#define LED_3 			3
#define LED_4 			4
#define LED_ACTIVITY	5
#define LED_ERROR		6
// All Leds


void leds_joystick_init (void);
void leds_all_on (void);
void leds_all_off (void);
void leds_invert (unsigned int led);
void led_on (unsigned int ledstate);
void led_off (unsigned int ledstate);
void led_error(void);

#endif /*LEDS_H_*/
