//*****************************************************************************
//   +--+
//   | ++----+
//   +-++    |
//     |     |
//   +-+--+  |
//   | +--+--+
//   +----+    Copyright (c) 2010 Code Red Technologies Ltd.
//
// timer.c provides example contains setup and configuration functions for the
// 4 timers to be found in the LPC1768 MCU fitted to the RDB1768 development
// board.
//
// Software License Agreement
//
// The software is owned by Code Red Technologies and/or its suppliers, and is
// protected under applicable copyright laws.  All rights are reserved.  Any
// use in violation of the foregoing restrictions may subject the user to criminal
// sanctions under applicable laws, as well as to civil liability for the breach
// of the terms and conditions of this license.
//
// THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
// OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
// USE OF THIS SOFTWARE FOR COMMERCIAL DEVELOPMENT AND/OR EDUCATION IS SUBJECT
// TO A CURRENT END USER LICENSE AGREEMENT (COMMERCIAL OR EDUCATIONAL) WITH
// CODE RED TECHNOLOGIES LTD.

#include "LPC17xx.h"
#include "timer.h"
#include <stdio.h>
#include "all.h"

/*************************************************
 * enable_timer()
 *
 * Enables appropriate timer, based on 'timer_num'
 *************************************************/
void enable_timer( uint8_t timer_num )
{
	if ( timer_num == 0 )
	{
		LPC_TIM0->TCR = 1;
	}
	else if ( timer_num == 1 )
	{
		LPC_TIM1->TCR = 1;
	}
	else if ( timer_num == 2 )
	{
		LPC_TIM2->TCR = 1;
	}
}

/**************************************************
 * disable_timer()
 *
 * Disables appropriate timer, based on 'timer_num'
 **************************************************/
void disable_timer( uint8_t timer_num )
{
	if ( timer_num == 0 )
	{
		timer0_counter = 0;
		LPC_TIM0->TCR = 0;
	}
	else if ( timer_num == 1 )
	{
		timer1_counter = 0;
		LPC_TIM1->TCR = 0;
	}
	else if ( timer_num == 2 )
	{
		timer2_counter = 0;
		LPC_TIM2->TCR = 0;
	}
}

/**************************************************
 * reset_timer()
 *
 * Resets appropriate timer, based on 'timer_num'
 **************************************************/
void reset_timer( uint8_t timer_num )
{
	uint32_t regVal;

	if ( timer_num == 0 )
	{
		regVal = LPC_TIM0->TCR;
		regVal |= 0x02;
		LPC_TIM0->TCR = regVal;
	}
	else if ( timer_num == 1 )
	{
		regVal = LPC_TIM1->TCR;
		regVal |= 0x02;
		LPC_TIM1->TCR = regVal;
	}
	else if ( timer_num == 2 )
	{
		regVal = LPC_TIM2->TCR;
		regVal |= 0x02;
		LPC_TIM2->TCR = regVal;
	}
}

/******************************************************
 * init_timer()
 *
 * Initialises appropriate timer, based on 'timer_num',
 * setting timer interval, resetting timer and
 * enabling timer interrupt.
 ******************************************************/
void init_timer ( uint8_t timer_num, uint32_t TimerInterval )
{
	if ( timer_num == 0 )
	{
		timer0_counter = 0;
		LPC_TIM0->MR0 = TimerInterval;
		LPC_TIM0->MCR = 3;				/* Interrupt and Reset on MR0 */
		NVIC_EnableIRQ(TIMER0_IRQn);
		NVIC_SetPriority(TIMER0_IRQn,4);
	}
	else if ( timer_num == 1 )
	{
		timer1_counter = 0;
		LPC_TIM1->MR0 = TimerInterval;
		LPC_TIM1->MCR = 3;				/* Interrupt and Reset on MR1 */
		NVIC_EnableIRQ(TIMER1_IRQn);
		NVIC_SetPriority(TIMER1_IRQn,5);
	}
	else if ( timer_num == 2 )
	{
		timer2_counter = 0;
		LPC_SC->PCONP |= 0x00400000;
		LPC_TIM2->MR0 = TimerInterval;
		LPC_TIM2->MCR = 3;				/* Interrupt and Reset on MR1 */
		NVIC_EnableIRQ(TIMER2_IRQn);
		NVIC_SetPriority(TIMER2_IRQn,6);
	}
}

void TIMER0_IRQHandler (void)
{
  LPC_TIM0->IR = 1;			/* clear interrupt flag */
  timer0_counter++;
  return;
}

void TIMER1_IRQHandler (void)
{
  LPC_TIM1->IR = 1;			/* clear interrupt flag */
  timer1_counter++;
  if(LPC_UART3->LSR&LSR_THRE&&StateUART_canal1==ENVIANT_2&&UART1Transmit_Buffer[1]!=0&&timer1_counter>5)
  {
	  //if(UART1Transmit_Buffer[1]!=0&&timer1_counter>3)UART3_THRE();
	  //else UART3_THRE();
	  UART3_THRE();
  }
  else if(LPC_UART0->LSR&LSR_THRE&&StateUART_canal1==ENVIANT&&UART0Transmit_Buffer[1]!=0&&timer1_counter>5)
  {
	  //if(UART0Transmit_Buffer[1]!=0&&timer1_counter>3)UART0_THRE();
	  //else UART0_THRE();
	  UART0_THRE();
  }
  return;
}

void TIMER2_IRQHandler (void)
{
  LPC_TIM2->IR = 1;			/* clear interrupt flag */
  timer2_counter++;
  if(LPC_UART2->LSR&LSR_THRE&&StateUART_canal2==ENVIANT_2&&UART3Transmit_Buffer[1]!=0&&timer2_counter>5)
  {
	  //if(UART3Transmit_Buffer[1]!=0&&timer2_counter>3)UART2_THRE();
	  //else UART2_THRE();
	  UART2_THRE();
  }
  else if(LPC_UART1->LSR&LSR_THRE&&StateUART_canal2==ENVIANT&&UART2Transmit_Buffer[1]!=0&&timer2_counter>5)
  {
	  //if(UART2Transmit_Buffer[1]!=0&&timer2_counter>3)UART1_THRE();
	  //else UART1_THRE();
	  UART1_THRE();
  }
  return;
}

