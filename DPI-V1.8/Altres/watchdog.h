
#ifndef WATCHDOG_H_
#define WATCHDOG_H_

#include <stdint.h>

void wd_init (uint8_t time);
void wd_reset (void);

#endif /*WATCHDOG_H_*/
