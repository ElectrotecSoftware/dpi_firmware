//*****************************************************************************
//   +--+       
//   | ++----+   
//   +-++    |  
//     |     |  
//   +-+--+  |   
//   | +--+--+  
//   +----+    Copyright (c) 2009 Code Red Technologies Ltd. 
//
// lcd_driver.h - Header file for driver for the lowest level access routines
// for the Sitronix ST7637 LCD Controller/driver used on the RDB1768 
// development board.
//
// Software License Agreement
// 
// The software is owned by Code Red Technologies and/or its suppliers, and is 
// protected under applicable copyright laws.  All rights are reserved.  Any 
// use in violation of the foregoing restrictions may subject the user to criminal 
// sanctions under applicable laws, as well as to civil liability for the breach 
// of the terms and conditions of this license.
// 
// THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
// OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
// USE OF THIS SOFTWARE FOR COMMERCIAL DEVELOPMENT AND/OR EDUCATION IS SUBJECT
// TO A CURRENT END USER LICENSE AGREEMENT (COMMERCIAL OR EDUCATIONAL) WITH
// CODE RED TECHNOLOGIES LTD. 

#ifndef LCD_DRIVER_H_
#define LCD_DRIVER_H_
#define NUM_LCD 	10
#define UP_LCD		(1<<24)
#define OK_LCD		(1<<25)
#define DOWN_LCD	(1<<28)
#define UP_DOWN_LCD (1<<26)//Número inventat

#define GREEN_CANAL 0
#define RED_CANAL 1

#include "stdint.h"
typedef enum {
	INICI,
	CONFIG,
	SETTINGS,
	MENU_LCD,
	IP,
	NM,
	GW,
	BRILLO
} SStateLCD;
uint8_t color_canal1;
uint8_t color_canal2;
uint32_t apretada;
extern SStateLCD StateLCD;
extern uint8_t change_state;
extern uint8_t brillo;
extern uint8_t PWM_high;
extern uint8_t PWM_cont;
extern uint8_t x_config;
extern uint8_t y_config;
extern uint8_t fila;
extern uint8_t columna;
extern char numero_LCD[2];
extern char text[16];
extern uint8_t IP_LCD[15];
extern uint8_t NM_LCD[15];
extern uint8_t GW_LCD[15];
extern volatile uint32_t msTicks_LCD;
extern uint8_t cont_LCD;
void LCDdriver_WriteData(unsigned char LCD_Data);
void LCDdriver_WriteCom(unsigned char LCD_Command);
void LCDdriver_initialisation(void);
void LCD_ClearScreen(int bgcolor);
void process_menu_LCD (void);
void process_PWM_LCD (void);
void dibuixar_tres (void);
void comprovar_numero_LCD(uint8_t cas);

#endif /*LCD_DRIVER_H_*/
