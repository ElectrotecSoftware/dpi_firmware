//*****************************************************************************
//   +--+       
//   | ++----+   
//   +-++    |  
//     |     |  
//   +-+--+  |   
//   | +--+--+  
//   +----+    Copyright (c) 2009 Code Red Technologies Ltd. 
//
// lcd_driver.c contains the lowest level access routines for the Sitronix
// ST7637 LCD Controller/driver used on the RDB1768 development board.
//
//
// Software License Agreement
// 
// The software is owned by Code Red Technologies and/or its suppliers, and is 
// protected under applicable copyright laws.  All rights are reserved.  Any 
// use in violation of the foregoing restrictions may subject the user to criminal 
// sanctions under applicable laws, as well as to civil liability for the breach 
// of the terms and conditions of this license.
// 
// THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
// OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
// USE OF THIS SOFTWARE FOR COMMERCIAL DEVELOPMENT AND/OR EDUCATION IS SUBJECT
// TO A CURRENT END USER LICENSE AGREEMENT (COMMERCIAL OR EDUCATIONAL) WITH
// CODE RED TECHNOLOGIES LTD. 

// Local copy of NXP LPC header file used due to issues with incorrect defintion
// for FIO2PIN0 used in optimized low level access to LCD controller.
//#include "NXP/LPC17xx/LPC17xx.h"
#include "LPC17xx.h"

#include "lcd.h"
#include "tcpip.h"
#include <stdio.h>
#include "casa.h"
#include "nodes.h"
#include "blockdev.h"


// Used to control whether the optimized version of the lowest level LCD access
// routines are used, rather than the simple, direct implementations
#define OPT_LCD_ACCESS 1

// Bits within GPIO port 2 used for LCD driver
#define LCD_CSB_PIN            (1<<13)
#define LCD_A0_PIN             (1<<8)
#define LCD_WR_PIN             (1<<11)
#define LCD_RD_PIN             (1<<12)
#define LCD_DATA_PIN			0xfc

// Bit within GPIO port 3 used for LCD driver
#define LCD_RESB_PIN           (1<<25)

// Bits to configure as outputs for driving LCD
#define LCD_PORT2_DIRECTIONS  (LCD_CSB_PIN | LCD_A0_PIN | LCD_WR_PIN | LCD_RD_PIN | LCD_DATA_PIN)
#define LCD_PORT3_DIRECTIONS  (LCD_RESB_PIN)


// Routine to introduce short delays required during LCD initialisation
void ms_delay (int n) __attribute__((noinline));
void ms_delay(int n)
{
   volatile int d;
   for (d=0; d<n*3000; d++){}
}

// Routine to write data to LCD driver. Normally called in combination
// with LCDdriver_WriteCom() routine
void LCDdriver_WriteData(unsigned char LCD_Data) __attribute__((noinline));
void LCDdriver_WriteData(unsigned char LCD_Data)
{
#ifdef OPT_LCD_ACCESS
    //  ** Optimised code **
	//LPC_GPIO0->FIOMASK2 = 0xe7;
	LPC_GPIO0->FIOPIN2 = ((LCD_Data&0x01)<<4)|((LCD_Data&0x02)<<2);
	//LPC_GPIO2->FIOMASK0 = 0x03;
	LPC_GPIO2->FIOPIN0 = LCD_Data&0xfc;

	LPC_GPIO2->FIOCLR = LCD_CSB_PIN | LCD_WR_PIN;
	__asm("nop\n"); // Very short delay for pin setup
	LPC_GPIO2->FIOSET = LCD_CSB_PIN | LCD_WR_PIN ;
#else
	// ** Original code **	
    LCD_DATA_CLR = LCD_DATA_PIN;
    LCD_DATA_SET = LCD_Data;
    LCD_CSB_CLR = LCD_CSB_PIN;
    LCD_WR_CLR = LCD_WR_PIN;
    __asm("nop\n");
    __asm("nop\n");
    __asm("nop\n");
    LCD_WR_SET = LCD_WR_PIN;
    LCD_CSB_SET = LCD_CSB_PIN;
#endif
}

// Routine to configure set LCD driver to accept particular command.
// A call to this routine will normally be followed by a call
// to LCDdriver_WriteData() to transfer appropriate parameters to driver.
void LCDdriver_WriteCom(unsigned char LCD_Command)  __attribute__((noinline));
void LCDdriver_WriteCom(unsigned char LCD_Command)
{
	uint8_t prova;
#ifdef OPT_LCD_ACCESS
	//  ** Optimised code **
	//LPC_GPIO2->FIOPIN0 = LCD_Command;
	//LPC_GPIO0->FIOMASK2 = 0xe7;
	LPC_GPIO0->FIOPIN2 = ((LCD_Command&0x01)<<4)|((LCD_Command&0x02)<<2);
	//LPC_GPIO2->FIOMASK0 = 0x03;
	LPC_GPIO2->FIOPIN0 = LCD_Command&0xfc;
	prova = (((LCD_Command&0x80)>>7)<<3)|(((LCD_Command&0x40)>>6)<<4);
	LPC_GPIO2->FIOCLR = LCD_A0_PIN | LCD_CSB_PIN | LCD_WR_PIN;
	__asm("nop\n");	// Very short delay for pin setup
	LPC_GPIO2->FIOSET = LCD_A0_PIN |LCD_CSB_PIN | LCD_WR_PIN ;
#else
	//	** Original code **		
	LCD_DATA_CLR = LCD_DATA_PIN;
    LCD_DATA_SET = LCD_Command;
    LCD_A0_CLR =	LCD_A0_PIN;
    LCD_CSB_CLR = LCD_CSB_PIN;
    LCD_WR_CLR = LCD_WR_PIN;
    __asm("nop\n"); // Very short delay for pin setup
    __asm("nop\n");
    __asm("nop\n");
    LCD_WR_SET = LCD_WR_PIN;
    LCD_CSB_SET = LCD_CSB_PIN;
	LCD_A0_SET = LCD_A0_PIN;
#endif
}

// Initialize GPIO connection to the LCD driver
void LCDdriver_ConfigGPIOtoLCD(void)
{
	LPC_PINCON->PINSEL4 &=~((3<<4)|(3<<6)|(3<<8)|(3<<10)|(3<<12)|(3<<14)|(3<<16)|(3<<22)|(3<<24)|(3<<26));
	LPC_PINCON->PINSEL7 &=~(3<<18);
	LPC_PINCON->PINSEL0 &=~(3<<6)|(3<<8);


	LPC_GPIO2->FIODIR |= LCD_PORT2_DIRECTIONS;
	LPC_GPIO3->FIODIR |= LCD_PORT3_DIRECTIONS;
	LPC_GPIO0->FIODIR |= ((1<<20) | (1<<19));

	// Set GPIO outputs connected to LCD to default values
	LPC_GPIO2->FIOSET = LCD_CSB_PIN;	// LCD_CSB_SET
	LPC_GPIO2->FIOSET = LCD_A0_PIN;		// LCD_A0_SET
	LPC_GPIO2->FIOSET = LCD_WR_PIN;		// LCD_WR_SET
	LPC_GPIO2->FIOSET = LCD_RD_PIN;		// LCD_RD_SET
	LPC_GPIO3->FIOSET = LCD_RESB_PIN;	// LCD_RESB_SET
	LPC_GPIO2->FIOCLR =  0x3f; 			// LCD_DATA_CL - data bus to zero
	LPC_GPIO0->FIOCLR = ((1<<20) | (1<<19));
}


// Initialisation routine to set up LCD
void LCDdriver_initialisation(void)
{
	LCDdriver_ConfigGPIOtoLCD();		// Initialize the GPIO for the display
	LPC_GPIO2->FIOCLR = LCD_CSB_PIN;
	LPC_GPIO3->FIOSET = LCD_RESB_PIN;
	ms_delay(20);
	LPC_GPIO3->FIOCLR = LCD_RESB_PIN;
	ms_delay(20);
	LPC_GPIO3->FIOSET = LCD_RESB_PIN;
	ms_delay(200);
	LPC_GPIO2->FIOSET = LCD_CSB_PIN;

	LCDdriver_WriteCom(0xe2);                       //(24)System Reset
	ms_delay(100);
	LCDdriver_WriteCom(0xeb);                      //(27)set LCD Bias Ratio
	LCDdriver_WriteCom(0x81);                      //(11)set Vbias Potentiometer
	LCDdriver_WriteCom(0x88);                      //(27)set LCD Bias Ratio
	LCDdriver_WriteCom(0x2f);                      //(7)set pump control
	LCDdriver_WriteCom(0x24);                      //(5)set Temp Compensation
	LCDdriver_WriteCom(0x2b);                      //(6)set Panel Loading
	LCDdriver_WriteCom(0xa3);                      //(15)set line Rate
	LCDdriver_WriteCom(0x84);                      //(12)set Partial display control
	LCDdriver_WriteCom(0x89);                      //(13)set RAM Address control
	LCDdriver_WriteCom(0xd1);                      //(21)set color pattern
	LCDdriver_WriteCom(0xd6);                      //(22)set color mode
	LCDdriver_WriteCom(0xc8);                      //(20)set N-line Inversion(0xc8,0x42)0x44
	LCDdriver_WriteCom(0x42);
	LCDdriver_WriteCom(0xc0);                      //(19)set LCD Mappng Control
	LCDdriver_WriteCom(0xde);                      //(23)set COM Scan Function
	LCDdriver_WriteCom(0xaf);                      //(16)set all-pixel-on
	LCD_ClearScreen(0);
}

// Routine to clear the LCD.
// Implemented by drawing a rectangle of the appropriate color 
// across the whole screen
void LCD_ClearScreen(int bgcolor)
{
	int x,y;
	for(x = 0; x < LCD_MAX_X; x++)
	{
		for(y=0;y<LCD_MAX_Y; y++)
		{
		     LCDdriver_WriteCom((0x0f&x));				//col	start
		 	 LCDdriver_WriteCom((0x07&(x>>4))|0x10); 	    //col start

			 LCDdriver_WriteCom((0x0f&(y))|0x60);	     	//Row start
			 LCDdriver_WriteCom((0x0f&((y)>>4))|0x70);  	//Row start

		     LCDdriver_WriteCom(0xf4);
		     LCDdriver_WriteCom(x);                  //y

		     LCDdriver_WriteCom(0xf5);  						//Row start        x
		     LCDdriver_WriteCom(y);  					//col start

		     LCDdriver_WriteCom(0xf6);
		     LCDdriver_WriteCom(x+8);

		     LCDdriver_WriteCom(0xf7);  						//Row End
		     LCDdriver_WriteCom(y+16);


		     LCDdriver_WriteCom(0xaf);

		     LCDdriver_WriteData(bgcolor>>8);

		     LCDdriver_WriteData(bgcolor);
		}
	}
}
void process_menu_LCD (void)
{
	if(cambiar_ethernet&2)
	{
		LCD_FilledRect (0, 127, 26, 127, COLOR_BLAU_CEL);
		LCD_Rect (8, 27, 2, 22, COLOR_BLAU_CEL);
		LCD_Rect (29, 48, 2, 22, COLOR_WHITE);
		LCD_Rect (50, 69, 2, 21, COLOR_WHITE);
		StateLCD = INICI;
		change_state = 0;
		cont_LCD = 0;
		apretada = 0;
		x_config = 0;
		y_config = 0;
		fila = 0;
		columna = 0;
		cambiar_ethernet &= ~2;
	}
	if(alarmes_activades_canal1)//Si hi ha problemes al canal 1 rodona LCD vermella
	{
		if(color_canal1==GREEN_CANAL)
		{
			LCD_FilledCircle (88, 12, 8, COLOR_RED);
			LCD_PrintString(88-4,12-6,"1", COLOR_BLACK, COLOR_RED);
			color_canal1=RED_CANAL;
		}
	}
	else
	{
		if(color_canal1==RED_CANAL)
		{
			LCD_FilledCircle (88, 12, 8, COLOR_GREEN);
			LCD_PrintString(88-4,12-6,"1", COLOR_BLACK, COLOR_GREEN);
			color_canal1=GREEN_CANAL;
		}
	}
	if(alarmes_activades_canal2)//Si hi ha problemes al canal 2 rodona verda
	{
		if(color_canal2==GREEN_CANAL)
		{
			LCD_FilledCircle (109, 12, 8, COLOR_RED);
			LCD_PrintString(109-3,12-6,"2", COLOR_BLACK, COLOR_RED);
			color_canal2=RED_CANAL;
		}
	}
	else
	{
		if(color_canal2==RED_CANAL)
		{
			LCD_FilledCircle (109, 12, 8, COLOR_GREEN);
			LCD_PrintString(109-3,12-6,"2", COLOR_BLACK, COLOR_GREEN);
			color_canal2=GREEN_CANAL;
		}
	}
	if (!(LPC_GPIO1->FIOPIN & (UP_LCD)))//&&(apretada==UP_DOWN_LCD||!apretada))
	{//Per resetejar IP i la interface
		if(!(LPC_GPIO1->FIOPIN & (DOWN_LCD)))
		{
			if(apretada!=UP_DOWN_LCD){cont_LCD=0;msTicks_LCD=0;}
			apretada=UP_DOWN_LCD;
			return;
		}
	}else apretada=0;
	/*if(StateLCD==INICI)
	{
		if(change_state==0)
		{//Perque no parpadeigi la pantalla si brillo<100
			sprintf((char *)text,"%2.2d.%2.2d.%1.1d.%2.2d\n",	    MYIP_1,
																	MYIP_2,
																	MYIP_3,
																	MYIP_4);
			LCD_PrintString(8,30,text, COLOR_WHITE, COLOR_BLAU_CEL);
			LCD_PrintString(8,46,"Nodos 1:", COLOR_BLACK, COLOR_BLAU_CEL);
			sprintf((char *)text,"%1.1d",NUM_NODES);
			LCD_PrintString(75,46,text, COLOR_WHITE, COLOR_BLAU_CEL);
			LCD_PrintString(8,62,"Nodos 2:", COLOR_BLACK, COLOR_BLAU_CEL);
			sprintf((char *)text,"%1.1d",NUM_NODES_2);
			LCD_PrintString(75,62,text, COLOR_WHITE, COLOR_BLAU_CEL);
			LCD_PrintString(8,78,"Puerto:", COLOR_BLACK, COLOR_BLAU_CEL);
			sprintf((char *)text,"%1.1d",PORT_COMUNICACIO);
			LCD_PrintString(66,78,text, COLOR_WHITE, COLOR_BLAU_CEL);
			change_state=100;
		}
		if (!(LPC_GPIO1->FIOPIN & (UP_LCD))&&(apretada==UP_LCD||!apretada))
		{
			apretada=UP_LCD;
			cont_LCD++;
			if(cont_LCD<NUM_LCD)return;
			change_state=1;
		}
		else if(!(LPC_GPIO1->FIOPIN & (DOWN_LCD))&&(apretada==DOWN_LCD||!apretada))
		{
			apretada=DOWN_LCD;
			cont_LCD++;
			if(cont_LCD<NUM_LCD)return;
			change_state=2;
		}
		else
		{
			apretada=0;
			cont_LCD = 0;
			if(change_state==1)
			{
				change_state = 0;
				StateLCD = CONFIG;
				LCD_Rect (8, 27, 2, 22, COLOR_WHITE);
				LCD_Rect (29, 48, 2, 22, COLOR_BLAU_CEL);
				LCD_FilledRect (0, 127, 26, 127, COLOR_BLAU_CEL);
			}
			else if(change_state==2)
			{
				change_state = 0;
				StateLCD = SETTINGS;
				LCD_Rect (8, 27, 2, 22, COLOR_WHITE);
				LCD_Rect (50, 69, 2, 21, COLOR_BLAU_CEL);
				LCD_FilledRect (0, 127, 26, 127, COLOR_BLAU_CEL);
			}
		}
	}
	else if(StateLCD == CONFIG)
	{
		if(change_state==0)
		{//Perque no parpadeigi la pantalla si brillo<100
			x_config = 4;
			y_config = 37;
			fila = 1;
			columna = 1;
			sprintf((char *)text,"%3.3d.%3.3d.%3.3d.%3.3d\n",	    MYIP_1,
																	MYIP_2,
																	MYIP_3,
																	MYIP_4);
			LCD_PrintString(8,30,"IP:", COLOR_BLACK, COLOR_BLAU_CEL);
			LCD_PrintString(4,42,text, COLOR_WHITE, COLOR_BLAU_CEL);
			sprintf((char *)text,"%3.3d.%3.3d.%3.3d.%3.3d\n",	    SUBMASK_1,
																	SUBMASK_2,
																	SUBMASK_3,
																	SUBMASK_4);
			LCD_PrintString(8,58,"Mascara subred:", COLOR_BLACK, COLOR_BLAU_CEL);
			LCD_PrintString(4,70,text, COLOR_WHITE, COLOR_BLAU_CEL);
			sprintf((char *)text,"%3.3d.%3.3d.%3.3d.%3.3d\n",	    GWIP_1,
																	GWIP_2,
																	GWIP_3,
																	GWIP_4);
			LCD_PrintString(8,86,"Puerta enlace:", COLOR_BLACK, COLOR_BLAU_CEL);
			LCD_PrintString(4,98,text, COLOR_WHITE, COLOR_BLAU_CEL);
			change_state = 100;//Perque no et torni a entrar
			LCD_Rect (30, 49, 3, 23, COLOR_WHITE);
		}
		if (!(LPC_GPIO1->FIOPIN & (UP_LCD))&&(apretada==UP_LCD||!apretada))
		{
			apretada=UP_LCD;
			cont_LCD++;
			if(cont_LCD<NUM_LCD)return;
			if(change_state==4)
			{
				change_state = 5;
			}
			else if(change_state==17)
			{
				MYIP_1=IP_LCD[0]*100+IP_LCD[1]*10+IP_LCD[2];
				MYIP_2=IP_LCD[4]*100+IP_LCD[5]*10+IP_LCD[6];
				MYIP_3=IP_LCD[8]*100+IP_LCD[9]*10+IP_LCD[10];
				MYIP_4=IP_LCD[12]*100+IP_LCD[13]*10+IP_LCD[14];
				SUBMASK_1=NM_LCD[0]*100+NM_LCD[1]*10+NM_LCD[2];
				SUBMASK_2=NM_LCD[4]*100+NM_LCD[5]*10+NM_LCD[6];
				SUBMASK_3=NM_LCD[8]*100+NM_LCD[9]*10+NM_LCD[10];
				SUBMASK_4=NM_LCD[12]*100+NM_LCD[13]*10+NM_LCD[14];
				GWIP_1=GW_LCD[0]*100+GW_LCD[1]*10+GW_LCD[2];
				GWIP_2=GW_LCD[4]*100+GW_LCD[5]*10+GW_LCD[6];
				GWIP_3=GW_LCD[8]*100+GW_LCD[9]*10+GW_LCD[10];
				GWIP_4=GW_LCD[12]*100+GW_LCD[13]*10+GW_LCD[14];
				MyIP[0]=MYIP_1 + (MYIP_2 << 8);
				MyIP[1]=MYIP_3 + (MYIP_4 << 8);
				BlockDevWrite(ADRESS_IP,(uint8_t *)MyIP,4);
				SubnetMask[0]=SUBMASK_1 + (SUBMASK_2 << 8);
				SubnetMask[1]=SUBMASK_3 + (SUBMASK_4 << 8);
				BlockDevWrite(ADRESS_NM,(uint8_t *)SubnetMask,4);
				GatewayIP[0]=GWIP_1 + (GWIP_2 << 8);
				GatewayIP[1]=GWIP_3 + (GWIP_4 << 8);
				BlockDevWrite(ADRESS_GW,(uint8_t *)GatewayIP,4);
				change_state=8;
			}
			else if(change_state==10)
			{
				change_state=9;
			}
			else if(change_state!=5&&change_state!=8&&change_state!=9)
			{
				change_state=1;
			}
		}
		else if(!(LPC_GPIO1->FIOPIN & (DOWN_LCD))&&(apretada==DOWN_LCD||!apretada))
		{
			apretada=DOWN_LCD;
			cont_LCD++;
			if(cont_LCD<NUM_LCD)return;
			if(change_state==4)
			{
				change_state = 6;
			}
			else if(change_state==17)
			{
				//IP inicial
				IP_LCD[0]=MYIP_1/100;
				IP_LCD[1]=(MYIP_1-((IP_LCD[0])*100))/10;
				IP_LCD[2]=MYIP_1-(((IP_LCD[0])*100)+((IP_LCD[1])*10));
				IP_LCD[4]=MYIP_2/100;
				IP_LCD[5]=(MYIP_2-((IP_LCD[4])*100))/10;
				IP_LCD[6]=MYIP_2-(((IP_LCD[4])*100)+((IP_LCD[5])*10));
				IP_LCD[8]=MYIP_3/100;
				IP_LCD[9]=(MYIP_3-((IP_LCD[8])*100))/10;
				IP_LCD[10]=MYIP_3-(((IP_LCD[8])*100)+((IP_LCD[9])*10));
				IP_LCD[12]=MYIP_4/100;
				IP_LCD[13]=(MYIP_4-((IP_LCD[12])*100))/10;
				IP_LCD[14]=MYIP_4-(((IP_LCD[12])*100)+((IP_LCD[13])*10));
				//NM inicial
				NM_LCD[0]=SUBMASK_1/100;
				NM_LCD[1]=(SUBMASK_1-((NM_LCD[0])*100))/10;
				NM_LCD[2]=SUBMASK_1-(((NM_LCD[0])*100)+((NM_LCD[1])*10));
				NM_LCD[4]=SUBMASK_2/100;
				NM_LCD[5]=(SUBMASK_2-((NM_LCD[4])*100))/10;
				NM_LCD[6]=SUBMASK_2-(((NM_LCD[4])*100)+((NM_LCD[5])*10));
				NM_LCD[8]=SUBMASK_3/100;
				NM_LCD[9]=(SUBMASK_3-((NM_LCD[8])*100))/10;
				NM_LCD[10]=SUBMASK_3-(((NM_LCD[8])*100)+((NM_LCD[9])*10));
				NM_LCD[12]=SUBMASK_4/100;
				NM_LCD[13]=(SUBMASK_4-((NM_LCD[12])*100))/10;
				NM_LCD[14]=SUBMASK_4-(((NM_LCD[12])*100)+((NM_LCD[13])*10));
				//GW inicial
				GW_LCD[0]=GWIP_1/100;
				GW_LCD[1]=(GWIP_1-((GW_LCD[0])*100))/10;
				GW_LCD[2]=GWIP_1-(((GW_LCD[0])*100)+((GW_LCD[1])*10));
				GW_LCD[4]=GWIP_2/100;
				GW_LCD[5]=(GWIP_2-((GW_LCD[4])*100))/10;
				GW_LCD[6]=GWIP_2-(((GW_LCD[4])*100)+((GW_LCD[5])*10));
				GW_LCD[8]=GWIP_3/100;
				GW_LCD[9]=(GWIP_3-((GW_LCD[8])*100))/10;
				GW_LCD[10]=GWIP_3-(((GW_LCD[8])*100)+((GW_LCD[9])*10));
				GW_LCD[12]=GWIP_4/100;
				GW_LCD[13]=(GWIP_4-((GW_LCD[12])*100))/10;
				GW_LCD[14]=GWIP_4-(((GW_LCD[12])*100)+((GW_LCD[13])*10));
				change_state=8;
			}
			else if(change_state==10)
			{
				change_state=9;
			}
			else if(change_state!=6&&change_state!=8&&change_state!=9)
			{
				change_state=2;
			}
		}
		else if(!(LPC_GPIO1->FIOPIN & (OK_LCD))&&(apretada==OK_LCD||!apretada))
		{
			apretada=OK_LCD;
			cont_LCD++;
			if(cont_LCD<NUM_LCD)return;
			if(change_state==10)
			{
				change_state=3;
				x_config-=32;
				columna-=4;
				return;
			}
			if(change_state!=17&&change_state!=3&&change_state!=7)
			{
				if(change_state==4)
				{
					sprintf((char *)numero_LCD,"%c",numero_LCD[0]);
					LCD_PrintString(x_config-8,y_config,numero_LCD, COLOR_WHITE, COLOR_BLAU_CEL);
					change_state=3;
					if(fila == 1)
					{
						IP_LCD[columna-2]=numero_LCD[0]-0x30;
					}
					else if(fila == 2)
					{
						NM_LCD[columna-2]=numero_LCD[0]-0x30;
					}
					else if(fila == 3)
					{
						GW_LCD[columna-2]=numero_LCD[0]-0x30;
					}
				}
				else
				{
					change_state=9;
					LCD_Rect (30, 49, 3, 23, COLOR_BLAU_CEL);
				}
			}
			if(msTicks_LCD>2000)
			{
				change_state=7;
				LCD_FilledRect (0, 127, 26, 127, COLOR_BLAU_CEL);
			}
		}
		else
		{
			apretada=0;
			cont_LCD = 0;
			if(change_state==2)
			{
				change_state = 0;
				StateLCD = INICI;
				LCD_Rect (8, 27, 2, 22, COLOR_BLAU_CEL);
				LCD_Rect (29, 48, 2, 22, COLOR_WHITE);

				LCD_FilledRect (0, 127, 26, 127, COLOR_BLAU_CEL);
			}
			else if(change_state==1)
			{
				change_state = 0;
				StateLCD = SETTINGS;
				LCD_Rect (29, 48, 2, 22, COLOR_WHITE);
				LCD_Rect (50, 69, 2, 21, COLOR_BLAU_CEL);
				LCD_FilledRect (0, 127, 26, 127, COLOR_BLAU_CEL);
			}
			else if (change_state==3)
			{
				if(x_config==4||x_config==36||x_config==68||x_config==100)
				{
					sprintf((char *)text,"%c%c%c",'0','0','0');
					LCD_PrintString(x_config,y_config,text, COLOR_WHITE, COLOR_BLAU_CEL);
					numero_LCD[0]='0';
				}
				if(columna%4!=0)
				{
					change_state=4;
					sprintf((char *)numero_LCD,"%c",'0');
					LCD_PrintString(x_config,y_config,numero_LCD, COLOR_WHITE, COLOR_AQUA);
				}
				columna++;
				x_config+=8;
				if((columna-1)%4==0)
				{
					change_state=9;
				}
			}
			else if(change_state == 5)
			{
				comprovar_numero_LCD(1);
				change_state = 4;
			}
			else if(change_state == 6)
			{
				comprovar_numero_LCD(2);
				change_state = 4;
			}
			else if(change_state == 7)
			{
				LCD_PrintString(8,50,"Guardar config?", COLOR_BLACK, COLOR_BLAU_CEL);
				LCD_PrintString(8,100,"Si          No", COLOR_BLACK, COLOR_BLAU_CEL);
				change_state=17;
			}
			else if(change_state==8)
			{
				LCD_FilledRect (0, 127, 26, 127, COLOR_BLAU_CEL);
				change_state=0;
			}
			else if(change_state==9)
			{
				change_state=10;
				if(columna==17)
				{
					if(fila==1)
					{
						if(x_config!=4)sprintf((char *)text,"%d%d%d",IP_LCD[columna-5],IP_LCD[columna-4],IP_LCD[columna-3]);
					}
					else if(fila==2)
					{
						if(x_config!=4)sprintf((char *)text,"%d%d%d",NM_LCD[columna-5],NM_LCD[columna-4],NM_LCD[columna-3]);
					}
					else if(fila==3)
					{
						if(x_config!=4)sprintf((char *)text,"%d%d%d",GW_LCD[columna-5],GW_LCD[columna-4],GW_LCD[columna-3]);
					}
					if(x_config!=4)LCD_PrintString(x_config-32,y_config,text, COLOR_BLACK, COLOR_BLAU_CEL);
					columna=1;
					x_config=4;
					fila++;
					if(fila==4)
					{
						fila = 1;
						x_config = 4;
						columna = 1;
						change_state=7;
						LCD_FilledRect (0, 127, 26, 127, COLOR_BLAU_CEL);
						return;
					}
				}
				if(x_config==0)x_config=4;
				dibuixar_tres();
				x_config+=32;
				columna+=4;
			}
		}
	}
	else if(StateLCD == SETTINGS)
	{
		if(change_state==0)
		{
			LCD_PrintString(8,30,"Brillo", COLOR_BLACK, COLOR_BLAU_CEL);
			change_state=100;
		}
		if (!(LPC_GPIO1->FIOPIN & (UP_LCD))&&(apretada==UP_LCD||!apretada))
		{
			apretada=UP_LCD;
			cont_LCD++;
			if(cont_LCD<NUM_LCD)return;
			change_state=1;
		}
		else if(!(LPC_GPIO1->FIOPIN & (DOWN_LCD))&&(apretada==DOWN_LCD||!apretada))
		{
			apretada=DOWN_LCD;
			cont_LCD++;
			if(cont_LCD<NUM_LCD)return;
			change_state=2;
		}
		else if(!(LPC_GPIO1->FIOPIN & (OK_LCD))&&(apretada==OK_LCD||!apretada))
		{
			apretada=OK_LCD;
			cont_LCD++;
			if(cont_LCD<NUM_LCD)return;
			if(change_state==4)
			{
				change_state = 5;
			}
			else if(change_state != 5)
			{
				if(change_state==100)
				{
					change_state=3;
					LCD_PrintString(8,30,"Brillo", COLOR_AQUA, COLOR_BLAU_CEL);
				}
			}
		}
		else
		{
			apretada=0;
			cont_LCD = 0;
			if(change_state==1)
			{
				change_state = 0;
				StateLCD = INICI;
				LCD_Rect (8, 27, 2, 22, COLOR_BLAU_CEL);
				LCD_Rect (50, 69, 2, 21, COLOR_WHITE);
				LCD_Rect (51, 70, 3, 22, COLOR_WHITE);
				LCD_FilledRect (0, 127, 26, 127, COLOR_BLAU_CEL);
			}
			else if(change_state==2)
			{
				change_state = 0;
				StateLCD = CONFIG;
				LCD_Rect (29, 48, 2, 22, COLOR_BLAU_CEL);
				LCD_Rect (50, 69, 2, 21, COLOR_WHITE);
				LCD_Rect (51, 70, 3, 22, COLOR_WHITE);
				LCD_FilledRect (0, 127, 26, 127, COLOR_BLAU_CEL);
			}
			else if(change_state==3)
			{
				change_state=4;
				LCD_Rect (51, 70, 3, 22, COLOR_BLAU_CEL);
				//LCD_FilledRect (0, 127, 23, 109, COLOR_BLAU_CEL);
			}
			else if(change_state==5)
			{
				StateLCD = BRILLO;
				if(brillo == 100)change_state = 0;
				else if(brillo == 75)change_state = 2;
				else if(brillo == 50)change_state = 3;
				else if(brillo == 25)change_state = 4;
				LCD_FilledRect (0, 127, 26, 127, COLOR_BLAU_CEL);
			}
		}
	}
	else if (StateLCD == BRILLO)
	{
		if(change_state==0)
		{
			PWM_high = 4;
			if(brillo!=100)LCD_FilledRect (0, 127, 26, 127, COLOR_BLAU_CEL);
			brillo = 100;
			LCD_PrintString(50,65,"100", COLOR_WHITE, COLOR_BLAU_CEL);
			LCD_FilledRect (10, 110, 52, 60, COLOR_BLUE);
			change_state=100;
		}
		else if(change_state==2)
		{
			PWM_high = 3;
			if(brillo!=75)LCD_FilledRect (0, 127, 26, 127, COLOR_BLAU_CEL);
			brillo = 75;
			LCD_PrintString(50,65," 75", COLOR_WHITE, COLOR_BLAU_CEL);
			LCD_FilledRect (10, 85, 52, 60, COLOR_BLUE);
			change_state=100;
		}
		else if(change_state==3)
		{
			PWM_high = 2;
			if(brillo!=50)LCD_FilledRect (0, 127, 26, 127, COLOR_BLAU_CEL);
			brillo = 50;
			LCD_PrintString(50,65," 50", COLOR_WHITE, COLOR_BLAU_CEL);
			LCD_FilledRect (10, 60, 52, 60, COLOR_BLUE);
			change_state=100;
		}
		else if(change_state==4)
		{
			PWM_high = 1;
			if(brillo!=25)LCD_FilledRect (0, 127, 26, 127, COLOR_BLAU_CEL);
			brillo = 25;
			LCD_PrintString(50,65," 25", COLOR_WHITE, COLOR_BLAU_CEL);
			LCD_FilledRect (10, 35, 52, 60, COLOR_BLUE);
			change_state=100;
		}
		if (!(LPC_GPIO1->FIOPIN & (OK_LCD))&&(apretada==OK_LCD||!apretada))
		{
			apretada=OK_LCD;
			cont_LCD++;
			if(cont_LCD<NUM_LCD)return;
			cont_LCD = 0;
			change_state=1;
		}
		else if(!(LPC_GPIO1->FIOPIN & (DOWN_LCD))&&(apretada==DOWN_LCD||!apretada))
		{
			apretada=DOWN_LCD;
			cont_LCD++;
			if(cont_LCD<NUM_LCD)return;
			cont_LCD = 0;
			change_state=5;
			LCD_PrintString(115,50,"+", COLOR_AQUA, COLOR_BLAU_CEL);
		}
		else if (!(LPC_GPIO1->FIOPIN & (UP_LCD))&&(apretada==UP_LCD||!apretada))
		{
			apretada=UP_LCD;
			cont_LCD++;
			if(cont_LCD<NUM_LCD)return;
			cont_LCD = 0;
			change_state=6;
			LCD_PrintString(0,50,"-", COLOR_AQUA, COLOR_BLAU_CEL);
		}
		else
		{
			apretada=0;
			LCD_PrintString(115,50,"+", COLOR_BLACK, COLOR_BLAU_CEL);
			LCD_PrintString(0,50,"-", COLOR_BLACK, COLOR_BLAU_CEL);
			if(change_state==1)
			{
				change_state = 0;
				LCD_Rect (50, 69, 2, 21, COLOR_BLAU_CEL);
				LCD_Rect (51, 70, 3, 22, COLOR_WHITE);
				LCD_FilledRect (0, 127, 26, 127, COLOR_BLAU_CEL);
				StateLCD = SETTINGS;
			}
			else if(change_state==5)
			{
				if(brillo == 100)change_state = 0;
				else if(brillo == 75)change_state = 0;
				else if(brillo == 50)change_state = 2;
				else if(brillo == 25)change_state = 3;
			}
			else if(change_state==6)
			{
				if(brillo == 100)change_state = 2;
				else if(brillo == 75)change_state = 3;
				else if(brillo == 50)change_state = 4;
				else if(brillo == 25)change_state = 4;
			}
		}
	}*/
}
void process_PWM_LCD (void)
{
	if(PWM_cont<PWM_high)
	{
		LPC_GPIO2->FIOSET = (1<<9);
	}
	else
	{
		LPC_GPIO2->FIOCLR = (1<<9);
	}
	PWM_cont++;
	if(PWM_cont==4)PWM_cont=0;
}
void dibuixar_tres (void)
{
	char numero_tres[4]="000";
	if(fila==1)
	{
		sprintf((char *)text,"%3.3d.%3.3d.%3.3d.%3.3d\n",	    MYIP_1,
																MYIP_2,
																MYIP_3,
																MYIP_4);
		y_config = 42;
		if(x_config!=4)sprintf((char *)numero_tres,"%d%d%d",IP_LCD[columna-5],IP_LCD[columna-4],IP_LCD[columna-3]);
	}
	else if(fila==2)
	{
		sprintf((char *)text,"%3.3d.%3.3d.%3.3d.%3.3d\n",	    SUBMASK_1,
																SUBMASK_2,
																SUBMASK_3,
																SUBMASK_4);
		y_config = 70;
		if(x_config!=4)sprintf((char *)numero_tres,"%d%d%d",NM_LCD[columna-5],NM_LCD[columna-4],NM_LCD[columna-3]);
	}
	else if(fila==3)
	{
		sprintf((char *)text,"%3.3d.%3.3d.%3.3d.%3.3d\n",	    GWIP_1,
																GWIP_2,
																GWIP_3,
																GWIP_4);
		y_config = 98;
		if(x_config!=4)sprintf((char *)numero_tres,"%d%d%d",GW_LCD[columna-5],GW_LCD[columna-4],GW_LCD[columna-3]);
	}
	if(x_config!=4)LCD_PrintString(x_config-32,y_config,numero_tres, COLOR_BLACK, COLOR_BLAU_CEL);
	sprintf((char *)numero_tres,"%c%c%c",text[columna-1],text[columna],text[columna+1]);
	LCD_PrintString(x_config,y_config,numero_tres, COLOR_WHITE, COLOR_BLUE);
}
void comprovar_numero_LCD(uint8_t cas)
{
	uint8_t maxim = 0;
	if((columna-2)%4==0)
	{//Centenes
		maxim=2+0x30;
	}
	else
	{
		if((columna-3)%4==0)
		{//Decenes
			if(text[columna-3]==0x32)maxim=5+0x30;
			else maxim=9+0x30;
		}
		else
		{//Unitats
			if(text[columna-4]==0x32)maxim=5+0x30;
			else maxim=9+0x30;
		}
	}
	if(cas==1)
	{//Baixant
		if(numero_LCD[0]==0x30)numero_LCD[0]=maxim+1;
		sprintf((char *)numero_LCD,"%c",numero_LCD[0]-1);
	}
	else if(cas==2)
	{//Pujant
		if(numero_LCD[0]==maxim)numero_LCD[0]=0x2F;
		sprintf((char *)numero_LCD,"%c",numero_LCD[0]+1);
	}
	LCD_PrintString(x_config-8,y_config,numero_LCD, COLOR_BLACK, COLOR_AQUA);
	text[columna-2]=numero_LCD[0];
}
